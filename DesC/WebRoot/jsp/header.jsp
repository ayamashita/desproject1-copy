<%@include file="admin/check_login.jsp" %>

<html>
<head>
<script language="javascript">
function chgBg(obj,color){
	if (document.all || document.getElementById)
  	obj.style.backgroundColor=color;
	else if (document.layers)
  	obj.style=color;
}
</script>

<script language="JavaScript" src="<%=request.getContextPath()%>/selectbox.js"></script>

<style type="text/css"">

fieldset
    {
    border:1px solid #aaaaaa;
    padding: 15px;
    }
legend
   {
    color:black;
    font-weight: bold;
    }

</style>



<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>




</head>
<title>[ simula . research laboratory ]</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/jsp/simula.css" media="screen">
<csscriptdict>
			<script><!--
CSInit = new Array;
function CSScriptInit() {
if(typeof(skipPage) != "undefined") { if(skipPage) return; }
idxArray = new Array;
for(var i=0;i<CSInit.length;i++)
	idxArray[i] = i;
CSAction2(CSInit, idxArray);}
CSAg = window.navigator.userAgent; CSBVers = parseInt(CSAg.charAt(CSAg.indexOf("/")+1),10);
CSIsW3CDOM = ((document.getElementById) && !(IsIE()&&CSBVers<6)) ? true : false;
function IsIE() { return CSAg.indexOf("MSIE") > 0;}
function CSIEStyl(s) { return document.all.tags("div")[s].style; }
function CSNSStyl(s) { if (CSIsW3CDOM) return document.getElementById(s).style; else return CSFindElement(s,0);  }
CSIImg=false;
function CSInitImgID() {if (!CSIImg && document.images) { for (var i=0; i<document.images.length; i++) { if (!document.images[i].id) document.images[i].id=document.images[i].name; } CSIImg = true;}}
function CSFindElement(n,ly) { if (CSBVers<4) return document[n];
	if (CSIsW3CDOM) {CSInitImgID();return(document.getElementById(n));}
	var curDoc = ly?ly.document:document; var elem = curDoc[n];
	if (!elem) {for (var i=0;i<curDoc.layers.length;i++) {elem=CSFindElement(n,curDoc.layers[i]); if (elem) return elem; }}
	return elem;
}
function CSGetImage(n) {if(document.images) {return ((!IsIE()&&CSBVers<5)?CSFindElement(n,0):document.images[n]);} else {return null;}}
CSDInit=false;
function CSIDOM() { if (CSDInit)return; CSDInit=true; if(document.getElementsByTagName) {var n = document.getElementsByTagName('DIV'); for (var i=0;i<n.length;i++) {CSICSS2Prop(n[i].id);}}}
function CSICSS2Prop(id) { var n = document.getElementsByTagName('STYLE');for (var i=0;i<n.length;i++) { var cn = n[i].childNodes; for (var j=0;j<cn.length;j++) { CSSetCSS2Props(CSFetchStyle(cn[j].data, id),id); }}}
function CSFetchStyle(sc, id) {
	var s=sc; while(s.indexOf("#")!=-1) { s=s.substring(s.indexOf("#")+1,sc.length); if (s.substring(0,s.indexOf("{")).toUpperCase().indexOf(id.toUpperCase())!=-1) return(s.substring(s.indexOf("{")+1,s.indexOf("}")));}
	return "";
}
function CSGetStyleAttrValue (si, id) {
	var s=si.toUpperCase();
	var myID=id.toUpperCase()+":";
	var id1=s.indexOf(myID);
	if (id1==-1) return "";
	s=s.substring(id1+myID.length+1,si.length);
	var id2=s.indexOf(";");
	return ((id2==-1)?s:s.substring(0,id2));
}
function CSSetCSS2Props(si, id) {
	var el=document.getElementById(id);
	if (el==null) return;
	var style=document.getElementById(id).style;
	if (style) {
		if (style.left=="") style.left=CSGetStyleAttrValue(si,"left");
		if (style.top=="") style.top=CSGetStyleAttrValue(si,"top");
		if (style.width=="") style.width=CSGetStyleAttrValue(si,"width");
		if (style.height=="") style.height=CSGetStyleAttrValue(si,"height");
		if (style.visibility=="") style.visibility=CSGetStyleAttrValue(si,"visibility");
		if (style.zIndex=="") style.zIndex=CSGetStyleAttrValue(si,"z-index");
	}
}

function CSClickReturn () {
	var bAgent = window.navigator.userAgent; 
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}

function CSButtonReturn () {
	var bAgent = window.navigator.userAgent; 
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return false; // follow link
	else return true; // follow link
}

CSIm=new Object();
function CSIShow(n,i) {
	if (document.images) {
		if (CSIm[n]) {
			var img=CSGetImage(n);
			if (img&&typeof(CSIm[n][i].src)!="undefined") {img.src=CSIm[n][i].src;}
			if(i!=0) self.status=CSIm[n][3]; else self.status=" ";
			return true;
		}
	}
	return false;
}
function CSILoad(action) {
	im=action[1];
	if (document.images) {
		CSIm[im]=new Object();
		for (var i=2;i<5;i++) {
			if (action[i]!='') {CSIm[im][i-2]=new Image(); CSIm[im][i-2].src=action[i];}
			else CSIm[im][i-2]=0;
		}
		CSIm[im][3] = action[5];
	}
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) { 
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false; 
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}			
		result=aa[0](ta);
	}
	return result;
}
CSAct = new Object;


// --></script>
		</csscriptdict>
		<csactiondict>
			<script><!--
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'search',/*URL*/'<%=request.getContextPath()%>/images/topright-search-normal.png',/*URL*/'<%=request.getContextPath()%>/images/topright-search-mouseover.png',/*URL*/'<%=request.getContextPath()%>/images/topright-search-mouseover.png','Search');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'advsearch',/*URL*/'<%=request.getContextPath()%>/images/topright-advsearch-normal.png',/*URL*/'<%=request.getContextPath()%>/images/topright-advsearch-mouseover.png',/*URL*/'<%=request.getContextPath()%>/images/topright-advsearch-mouseover.png','Advanced Search');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'home',/*URL*/'<%=request.getContextPath()%>/images/menu-home-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-home-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-home-selected.png','Home Page');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'news',/*URL*/'<%=request.getContextPath()%>/images/menu-news-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-news-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-news-selected.png','News and press releases at Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'publications',/*URL*/'<%=request.getContextPath()%>/images/menu-publications-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-publications-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-publications-selected.png','Publications by Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'projects',/*URL*/'<%=request.getContextPath()%>/images/menu-projects-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-projects-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-projects-selected.png','Scientific projects at Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'research',/*URL*/'<%=request.getContextPath()%>/images/menu-research-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-research-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-research-selected.png','Research Departments at Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'innovation',/*URL*/'<%=request.getContextPath()%>/images/menu-innovation-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-innovation-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-innovation-selected.png','Innovation at Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'people',/*URL*/'<%=request.getContextPath()%>/images/menu-people-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-people-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-people-selected.png','People that work at Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'opportunities',/*URL*/'<%=request.getContextPath()%>/images/menu-opportunities-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-opportunities-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-opportunities-selected.png','Opportunities for students and professionals at Simula Research Laboratory');
CSInit[CSInit.length] = new Array(CSILoad,/*CMP*/'studies',/*URL*/'<%=request.getContextPath()%>/images/menu-Studies-normal.png',/*URL*/'<%=request.getContextPath()%>/images/menu-Studies-selected.png',/*URL*/'<%=request.getContextPath()%>/images/menu-Studies-selected.png','Studies at Simula Research Laboratory');

// --></script>
		</csactiondict>



<body bgcolor="#FE7519" onload="CSScriptInit();MM_preloadImages('<%=request.getContextPath()%>/images/menu-intranet-selected.png')" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#aeaeae" onload="" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c">


<table height='117' width='100%' cellspacing='0' cellpadding='0' border='0'>
<tr>
<td bgcolor="#FE7519" height='90' valign="bottom" colspan="10"><img src="<%=request.getContextPath()%>/images/logo-part1.png">&nbsp;<img src="<%=request.getContextPath()%>/images/logo-part2.png"></td>
</tr>

<tr>
<td class='topMenu' valign='middle' width="15%">&nbsp;</td> 
<td class='topMenu' valign='middle' height="23"> 

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="23">
        <tr height="23"> 
          <td width="601"><csobj w="49" h="23" t="Button" st="Home Page" ht="<%=request.getContextPath()%>/images/menu-home-selected.png" cl="<%=request.getContextPath()%>/images/menu-home-selected.png"><a href="http://www.simula.no/index.php" onmouseover="return CSIShow(/*CMP*/'home',1)" onmouseout="return CSIShow(/*CMP*/'home',0)" onclick="CSIShow(/*CMP*/'home',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-home-normal.png" width="49" height="23" name="home" border="0" alt="Home Page"></a></csobj><csobj w="46" h="23" t="Button" st="News and press releases at Simula Research Laboratory" ht="<%=request.getContextPath()%>/images/menu-news-selected.png" cl="<%=request.getContextPath()%>/images/menu-news-selected.png"><a href="http://www.simula.no/news.php" onmouseover="return CSIShow(/*CMP*/'news',1)" onmouseout="return CSIShow(/*CMP*/'news',0)" onclick="CSIShow(/*CMP*/'news',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-news-normal.png" width="46" height="23" name="news" border="0" alt="News and press releases at Simula Research Laboratory"></a></csobj><csobj w="82" h="23" t="Button" st="Publications by Simula Research Laboratory" ht="<%=request.getContextPath()%>/images/menu-publications-normal.png" cl="<%=request.getContextPath()%>/images/menu-publications-normal.png"><a href="http://www.simula.no/publication.php" onmouseover="return CSIShow(/*CMP*/'publications',1)" onmouseout="return CSIShow(/*CMP*/'publications',0)" onclick="CSIShow(/*CMP*/'publications',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-publications-selected.png" width="82" height="23" name="publications" border="0" alt="Publications by Simula Research Laboratory"></a></csobj><csobj w="60" h="23" t="Button" st="Scientific projects at Simula Research Laboratory" ht="<%=request.getContextPath()%>/images/menu-projects-selected.png" cl="<%=request.getContextPath()%>/images/menu-projects-selected.png"><a href="http://www.simula.no/project.php" onmouseover="return CSIShow(/*CMP*/'projects',1)" onmouseout="return CSIShow(/*CMP*/'projects',0)" onclick="CSIShow(/*CMP*/'projects',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-projects-normal.png" width="60" height="23" name="projects" border="0" alt="Scientific projects at Simula Research Laboratory"></a></csobj><csobj w="64" h="23" t="Button" st="Research Departments at Simula Research Laboratory" cl="<%=request.getContextPath()%>/images/menu-research-selected.png" ht="<%=request.getContextPath()%>/images/menu-research-selected.png"><a href="http://www.simula.no/department.php" onmouseover="return CSIShow(/*CMP*/'research',1)" onmouseout="return CSIShow(/*CMP*/'research',0)" onclick="CSIShow(/*CMP*/'research',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-research-normal.png" width="64" height="23" name="research" border="0" alt="Research Departments at Simula Research Laboratory"></a></csobj><csobj w="72" h="23" t="Button" st="Innovation at Simula Research Laboratory" cl="<%=request.getContextPath()%>/images/menu-innovation-selected.png" ht="<%=request.getContextPath()%>/images/menu-innovation-selected.png"><a href="http://www.simula.no/innovation.php" onmouseover="return CSIShow(/*CMP*/'innovation',1)" onmouseout="return CSIShow(/*CMP*/'innovation',0)" onclick="CSIShow(/*CMP*/'innovation',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-innovation-normal.png" width="72" height="23" name="innovation" border="0" alt="Innovation at Simula Research Laboratory"></a></csobj><csobj w="54" h="23" t="Button" st="People that work at Simula Research Laboratory" ht="<%=request.getContextPath()%>/images/menu-people-selected.png" cl="<%=request.getContextPath()%>/images/menu-people-selected.png"><a href="http://www.simula.no/people.php" onmouseover="return CSIShow(/*CMP*/'people',1)" onmouseout="return CSIShow(/*CMP*/'people',0)" onclick="CSIShow(/*CMP*/'people',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-people-normal.png" width="54" height="23" name="people" border="0" alt="People that work at Simula Research Laboratory"></a></csobj><csobj w="88" h="23" t="Button" st="Opportunities for students and professionals at Simula Research Laboratory" cl="<%=request.getContextPath()%>/images/menu-opportunities-selected.png" ht="<%=request.getContextPath()%>/images/menu-opportunities-selected.png"><a href="http://www.simula.no/opportunity.php" onmouseover="return CSIShow(/*CMP*/'opportunities',1)" onmouseout="return CSIShow(/*CMP*/'opportunities',0)" onclick="CSIShow(/*CMP*/'opportunities',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-opportunities-normal.png" width="88" height="23" name="opportunities" border="0" alt="Opportunities for students and professionals at Simula Research Laboratory"></a></csobj><csobj w="46" h="23" t="Button" st="Studies at Simula Research Laboratory" ht="<%=request.getContextPath()%>/images/menu-Studies-selected.png" cl="<%=request.getContextPath()%>/images/menu-Studies-selected.png"><a href="<%=request.getContextPath()%>/jsp/search.jsp" onmouseover="return CSIShow(/*CMP*/'studies',1)" onmouseout="return CSIShow(/*CMP*/'studies',0)" onclick="CSIShow(/*CMP*/'studies',2);return CSButtonReturn()"><img src="<%=request.getContextPath()%>/images/menu-Studies-normal.png" width="46" height="23" name="studies" border="0" alt="Studies at Simula Research Laboratory"></a></csobj></td>
          <td height="23"><img src="<%=request.getContextPath()%>/images/menu-all-grey.png" width="78" height="23"><a href="https://www.simula.no/intranet/"><img src="<%=request.getContextPath()%>/images/menu-intranet-normal.png" width="59" height="23" name="Image1" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','<%=request.getContextPath()%>/images/menu-intranet-selected.png',1)" border="0"></a></td>
        </tr>
</table>


</td>
</tr>


</table>

<table width="100%" border="0">


