<?php
    $LOGGED_OFF = "logged off";
    $LOGGED_ON = "logged on";

    session_start();
    $todo = $_REQUEST["action"];
    $status = $_SESSION["status"];
    $username = $_REQUEST["simula_username"];
    $userid = $_REQUEST["simula_id"];
    $userright = $_REQUEST["simula_right"];

    $shouldlogon = ($status == $LOGGED_OFF || strlen($status) == 0 );

    if ( $todo=="logoff" ) {
        $_SESSION["status"]=$LOGGED_OFF;
        session_unregister("simula_username");
        session_unregister("simula_id");
        session_unregister("simula_right");
        $status = $_SESSION["status"];
    } else if ( $todo=="logon" ) {
        $_SESSION["status"]=$LOGGED_ON;
		session_register("simula_username");
		session_register("simula_id");
		session_register("simula_right");
        $simula_username=$username;
		$simula_id=$userid;
		$simula_right=$userright;
        $status = $_SESSION["status"];
    }

php?>

<html>
<head>
<title>Simula Login Test</title>
</head>
<body bgcolor="#00CCCC" text="#000000">

<h4>You are <?php echo $status; php?> <br>
    Using sessionId <?php echo $status ++ $_REQUEST["PHPSESSID"]; php?><br>
</h4>

<form method="POST" action="login.php">

    <?php if ( shouldLogon ) { php?>
        Username: <input type="text" name="simula_username"/><br>
        Id: <input type="text" name="simula_id"/><br>
        Right: <input type="text" name="simula_right"/><br>
        <input type="hidden" name="action" value="logon"/>
        <input type="submit" value="Log on"/>
    <?php } else { php?>
        <input type="hidden" name="action" value="logoff"/>
        <input type="submit" value="Log off"/>
    <?php } php?>

</form>
</body>
</html>

