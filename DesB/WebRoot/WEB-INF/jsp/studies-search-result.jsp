<%@include file="include/include-top.jsp"%>

  <h1>Search studies result</h1>

  <logic:present name="<%= Constants.TABLE_SEARCH_STUDIES %>">
    <logic:notEmpty name="<%= Constants.TABLE_SEARCH_STUDIES %>" property="source.content">
      <halogen:table name="<%= Constants.TABLE_SEARCH_STUDIES %>"/>
      <html:link target="_blank" page="/pdf/studies.pdf">printer friendly version</html:link>
    </logic:notEmpty>
    <logic:empty name="<%= Constants.TABLE_SEARCH_STUDIES %>" property="source.content">
      No matching studies found.
    </logic:empty>
  </logic:present>
  <logic:notPresent name="<%= Constants.TABLE_SEARCH_STUDIES %>">
    No matching studies found.
  </logic:notPresent>

<%@include file="include/include-bottom.jsp"%>