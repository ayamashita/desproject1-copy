<%@include file="include/include-top.jsp"%>

  <html:form action="/adm/openingtext/edit" method="post">

  <h1>Edit admin welcome text</h1>

  <%-- The main table --%>
  <table class="layout">
    <tr>
      <td>
        <html:textarea property="openingText" cols="50" rows="25"/>
      </td>
    </tr>
  </table>
  <%-- End of Main Table --%>

  <html:submit property="save" value="save changes"/>
  <html:cancel value="cancel"/>

 </html:form>

<%@include file="include/include-bottom.jsp"%>
