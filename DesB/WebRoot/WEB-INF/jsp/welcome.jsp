<%@page import="no.simula.SimulaFactory,no.simula.des.AdminModule"%>
<%@include file="include/include-top.jsp"%>

<% 
  AdminModule admin = SimulaFactory.getSimula().getAdminModule();
  if(admin != null) 
  {
    out.println(admin.getOpeningText());
  }
%>



  <logic:messagesPresent>
    <table class="container">
      <tr>
        <td>
          <ul>
            <html:messages message="true" id="message">
              <li><bean:write name="message"/></li>
            </html:messages>
          </ul>
        </td>
      </tr>
    </table>
  </logic:messagesPresent>

<%@include file="include/include-bottom.jsp"%>