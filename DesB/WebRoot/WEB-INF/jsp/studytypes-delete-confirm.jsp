<%@include file="include/include-top.jsp"%>

    <h1>Confirm delete study type</h1>

    <html:form action="/adm/studytypes/delete">

      <table class="container">
        <tr>
          <td>

            <logic:present name="<%= Constants.DELETE_THIS_STUDYTYPE %>" scope="request">
              <html:hidden name="<%= Constants.DELETE_THIS_STUDYTYPE %>" property="id"/>
              Are you sure you want to delete study type
              <b><bean:write name="<%= Constants.DELETE_THIS_STUDYTYPE%>" property="name"/></b>?
            </logic:present>

            <logic:notPresent name="<%= Constants.DELETE_THIS_STUDYTYPE%>" scope="request">
              study type
              <b><bean:write name="<%= Constants.DONT_DELETE_THIS_STUDYTYPE%>" property="name"/></b>
              could not be deleted. It is linked to the following studies:
              <ul>
                <logic:iterate name="<%= Constants.BEAN_STUDIES %>" id="study" type="no.simula.des.Study">
                  <li><bean:write name="study" property="name"/></li>
                </logic:iterate>
              </ul>
            </logic:notPresent>
            
          </td>
        </tr>
      </table>

      <logic:present name="<%= Constants.DELETE_THIS_STUDYTYPE %>" scope="request">
        <html:submit property="confirm" value="delete"/>
      </logic:present>
      <logic:notPresent name="<%= Constants.DELETE_THIS_STUDYTYPE %>" scope="request">
        <html:submit property="confirm" value="delete" disabled="true"/>
      </logic:notPresent>
      <html:cancel property="skip" value="skip"/>
      <html:cancel value="cancel"/>
    </html:form>

<%@include file="include/include-bottom.jsp"%>