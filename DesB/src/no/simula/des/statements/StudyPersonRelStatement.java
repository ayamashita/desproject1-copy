/**
 * @(#) PersonStudyRelStatement.java
 */

package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;

/**
 * @author Frode Langseth
 */
public class StudyPersonRelStatement extends ObjectStatementImpl {
  private static Log log = LogFactory.getLog(StudyPersonRelStatement.class);

  private final String BEAN_NAME = "java.util.List";

  private final String INSERT_COLUMNS = "stu_resp_id, people_id";

  private final String INSERT_VALUES = "(?, ?)";

  private final String KEY = "people_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "stu_resp_id, people_id";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "study_responsible_rel";

  private final String UPDATE_VALUES = "stu_resp_id = ?, people_id = ?";

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Object entity = null;

    try {
      if (getDataBean() instanceof Integer) {
        entity = getDataBean();
      } else if (getDataBean() == null) {
        entity = new Integer(0);
      } else {
        entity = getDataBean().getClass().newInstance();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new study person rel data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new study person rel data object");
      e.printStackTrace();
    }

    if (entity instanceof Integer) {
      entity = new Integer(rs.getInt("stu_resp_id"));
    }

    return entity;
  }

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * 
   * @author Frode Langseth
   * @version 0.1 
   */
  public String getKey() {
    return (KEY);
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /**
   * Returns the database name of the table
   * 
   * @author Frode Langseth
   * @version 0.1 
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   */
  public String getUpdateValuesString() {
    return (UPDATE_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      List entity = (List) getDataBean();
      Integer studyId = (Integer) entity.get(0);
      Integer personId = (Integer) entity.get(1);

      pstmt.setInt(1, studyId.intValue());
      pstmt.setInt(2, personId.intValue());
    }
  }

}
