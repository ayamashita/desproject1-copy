/*
 * Created on 30.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.des.StudyMaterial;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyMaterialStatement extends ObjectStatementImpl {
  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.StudyMaterial";

  private final String INSERT_COLUMNS = "sm_description";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?)";

  /**
   * Field KEY
   */
  private final String KEY = "sm_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "sm_id, sm_description";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "sm_studymaterial";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "sm_description= ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return (UPDATE_VALUES);
  }

  /** 
   * Populates the data bean with the result from a query
   * Since StudyMaterial is abstract, it's not possible to populate ....
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    return (null);
  }

  /**
   * Populates the prepared statement string for an update pr insert statement with values from the data bean
   * 
   * Used e.g. by INSERT statements
   * The string does not contain the id 
   * 
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      StudyMaterial studyMaterial = (StudyMaterial) getDataBean();

      pstmt.setString(1, studyMaterial.getDescription());
    }
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

}
