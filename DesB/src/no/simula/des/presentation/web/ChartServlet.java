package no.simula.des.presentation.web;

import com.keypoint.PngEncoderB;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.*;
import javax.servlet.http.*;
import net.sourceforge.chart2d.Chart2DProperties;
import net.sourceforge.chart2d.Dataset;
import net.sourceforge.chart2d.GraphChart2DProperties;
import net.sourceforge.chart2d.GraphProperties;
import net.sourceforge.chart2d.LBChart2D;
import net.sourceforge.chart2d.LegendProperties;
import net.sourceforge.chart2d.MultiColorsProperties;
import net.sourceforge.chart2d.Object2DProperties;
import no.halogen.search.SearchFactory;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;
import no.simula.des.StudyType;
import no.simula.des.search.SearchFactoryImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** This servlet retuns a chart summarizing all studies in <CODE>Simula</CODE> by
 * type of study each year.
 *
 * The chart is used by a library called chart2d
 * (See http://chart2d.sourceforge.net/)
 *
 * The <CODE>BufferedImage</CODE> is encoded as png using
 * <CODE>com.keypoint.PngEncoderB</CODE>. This could be replaced by the
 * <CODE>javax.imageio</CODE> library (since 1.4.1)
 * (See: http://catcode.com/pngencoder/)
 * @author Stian Eide
 */
public class ChartServlet extends HttpServlet {
  
  private static Log log = LogFactory.getLog(ChartServlet.class);
  
  /** Initializes the servlet.
   */
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    
  }
  
  /** Destroys the servlet.
   */
  public void destroy() {
    
  }
  
  /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   * @param request servlet request
   * @param response servlet response
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    // Setting headless to allow chart2d to run in non-x-server environment
    System.setProperty("java.awt.headless", "true");
    response.setContentType("image/png");
    ServletOutputStream out = response.getOutputStream();
    PngEncoderB png = new PngEncoderB();
    png.setImage(getChart());
    out.write(png.pngEncode());
    out.close();
  }
  
  /** Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    processRequest(request, response);
  }
  
  /** Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    processRequest(request, response);
  }
  
  /** Returns a short description of the servlet.
   */
  public String getServletInfo() {
    return "Short description";
  }
  
  /** Returns a chart based on data in <CODE>Simula</CODE>.
   * @return a <CODE>BufferedImage</CODE> representing the chart
   */  
  private BufferedImage getChart() {
    
    try {
      
      // Configure object properties
      Object2DProperties object2DProps = new Object2DProperties();
      object2DProps.setObjectTitleText("Number of Studies per Year by Type of Study");
      
      // Configure chart properties.
      Chart2DProperties chart2DProps = new Chart2DProperties();
      // Setting precision to 0 give integer labels.
      chart2DProps.setChartDataLabelsPrecision(0);

      //Configure dataset
      Simula simula = SimulaFactory.getSimula();
      
      SearchFactory searchFactory = new SearchFactoryImpl();
      List studies = simula.getStudiesByCriteria(null);
      
      SortedMap years = new TreeMap();
      Set allStudyTypes = new TreeSet();
            
      for(Iterator i = studies.iterator(); i.hasNext();) {
        Study study = (Study)i.next();
        Integer year = new Integer(study.getEnd().getYear() + 1900);
        if(!years.containsKey(year)) {
          years.put(year, new TreeMap());
        }
        SortedMap studyTypes = (SortedMap)years.get(year);
        if(!studyTypes.containsKey(study.getType())) {
          studyTypes.put(study.getType(), new Integer(1));
        }
        else {
          Integer numberOfStudies = (Integer)studyTypes.get(study.getType());
          numberOfStudies = new Integer(numberOfStudies.intValue() + 1);
          studyTypes.put(study.getType(), numberOfStudies);
        }
        allStudyTypes.add(study.getType());
      }

      if(log.isDebugEnabled()) {
        log.debug("All Years: " + years.toString());
        log.debug("All Study Types: " + allStudyTypes.toString());
      }
      
      Dataset dataset = new Dataset(allStudyTypes.size(), years.size(), 1);
      
      int yearIndex = 0;
      int maxNumberOfStudies = 0;
      for(Iterator i = years.keySet().iterator(); i.hasNext(); yearIndex++) {
        Integer year = (Integer)i.next();
        SortedMap studiesThisYear = (SortedMap)years.get(year);
        int studyTypeIndex = 0;
        for(Iterator j = allStudyTypes.iterator(); j.hasNext(); studyTypeIndex++) {
          StudyType studyType = (StudyType)j.next();
          if(studiesThisYear.containsKey(studyType)) {
            int noOfStudiesOfStudyType = ((Integer)studiesThisYear.get(studyType)).intValue();
            dataset.set(studyTypeIndex, yearIndex, 0, (float)noOfStudiesOfStudyType);
            if(noOfStudiesOfStudyType > maxNumberOfStudies) {
              maxNumberOfStudies = noOfStudiesOfStudyType;
            }
          }
        }
      }
      
      // Configure legend properties
      LegendProperties legendProps = new LegendProperties();
      String[] legendLabels = new String[allStudyTypes.size()];
      int studyTypeIndex = 0;
      for(Iterator i = allStudyTypes.iterator(); i.hasNext(); studyTypeIndex++) {
        legendLabels[studyTypeIndex] = ((StudyType)i.next()).getName();
      }
      legendProps.setLegendLabelsTexts(legendLabels);

      // Configure graph chart properties
      GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
      String[] labelsAxisLabels = new String[years.size()];
      yearIndex = 0;
      for(Iterator i = years.keySet().iterator(); i.hasNext(); yearIndex++) {
        labelsAxisLabels[yearIndex] = ((Integer)i.next()).toString();
      }
      graphChart2DProps.setLabelsAxisLabelsTexts(labelsAxisLabels);
      
      graphChart2DProps.setLabelsAxisTitleText("Year");
      graphChart2DProps.setNumbersAxisTitleText("Number of Studies");

      int gratestValue;
      if(maxNumberOfStudies < 5) {
        gratestValue = 5;
      }
      else if((maxNumberOfStudies / 5) * 5 == maxNumberOfStudies) {
        gratestValue = maxNumberOfStudies;
      }
      else {
        gratestValue = ((maxNumberOfStudies / 5) * 5) + 5;
      }
      
      graphChart2DProps.setChartDatasetCustomizeLeastValue(true);
      graphChart2DProps.setChartDatasetCustomLeastValue(0f);
      graphChart2DProps.setChartDatasetCustomizeGreatestValue(true);
      graphChart2DProps.setChartDatasetCustomGreatestValue((float)gratestValue);
      graphChart2DProps.setNumbersAxisNumLabels(6);
      
      // Configure graph properties
      GraphProperties graphProps = new GraphProperties();
      graphProps.setGraphBarsRoundingRatio(0f);
      
      // Configure graph component colors
      MultiColorsProperties multiColorsProps = new MultiColorsProperties();
      
      // Configure chart
      LBChart2D chart2D = new LBChart2D();
      chart2D.setObject2DProperties(object2DProps);
      chart2D.setChart2DProperties(chart2DProps);
      chart2D.setLegendProperties(legendProps);
      chart2D.setGraphChart2DProperties(graphChart2DProps);
      chart2D.addGraphProperties(graphProps);
      chart2D.addDataset(dataset);
      chart2D.addMultiColorsProperties(multiColorsProps);
      
      // Optional validation:  Prints debug messages if invalid only.
      if (!chart2D.validate(false)) {
        chart2D.validate(true);
      }
            
      return(chart2D.getImage());
    }
    
    catch(Exception e) {
      e.printStackTrace();
      return(null);
    }
  }
}