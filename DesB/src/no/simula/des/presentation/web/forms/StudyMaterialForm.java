package no.simula.des.presentation.web.forms;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import no.halogen.presentation.web.CheckNewActionForm;
import org.apache.struts.upload.FormFile;

/** Holds information about a study material. This form contains data for both study
 * material of type <CODE>File</CODE> and <CODE>Link</CODE>.
 */
public class StudyMaterialForm extends CheckNewActionForm {
    
  /** Holds value of property type. */
  private String type;
  
  /** Holds value of property description. */
  private String description;
  
  /** Holds value of property name. */
  private String name;
  
  /** Holds value of property url. */
  private String url;
  
  /** Holds value of property types. */
  private List types;
  
  /** Holds value of property remoteFile. */
  private FormFile remoteFile;
  
  /** Holds value of property content. */
  private byte[] content;
  
  /** Getter for property type.
   * @return Value of property type.
   *
   */
  public String getType() {
    return(type);
  }
  
  /** Setter for property type.
   * @param type New value of property type.
   *
   */
  public void setType(String type) {
    this.type = type;
  }
  
  /** Getter for property description.
   * @return Value of property description.
   *
   */
  public String getDescription() {
    return(description);
  }
  
  /** Setter for property description.
   * @param description New value of property description.
   *
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  /** Getter for property file.
   * @return Value of property file.
   *
   */
  public String getName() {
    return(name);
  }
  
  /** Setter for property name.
   * @param name New value of property name.
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /** Getter for property url.
   * @return Value of property url.
   *
   */
  public String getUrl() {
    return(url);
  }
  
  /** Setter for property url.
   * @param url New value of property url.
   *
   */
  public void setUrl(String url) {
    this.url = url;
  }
  
  /** Getter for property types.
   * @return Value of property types.
   *
   */
  public List getTypes() {
    return(types);
  }
  
  /** Setter for property types.
   * @param types New value of property types.
   *
   */
  public void setTypes(List types) {
    this.types = types;
  }
  
  /** Getter for property upload.
   * @return Value of property upload.
   *
   */
  public FormFile getRemoteFile() {
    return this.remoteFile;
  }
  
  /** Sets the value of the uploaded file.
   * @param remoteFile New value of property remoteFile.
   */
  public void setRemoteFile(FormFile remoteFile) {
    this.remoteFile = remoteFile;
  }  
  
  /** Getter for property content.
   * @return Value of property content.
   *
   */
  public byte[] getContent() {
    return this.content;
  }
  
  /** Setter for property content.
   * @param content New value of property content.
   *
   */
  public void setContent(byte[] content) {
    this.content = content;
  }
  
}