package no.simula.des.presentation.web.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import no.halogen.presentation.web.CheckNewActionForm;
import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.DurationUnit;
import no.simula.des.StudyType;
import no.simula.utils.PersonFamilyNameComparator;
import org.apache.struts.action.ActionMapping;

/** Holds temporary data in the presentation layer when editing or creating a study.
 *
 * Dates are stored internally as day, month and year. Methods exist, however to
 * get and set these fields as <CODE>Date</CODE>s. This allows us to autimatically
 * copy dates between the presentation layer bean (<CODE>StudyForm</CODE>) and the
 * application layer bean (<CODE>Study</CODE>).
 */
public class StudyForm extends CheckNewActionForm {

  /** Creates a new <CODE>StudyForm</CODE>.
   * @throws Exception if unable to get an instance of <CODE>Simula</CODE>
   */  
  public StudyForm() throws Exception {
    simula = SimulaFactory.getSimula();
  }
  
  private Simula simula;
    
  private String description;
  private Integer typeId;
  private String duration;
  private Integer durationUnitId;
  private String keywords;
  private String name;
  private String noOfProfessionals;
  private String noOfStudents;
  private String notes;
  private List studyMaterial;
  private List publications;
  private Person ownedBy;
  private Person lastEditedBy;

  private String startDay;
  private String startMonth;
  private String startYear;
  
  private String endDay;
  private String endMonth;
  private String endYear;
  
  private List persons;
  private List responsibles;
  private Integer[] addResponsible;
  private Integer[] removeResponsible;
  
  /** <CODE>addResponsible</CODE> and <CODE>removeResponsible</CODE> is reset because
   * Struts is not able to do so automatically.
   * @param mapping current action mapping
   * @param request current http request
   */  
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    addResponsible = new Integer[0];
    removeResponsible = new Integer[0];
  }
  
  /** Returns a list of attached publications.
   * @return the attached publications
   */  
  public List getPublications() {
    return(publications);
  }
  
  /** Returns a list responsible persons
   * @return responsible persons
   */  
  public List getResponsibles() {
    return(responsibles);
  }
  
  /** Returns a list of attached study material
   * @return attached study material
   */  
  public List getStudyMaterial() {
    return(studyMaterial);
  }
  
  /** Getter for property description.
   * @return Value of property description.
   *
   */
  public String getDescription() {
    return description;
  }
  
  /** Setter for property description.
   * @param description New value of property description.
   *
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  /** Getter for property duration.
   * @return Value of property duration.
   *
   */
  public String getDuration() {
    return(duration);
  }
  
  /** Setter for property duration.
   * @param duration New value of property duration.
   *
   */
  public void setDuration(String duration) {
    this.duration = duration;
  }
    
  /** Getter for property keywords.
   * @return Value of property keywords.
   *
   */
  public String getKeywords() {
    return(keywords);
  }
  
  /** Setter for property keywords.
   * @param keywords New value of property keywords.
   *
   */
  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }
    
  /** Getter for property name.
   * @return Value of property name.
   *
   */
  public String getName() {
    return(name);
  }
  
  /** Setter for property name.
   * @param name New value of property name.
   *
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /** Getter for property noOfProfessionals.
   * @return Value of property noOfProfessionals.
   *
   */
  public String getNoOfProfessionals() {
    return(noOfProfessionals);
  }
  
  /** Setter for property noOfProfessionals.
   * @param noOfProfessionals New value of property noOfProfessionals.
   *
   */
  public void setNoOfProfessionals(String noOfProfessionals) {
    this.noOfProfessionals = noOfProfessionals;
  }
  
  /** Getter for property noOfStudents.
   * @return Value of property noOfStudents.
   *
   */
  public String getNoOfStudents() {
    return(noOfStudents);
  }
  
  /** Setter for property noOfStudents.
   * @param noOfStudents New value of property noOfStudents.
   *
   */
  public void setNoOfStudents(String noOfStudents) {
    this.noOfStudents = noOfStudents;
  }
  
  /** Getter for property notes.
   * @return Value of property notes.
   *
   */
  public String getNotes() {
    return(notes);
  }
  
  /** Setter for property notes.
   * @param notes New value of property notes.
   *
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /** Getter for property endDay.
   * @return Value of property endDay.
   *
   */
  public String getEndDay() {
    return endDay;
  }
  
  /** Setter for property endDay.
   * @param endDay New value of property endDay.
   *
   */
  public void setEndDay(String endDay) {
    this.endDay = endDay;
  }
  
  /** Getter for property endMonth.
   * @return Value of property endMonth.
   *
   */
  public String getEndMonth() {
    return endMonth;
  }
  
  /** Setter for property endMonth.
   * @param endMonth New value of property endMonth.
   *
   */
  public void setEndMonth(String endMonth) {
    this.endMonth = endMonth;
  }
  
  /** Getter for property endYear.
   * @return Value of property endYear.
   *
   */
  public String getEndYear() {
    return endYear;
  }
  
  /** Setter for property endYear.
   * @param endYear New value of property endYear.
   *
   */
  public void setEndYear(String endYear) {
    this.endYear = endYear;
  }
  
  /** Getter for property startDay.
   * @return Value of property startDay.
   *
   */
  public String getStartDay() {
    return startDay;
  }
  
  /** Setter for property startDay.
   * @param startDay New value of property startDay.
   *
   */
  public void setStartDay(String startDay) {
    this.startDay = startDay;
  }
  
  /** Getter for property startMonth.
   * @return Value of property startMonth.
   *
   */
  public String getStartMonth() {
    return startMonth;
  }
  
  /** Setter for property startMonth.
   * @param startMonth New value of property startMonth.
   *
   */
  public void setStartMonth(String startMonth) {
    this.startMonth = startMonth;
  }
  
  /** Getter for property startYear.
   * @return Value of property startYear.
   *
   */
  public String getStartYear() {
    return startYear;
  }
  
  /** Setter for property startYear.
   * @param startYear New value of property startYear.
   *
   */
  public void setStartYear(String startYear) {
    this.startYear = startYear;
  }

  /** Set the start date.
   * @param date the start date
   */  
  public void setStart(Date date) {
    if(date != null) {
      startDay = String.valueOf(date.getDate());
      startMonth = String.valueOf(date.getMonth() + 1);
      startYear = String.valueOf(date.getYear() + 1900);
    }
  }
    
  /** Returns the start date
   * @return the start date
   */  
  public Date getStart() {
    try {
      int y = Integer.parseInt(startYear);
      int m = Integer.parseInt(startMonth);
      int d = Integer.parseInt(startDay);
      return(new Date(y - 1900, m - 1, d));
    }
    catch(Exception e) {
      return(null);
    }
  }
    
  /** Sets the end date
   * @param date the end date
   */  
  public void setEnd(Date date) {
    if(date != null) {
      endDay = String.valueOf(date.getDate());
      endMonth = String.valueOf(date.getMonth() + 1);
      endYear = String.valueOf(date.getYear() + 1900);
    }
  }
  
  /** Returns the end date.
   * @return the end date
   */  
  public Date getEnd() {
    try {
      int y = Integer.parseInt(endYear);
      int m = Integer.parseInt(endMonth);
      int d = Integer.parseInt(endDay);
      return(new Date(y - 1900, m - 1, d));
    }
    catch(Exception e) {
      return(null);
    }
  }
  
  /** Getter for property type.
   * @return Value of property type.
   *
   */
  public java.lang.Integer getTypeId() {
    return typeId;
  }
  
  /** Sets the selected study type id.
   * @param typeId the study type id
   */
  public void setTypeId(java.lang.Integer typeId) {
    this.typeId = typeId;
  }
  
  /** Getter for property addResponsible.
   * @return Value of property addResponsible.
   *
   */
  public Integer[] getAddResponsible() {
    return(addResponsible);
  }
  
  /** Setter for property addResponsible.
   * @param addResponsible New value of property addResponsible.
   *
   */
  public void setAddResponsible(Integer[] addResponsible) {
    this.addResponsible = addResponsible;
  }

  /** Getter for property removeResponsible.
   * @return Value of property removeResponsible.
   *
   */
  public Integer[] getRemoveResponsible() {
    return(removeResponsible);
  }
    
  /** Setter for property removeResponsible.
   * @param removeResponsible New value of property removeResponsible.
   *
   */
  public void setRemoveResponsible(Integer[] removeResponsible) {
    this.removeResponsible = removeResponsible;
  }
    
  /** Getter for property unit.
   * @return Value of property unit.
   *
   */
  public java.lang.Integer getDurationUnitId() {
    return durationUnitId;
  }
  
  /** Sets the duration unit id.
   * @param durationUnitId the duration unit id
   */
  public void setDurationUnitId(java.lang.Integer durationUnitId) {
    this.durationUnitId = durationUnitId;
  }
  
  /** Setter for property studyMaterial.
   * @param studyMaterial New value of property studyMaterial.
   *
   */
  public void setStudyMaterial(List studyMaterial) {
    this.studyMaterial = studyMaterial;
  }
  
  /** Setter for property publications.
   * @param publications New value of property publications.
   *
   */
  public void setPublications(List publications) {
    this.publications = publications;
  }
  
  /** Setter for property responsibles. This list is always sorted.
   * @param responsibles New value of property responsibles.
   *
   */
  public void setResponsibles(List responsibles) {
    if(responsibles == null) {
      this.responsibles = null;
    }
    else {
      this.responsibles = new ArrayList(responsibles);
      sortResponsibles();
    }
  }
  
  /** Getter for property persons.
   * @return Value of property persons.
   *
   */
  public List getPersons() {
    return(persons);
  }
  
  /** Setter for property persons. This list is always sorted.
   * @param persons New value of property persons.
   *
   */
  public void setPersons(List persons) {
    if(persons == null) {
      this.persons = null;
    }
    else {
      this.persons = new ArrayList(persons);
      sortPersons();
    }
  }
  
  /** Getter for property lastEditedBy.
   * @return Value of property lastEditedBy.
   *
   */
  public Person getLastEditedBy() {
    return(lastEditedBy);
  }
  
  /** Setter for property lastEditedBy.
   * @param lastEditedBy New value of property lastEditedBy.
   *
   */
  public void setLastEditedBy(Person lastEditedBy) {
    this.lastEditedBy = lastEditedBy;
  }
  
  /** Getter for property ownedBy.
   * @return Value of property ownedBy.
   *
   */
  public Person getOwnedBy() {
    return(ownedBy);
  }
  
  /** Setter for property ownedBy.
   * @param ownedBy New value of property ownedBy.
   *
   */
  public void setOwnedBy(Person ownedBy) {
    this.ownedBy = ownedBy;
  }
 
  /** Retuns the type of study.
   * @throws SimulaException if study type could not be retrieved
   * @return the type of study
   */  
  public StudyType getType() throws SimulaException {
    return(simula.getStudyType(typeId));
  }
  
  /** Sets the type of study
   * @param type the type of study
   */  
  public void setType(StudyType type) {
    if(type == null) {
      typeId = null;
    }
    else {
      typeId = type.getId();
    }
  }
  
  /** Returns the duration unit.
   * @throws SimulaException if duration unit could not be retrieved
   * @return the duration unit
   */  
  public DurationUnit getDurationUnit() throws SimulaException {
    return(simula.getDurationUnit(durationUnitId));
  }
  
  /** Sets the duration unit.
   * @param durationUnit the duration unit
   */  
  public void setDurationUnit(DurationUnit durationUnit) {
    if(durationUnit == null) {
      durationUnitId = null;
    }
    else {
      durationUnitId = durationUnit.getId();
    }
  }
  
  /** Sorts the list of non-responsible persons by family name. */  
  public void sortPersons() {
    Collections.sort(this.persons, new PersonFamilyNameComparator());
  }
  
  /** Sort the list of responsible persons by family name. */  
  public void sortResponsibles() {
    Collections.sort(this.responsibles, new PersonFamilyNameComparator());
  }  
}