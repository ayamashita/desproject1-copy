package no.simula.des.presentation.web;

import java.text.SimpleDateFormat;
import java.util.Locale;
import no.halogen.utils.table.ColumnDecorator;

/** Formats a date as "dd MMM yyyy" (i.e. 23 mar 1974) format using a UK locale.
 *
 * @author Stian Eide
 */
public class DateColumnDecorator implements ColumnDecorator {
  
  /** Creates a new <CODE>DateColumnDecorator</CODE>. */
  public DateColumnDecorator() {
    sdf = new SimpleDateFormat("dd MMM yyyy", new Locale("en"));
  }
  
  SimpleDateFormat sdf;
  
  /** Returns the formatted object. Please note that the behaviour of this method is
   * is undefined if <CODE>!value instanceof Date</CODE>.
   * @param value the date to be formatted
   * @return the formatted string representing the date
   */  
  public String render(Object value) {
    return(sdf.format(value));
  }  
}