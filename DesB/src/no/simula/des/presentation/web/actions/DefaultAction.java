package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** Handles requests that has no specific action. This action returns the action
 * forwards <CODE>guest</CODE> or <CODE>admin</CODE> depending on whether the
 * current user is logged in or not.
 */
public class DefaultAction extends Action {

  private static Log log = LogFactory.getLog(DefaultAction.class);
  
  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {      
    if(request.getUserPrincipal() != null) {
      log.debug("User is authenticated and redirected to the default admin page.");
      return(mapping.findForward("admin"));
    }
    else {
      log.debug("User is guest and redirected to the search studies-page.");
      return(mapping.findForward("guest"));
    }
  }
}