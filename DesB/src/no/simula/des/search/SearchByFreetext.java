/**
 * @(#) SearchByFreetext.java
 */

package no.simula.des.search;

import no.halogen.search.*;
import no.halogen.search.Criterium;

public interface SearchByFreetext extends Criterium, CompositeCriterium
{
	
}
