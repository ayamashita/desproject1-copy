/**
 * @(#) SearchByResponsibleImpl.java
 */

package no.simula.des.search;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.search.CriteriumException;
import no.halogen.search.CriteriumImpl;
import no.halogen.statements.ObjectStatement;
import no.simula.des.statements.StudyPersonRelStatement;

/**
 * @author Frode Langseth
 */
public class SearchByResponsibleImpl extends CriteriumImpl implements SearchByResponsible {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SearchByResponsible.class);

  /**
   * Field DATABASE_COLUMN
   */
  private final String DATABASE_COLUMN = "people_id";
  /**
   * Field DEFAULT_OPERATOR
   */
  private final String DEFAULT_OPERATOR = new String(" IN ");

  /** 
   * Generates the criteria and "?" character as value representators as a part of a SQL where clause
   * Due to use of prepared statements, the value attributes must be fetched when a prepared statement is instatiated and will use the criterium
   * The method does not generate the start of the clause (WHERE), nor the operator (AND, OR ..)
   * 
   * @author Frode Langseth
   * @version 0.1
   * @return String
   * @throws CriteriumException
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  public String generateWhereClause() throws CriteriumException {
    StringBuffer statement = new StringBuffer();

    if (getAttributes() == null) {
      log.error("generateWhereClause - No attributes in SearchByResponsible  criterium");
      throw new CriteriumException("Error! No attributes ...");
    }

    statement.append("("); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    statement.append(" " + getCriteriumColumn() + " IN (");

    Iterator i = getAttributes().iterator();
    boolean firstIteration = true;
    while (i.hasNext()) {
      if (!firstIteration) { // if more than one responsible
        statement.append(", ");
      }

      statement.append("?");

      i.next();
      firstIteration = false;
    }

    statement.append("))"); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    return (statement.toString());
  }

  /**
   * @return String
   */
  private String getCriteriumColumn() {
    return (DATABASE_COLUMN);
  }

  /**
   * Returns the statement object that represents the database table tha attribute relates to
   * 
   * @return An object statement 
   * @see no.halogen.search.Criterium#getStatement()
   */
  public ObjectStatement getStatement() {
    return (new StudyPersonRelStatement());
  }

}
