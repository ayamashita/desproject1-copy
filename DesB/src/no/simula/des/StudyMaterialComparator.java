package no.simula.des;

/**
 * This class offers a convenient way to compare
 * a given id to the id of a <code>StudyMaterial</code>
 * implementation
 *
 * @author  Stian Eide
 */
public class StudyMaterialComparator extends StudyMaterial {
  
  /** Creates a new instance of <CODE>StudyMaterialComparator</CODE>with the id to be
   * compared to the id of a <CODE>StudyMaterial</CODE> implementation.
   * @param id the id to be compared
   */
  public StudyMaterialComparator(Integer id) {
    this.id = id;
  }
  
  /** Returns <CODE>null</CODE>.
   * @return <CODE>null</CODE>
   */  
  public String getType() {
    return(null);
  }
  
  /** Returns <CODE>null</CODE>.
   * @return <CODE>null</CODE>
   */  
  public String getUrl() {
    return(null);
  }
}