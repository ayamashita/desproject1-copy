/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.DurationUnit;
import no.simula.des.statements.DurationUnitStatement;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentDurationUnit implements PersistentObject {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersistentDurationUnit.class);

  /**
   * 
   * @param instance 
   * @return null
   * 
   * @throws CreatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#create(Object)
   */
  public Integer create(Object instance) throws CreatePersistentObjectException {
    Integer durationUnitId = new Integer(0);

    DurationUnit durationUnit = (DurationUnit) instance;

    DurationUnitStatement statement = new DurationUnitStatement();

    /* Tells the statement what study object to save */
    statement.setDataBean(durationUnit);

    /* Save the study */
    try {
      durationUnitId = statement.executeInsert();
    } catch (StatementException se) {
      log.error("Couldn't create duration unit");

      se.printStackTrace();

      throw new CreatePersistentObjectException();
    }

    return (durationUnitId);
  }

  /**
   * 
   * @param id 
   * @return boolean
   * @throws DeletePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#delete(Integer)
   */
  public boolean delete(Integer id) throws DeletePersistentObjectException {
    boolean result = false;

    DurationUnitStatement statement = new DurationUnitStatement();

    /* Delete the admin module text */
    try {
      result = statement.executeDelete(id);
    } catch (StatementException se) {
      log.error("Couldn't delete duration unit");

      se.printStackTrace();

      throw new DeletePersistentObjectException();
    }

    return (result);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKey(java.lang.Integer)
   */
  /**
   * Method findByKey
   * @param id Integer
   * @return Object
   * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
   */
  public Object findByKey(Integer id) {
    Object result = new Object();
    DurationUnitStatement statement = new DurationUnitStatement();

    DurationUnit durationUnit = new DurationUnit();

    statement.setDataBean(durationUnit);

    try {
      result = statement.executeSelect(id);
    } catch (StatementException e) {
      log.error("findBykey() - Fetching of study type " + id + " failed!!", e);
      return (null);
    }

    if (result instanceof DurationUnit) {
      durationUnit = (DurationUnit) result;
    }

    return (durationUnit);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKeys(java.util.List)
   */
  /**
   * Method findByKeys
   * @param entityIds List
   * @return List
   * @see no.halogen.persistence.PersistentObject#findByKeys(List)
   */
  public List findByKeys(List entityIds) {
    List results = new ArrayList();

    if (!entityIds.isEmpty()) {
      DurationUnitStatement statement = new DurationUnitStatement();

      StringBuffer stmtWhere = new StringBuffer();

      DurationUnit durationUnit = new DurationUnit();

      statement.setDataBean(durationUnit);

      if (entityIds != null && !entityIds.isEmpty()) {
        stmtWhere.append(" WHERE du_id IN (");

        Iterator i = entityIds.iterator();
        boolean firstIteration = true;

        while (i.hasNext()) {
          if (!firstIteration) {
            stmtWhere.append(", ");
          }

          stmtWhere.append("?");

          statement.getConditions().add(i.next());

          firstIteration = false;
        }

        stmtWhere.append(")");
      }

      statement.setWhereClause(stmtWhere.toString());

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of duration units failed!!", e);
        return (null);
      }
    }

    return (results);
  }

  /**
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Object)
   */
  public boolean update(Object instance) throws UpdatePersistentObjectException {
    DurationUnit durationUnit = (DurationUnit) instance;

    return (update(durationUnit.getId(), instance));
  }

  /**
   * There's not possible to update study types (e.g. using an administrator's interface), except directly in the database
   * This method is not implemented
   * @param id Integer
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
   */
  public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
    boolean result = false;

    DurationUnit durationUnit = (DurationUnit) instance;

    DurationUnitStatement statement = new DurationUnitStatement();

    /* Tells the statement what study object to update */
    statement.setDataBean(durationUnit);

    /* Update the study */
    try {
      result = statement.executeUpdate(id);
    } catch (StatementException se) {
      log.error("Couldn't update duration unit");

      se.printStackTrace();

      throw new UpdatePersistentObjectException();
    }

    return (result);
  }

  /**
   * Finding all Study types
   * @return - All study types in the database
   * 
   * @see no.halogen.persistence.PersistentObject#find()
   */
  public List find() {
    List results = new ArrayList();

    DurationUnitStatement statement = new DurationUnitStatement();

    DurationUnit durationUnit = new DurationUnit();

    statement.setDataBean(durationUnit);

    try {
      results = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("find() - Fetching of all duration unitsfailed!!", e);
      return (null);
    }

    return (results);
  }

}
