package no.simula.struts;

import java.lang.StringBuffer;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import no.simula.Person;
import no.simula.SimulaFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.RequestProcessor;

/** This request processor hooks into Struts and makes sure that the
 * <CODE>Person</CODE>-object of an authorized user always is available in the
 * session scope under the key <code>PERSON_KEY</code>.
 * <p>
 * Additionally, it updates the navigation path and active action that may be used
 * by halogen-taglibs when rendering navigation paths and menu-items.
 * @author Stian Eide
 */
public class SimulaRequestProcessor extends RequestProcessor {
  
  /** The key in session where the <CODE>Person</CODE>-object of an authenticated user
   * is stored.
   */  
  public static final String PERSON_KEY = "no.simula.struts.PERSON_KEY";
  private static Log log = LogFactory.getLog(SimulaRequestProcessor.class);
  
  /** Pre-process all requests before they are processed by the relevant
   * <CODE>Action</CODE>-object.
   * @param request current http request
   * @param response current http response
   * @return <CODE>false</CODE> - if session is timed out (the request is also redirected to
   * the default action). Otherwise, <CODE>true</CODE>.
   */  
  protected boolean processPreprocess(HttpServletRequest request, HttpServletResponse response) {

    try {
      String path = processPath(request, response);
      
      try {
        if(request.getSession().isNew() && !"/default".equals(path)) {
          ActionMessages messages = new ActionMessages();
          messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("messages.session.timeout"));
          request.setAttribute(Globals.MESSAGE_KEY, messages);
          Principal user = request.getUserPrincipal();
          if(user != null) {
            Person person = SimulaFactory.getSimula().getPersonByEmail(user.getName());
            request.getSession().setAttribute(PERSON_KEY, person);
          }
          request.getRequestDispatcher("/do/default").forward(request, response);
          return(false);
        }
      }
      catch(IllegalStateException ise) {
        response.sendRedirect(request.getContextPath() + "/do/default");
        log.error("Error during redirect after session-timeout.", ise);
        return(false);
      }
        
      
      ActionMapping mapping = processMapping(request, response, path);

      List navigationPath = createNavigationPath(mapping);
      request.setAttribute(no.halogen.presentation.web.Constants.NAVIGATION_PATH, navigationPath);
      request.setAttribute(no.halogen.presentation.web.Constants.ACTIVE_ACTION, mapping.getPath());

      if(request.getSession().getAttribute(PERSON_KEY) != null) {
        // No need for further processing as the user already exist
        // in the session scope
        return(true);
      }
      try {
        Principal user = request.getUserPrincipal();
        if(user != null) {
          Person person = SimulaFactory.getSimula().getPersonByEmail(user.getName());
          request.getSession().setAttribute(PERSON_KEY, person);
        }
      }
      catch(Exception e) {
        log.error("Failed to store logged on user in session.", e);
      }
      return(true);
    }
    
    catch(Exception e) {
      return(true);
    }
  }

  private List createNavigationPath(ActionMapping mapping) {
    List navigationPath = new ArrayList();
    ActionForward returnForward = mapping.findForward("return");
    if(returnForward != null) {
      String path = returnForward.getPath().substring(3);
      ActionMapping returnMapping = (ActionMapping)moduleConfig.findActionConfig(path);
      navigationPath = getNavigationPathFragment(returnMapping, navigationPath);
    }
    if(mapping.getParameter() == null) {
      navigationPath.add(mapping.getPath());
    }
    else {
      navigationPath.add(mapping.getPath() + "." + mapping.getParameter());
    }
    return(navigationPath);
  }

  private List getNavigationPathFragment(ActionMapping mapping, List navigationPath) {
    ActionForward returnForward = mapping.findForward("return");
    if(returnForward != null) {
      String path = returnForward.getPath().substring(3);
      ActionMapping returnMapping = (ActionMapping)moduleConfig.findActionConfig(path);
      navigationPath = getNavigationPathFragment(returnMapping, navigationPath);
    }
    if(mapping.getParameter() == null) {
      navigationPath.add(mapping.getPath());
    }
    else {
      navigationPath.add(mapping.getPath() + "." + mapping.getParameter());
    }
    return(navigationPath);
  }
}