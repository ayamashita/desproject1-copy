/*
 * Created on 31.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.search;

import java.sql.DriverManager;

/**
 * Exception thrown when an Criterium cannot be generated as SQL
 * @author Frode Langseth
 */
public class CriteriumException extends Exception {
	/**
	 * Constructor for CriteriumException
	 * @param reason String why is the exception thrown
	 */
	public CriteriumException(String reason) {
		super(reason);

		if (DriverManager.getLogWriter() != null) {
			printStackTrace(DriverManager.getLogWriter());
		}
	}

	/**
	 * Constructs an <code>StatementException</code> object;
	 *
	 */
	public CriteriumException() {
		super();

		if (DriverManager.getLogWriter() != null) {
			printStackTrace(DriverManager.getLogWriter());
		}
	}
}
