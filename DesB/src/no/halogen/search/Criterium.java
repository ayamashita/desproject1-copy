/**
 * @(#) Criterium.java
 */

package no.halogen.search;

import java.util.List;

import no.halogen.statements.ObjectStatement;


/**
 * Interface for search attributes.
 * Attributes contains <code>values</code>, and can <code>generate code</code> needed to execute a SQL statement
 * 
 * @author Frode Langseth
 */
public interface Criterium
{
	/**
	 * Method addAttributes
   * Adds a list of attribute <code>values</code>
   * 
	 * @param attributes List <code>values</code> that will be included as search conditions
	 */
	void addAttributes( List attributes );
	/**
	 * Method addAttribute
   * Adds a single attribute <code>value</code>
	 * @param attribute Object the <code>value</code> that will be included as a search condition
	 */
	void addAttribute(Object attribute);
	/**
	 * Method getAttributes
   * Gets all the <code>Criterium</code><code>values</code> 
	 * @return List the attribute <code>values</code>
	 */
	List getAttributes();
	
	/**
	 * Method generateWhereClause
   * Based on the <code>Criterium</code>'s attribute values, this method returns SQL code that can be used in a search statement 
	 * @return String the generated string with SQL code
	 * @throws CriteriumException if the SQL couldn't be generated
	 */
	String generateWhereClause( ) throws CriteriumException;
	
	/**
	 * Gets the operator for the criteria (=, !=, IN, NOT, etc.)
	 * @return The operator
	 * 
	 */
	String getOperator();
	
	/**
	 * Sets the operator for the criteria (=, !=, IN, NOT, etc.)
	 * 
   * Noone of the classes implementing this interface, has an implementation of this metod in this version of the application
   *
	 * @param operator - The operator value to set
	 * 
	 */
	void setOperator(String operator);
	
	/**
	 * Gets the join condition to set first of the criterium (AND, OR)
	 * If the  criterium is the first in the where clause, the join condition is of no interest, as the clause starts with WHERE
	 * 
   * Noone of the classes implementing this interface, has an implementation of this metod in this version of the application
   *
	 * @return The join condition 
	 * 
	 */
	String getJoinCondition();
	
	/**
	 * Sets the join condition to set first of the criterium (AND, OR)
	 * If the  criterium is the first in the where clause, the join condition is of no interest, as the clause starts with WHERE
	 * 
   * Noone of the classes implementing this interface, has an implementation of this metod in this version of the application
   *
	 * @author Frode Langseth
	 * @version 0.1
	 * @param joinCondition - The join condition to set
	 * 
	 */
	void setJoinCondition(String joinCondition);
	
	/**
	 * Returns the statements involved in executeing a search based on the given criterium
	 * 
   * Noone of the classes implementing this interface, has an implementation of this metod in this version of the application
   *
	 * @return - A List of statements
	 */
	ObjectStatement getStatement();
			
}
