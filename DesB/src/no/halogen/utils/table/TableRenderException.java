package no.halogen.utils.table;

/** Thrown if a <CODE>Table</CODE> cannot be rendered by <CODE>TableRenderer</CODE>.
 * @author Stian Eide
 */
public class TableRenderException extends Exception {
  
  /** Creates a new <CODE>TableRendererException</CODE>. */  
  public TableRenderException() {}
  
  /** Creates a new <CODE>TableRendererException</CODE> with the specified message.
   * @param message a text message that explains the error
   */  
  public TableRenderException(String message) {
    super(message);
  }
}