package no.halogen.utils.table;

import java.util.List;

/** A wrapper around a <CODE>List</CODE> in order to use this as a
 * <CODE>TableSource</CODE>.
 * @author Stian Eide
 */
public class TableSourceList implements TableSource {
  
  /** Creates a new <CODE>TableSourceList</CODE> with <CODE>list</CODE> as the source.
   * @param list the <CODE>list</CODE> to be used as content for this source
   */  
  public TableSourceList(List list) {
    this.list = list;
  }
  
  /** The list, aka the source. */  
  private List list;
  
  /** Returns the list as the content of this source
   * @return the list
   */  
  public List getContent() {
    return(list);
  }
  
  /** Does nothing because the list will always be updated vis-a-vis itself. */  
  public void update() {
    // Do nothin'
  }
  
  /** Returns the <CODE>List</CODE> that is the content of this source.
   * @return the list
   */
  public List getList() {
    return(list);
  }
  
  /** Sets the <CODE>List</CODE> that is the content of this source.
   * @param list the new content of this source
   */
  public void setList(List list) {
    this.list = list;
  }
}