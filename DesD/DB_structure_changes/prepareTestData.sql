DELETE FROM publication_study;

INSERT INTO publication_study (publication_id,study_id) VALUES ('Simula.SE.155',2);
INSERT INTO publication_study (publication_id,study_id) VALUES ('Simula.SE.156 ',2);
INSERT INTO publication_study (publication_id,study_id) VALUES ('Simula.SE.157',3);
INSERT INTO publication_study (publication_id,study_id) VALUES ('Simula.SE.101',5);
INSERT INTO publication_study (publication_id,study_id) VALUES ('Simula.SE.263',7);

DELETE FROM people_study;

INSERT INTO  people_study (people_id,study_id) VALUES ('aiko',5);
INSERT INTO  people_study (people_id,study_id) VALUES ('aiko',7);
INSERT INTO  people_study (people_id,study_id) VALUES ('bentea',2);
INSERT INTO  people_study (people_id,study_id) VALUES ('bentea',3);
INSERT INTO  people_study (people_id,study_id) VALUES ('bentea',5);
INSERT INTO  people_study (people_id,study_id) VALUES ('bentea',7);
INSERT INTO  people_study (people_id,study_id) VALUES ('dagsj',5);
INSERT INTO  people_study (people_id,study_id) VALUES ('dagsj',7);
INSERT INTO  people_study (people_id,study_id) VALUES ('benestad',5);
INSERT INTO  people_study (people_id,study_id) VALUES ('briand',2);


DELETE FROM people_role;

INSERT INTO people_role VALUES ('aiko',1);
INSERT INTO people_role VALUES ('bentea ',2);
