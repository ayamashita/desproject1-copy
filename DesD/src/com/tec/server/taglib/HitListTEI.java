package com.tec.server.taglib;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

/**
 * Hit list JSP tag extra info class.
 *
 * @author Norunn Haug Christensen
 */
public class HitListTEI extends TagExtraInfo {
   public HitListTEI() {
      super();
   }

   public VariableInfo[] getVariableInfo(TagData data) {
      VariableInfo info1
         = new VariableInfo(
            data.getAttributeString("name"),
            "String",	
            true,
            VariableInfo.NESTED);
      VariableInfo [] info = { info1 };
      return info;
   }

   public boolean isValid(TagData data) {
      return maxHitsOK(data) && modeOK(data);
   }

   private boolean modeOK(TagData data) {
      Object o = data.getAttribute("mode");
      if (o != null && o != TagData.REQUEST_TIME_VALUE) {
         if (HitListTag.MODE_LIST.equals(o) || HitListTag.MODE_SEARCH.equals(o)) {
           return true;
         } else {
           return false;
         }
      }
      return true;
   }

   private boolean maxHitsOK(TagData data) {
      Object o = data.getAttribute("maxHits");
      if (o != null && o != TagData.REQUEST_TIME_VALUE) {
         try {
            int maxHits = Integer.parseInt((String)o);
            return true;
         } catch (Exception e) {
            return false;
         }
      }
      return true;
   }

}