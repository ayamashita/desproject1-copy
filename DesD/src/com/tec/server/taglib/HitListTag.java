package com.tec.server.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.tec.shared.util.ExceptionMessages;

/**
 * Hit list JSP tag library.
 *
 * @author Norunn Haug Christensen
 */
public class HitListTag extends BodyTagSupport {

   static final String MODE_SEARCH = "search";
   static final String MODE_LIST = "list";
   private static final int MODE_SEARCH_INT = 0;
   private static final int MODE_LIST_INT = 1;

   private int mode;
   private Object[] members;
   private String name;
   private int maxHits;
   private boolean usingMaxHits = false;

   public void setName(String name) {
      this.name = name;
   }

   public void setMode(String mode) {
      if (MODE_SEARCH.equals(mode)) {
        this.mode = MODE_SEARCH_INT;
      } else if (MODE_LIST.equals(mode)) {
        this.mode = MODE_LIST_INT;
      }
   }

   public void setMaxHits(int maxHits) {
      this.maxHits = maxHits;
      this.usingMaxHits = true;
   }
   
   public void setGroup(Object[] members) {
      this.members = members;
   }

   public int doStartTag() throws JspException {
     try {
       int size = (members == null) ? 0 : members.length;
       if(mode == MODE_SEARCH_INT && members == null) {     
          return SKIP_BODY;
       } else if (mode == MODE_SEARCH_INT && size == 0) {
          pageContext.getOut().print(ExceptionMessages.NO_HITS); 
          return SKIP_BODY;
       } else if (usingMaxHits && size > maxHits) {
          pageContext.getOut().print(ExceptionMessages.TOO_MANY_HITS); 
          return SKIP_BODY;
       } else {
         pageContext.setAttribute(name, Integer.toString(size));         
         return EVAL_BODY_BUFFERED;
       }
     } catch (IOException e) {
        throw new JspTagException("HitListTag: " + e.getMessage());
     }
   }

   
   public int doAfterBody() throws JspException {
      BodyContent body = getBodyContent();
      try {
         body.writeOut(getPreviousOut());
      } catch (IOException e) {
         throw new JspTagException("HitListTag: " + e.getMessage());
      }
      return SKIP_BODY;    
   }

}