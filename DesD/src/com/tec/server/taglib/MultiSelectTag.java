package com.tec.server.taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Multi select JSP tag library.
 *
 * @author Per Kristian Foss
 */
public class MultiSelectTag extends TagSupport 
{
  private String name = "multiselect1";
  private String[] optionvalues;
  private String[] displayedvalues;
  private String attributes = "";
  private String boxsize = "";
  
  public int doStartTag () throws JspException 
  {
    try 
    {

      if (optionvalues != null) 
      {
          StringBuffer htmlOutput = new StringBuffer();
          htmlOutput.append("<select multiple name=\"" + name + "\" " + attributes + " size=\"" + boxsize + "\" >");
          String displayvalue; 
          for (int i=0; i<optionvalues.length; i++) 
          {
            
            displayvalue = (displayedvalues != null) ? displayedvalues[i] : optionvalues[i];
            htmlOutput.append("<option value =\"" + optionvalues[i] + "\">" + displayvalue + "</option>");
          }
          htmlOutput.append("</select>");
          pageContext.getOut().print(htmlOutput.toString());
          
      }
      
    } catch (Exception ex) 
    {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setName(String name) 
  {
      this.name = name; 
  }

  public void setOptionvalues(String[] optionvalues) 
  {
    this.optionvalues = optionvalues;
  }

  public void setDisplayedvalues(String[] displayedvalues) 
  {
    this.displayedvalues = displayedvalues;
  }

  public void setAttributes(String attributes) 
  {
    this.attributes = attributes;
  }
  
  public void setBoxsize(String boxsize) 
  {
    this.boxsize = boxsize;
  }
}