package com.tec.server.taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * JSP tag library displaying a drop down list.
 *
 * @author Norunn Haug Christensen
 */
public class DropDownListTag extends TagSupport {
  private String name = "select1";
  private String[] optionValues;
  private String[] displayedvalues;
  private String attributes = "";
  private String optional = "";
  private String selectedvalue;
  private String TRUE = "true";

/**
 * Method called when the DropDownListTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */
  public int doStartTag() throws JspException {
    try {

      if (optionValues != null) {
          StringBuffer htmlOutput = new StringBuffer();
          htmlOutput.append("<select name=\"" + name + "\" " + attributes + " >");
          if ((TRUE).equals(optional)){
            htmlOutput.append("<option value =\"" + "" + "\">" + "" + "</option>");
          }
          String displayvalue;
          String value;
          boolean selected = false;
          for (int i=0; i<optionValues.length; i++) {
            value = optionValues[i];
            displayvalue = (displayedvalues != null) ? displayedvalues[i] : value;
            selected = value.equals(selectedvalue);
            htmlOutput.append("<option value =\"" + value + "\" " 
              + ((selected)?"selected = \"selected\"":"")  + ">" + displayvalue +"</option>");
          }
          htmlOutput.append("</select>");
          pageContext.getOut().print(htmlOutput.toString());
          
      }
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setName(String name) {
      this.name = name; 
  }

  public void setOptionValues(String[] optionValues) { 
    this.optionValues = optionValues;
  }

  public void setDisplayedvalues(String[] displayedvalues) { 
    this.displayedvalues = displayedvalues;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }

  public void setOptional(String optional) {
    this.optional = optional;
  }

  public void setSelectedvalue(String selectedvalue) {
    this.selectedvalue = selectedvalue;
  }
  
}