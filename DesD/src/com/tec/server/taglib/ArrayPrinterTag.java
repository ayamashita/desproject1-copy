package com.tec.server.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * JSP tag library for printing items of an array separated with the
 * input parameter separator. If no separator is given, det default ',' is used.
 *
 * @author Norunn Haug Christensen 
 */
public class ArrayPrinterTag extends TagSupport {
  
  private String separator = ",";
  private String[] items;

/**
 * Method called when the ArrayPrinterTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */  
  public int doStartTag () throws JspException {
    try {

      if (items != null) {
          StringBuffer htmlOutput = new StringBuffer();

          for (int i=0; i<items.length; i++) {
            htmlOutput.append(items[i]);
            
            if (i<items.length-1) {
              htmlOutput.append(separator);
            }
          }

          pageContext.getOut().print(htmlOutput.toString());          
      }
      
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setSeparator(String separator) {
      this.separator = separator; 
  }

  public void setItems(String[] items) {
    this.items = items;
  }

}