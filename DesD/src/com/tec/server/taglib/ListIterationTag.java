package com.tec.server.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.tec.shared.util.ArrayIterator;

/**
 * Simple List iteration JSP tag library.
 *
 * @author Norunn Haug Christensen
 */
public class ListIterationTag extends BodyTagSupport {
   private String name, type;
   private ArrayIterator iterator;
   private boolean rowAlternatorStatus;
   private String rowAlternator;
   private static final String ROW_ALT_1 = "Alternate1";
   private static final String ROW_ALT_2 = "Alternate2";

   public void setName(String name) {
      this.name = name;
   }

   public void setType(String type) {
      this.type = type;
   }

   public void setRowAlternator(String rowAlternator) {
     this.rowAlternator = rowAlternator;
   }

   public void setGroup(Object[] members) {
      if(members != null && members.length > 0)
         iterator = new ArrayIterator(members);
   }

   public int doStartTag() {
      rowAlternatorStatus = true;   
      if(iterator == null) 
         return SKIP_BODY;
      if(iterator.hasNext()) {
         pageContext.setAttribute(name, iterator.next());
         setRowAlternatorStatus();
         return EVAL_BODY_BUFFERED;
      }
      else {
         return SKIP_BODY;
      }
   }

   public int doAfterBody() throws JspException {
      BodyContent body = getBodyContent();
      try {
         body.writeOut(getPreviousOut());
      } catch (IOException e) {
         throw new JspTagException("IterationTag: " + e.getMessage());
      }
      // clear up so the next time the body content is empty
      body.clearBody();
      if (iterator.hasNext()) {         
         pageContext.setAttribute(name, iterator.next());
         setRowAlternatorStatus();
         return EVAL_BODY_AGAIN;
      } else {
         return SKIP_BODY;
      }
   }

   /**
    * Sets alternate values of the row class for each rows.
    */
   private void setRowAlternatorStatus() {
     if (rowAlternator != null) {
       if (rowAlternatorStatus == true) {
         rowAlternatorStatus = false;
       } else {
         rowAlternatorStatus = true;
       }
       String status = rowAlternatorStatus ? ROW_ALT_1 : ROW_ALT_2; 
       pageContext.setAttribute(rowAlternator, status);
     }
   }
}