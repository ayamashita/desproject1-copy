package com.tec.server.log4j;

import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * Factory class for log4j Logger objects. Caches already used objects.
 *
 * @author Norunn Haug Christensen
 */ 
public class LoggerFactory  {
  private LoggerFactory() {} // no instantiation

  private static HashMap loggers = new HashMap();
  
  /**
   * Obtain a logger of specified class.
   */
  public static Logger getLogger(Class logClass) {
    Logger result = (Logger) loggers.get(logClass);
    if (result == null) {
      result = Logger.getLogger(logClass);
      loggers.put(logClass, result);
    }
    return result;
  }
}