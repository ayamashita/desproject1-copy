package com.tec.server.db;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tec.shared.util.Formatter;
import com.tec.shared.util.Nuller;

/**
 * Helper class with methods to set null values from ResultSet correctly.
 * Should be used for all obtaining of values from ResultSets.
 *
 * @author: Norunn Haug Christensen
 */
public class ResultSetHelper {

ResultSet rs;

/**
 * Creates a new resultsethelper for the given resultset.
 */
public ResultSetHelper(ResultSet rs) {
	super();
  this.rs = rs; 
}

/**
* Returns a double value from the selected resultset and column.
* Database nulls are set to the corresponding null value defined in the Nuller class. 
*/
public double getDouble(String columnName)
    throws SQLException {
    double d = rs.getDouble(columnName);
    if (rs.wasNull()) {
        d = Nuller.getDoubleNull();
    }    
    return d;
}

/**
* Returns a long value from the selected resultset and column.
* Database nulls are set to the corresponding null value defined in the Nuller class. 
*/
public long getLong(String columnName) throws SQLException {
    long l = rs.getLong(columnName);
    if (rs.wasNull()) {
        l = Nuller.getLongNull();
    }    
    return l;
}

/**
* Returns a int value from the selected resultset and column.
* Database nulls are set to the corresponding null value defined in the Nuller class. 
*/
public int getInt(String columnName) throws SQLException {
    int l = rs.getInt(columnName);
    if (rs.wasNull()) {
        l = Nuller.getIntNull(); 
    }    
    return l;
}

/**
* Returns a String value from the selected resultset and column.
* Database nulls are set to the corresponding null value defined in the Nuller class. 
*/
public String getString(String columnName) throws SQLException {
	String s = rs.getString(columnName);
	if (rs.wasNull()) {
		s = "";
	} 
	return s;
}

/**
* Returns a date as a formatted String from the selected resultset and column.
* The date format is defined in the Formatter class.
* Database nulls are returned as an empty String.
*/
public String getDateString(String columnName) throws SQLException {
	Date date = rs.getDate(columnName);
	if (rs.wasNull()) {
            date = null;
	} 
	return Formatter.formatDate(date); 
}

/**
* Returns a int value as a String from the selected resultset and column.
* Database nulls are returned as an empty String.
*/
public String getIntString(String columnName) throws SQLException {
    int l = rs.getInt(columnName);
    String s;
    if (rs.wasNull()) {
        s = Nuller.getStringNull(); 
    } else {
        s = String.valueOf(l);
    }
    
    return s;
}

/**
* Returns a date as a Date from the selected resultset and column.
*/
public Date getDate(String columnName)
	throws SQLException {
	Date date = rs.getDate(columnName);
	if (rs.wasNull()) {
            date = null;
	} 
	return date;
}

/**
* Returns a Object value from the selected resultset and column.
*/
public Object getObject(String columnName) throws SQLException {
	Object o = rs.getObject(columnName);
	if (rs.wasNull()) {
            o = null;
	} 
	return o;
}

/**
* Returns true if the value from the selected resultset and column is 1.
*/
public boolean getIntAsBoolean(String columnName) throws SQLException {
	int i = rs.getInt(columnName);
	if (i == 1) {
            return true;
	} else {
            return false;
        }
}

/**
 * Skips the first n rows of a ResultSet.
 * @return True if there are more rows in the ResultSet after the skipping, false otherwise.
 */
public static boolean skipRows(ResultSet rs, int n) throws SQLException {
	for (int i=0; i<n; i++) {
            if (!rs.next()) return false;
	}
	return true;
}
}