package com.tec.shared.exceptions;
/**
 * Parse exception is a user exception thrown when the user input cannot
 * be parsed to the expected type.
 *
 * @author :  Norunn Haug Christensen
 */
public class ParseException extends UserException {

public static final int LONG_TYPE = 2;
public static final int INT_TYPE = 2;
public static final int DOUBLE_TYPE = 3;
public static final int BOOLEAN_TYPE = 4;


  /**
   * Creates a ParseException with message specified by input parameter dateMask and value.
   */
  public ParseException(String dateMask, String value) {
    super("The format of the date field containing value '" + value + "' is incorrect. The format must be " + dateMask + ".");
  }

  /**
   * Creates a ParseException with message specified by input parameter type and value.
   */
  public ParseException(int type, String value) {
    super("The field containing value '" + value + "' must be of type " + getTypeString(type) + ".");
  }

  private static String getTypeString(int type){
    switch (type)  {
      case 2: {return "integer";}
      case 3: {return "decimal numeral";}  
      case 4: {return "boolean value";}  
      default: {return "uknown";}
    }
  }
}