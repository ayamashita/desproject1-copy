package com.tec.shared.util;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
/**
 * Class with methods for often performed formatting tasks.
 * Format masks exists as private constants in this class.
 *
 * @author :  Norunn Haug Christensen
 */
public class Formatter  {
  private static final String NR_MASK = "0000000000000000"; 
	private static final String DATE_MASK = "dd.MM.yyyy";
  private static final String TIMESTAMP_MASK = "dd.MM.yyyy HH.mm.ss,";
  private static final String TIME_MASK = "dd.MM.yyyy HH:mm";
  private static final String NO_DECI_MASK = "0";
	private static final String PRICE_MASK = "0.00";
  private static final String SINGLE_APOSTROPH = "'";
  private static final String DOUBLE_APOSTROPH = "''";
  

  private Formatter() {
  }

 /**
  * Formats a date according to the default date mask. 
  * If the given date is null, "" is returned.
  *
  * @param date The date to be formatted
  * @return The formatted String
  */
  public static String formatDate(Date date){
    if (date == null){
      return "";
    }  
    SimpleDateFormat formatter = new SimpleDateFormat(DATE_MASK);
    return formatter.format(date);
  }

 /**
  * Formats a date according to the specified format. 
  * If the given date is null, "" is returned.
  *
  * @param date The date to be formatted
  * @param date The date format
  * @return The formatted String
  */
  public static String formatDate(Date date, String format){
    if (date == null){
      return "";
    }  
    SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
    return formatter.format(date);
  }
  
  /**
  * Formats a timestamp according to the timestamp mask + 6 decimals. 
  * If the given timestamp is null, "" is returned.
  *
  * @param timestamp The timestamp to be formatted
  * @return The formatted String
  */
  public static String formatTimestamp(Timestamp timestamp){
    if (timestamp == null){
      return "";
    }  
    SimpleDateFormat formatter = new SimpleDateFormat(TIMESTAMP_MASK);
    String ts =  formatter.format(timestamp);
    int nanos = timestamp.getNanos();
    String decimals = Formatter.formatNumberWithLeadingNulls(nanos/1000, 6);
    ts += decimals;
    return ts;
  }
  
  /**
  * Formats a timestamp according to the timestamp mask + 6 decimals. 
  * If the given timestamp is null, "" is returned.
  *
  * @param timestamp The timestamp to be formatted
  * @return The formatted String
  */
  public static String formatTime(Timestamp timestamp){
    if (timestamp == null){
      return "";
    }  
    SimpleDateFormat formatter = new SimpleDateFormat(TIME_MASK);
    String ts =  formatter.format(timestamp);
    return ts;
  }

 /**
  * Formats a price according to the price mask. 
  * If the given double is null, "" is returned.
  *
  * @param price The double to be formatted
  * @return The formatted String
  */
  public static String formatPrice(double price){
    if (Nuller.isNull(price)){
      return "";
    }
    DecimalFormat df = new DecimalFormat(PRICE_MASK);
    return df.format(price);
  }
 /**
  * Formats a doubble according to the no_deci_mask. 
  * If the given double is null, "" is returned.
  *
  * @param price The double to be formatted
  * @return The formatted String
  */
  public static String formatDoubleNoDeci(double d){
    if (Nuller.isNull(d)){
      return "";
    }
    DecimalFormat df = new DecimalFormat(NO_DECI_MASK);
    return df.format(d);
  }

 /**
  * Formats a number with leading nulls
  * If the given int is null or the specified length is too long, "" is returned.
  *
  * @param number The number to be formatted
  * @param length The length of the returned String
  * @return The formatted String
  */
  public static String formatNumberWithLeadingNulls(long number, int length){
    try {
      if (Nuller.isNull(number)||(length > NR_MASK.length())){
        return "";
      } else {  
        DecimalFormat df = new DecimalFormat(NR_MASK.substring(0,length));
        return df.format(number);  
      } 
    } catch (NumberFormatException ex){
      return "";
    }
  }  

 /**
  * Formats a search text by eliminating String elements that could harm the database.
  * All apostrophs or groups of apostrophs are replaced with 2 apostrophs: ''
  */
  public static String formatSearchText(String theString){
    if (theString.indexOf(SINGLE_APOSTROPH) != -1) {
      StringTokenizer st = new StringTokenizer(theString, SINGLE_APOSTROPH);
      StringBuffer sb = new StringBuffer();
      if (theString.startsWith(SINGLE_APOSTROPH)){
        sb.append(DOUBLE_APOSTROPH);
      }
      while (st.hasMoreTokens()) {
        sb.append(st.nextToken());
        sb.append(DOUBLE_APOSTROPH);
      }
      if (!theString.endsWith(SINGLE_APOSTROPH)){
        sb.delete(sb.length()-2, sb.length());
      }
      theString = sb.toString();
    }
    return theString;
  }
  
 /**
  * Removes all instances of the specified character from the input string.
  * @return String where the specified character have been removed.
  */ 
  public static String removeCharFromString(String inputString, String charToRemove) {
      if (inputString == null) {
          return null;
      } else {
        StringBuffer s = new StringBuffer(inputString);
         while (s.indexOf(charToRemove) != -1)
              s.deleteCharAt(s.indexOf(charToRemove));
      
        return s.toString();
      }
  }
}  
