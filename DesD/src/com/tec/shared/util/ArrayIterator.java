package com.tec.shared.util;
/**
 * Iterator class for arrays
 *
 * @author : Norunn Haug Christensen
 */
public class ArrayIterator {
  private Object[] array;
  private int length;
  private int counter;
  
 /**
  * Constructs an ArrayIterator
  */ 
  public ArrayIterator(Object[] array) {
    this.array = array;
    length = (array == null)?0:array.length;
    counter = 0;
  }

 /**
  * Returns true if the array has another element
  */
  public boolean hasNext() {
    return (counter < length);
  }

 /**
  * Returns the next element in the array
  */
  public Object next() {
    return array[counter++];
  }
}