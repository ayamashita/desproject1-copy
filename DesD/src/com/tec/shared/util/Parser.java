package com.tec.shared.util;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tec.shared.exceptions.ParseException;
import com.tec.shared.exceptions.UserException;

/**
 * Class with static methods for parsing.
 *
 * @author :  Norunn Haug Christensen
 */
public class Parser  {

	private static final String DATE_MASK_ERROR_MESSAGE = "yyyy/mm/dd";
        private static final String TRUE = "true";
        private static final String FALSE = "false";
   /**
    * Parser can not be instantiated.  
    */
  private Parser() {
    super();
  }

/**
 * Returns the double as a String.
 */
public static String toString(double value) {
	if (Nuller.isNull(value)){
		return Nuller.getStringNull();
	} else { 
		return String.valueOf(value);
	}	
}
/**
 * Returns the long as a String.
 */
public static String toString(long value) {
	if (Nuller.isNull(value)){
		return Nuller.getStringNull();
	} else { 
		return String.valueOf(value);
	}	
}

 /**
  * Parses a String to a boolean, and returns the boolean. 
  *
  * @throws UserException If the String cannot be parsed to a boolean.  
  */
public static boolean parseBoolean(String value) throws UserException {
  if ((value!=null)&&(TRUE).equalsIgnoreCase(value)){
    return true;
  } else if ((value!=null)&&(FALSE).equalsIgnoreCase(value)) {
    return false;
  } else {
    throw new ParseException(ParseException.BOOLEAN_TYPE, value);
  }
}

 /**
  * Parses a String to a long, and returns the long. 
  *
  * @throws UserException If the String cannot be parsed to an int.  
  */
public static long parseLong(String value) throws UserException {
  return parseLong(value, false);
}

 /**
  * Parses a String to a long, and returns the long. 
  *
  * @param notNull If true, an exception is thrown if the field is null.
  * @throws UserException If the String cannot be parsed to a long.  
  */
public static long parseLong(String value, boolean notNull) throws UserException {
  if ((value==null)||(Nuller.isNull(value))){
    if (notNull) {
      throw new ParseException(ParseException.LONG_TYPE, "null");
    } else {
      return Nuller.getLongNull();
    }
  } else {
    try {
      return Long.parseLong(value);
    } catch (NumberFormatException ex){
      throw new ParseException(ParseException.LONG_TYPE, value);
    }    
  }
}

 /**
  * Parses a String to a int, and returns the int. 
  *
  * @throws UserException If the String cannot be parsed to a int.  
  */
public static int parseInt(String value) throws UserException {
  if ((value == null)||(Nuller.isNull(value))){
      return Nuller.getIntNull();
  } else {    
      try {
        return Integer.parseInt(value);
      } catch (NumberFormatException ex){
        throw new ParseException(ParseException.INT_TYPE, value);
      }
  }
}

 /**
  * Parses a String to a Date using the indicated date mask, and returns the Date. 
  *
  * @throws UserException If the String cannot be parsed to a Date.  
  */
public static Date parseDate(String value, String dateMask) throws UserException {
  try{ 
    if ((value==null)||(Nuller.isNull(value))){
      return null;
    } else {  
      SimpleDateFormat formatter = new SimpleDateFormat(dateMask);  
      formatter.setLenient(false);
      return formatter.parse(value);
    }
  } catch (java.text.ParseException ex){
    throw new ParseException(DATE_MASK_ERROR_MESSAGE, value);
  }  
}

}