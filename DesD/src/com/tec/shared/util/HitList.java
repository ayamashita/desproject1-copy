package com.tec.shared.util;
import java.util.Vector;

/**
 * List class with additional metod getCount() for returning the total number
 * of search hits. 
 *
 * @author: Norunn Haug Christensen
 */
public class HitList extends Vector  {
  private int count = Nuller.getIntNull();
  
  public HitList() {
    super();
  }
  
  public HitList(int initialCapacity) {
    super(initialCapacity);
  }
  
  public HitList(int initialCapacity, int capacityIncrement) {
      super(initialCapacity, capacityIncrement);
  }

  /**
   * Returns the number of search hits.
   */
  public int getCount() {
    return count;
  }

  /**
   * Sets the number og search hits.
   */
  public void setCount(int newCount) {
    count = newCount;
  }

}