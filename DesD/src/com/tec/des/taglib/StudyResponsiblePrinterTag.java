package com.tec.des.taglib;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.util.Validator;

/**
 * JSP tag library for printing id of StudyResponsibleDTO in a vector separated 
 * with the input parameter separator. If no separator is given, det default ',' is used.
 * If printName is true, the name is printed instred of the id.
 *
 * @author Norunn Haug Christensen 
 */
public class StudyResponsiblePrinterTag extends TagSupport {
  
  private String separator = ",";
  private Vector items;
  private boolean printName;

/**
 * Method called when the ArrayPrinterTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */  
  public int doStartTag () throws JspException {
    try {

      if (items != null) {
          StringBuffer htmlOutput = new StringBuffer();
          Iterator it = items.iterator();
          String item = "";
          StudyResponsibleDTO responsible;

          while (it.hasNext()) {
            responsible = (StudyResponsibleDTO)it.next();
            if (printName) {
                item = Validator.replaceChar(responsible.getSingleName());
            } else {
                item = responsible.getId();
            }
                   
            htmlOutput.append(item);
            
            if (it.hasNext()) {
              htmlOutput.append(separator);
            }
          }

          pageContext.getOut().print(htmlOutput.toString());          
      }
      
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setSeparator(String separator) {
      this.separator = separator; 
  }

  public void setItems(Vector items) {
    this.items = items;
  }

  public void setPrintName(boolean printName) {
    this.printName = printName;
  }
  
}