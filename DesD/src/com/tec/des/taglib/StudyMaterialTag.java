package com.tec.des.taglib;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.dto.StudyMaterialDTO;
import com.tec.des.http.WebKeys;

/**
 * JSP tag library displaying study material.
 * The tag loops through the vector containing study material. If printerFriendly
 * is true, only the description of each study material is displayed. Otherwise
 * a link is displayed for each study material, the link refers either to a 
 * webpage or a file.
 *
 * @author Norunn Haug Christensen 
 */
public class StudyMaterialTag extends TagSupport {

  private Vector items;
  private String separator = "<BR>";
  private boolean fromTemp = false;
  private boolean printerFriendly;

/**
 * Method called when the StudyMaterialTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */
  public int doStartTag() throws JspException {
    try {

        if (items != null) {
          pageContext.getOut().print(generateOutput());
          
        }
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

public String generateOutput() {
	StringBuffer htmlOutput = new StringBuffer();
	Iterator it = items.iterator();
	StudyMaterialDTO material;
	while (it.hasNext()) {
	    material = (StudyMaterialDTO)it.next();
	    if (printerFriendly) {
	        htmlOutput.append(material.getDescription());
	    } else {
	        if (material.isFile()) {
	            htmlOutput.append("<A href=\"downloadstudymaterial.jsp?" + WebKeys.REQUEST_PARAM_STUDY_MATERIAL_ID + "=" + material.getId() + 
	                "&" + WebKeys.REQUEST_PARAM_FROM_TEMP + "=" + fromTemp +"\">");
	        } else {
	            htmlOutput.append("<A href=\"" + material.getUrl() + "\" target='_blank'>");
	        }
	        
	        htmlOutput.append(material.getDescription());
	        htmlOutput.append("</A>");
	    }
	    
	    if (it.hasNext())
	        htmlOutput.append(separator);
	}
	return htmlOutput.toString();
}

  public void setItems(Vector items) { 
    this.items = items;
  }
  
  public void setSeparator(String separator) { 
    this.separator = separator;
  }  
  
  public void setFromTemp(boolean fromTemp) { 
    this.fromTemp = fromTemp;
  }

  public void setPrinterFriendly(boolean printerFriendly) { 
    this.printerFriendly = printerFriendly;
  }
 
  
}