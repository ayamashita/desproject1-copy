package com.tec.des.taglib;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.dto.UserDTO;
import com.tec.shared.util.Nuller;

/**
 * JSP tag library displaying a multi drop down list.
 *
 * @author Per Kristian Foss
 */
public class MultiDropDownListTag extends TagSupport {
  private String name = "select1";
  private Vector peopleRole;
  private Hashtable roles;
  private String attributes = "";
  private String optional = "";
  private String TRUE = "true";

/**
 * Method called when the MultiDropDownListTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */
  public int doStartTag() throws JspException {
    try {
        StringBuffer htmlOutput = new StringBuffer();
        
        int noOfRoles = roles.size();
        String[] roleId = (String[])roles.keySet().toArray(new String[noOfRoles+1]);
        String[] roleName = (String[])roles.values().toArray(new String[noOfRoles+1]);        
        roleId[noOfRoles] = "0";
        roleName[noOfRoles] = "None";   
        Iterator it = peopleRole.iterator();
        
        while (it.hasNext()) {
            UserDTO user = (UserDTO)it.next();                    
            htmlOutput.append("<TR><TD class=\"bodytext-bold\" valign=\"top\">" + user.getListName() + "</TD>");
            htmlOutput.append("<TD class=\"bodytext-bold\">");
            htmlOutput.append("<select name=\"" + name + user.getId() + "\" " + attributes + " >");        
            
            String value;
            boolean selected = false;
            String userRole;
            if (user.getRoleId() == Nuller.getIntNull())
                userRole = "0";
            else userRole = String.valueOf(user.getRoleId());
        
            for (int i=0; i<noOfRoles+1; i++) {
                value = roleId[i];            
                selected = value.equals(userRole);
                htmlOutput.append("<option value =\"" + value + "\" " 
                + ((selected)?"selected = \"selected\"":"")  + ">" + roleName[i] +"</option>");
            }        
            htmlOutput.append("</select>");                 
            htmlOutput.append("</TD></TR>");
        }
        pageContext.getOut().print(htmlOutput.toString());                        
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setName(String name) {
      this.name = name; 
  }

  public void setPeopleRole(Vector peopleRole) { 
    this.peopleRole = peopleRole;
  }

  public void setRoles(Hashtable roles) { 
    this.roles = roles;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }

  public void setOptional(String optional) {
    this.optional = optional;
  }
  
}