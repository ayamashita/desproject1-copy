package com.tec.des.taglib;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * JSP tag library displaying one or more links. The displayed link is a 
 * concationation of the input parameter url and a key of the input parameter
 * items. The value of the items hastable is used as the name of the link.
 * The tag is used to display links to persons and publications in simulaweb.
 *
 * @author Norunn Haug Christensen 
 */
public class LinkTag extends TagSupport {
  private String url;
  private Hashtable items;
  private String separator = "<BR>";

/**
 * Method called when the LinkTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */
  public int doStartTag() throws JspException {
    try {

        if (items != null) {
            StringBuffer htmlOutput = new StringBuffer();
            Iterator it = items.keySet().iterator();
            while (it.hasNext()) {
                String id = (String)it.next();
                htmlOutput.append("<A href=\""+ url + id +"\" target='_blank'>");
                htmlOutput.append(items.get(id));
                htmlOutput.append("</A>");
                
                if (it.hasNext())
                    htmlOutput.append(separator);
            }
          pageContext.getOut().print(htmlOutput.toString());
          
        }
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setUrl(String url) {
      this.url = url; 
  }

  public void setItems(Hashtable items) { 
    this.items = items;
  }
  
  public void setSeparator(String separator) { 
    this.separator = separator;
  }  
  
}