package com.tec.des.taglib;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.dto.StudyResponsibleDTO;

/**
 * Multi select JSP tag library, used to select study responsibles.
 *
 * @author Norunn Haug Christensen
 */
public class StudyResponsibleSelectTag extends TagSupport 
{
  private String name = "multiselect1";
  private Vector items;
  private String attributes = "";
  private String boxsize = "";
  
  public int doStartTag () throws JspException {
    try {

      if (items != null) {
          StringBuffer htmlOutput = new StringBuffer();
          htmlOutput.append("<select multiple name=\"" + name + "\" " + attributes + " size=\"" + boxsize + "\" >");
          String displayvalue;
          String optionvalue;
          Iterator it = items.iterator();
          StudyResponsibleDTO responsible;
          while (it.hasNext()) {
            responsible = (StudyResponsibleDTO)it.next();
            displayvalue = responsible.getListName();
            optionvalue = responsible.getId();
            htmlOutput.append("<option value =\"" + optionvalue + "\">" + displayvalue + "</option>");
          }
          htmlOutput.append("</select>");
          pageContext.getOut().print(htmlOutput.toString());
          
      }
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

  public void setName(String name) {
      this.name = name; 
  }

  public void setItems(Vector items) {
    this.items = items;
  }


  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }
  
  public void setBoxsize(String boxsize) {
    this.boxsize = boxsize;
  }
}