package com.tec.des.taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

/**
 * This class is a tag creating a navigation line based on the length of the
 * hitlist and the total number of hits. The navigation line include a label
 * telling which hits is shown and '<<''1' '2' '3' ... and '>>' links. 
 * If the number of links are more than det default max value, then only the 
 * default number of links are shown. First when the last link is viewed and the 
 * '>>' link is activated, the next block of links are shown. 
 *
 * @author Norunn Haug Christensen
 */
public class NavigationTag extends TagSupport {

    private int totalHitListCount = Nuller.getIntNull();
    private int hitListLength = Nuller.getIntNull();
    private int currentStartPosition = Nuller.getIntNull();
    
    
/**
 * Method called when the Navigation Tag is called.
 *
 * @throws JspException When an exception occurs.
 */
  public int doEndTag() throws JspException {
    try {

      if (!Nuller.isNull(totalHitListCount) && !Nuller.isNull(hitListLength) && !Nuller.isNull(currentStartPosition)) {

        //Decide the number of navigation links
        if (totalHitListCount > hitListLength) {
          int numberOfLinks = totalHitListCount/hitListLength;
          if (totalHitListCount % hitListLength != 0)
            numberOfLinks = numberOfLinks + 1;

        //Create output of tag
        StringBuffer outputString = new StringBuffer();
        String toField;  
        if ((totalHitListCount > hitListLength) && (totalHitListCount > currentStartPosition + hitListLength)) {
            toField = String.valueOf(currentStartPosition + hitListLength);
          } else {
            toField = String.valueOf(totalHitListCount);          
          }
        outputString.append("Hits " + String.valueOf(currentStartPosition + 1) + " - " + toField + " of " + totalHitListCount + ". &nbsp;&nbsp;");

        //The 'Previous' navigation link
        if ((currentStartPosition - hitListLength) < 0) {
          outputString.append(" << ");
        } else {
          int startPosition = currentStartPosition - hitListLength;
          outputString.append("<A href=\"javascript:document.form.submit();\" " +
            "onclick=\"document.form.target='_self';" +
            "document.form." + WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY + ".value='false';" +
            "document.form." + WebKeys.REQUEST_PARAM_START_POSITION + ".value='"+ startPosition +"';" +
            "document.form." + WebKeys.REQUEST_PARAM_USECASE + ".value='" + WebConstants.USECASE_LIST_STUDIES +"';\"><<</A> \n");
        }

        //The number navigation links
        if (numberOfLinks > WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN) {
          //More than the default number of links to show
          int numberOfBlocksOfLinks = numberOfLinks/WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN;
          if (numberOfLinks % WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN != 0)
              numberOfBlocksOfLinks = numberOfBlocksOfLinks + 1;

          //Find which block of links to show
          int startLink = 0;
          for (int i = 1; i < numberOfBlocksOfLinks + 1; i++) {
            int startPositionOfBlock = hitListLength*WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN*(i-1);
            int endPositionOfBlock = startPositionOfBlock + (hitListLength*WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN) - 1;
            if (endPositionOfBlock > totalHitListCount)
              endPositionOfBlock = totalHitListCount;
            if (currentStartPosition >= startPositionOfBlock && currentStartPosition <= endPositionOfBlock) {
              startLink = WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN*(i - 1) + 1;
              break;
            }
          }

          for (int i = startLink; i < startLink + WebConstants.DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN && i < numberOfLinks +1; i++) {
            int startPosition = hitListLength*(i-1);
            if (currentStartPosition == startPosition) {
              outputString.append(" "+ i +" ");
            } else {
              outputString.append(" <A href=\"javascript:document.form.submit();\" " +
                "onclick=\"document.form.target='_self';" +
                "document.form." + WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY + ".value='false';" +
                "document.form." + WebKeys.REQUEST_PARAM_START_POSITION + ".value='" + startPosition + "';" +
                "document.form." + WebKeys.REQUEST_PARAM_USECASE + ".value='" + WebConstants.USECASE_LIST_STUDIES + "';\">"+ i +"</A> \n");
            }
          }
        } else {
          //Less than the default number of links to show
          for (int i = 1; i < numberOfLinks + 1; i++) {
            int startPosition = hitListLength*(i-1);
            if (currentStartPosition == startPosition) {
              outputString.append(" "+ i +" ");
            } else {
              outputString.append(" <A href=\"javascript:document.form.submit();\" " +
                "onclick=\"document.form.target='_self';" +
                "document.form." + WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY + ".value='false';" +
                "document.form." + WebKeys.REQUEST_PARAM_START_POSITION + ".value='" + startPosition + "';" +
                "document.form." + WebKeys.REQUEST_PARAM_USECASE + ".value='" + WebConstants.USECASE_LIST_STUDIES + "';\">"+ i +"</A> \n");
            }
          }
        }

        //The 'Next' navigation link
        if ((currentStartPosition + hitListLength) >= totalHitListCount) {
          outputString.append(" >> ");
        } else {
          int startPosition = currentStartPosition + hitListLength;
          outputString.append(" <A href=\"javascript:document.form.submit();\" " +
            "onclick=\"document.form.target='_self';" +
            "document.form." + WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY + ".value='false';" +
            "document.form." + WebKeys.REQUEST_PARAM_START_POSITION + ".value='"+ startPosition +"';" +
            "document.form." + WebKeys.REQUEST_PARAM_USECASE + ".value='" + WebConstants.USECASE_LIST_STUDIES +"';\">>></A> \n");
        }

        pageContext.getOut().print(outputString);

      }
    }
  } catch (Exception e) {
      throw new JspException(e.getMessage());
  }
    return SKIP_BODY;
}

  public void setTotalHitListCount(int totalHitListCount) {
      this.totalHitListCount = totalHitListCount;
  }

  public void setHitListLength(String hitListLength) {
      if (Nuller.isReallyNull(hitListLength)) {
        this.hitListLength = WebConstants.MAX_HIT_LIST_LENGTH;
      } else {
        this.hitListLength = new Integer(hitListLength).intValue();
      }
  }

  public void setStartPosition(String startPosition) {
      if (Nuller.isReallyNull(startPosition)) {
        this.currentStartPosition = WebConstants.DEFAULT_START_POSITION;
      } else {
        this.currentStartPosition = new Integer(startPosition).intValue();
      }
  }
  
}
