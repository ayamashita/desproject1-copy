package com.tec.des.taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.dto.SearchDTO;
import com.tec.des.http.WebKeys;

/**
 * This class is a tag creating hidden input parameters for search criterias.
 * They are needed to 'remember' the search if a hitlist has more counts than 
 * the hitlist length and the user uses the navigation links to show another 
 * part of the hitlist, and for viewing the hitlist in a printerfriendly version.
 *
 * @author Norunn Haug Christensen
 */
public class SearchCriteriaTag extends TagSupport {
    
    private SearchDTO searchCriteria;
    
/**
 * Method called when the SearchCriteria tag is called.
 *
 * @throws JspException When an exception occurs.
 */
  public int doEndTag() throws JspException {
  try {

    if (searchCriteria != null) {
        StringBuffer outputString = new StringBuffer();
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_TEXT +"\" value=\""+ searchCriteria.getFreeText() +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY +"\" value=\""+ searchCriteria.getTypeOfStudy() +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM +"\" value=\""+ searchCriteria.getEndOfStudyFrom() +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO +"\" value=\""+ searchCriteria.getEndOfStudyTo() +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES +"\" value=\""+ searchCriteria.getStudyResponsibles() +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_SORT_BY +"\" value=\""+ searchCriteria.getSortBy() +"\"> \n");
        outputString.append("<INPUT type=\"hidden\" name=\""+ WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING +"\" value=\""+ searchCriteria.getSortOrder() +"\">");

        pageContext.getOut().println(outputString);
      } 
    
  } catch (Exception e) {
      throw new JspException(e.getMessage());
  }
    return SKIP_BODY;
  }

  public void setSearchCriteria(SearchDTO searchCriteria) {
      this.searchCriteria = searchCriteria;
  }
  
}
