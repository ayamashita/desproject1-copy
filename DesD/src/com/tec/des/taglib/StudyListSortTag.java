package com.tec.des.taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;

/**
 * JSP tag library send sort parameters (column, sort direction) to application
 *
 * @author Petr Novak 
 */
public class StudyListSortTag extends TagSupport {
  private String columnName;

/**
 * Method called when the LinkTag tag is invoked.
 *
 * @throws JspException If an exception occurs.
 */
  public int doStartTag() throws JspException {
    try {
    	StringBuffer htmlOutputPart1 = new StringBuffer();
    	htmlOutputPart1
    	.append("<img src=\"images/icon_sort_");
    	
    	StringBuffer htmlOutputPart2 = new StringBuffer();
    	htmlOutputPart2
    	.append(".gif\" title=\"Click to sort\" onclick=\"")
    	.append("document.form.target='_self';document.form.action='").append(WebConstants.FRONTCONTROLLER_URL).append("';")
    	.append("document.form.").append(WebKeys.REQUEST_PARAM_USECASE).append(".value='").append(WebConstants.USECASE_LIST_STUDIES).append("';")
    	.append("document.form.").append(WebKeys.REQUEST_PARAM_START_POSITION).append(".value='").append(WebConstants.DEFAULT_START_POSITION).append("';")
    	.append("document.form.").append(WebKeys.REQUEST_PARAM_SEARCH_SORT_BY).append(".value='").append(getColumnName()).append("';")
    	.append("document.form.").append(WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING).append(".value='");
    	
    	String htmlOutputPart3 = "';document.form.submit();\">";
    	
        pageContext.getOut().println(htmlOutputPart1+"asc"+htmlOutputPart2+WebConstants.SORT_ORDER_ASCENDING+htmlOutputPart3);
        pageContext.getOut().println(htmlOutputPart1+"desc"+htmlOutputPart2+WebConstants.SORT_ORDER_DESCENDING+htmlOutputPart3);
      
    } catch (Exception ex) {
      throw new JspTagException(ex.getMessage());
    }

    return SKIP_BODY;
  }

public String getColumnName() {
	return columnName;
}

public void setColumnName(String columnName) {
	this.columnName = columnName;
}  
  
}