package com.tec.des.dto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;

import com.tec.des.util.Constants;
import com.tec.shared.util.Nuller;

/**
 * Data Transfer Object for a user.
 *
 * @author : Norunn Haug Christensen
 */
public class UserDTO extends StudyResponsibleDTO implements DTO{

  private int roleId = Nuller.getIntNull();
  private String roleName = Nuller.getStringNull();
  private Hashtable studyReports;
  public UserDTO() {
    super();    
  }
  
  public void setRoleId(int roleId) {
    this.roleId = roleId;
  }

  public int getRoleId() {
    return roleId;
  }
  
  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleName() {
    return roleName;
  }
  
  public boolean hasRole() {
    return !Nuller.isNull(roleId);
  }

  public boolean isDbAdmin() {
    return roleId == Constants.ROLE_DATABASE_ADMIN;
  }
  
	public Hashtable getStudyReports() {
		if (studyReports == null) {
			studyReports = new Hashtable();
		}
		return studyReports;
	}

	public ArrayList getStudyReportsSortedlist() {
		if (getStudyReports() == null) {
			return null;
		}
		ArrayList list = new ArrayList();
		
		for (Iterator iterator = getStudyReports().entrySet().iterator(); iterator.hasNext();) {
			Entry entry = (Entry) iterator.next();
			list.add(entry.getValue());
		}
		
		Comparator studyReportComparator = new Comparator() {
	        public int compare(Object o1, Object o2) {
	            return ((UserStudyReportDTO) o1).getName().toLowerCase().compareTo(((UserStudyReportDTO) o2).getName().toLowerCase());
	        }
	    };
		
	    Collections.sort(list, studyReportComparator);
		
		return list;
	}
	
	public void setStudyReports(Hashtable userReports) {
		this.studyReports = userReports;
	}
}