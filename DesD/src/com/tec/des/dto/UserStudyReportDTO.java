package com.tec.des.dto;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enum.Enum;

import com.tec.des.http.WebConstants;
import com.tec.des.util.Constants;
import com.tec.shared.util.Nuller;

public class UserStudyReportDTO {
	
	int id = -1;
	String userId;
	String name = Nuller.getStringNull();
	SortOperator sortEndDate = null;
	WhereOperator whereResponsibleOperator = WhereOperator.AND;
	String columnSelected = Nuller.getStringNull();
	ArrayList studyColumns;
	Hashtable responsibles;
	
    public final static class WhereOperator extends Enum {

    	public static final WhereOperator AND = new WhereOperator("AND");
    	public static final WhereOperator OR = new WhereOperator("OR");

    	protected WhereOperator(String name) {
			super(name);
		}
    	
    	public static List getEnumList() {
    	  return getEnumList(WhereOperator.class);
    	}

    	public static ArrayList getList() {
    		
      	  ArrayList list = new ArrayList();
      	  for (Iterator iterator = getEnumList(WhereOperator.class).iterator(); iterator.hasNext();) {
      		WhereOperator oper = (WhereOperator) iterator.next();
      		  list.add(oper.getName());
      	  }
      	  return list;
      	}    	
    	/*
    	 * @see java.lang.Object#toString()
    	 */
    	public String toString() {
    		return getName();
    	}
    	
    	/**
    	 * @param operator
    	 * @return
    	 */
    	public static WhereOperator valueOf(String operator) {
    		if (StringUtils.isEmpty(operator)) {
    			return null;
    		}
    		
    	  return (WhereOperator) getEnum(WhereOperator.class, operator);
    	}
    }

    public final static class SortOperator extends Enum {

    	public static final SortOperator ASCENDANT = new SortOperator(WebConstants.SORT_ORDER_ASCENDING);
    	public static final SortOperator DESCENDANT = new SortOperator(WebConstants.SORT_ORDER_DESCENDING);

    	protected SortOperator(String name) {
			super(name);
		}
    	
    	public static ArrayList getList() {
    		
    	  ArrayList list = new ArrayList();
    	  for (Iterator iterator = getEnumList(SortOperator.class).iterator(); iterator.hasNext();) {
    		  SortOperator oper = (SortOperator) iterator.next();
    		  list.add(oper.getName());
    	  }
    	  return list;
    	}
    	
    	/*
    	 * @see java.lang.Object#toString()
    	 */
    	public String toString() {
    		return getName();
    	}
    	
    	/**
    	 * @param operator
    	 * @return
    	 */
    	public static SortOperator valueOf(String operator) {
    		if (StringUtils.isEmpty(operator)) {
    			return null;
    		}
    		
    	  return (SortOperator) getEnum(SortOperator.class, operator);
    	}
    }
	
	public UserStudyReportDTO(String userId) {
		super();
		this.userId = userId;
	}

	public UserStudyReportDTO() {
		super();
	}
	
	public int getId() {
		return id;
	}
	/**
	 * @return id (Integer) for using as HashMap key
	 */
	public Integer getIdObj() {
		return Integer.valueOf(id);
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSortEndDate() {
		if (sortEndDate == null) {
			return "";
		}
		
		return sortEndDate.toString();
	}

	public void setSortEndDate(String sortEndDate) {
		this.sortEndDate = SortOperator.valueOf(sortEndDate);
	}

	public String getWhereResponsibleOperator() {
		if (whereResponsibleOperator == null) {
			whereResponsibleOperator = WhereOperator.AND;
		}
		return whereResponsibleOperator.toString();
	}

	public void setWhereResponsibleOperator(String whereResponsibleOperator) {
		this.whereResponsibleOperator = WhereOperator.valueOf(whereResponsibleOperator);
	}


	public Hashtable getResponsibles() {
		if (responsibles == null) {
			responsibles = new Hashtable();
		}

		return responsibles;
	}
	
	public void setResponsibles(Hashtable responsibles) {
		this.responsibles = responsibles;
	}

	public ArrayList getStudyColumns() {
		if (studyColumns == null) {
			studyColumns = new ArrayList();
		}
		return studyColumns;
	}

	public void setStudyColumns(ArrayList studyColumns) {
		this.studyColumns = studyColumns;
	}

	public UserStudyReportDTO getReportCopy() {
		UserStudyReportDTO copyReport = new UserStudyReportDTO();
		
		copyReport.setId(this.getId());
		copyReport.setUserId(this.getUserId());
		copyReport.setName(this.getName());
		copyReport.setSortEndDate(this.getSortEndDate());
		copyReport.setWhereResponsibleOperator(this.getWhereResponsibleOperator());
		
		copyReport.getResponsibles().putAll(this.getResponsibles());
		copyReport.getStudyColumns().addAll(this.getStudyColumns());
		
		return copyReport;
	}

	public void setResponsibles(String[] ids, String[] values) {
        responsibles = new Hashtable();
        for (int i = 0; i < ids.length; i++) { 
            if (!Constants.EMPTY_ID_STUDY_RESPONSIBLES.equals(ids[i])) {
                responsibles.put(ids[i], values[i]);
            }
        }
		
	}

	public String getColumnSelected() {
		return columnSelected;
	}

	public void setColumnSelected(String columnSelected) {
		this.columnSelected = columnSelected;
	}

	
	
	
}
