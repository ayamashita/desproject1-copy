package com.tec.des.dto;
import java.util.ArrayList;

import com.tec.des.dao.StudyDAO;
import com.tec.shared.util.Nuller;

/**
 * Data transfer object for a search.
 * This object encapsulates all search criterias.
 *
 * @author  Norunn Haug Christensen
 */
public class SearchDTO implements DTO {

    private String typeOfStudy = Nuller.getStringNull();   
    private String freeText = Nuller.getStringNull();
    private String endOfStudyFrom = Nuller.getStringNull();
    private String endOfStudyTo = Nuller.getStringNull();
    private String studyResponsibles = Nuller.getStringNull();
    private String sortBy = Nuller.getStringNull();
    private String sortOrder = Nuller.getStringNull();
    private UserStudyReportDTO report = null;
	private String sortByDefault = Nuller.getStringNull();

    public SearchDTO() {
        super();
    }
    
    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }
    
    public String getTypeOfStudy() {
        return typeOfStudy;
    }
    
    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public String getFreeText() {
        return freeText;
    }
    
    public void setEndOfStudyFrom(String endOfStudyFrom) {
        this.endOfStudyFrom = endOfStudyFrom;
    }

    public String getEndOfStudyFrom() {
        return endOfStudyFrom;
    }


    public void setEndOfStudyTo(String endOfStudyTo) {
        this.endOfStudyTo = endOfStudyTo;
    }

    public String getEndOfStudyTo() {
        return endOfStudyTo;
    }
    
    public void setStudyResponsibles(String studyResponsibles) {
        this.studyResponsibles = studyResponsibles;
    }
    
    public String getStudyResponsibles() {
        return studyResponsibles;
    }
    
    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    public String getSortOrder() { 
        return sortOrder;
    } 

    public String getResponsiblesOperator() {
    	if (report == null) {
    		return Nuller.getStringNull();
    	}
    	return report.getWhereResponsibleOperator();
    }

    public ArrayList getColumns() {
    	if (report == null) {
			return StudyDAO.ReportColumns.DEFAULT_COLUMNS;
    	}
    	return report.getStudyColumns();
    }

	public String getUserReportId() {
		if (report == null) {
			return Nuller.getStringNull();
		}
		return report.getId() + "";
	}

	public ArrayList getStudyResponsibleIds() {
		if (report == null) {
			return new ArrayList();
		}
		return new ArrayList(report.getResponsibles().keySet());
	}

	public String getSortByDefault() {
		return sortByDefault;
	}

	public void setSortByDefault(String sortByDefault) {
		this.sortByDefault  = sortByDefault;
	}

	public UserStudyReportDTO getReport() {
		return report;
	}

	public void setReport(UserStudyReportDTO report) {
		this.report = report;
	}
}
