package com.tec.des.dto;
import com.tec.shared.util.Nuller;

/**
 * Data Transfer Object for a publication.
 *
 * @author : Per Kristian Foss
 */
public class PublicationDTO implements DTO {

  private String id = Nuller.getStringNull();
  private String title = Nuller.getStringNull();
  private String source = Nuller.getStringNull();
  private String url = Nuller.getStringNull();
  
  /**
   * Creates a new publication with title, source and Id.
   */
  public PublicationDTO() {
    super();
  }
  
  public void setId(String id) {
    this.id = id;
  }
    
  public String getId() {
    return id;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }
  
  public void setSource(String source) {
    this.source = source;
  }

  public String getSource() {
    return Nuller.noNull(source);
  }

  public String getUrl() {
	return url;
  }

  public void setUrl(String url) {
	this.url = url;
  }
}