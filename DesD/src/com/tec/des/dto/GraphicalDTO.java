package com.tec.des.dto;
import java.util.Vector;

import com.tec.shared.util.Nuller;

/**
 * Data transfer object used in a graphical presentation.
 *
 * @author  Norunn Haug Christensen
 */
public class GraphicalDTO implements DTO {

    private String year = Nuller.getStringNull();
    private Vector studyTypes;
    private boolean noStudiesThisYear;

    public GraphicalDTO() {
        super();
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    public String getYear() {
        return year;
    }

    public void setStudyTypes(Vector studyTypes) {
        this.studyTypes = studyTypes;
    }
    
    public Vector getStudyTypes() {
        if (studyTypes == null)
            studyTypes = new Vector();
        return studyTypes;
    }
    
    public void setNoStudiesThisYear(boolean noStudiesThisYear) {
        this.noStudiesThisYear = noStudiesThisYear;
    }

    public boolean noStudiesThisYear() {
        return noStudiesThisYear;
    }
    
    


}
