package com.tec.des.dto;
import java.io.File;

import com.tec.shared.util.Nuller;

/**
 * Data transfer object for study material.
 *
 * @author  Norunn Haug Christensen
 */
public class StudyMaterialDTO implements DTO {

    private int id = Nuller.getIntNull();   
    private boolean isFile;
    private String fileName = Nuller.getStringNull();
    private String url = Nuller.getStringNull();
    private String description = Nuller.getStringNull();
    private File file;

    public StudyMaterialDTO() {
        super();
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setIsFile(boolean isFile) {
        this.isFile = isFile;
    }
    
    public boolean isFile() {
        return isFile;
    }
    
    public void setFileName(String name) {
        this.fileName = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public File getFile() {
        return file;
    }

}
