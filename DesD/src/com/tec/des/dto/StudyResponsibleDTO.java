package com.tec.des.dto;
import java.util.Comparator;
import java.util.HashMap;

import com.tec.shared.util.Nuller;

/**
 * Data Transfer Object for a study responsible.
 *
 * @author : Norunn Haug Christensen
 */
public class StudyResponsibleDTO implements DTO {

  private String id = Nuller.getStringNull();
  // ListName is used when displaying people's name in a list: Lastname, Firstname
  private String listName = Nuller.getStringNull();
  // SingleName is used when displaying a single person's name: Firstname, Lastname 
  private String singleName = Nuller.getStringNull();
  private String url = Nuller.getStringNull();

  public static Comparator PEOPLE_COMPARATOR = new Comparator() {
      public int compare(Object o1, Object o2) {
          return ((StudyResponsibleDTO) o1).getListName().toLowerCase().compareTo(((StudyResponsibleDTO) o2).getListName().toLowerCase());
      }
  };
  
  public StudyResponsibleDTO() {
    super();
  }
  
  public StudyResponsibleDTO(HashMap element) {
	  super();
	  
  }

public void setListName(String listName) {
    this.listName = listName;
  }

  public String getListName() {
    return listName;
  }
  
  public void setSingleName(String singleName) {
    this.singleName = singleName;
  }

  public String getSingleName() {
    return singleName;
  }

  
  public void setId(String id) {
    this.id = id;
  }
    
  public String getId() {
    return id;
  }

public String getUrl() {
	return url;
}

public void setUrl(String url) {
	this.url = url;
}

}