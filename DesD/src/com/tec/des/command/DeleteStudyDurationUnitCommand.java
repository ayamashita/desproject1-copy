package com.tec.des.command;
import java.util.Hashtable;

import com.tec.des.dao.StudyDurationUnitDAO;

/**
 * Command for deleting study duration units.
 * The result of the command is a Hashtable containing study duration units, after deletion
 *
 * @author : Norunn Haug Christensen
 * 
 */
public class DeleteStudyDurationUnitCommand extends Command {
    
    private String[] deletedDurationUnits;
    private Hashtable allDurationUnits;
    
/**
 * Constructs a new Command object.
 */
public DeleteStudyDurationUnitCommand() {
	super();
}

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && deletedDurationUnits != null;
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {
    StudyDurationUnitDAO dao = new StudyDurationUnitDAO();
    try {
        dao.deleteDurationUnits(deletedDurationUnits);
        allDurationUnits = dao.getDurationUnits();
    } finally {
        dao.closeConnection();
    }
}

public void setDurationUnits(String[] deletedDurationUnits) {
    this.deletedDurationUnits = deletedDurationUnits;
}

public Hashtable getResult() {
    return allDurationUnits;
}

}
