package com.tec.des.command;
import com.tec.des.dao.StudyDAO;
import com.tec.des.dto.SearchDTO;
import com.tec.shared.util.HitList;
import com.tec.shared.util.Nuller;

/**
 * Command for finding studies based on search criterias.
 * The result of the command is a HitList containing StudyDTOs.
 * The maxHitListLength parameter indicates the maximal number of hits to
 * be fetched. The start position indicates the start position of the segment to
 * fetch, in the list of all possible hits. 
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class FindStudiesCommand extends Command {

    private SearchDTO searchCriteria;
    private int maxHitListLength = Nuller.getIntNull();
    private int startPosition = Nuller.getIntNull();
    private HitList studies;
    
/**
 * Constructs a new Command object.
 */
public FindStudiesCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() 
    && searchCriteria != null 
    && !Nuller.isNull(maxHitListLength)
    && !Nuller.isNull(startPosition);
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {
    StudyDAO dao = new StudyDAO();
    try {
        studies = dao.findStudies(searchCriteria, maxHitListLength, startPosition);
    } finally {
        dao.closeConnection();
    }
}

public void setSearchCriteria(SearchDTO searchCriteria) { 
    this.searchCriteria = searchCriteria;
}

public void setMaxHitListLength(int maxHitListLength) {
    this.maxHitListLength = maxHitListLength;
}

public void setStartPosition(int startPosition) {
    this.startPosition = startPosition;
}

public HitList getResult() {
    return studies;
}

}
