package com.tec.des.command;
import java.util.Hashtable;

import com.tec.des.dao.StudyTypeDAO;

/**
 * Command for adding new study types.
 * The result of the command is a Hashtable containing study types, including the newly added type
 *
 * @author :  Per Kristian Foss
 * 
 */
public class AddStudyTypeCommand extends Command {
    
    private String newStudyType;
    private Hashtable allStudyTypes;
    
/**
 * Constructs a new Command object.
 */
public AddStudyTypeCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && newStudyType != null;
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {
    StudyTypeDAO dao = new StudyTypeDAO();
    try {
        dao.addStudyType(newStudyType);
        allStudyTypes = dao.getStudyTypes();
               
    } finally {
        dao.closeConnection();
    }
}

public void setStudyType(String newStudyType) {
    this.newStudyType = newStudyType;
}

public Hashtable getResult() {
    return allStudyTypes;
}

}