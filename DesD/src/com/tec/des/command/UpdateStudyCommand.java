package com.tec.des.command;
import com.tec.des.dao.StudyDAO;
import com.tec.des.dto.StudyDTO;
import com.tec.des.util.Constants;
import com.tec.shared.util.Nuller;

/**
 * Command for updating a study.
 * The parameter mode can have the following values:
 * - Constants.MAINTAIN_MODE_FINAL - the update is final ie is done in table study.
 * - Constants.MAINTAIN_MODE_TEMP_EDIT - the update is temporary ie is done in 
 *   table temp_study, and the update is on an existing study.
 * - Constants.MAINTAIN_MODE_TEMP_NEW - the update is temporary ie is done in 
 *   table temp_study, and the update is on a new study.
 * The parameter studyMaterialUpdateMode can have the follwing values:
 * - Constants.MODE_OK - the study material is added or deleted, and updates in 
 *   temp_study_material is done accordingly.
 * - Constants.MODE_CANCEL - the  update process of updateing study material 
 *   canceled and updates in temp_study_material is done accordingly..
 * - Nuller.getIntNull() - no update of study material is necessary.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class UpdateStudyCommand extends Command {

    private StudyDTO study;
    private int mode = Nuller.getIntNull();
    private int studyMaterialUpdateMode = Nuller.getIntNull();
    
/**
 * Constructs a new Command object.
 */
public UpdateStudyCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && study != null && !Nuller.isNull(mode);
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {

    StudyDAO dao = new StudyDAO();
    try {
        if (mode == Constants.MAINTAIN_MODE_FINAL) {
            dao.updateStudy(study);
        } else {  
            study = dao.updateStudyTemp(study, mode == Constants.MAINTAIN_MODE_TEMP_NEW, studyMaterialUpdateMode); 
        }
    } finally {
        dao.closeConnection();
    }
}

public void setStudy(StudyDTO study) {
    this.study = study;
}

public void setMode(int mode) {
    this.mode = mode;
}

public void setStudyMaterialUpdateMode(int mode) {
    this.studyMaterialUpdateMode = mode;
}

public StudyDTO getResult() {
    return study;
}

}