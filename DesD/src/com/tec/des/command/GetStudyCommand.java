package com.tec.des.command;
import com.tec.des.dao.StudyDAO;
import com.tec.des.dto.StudyDTO;
import com.tec.shared.util.Nuller;

/**
 * Command fetching a study based on the id.
 * The result of the command is a StudyDTO.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class GetStudyCommand extends Command {

    private int id = Nuller.getIntNull();
    private StudyDTO study;
    private boolean readOnly = true;
    
/**
 * Constructs a new Command object.
 */
public GetStudyCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute()  
    && !Nuller.isNull(id);
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {

    StudyDAO dao = new StudyDAO();
    try {
        study = dao.getStudy(id, readOnly);
    } finally {
        dao.closeConnection();
    }
}

public void setId(int id) {
    this.id = id;
}

public void setIsReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
}

public StudyDTO getResult() {
    return study;
}

}