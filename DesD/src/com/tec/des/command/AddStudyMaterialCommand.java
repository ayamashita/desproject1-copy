package com.tec.des.command;
import java.util.Vector;

import com.tec.des.dao.StudyDAO;
import com.tec.des.dto.StudyMaterialDTO;

/**
 * Command for adding Study Material to a Study.
 * The result of the command is a Vector containing selected Study Material, after new material has been added.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class AddStudyMaterialCommand extends Command  {
    
    private Vector studyMaterial;
    private StudyMaterialDTO newMaterial;
    private int studyId;

/**
 * Constructs a new Command object.
 */
    public AddStudyMaterialCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    StudyDAO dao = new StudyDAO();
        try {
            dao.addStudyMaterial(studyId, newMaterial);
            studyMaterial = dao.getStudyMaterial(studyId); 
        } finally {
            dao.closeConnection();
        }
    }

    public void setNewMaterial (StudyMaterialDTO newMaterial) {
        this.newMaterial = newMaterial;
    }
    
    public void setStudyId (int studyId) {
        this.studyId = studyId;
    }
    
    public Vector getResult() {
        return studyMaterial;
    }

}