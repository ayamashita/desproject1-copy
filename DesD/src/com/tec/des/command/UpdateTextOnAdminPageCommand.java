package com.tec.des.command;
import com.tec.des.dao.AdminPageDAO;

/**
 * Command for updating the text on the admin page.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class UpdateTextOnAdminPageCommand extends Command  {
    
    private String text;

/**
 * Constructs a new Command object.
 */
    public UpdateTextOnAdminPageCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    AdminPageDAO dao = new AdminPageDAO();
        try {
            text = dao.updateTextOnAdminPage(text);
        } finally {
            dao.closeConnection();
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResult() {
        return text;
    }

}
