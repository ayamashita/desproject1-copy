package com.tec.des.command;
import com.tec.des.dao.StudyDAO;
import com.tec.des.dao.StudyTypeDAO;
import com.tec.des.dto.GraphicalReportDTO;

/**
 * Command fetching a study information needed to generate a graphical report.
 * The result of the command is a GraphicalReportDTO containing a vector with
 * GraphicalDTOs and hashtable containing all study types and its colours.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class GetStudyInfoGraphicallyCommand extends Command { 
    
    private GraphicalReportDTO graphicalItems;
    
    
/**
 * Constructs a new Command object.
 */
public GetStudyInfoGraphicallyCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute();  
    
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {


    StudyTypeDAO stDao = new StudyTypeDAO();
    StudyDAO sDao = new StudyDAO();
    try {

        graphicalItems = sDao.getStudyInfoGraphically(stDao.getStudyTypes());
        
    } finally {
        stDao.closeConnection();
        sDao.closeConnection();
    }
}

public GraphicalReportDTO getResult() {
    return graphicalItems;
}

}