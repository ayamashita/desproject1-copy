package com.tec.des.command;
import com.tec.des.dao.StudyDAO;
import com.tec.des.dto.StudyDTO;

/**
 * Command for adding a new study.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class AddStudyCommand extends Command {

    private StudyDTO study;
    
/**
 * Constructs a new Command object.
 */
public AddStudyCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && study != null; 
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {

    StudyDAO dao = new StudyDAO();
    try {
        dao.addStudy(study);
    } finally {
        dao.closeConnection();
    }
}

public void setStudy(StudyDTO study) {
    this.study = study;
}

}