package com.tec.des.command;

import com.tec.des.dto.UserDTO;
import com.tec.des.dto.UserStudyReportDTO;

public abstract class UserReportCommand extends Command {

	protected UserDTO user = null;
	
	protected UserStudyReportDTO report;

	public UserStudyReportDTO getResult() {
		return report;
	}

	public void setReport(UserStudyReportDTO report) {
		this.report = report;
	}
	
	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	public boolean isReadyToCallExecute() {
		return super.isReadyToCallExecute() && (report != null);
	}
}
