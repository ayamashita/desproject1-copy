package com.tec.des.command;
import java.util.Vector;

import com.tec.des.dao.PublicationDAO;

/**
 * Command for finding Publications, based on search criteria
 * The result of the command is a Vector containing Publications matching search criteria.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class FindPublicationsCommand extends Command  {
    
    private String searchText;
    Vector publications;
    
/**
 * Constructs a new Command object.
 */
    public FindPublicationsCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
	    PublicationDAO dao = new PublicationDAO();
		publications = dao.findPublications(searchText);
   }
    
    public void setSearchText (String searchText) {
        this.searchText = searchText;
    }
    
    public Vector getResult() {
        return publications; 
    }
}