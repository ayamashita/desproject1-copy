package com.tec.des.command;
import java.util.Vector;

import com.tec.des.dao.PublicationDAO;

/**
 * Command for fetching selected Publications.
 * The result of the command is a Vector containing selected Publications.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetSelectedPublicationsCommand extends Command  {
    
    private Vector publications;
    private String selectedPublications;

/**
 * Constructs a new Command object.
 */
    public GetSelectedPublicationsCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    	PublicationDAO dao = new PublicationDAO();
		publications = dao.getSelectedPublications(selectedPublications);
    }

    public void setSelectedPublications (String selectedPublications) {
        this.selectedPublications = selectedPublications;
    }
    
    public Vector getResult() {
        return publications;
    }

}