package com.tec.des.command;
import java.util.Hashtable;

import com.tec.des.dao.StudyDurationUnitDAO;

/**
 * Command for fetching all study duration units.
 * The result of the command is a Hashtable containing all study duration units.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class GetStudyDurationUnitsCommand extends Command  {
    
    private Hashtable durationUnits;

/**
 * Constructs a new Command object.
 */
    public GetStudyDurationUnitsCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    StudyDurationUnitDAO dao = new StudyDurationUnitDAO();
        try {
            durationUnits = dao.getDurationUnits();
        } finally {
            dao.closeConnection();
        }
    }

    public Hashtable getResult() {
        return durationUnits;
    }

}
