package com.tec.des.command;
import com.tec.des.util.ExceptionMessages;
import com.tec.shared.exceptions.GeneralException;
import com.tec.shared.exceptions.SystemException;

/**
 * Abstract superclass for all command classes. 
 * A Command links the presentation layer and the data layer by wrapping the 
 * calling of a specified data method. The Command takes in input parameters, 
 * calls the data layer with these parameters and returns the result. 
 * 
 * All subclasses should implement performExecute() and isReadyToCallExecute().
 * All subclasses should have setter methods for the imputparameters and 
 * getter methods for the results.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public abstract class Command {

/**
 * Constructs a new Command object.
 */
public Command() {
	super();
}

/**
 * Executes the command by calling performExecute().
 */
public void execute() throws GeneralException {
  if (isReadyToCallExecute()){
    try {
      performExecute();
    } catch (GeneralException ex) {
      throw ex;
    } catch (Throwable ex) {
      ex.printStackTrace();
      throw new SystemException(ExceptionMessages.UNKNOWN_EXCEPTION + ex.toString());
    }
  } else {
    throw new SystemException(ExceptionMessages.COMMAND_NOT_READY);
  }
}

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return true;
}

/**
 * Executes the command. Called from execute().
 */
protected abstract void performExecute() throws Exception;
}