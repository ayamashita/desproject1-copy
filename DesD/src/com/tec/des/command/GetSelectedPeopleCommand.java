package com.tec.des.command;
import java.util.Vector;

import com.tec.des.dao.PeopleDAO;

/**
 * Command for fetching selected Study Responsibles.
 * The result of the command is a Vector containing selected Study Responsibles.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetSelectedPeopleCommand extends Command  {
    
    private Vector people;
    private String[] selectedPeople;

/**
 * Constructs a new Command object.
 */
    public GetSelectedPeopleCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    PeopleDAO dao = new PeopleDAO();
        try {
            people = dao.getSelectedPeople(selectedPeople);
        } finally {
            dao.closeConnection();
        }
    }

    public void setSelectedPeople (String[] selectedPeople) {
        this.selectedPeople = selectedPeople;
    }
    
    public Vector getResult() {
        return people;
    }

}