package com.tec.des.command;
import java.util.Hashtable;
import java.util.Vector;

import com.tec.des.dao.PeopleDAO;

/**
 * Command for saving changes to the people_role table
 * The result of the command is a Vector containing all people having a role
 *
 * @author :  Per Kristian Foss
 * 
 */
public class SaveRolesCommand extends Command {
    
    private Hashtable peopleRole;
    private Vector peopleWithRole;
    
/**
 * Constructs a new Command object.
 */
public SaveRolesCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && peopleRole != null;
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {
    PeopleDAO dao = new PeopleDAO();
    try {
        dao.setPeopleRole(peopleRole);
        peopleWithRole = dao.getPeopleRole(true, null);
               
    } finally {
        dao.closeConnection();
    }
}

public void setPeopleRole(Hashtable peopleRole) {
    this.peopleRole = peopleRole;
}

public Vector getResult() {
    return peopleWithRole;
}

}