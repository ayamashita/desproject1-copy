package com.tec.des.command;

import com.tec.des.dao.UserStudyReportDAO;

public class DeleteUserReportCommand extends UserReportCommand {

	/* (non-Javadoc)
	 * @see com.tec.des.command.Command#performExecute()
	 */
	protected void performExecute() throws Exception {
	    UserStudyReportDAO dao = new UserStudyReportDAO();
	    try {
    		dao.delete(report.getId());
	    } finally {
	        dao.closeConnection();
	    }
		
		if (user.getStudyReports().containsKey(report.getIdObj())) {
	    	user.getStudyReports().remove(report.getIdObj());
	    }		
	}

	public boolean isReadyToCallExecute() {
		return super.isReadyToCallExecute() && (user != null);
	}

}
