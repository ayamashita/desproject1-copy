package com.tec.des.command;

import com.tec.des.dto.UserStudyReportDTO;

public class AddUserReportCommand extends UserReportCommand {

	protected void performExecute() throws Exception {
	}

	public UserStudyReportDTO getResult() {
		return new UserStudyReportDTO();
	}

}
