package com.tec.des.command;
import java.util.Hashtable;

import com.tec.des.dao.PeopleDAO;

/**
 * Command for fetching all roles in the database
 * The result of the command is a Hashtable containing all people.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetRoleCommand extends Command  {

    private Hashtable roles;

/**
 * Constructs a new Command object.
 */
    public GetRoleCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    PeopleDAO dao = new PeopleDAO();
        try {
            roles = dao.getRoles();
        } finally {
            dao.closeConnection();
        }
    }
    
    public Hashtable getResult() {
        return roles; 
    }
}