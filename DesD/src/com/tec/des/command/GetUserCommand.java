package com.tec.des.command;
import com.tec.des.dao.PeopleDAO;
import com.tec.des.dto.UserDTO;
import com.tec.shared.util.Nuller;

/**
 * Command for fetching a user based on the username and password.
 * The result of the command is a UserDTO.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class GetUserCommand extends Command {

    private String userName = Nuller.getStringNull();
    private String password = Nuller.getStringNull();
    private UserDTO user;
    
/**
 * Constructs a new Command object.
 */
public GetUserCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute()  
    && !Nuller.isNull(userName) && !Nuller.isNull(password);
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {

    PeopleDAO dao = new PeopleDAO();
    try {
        user = dao.getUser(userName, password);
    } finally {
        dao.closeConnection();
    }
}

public void setUserName(String userName) {
    this.userName = userName;
}

public void setPassword(String password) {
    this.password = password;
}

public UserDTO getResult() {
    return user;
}

}