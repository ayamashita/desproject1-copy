package com.tec.des.command;

import com.tec.des.dao.UserStudyReportDAO;
import com.tec.des.util.Validator;

public class SaveUserReportCommand extends UserReportCommand {
	
	protected void performExecute() throws Exception {
		
		Validator.validateUserReport(report, user.getStudyReportsSortedlist());
		
	    UserStudyReportDAO dao = new UserStudyReportDAO();
	    report.setUserId(user.getId());

	    try {
	    	if (report.getId() > 0) {
	    		dao.update(report);
	    	}
	    	else {
	    		dao.create(report);
	    	}
	    } finally {
	        dao.closeConnection();
	    }

	    Integer reportKey = report.getIdObj();
		if (user.getStudyReports().containsKey(reportKey)) {
	    	user.getStudyReports().remove(reportKey);
	    }

		user.getStudyReports().put(reportKey, report);
	}

	public boolean isReadyToCallExecute() {
		return super.isReadyToCallExecute() && user != null;
	}
}
