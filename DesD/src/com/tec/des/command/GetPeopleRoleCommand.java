package com.tec.des.command;
import java.util.Vector;

import com.tec.des.dao.PeopleDAO;

/**
 * Command for fetching people and theis roles.
 * The result of the command is a Vector containing all people.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetPeopleRoleCommand extends Command  {
    
    private Vector peopleRole;
    private boolean onlyRole;
    private String searchText;

/**
 * Constructs a new Command object.
 */
    public GetPeopleRoleCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    PeopleDAO dao = new PeopleDAO();
        try {
            peopleRole = dao.getPeopleRole(onlyRole, searchText);
        } finally {
            dao.closeConnection();
        }
    }

    public void setOnlyRole (boolean onlyRole) {
        this.onlyRole = onlyRole;
    }
    
    public void setSearchText (String searchText) {
        this.searchText = searchText;
    }
    
    public Vector getResult() {
        return peopleRole; 
    }
}