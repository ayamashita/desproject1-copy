package com.tec.des.command;
import java.util.Vector;

import com.tec.des.dao.PeopleDAO;

/**
 * Command for fetching all people.
 * The result of the command is a Vector containing all people.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetAllPeopleCommand extends Command  {
    
    private Vector people;

/**
 * Constructs a new Command object.
 */
    public GetAllPeopleCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    PeopleDAO dao = new PeopleDAO();
        try {
            people = dao.getAllPeople();
        } finally {
            dao.closeConnection();
        }
    }

    public Vector getResult() {
        return people;
    }

}
