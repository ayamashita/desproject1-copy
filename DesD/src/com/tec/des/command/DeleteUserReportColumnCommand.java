package com.tec.des.command;

import com.tec.des.dao.StudyDAO;
import com.tec.shared.util.Nuller;


public class DeleteUserReportColumnCommand extends UserReportCommand {

	protected String columnName = Nuller.getStringNull();
	
	protected void performExecute() throws Exception {
		if (report.getStudyColumns().contains(columnName)) {
			report.getStudyColumns().remove(columnName);
		}
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public boolean isReadyToCallExecute() {
		return super.isReadyToCallExecute() && (StudyDAO.ReportColumns.valueOf(columnName) != null); 
	}
	


}
