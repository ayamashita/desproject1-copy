package com.tec.des.command;
import java.util.Hashtable;

import com.tec.des.dao.StudyTypeDAO;

/**
 * Command for deleting study types.
 * The result of the command is a Hashtable containing study types, after deletion
 *
 * @author :  Per Kristian Foss
 * 
 */
public class DeleteStudyTypeCommand extends Command {
    
    private String[] deletedStudyTypes;
    private Hashtable allStudyTypes;
    
/**
 * Constructs a new Command object.
 */
public DeleteStudyTypeCommand() {
	super();
}

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && deletedStudyTypes != null;
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {
    StudyTypeDAO dao = new StudyTypeDAO();
    try {
        dao.deleteStudyType(deletedStudyTypes);
        allStudyTypes = dao.getStudyTypes();
    } finally {
        dao.closeConnection();
    }
}

public void setStudyType(String[] deletedStudyTypes) {
    this.deletedStudyTypes = deletedStudyTypes;
}

public Hashtable getResult() {
    return allStudyTypes;
}

}
