package com.tec.des.command;
import java.util.Hashtable;

import com.tec.des.dao.StudyDurationUnitDAO;

/**
 * Command for adding new study duration units.
 * The result of the command is a Hashtable containing study duration units, including the newly added duration unit.
 *
 * @author : Norunn Haug Christensen
 * 
 */
public class AddStudyDurationUnitCommand extends Command {
    
    private String newDurationUnit;
    private Hashtable allDurationUnits;
    
/**
 * Constructs a new Command object.
 */
public AddStudyDurationUnitCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute() && newDurationUnit != null;
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {
    StudyDurationUnitDAO dao = new StudyDurationUnitDAO();
    try {
        dao.addDurationUnit(newDurationUnit);
        allDurationUnits = dao.getDurationUnits();
               
    } finally {
        dao.closeConnection();
    }
}

public void setDurationUnit(String newDurationUnit) {
    this.newDurationUnit = newDurationUnit;
}

public Hashtable getResult() {
    return allDurationUnits;
}

}