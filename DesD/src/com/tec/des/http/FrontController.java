package com.tec.des.http;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tec.des.dto.UserDTO;
import com.tec.des.http.handler.UsecaseHandler;
import com.tec.des.util.AccessController;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Message;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;


/**
 * Front Controller servlet. Handles all user requests by finding 
 * the correct usecase and invoking its handler. After handling, the controller
 * passes the result to the correct view (typically a jsp page). The controller
 * also handles access to restricted pages.
 *
 * @author Norunn Haug Christensen
 */
public class FrontController extends HttpServlet {

  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  } 


/**
 * Calls the doPost with the request and the response.
 * 
 * @param request the ServletEngine created HttpServletRequest
 * @param response the ServletEngine created HttpServletResponse
 * @throw IOException, ServletException
 */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    doPost(request, response);  
  }

/**
 * Handles all the requests for the controller. It expects a request parameter,
 * SERVICE_NAME, specifying the name of the transaction to be executed.
 * The execution returns the url of the target JSP, and the request is forwarded 
 * to this url. If something failes, a message is created, and request goes 
 * back to the specified CASE_OF_EXCEPION JSP, or the calling JSP, if no such 
 * parameter is specified.
 * 
 * @param request the ServletEngine created HttpServletRequest
 * @param response the ServletEngine created HttpServletResponse
 * @throw IOException, ServletException
 */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    //The view making the request is temporarily set to the resulting view.
    //In case of an exception the user is returned to this view.  
    String view = request.getParameter(WebKeys.REQUEST_PARAM_SENDER_VIEW);
    if (Nuller.isReallyNull(view))
        view = WebConstants.VIEW_ERROR_PAGE;

    try {
      //Obtaining the requested usecase.  
      String usecase = request.getParameter(WebKeys.REQUEST_PARAM_USECASE);
      if (Nuller.isReallyNull(usecase)) {
        throw new UserException(ExceptionMessages.NO_USECASE);  
      }

      Class handlerClass = Class.forName(usecase);
      UsecaseHandler usecaseHandler = (UsecaseHandler) handlerClass.newInstance();

      if (!Nuller.isNull(usecaseHandler.getRequiredRole())) {
        //Restricted page requested.
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(WebKeys.SESSION_PARAM_USER) == null) {
            //Session time out, forward to login page
            view = WebConstants.VIEW_LOGIN;
        } else {
            //Check user rights
            UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER);
            if (!AccessController.hasAccess(user.getRoleId(), usecaseHandler.getRequiredRole())) 
                throw new UserException(ExceptionMessages.INSUFFICIENT_RIGHTS);
            
            view = usecaseHandler.doHandle(request);
        }
      } else {
          view = usecaseHandler.doHandle(request);
      }

      forward(request, response, view);

      
    } catch (Exception ex) { 
        // The user receives an error message and is returned to the calling view or the error page.
        String errorMessage = ex.getMessage();
        if (!(ex instanceof UserException)) {
            errorMessage = ExceptionMessages.SYSTEM_EXCEPTION;
        } 
        request.setAttribute(WebKeys.REQUEST_ERROR_MESSAGE, new Message(errorMessage));
        LoggerFactory.getLogger(FrontController.class).error("Error.", ex);
        try { 
            forward(request, response, view);
        } catch (Exception e) {
            //Fatal error: Can not forward to calling view.
            LoggerFactory.getLogger(FrontController.class).fatal("Fatal exception.", e);
            e.printStackTrace();
            throw new ServletException(e.getMessage()); 
        }
    }
   }

/**
 * Forwards the result to the passed in url.
 * 
 * @param request the ServletEngine created HttpServletRequest
 * @param response the ServletEngine created HttpServletResponse
 * @param url the url to forward to
 * @throws ServletException, IOException, SystemException.
 */
  protected void forward(HttpServletRequest request, HttpServletResponse response, String url) throws IOException, ServletException, SystemException {

    RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
    LoggerFactory.getLogger(FrontController.class).debug("Forward to " + url + " , dispatcher: " + rd.toString());
    if (rd == null) {
      throw new SystemException(ExceptionMessages.UNKNOWN_USECASE_VIEW);
    }  
    rd.forward(request, response); 

}
  
}

