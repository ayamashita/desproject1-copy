package com.tec.des.http;

/**
 * Class containig constants representing usecases, their corresponding views
 * and other constants uses in the JSPs and handler classes.
 *
 * @author  Norunn Haug Christensen
 */
public class WebConstants {

 /**
  * The class can not be instantiated
  */
    private WebConstants() {}    

 /*
  * Usecases
  */
    public final static String USECASE_LOGIN = "com.tec.des.http.handler.LoginHandler";
    public final static String USECASE_LOGOUT = "com.tec.des.http.handler.LogoutHandler";
    public final static String USECASE_LIST_STUDIES = "com.tec.des.http.handler.FindStudiesHandler";
    public final static String USECASE_SINGLE_STUDY_REPORT = "com.tec.des.http.handler.GetStudyHandler";
    public final static String USECASE_ADD_STUDY = "com.tec.des.http.handler.AddStudyHandler";    
    public final static String USECASE_UPDATE_STUDY = "com.tec.des.http.handler.UpdateStudyHandler";    
    public final static String USECASE_DELETE_STUDY = "com.tec.des.http.handler.DeleteStudyHandler";    
    public final static String USECASE_ADD_STUDY_TYPE = "com.tec.des.http.handler.AddStudyTypeHandler";
    public final static String USECASE_DELETE_STUDY_TYPE = "com.tec.des.http.handler.DeleteStudyTypeHandler";
    public final static String USECASE_ADD_STUDY_DURATION_UNIT = "com.tec.des.http.handler.AddStudyDurationUnitHandler";
    public final static String USECASE_DELETE_STUDY_DURATION_UNIT = "com.tec.des.http.handler.DeleteStudyDurationUnitHandler";
    public final static String USECASE_SELECT_STUDY_RESPONSIBLES = "com.tec.des.http.handler.SelectStudyResponsiblesHandler";
    public final static String USECASE_SELECT_PUBLICATIONS = "com.tec.des.http.handler.SelectPublicationsHandler";
    public final static String USECASE_FIND_PUBLICATIONS = "com.tec.des.http.handler.FindPublicationsHandler";  
    public final static String USECASE_DELETE_STUDY_MATERIAL = "com.tec.des.http.handler.DeleteStudyMaterialHandler";  
    public final static String USECASE_ADD_STUDY_MATERIAL = "com.tec.des.http.handler.AddStudyMaterialHandler"; 
    public final static String USECASE_FIND_PEOPLE_ROLE = "com.tec.des.http.handler.UserAdminHandler";
    public final static String USECASE_SAVE_ROLES = "com.tec.des.http.handler.SaveRolesHandler";    
    public final static String USECASE_EDIT_ADMIN_PAGE = "com.tec.des.http.handler.EditAdminPageHandler";
    public final static String USECASE_GET_GRAPHICAL_REPORT = "com.tec.des.http.handler.GetStudyInfoGraphicallyHandler";
    public final static String USECASE_OPEN_LIST_STUDIES = "com.tec.des.http.handler.OpenListStudiesHandler"; 
    public final static String USECASE_OPEN_ADMIN_PAGE = "com.tec.des.http.handler.OpenAdminPageHandler"; 
    public final static String USECASE_OPEN_EDIT_ADMIN_PAGE = "com.tec.des.http.handler.OpenEditAdminPageHandler"; 
    public final static String USECASE_OPEN_NEW_STUDY = "com.tec.des.http.handler.OpenNewStudyHandler";        
    public final static String USECASE_OPEN_USER_ADMIN = "com.tec.des.http.handler.OpenUserAdminHandler";
    public final static String USECASE_OPEN_STUDY_RESPONSIBLES = "com.tec.des.http.handler.OpenStudyResponsiblesHandler";
    public final static String USECASE_OPEN_PUBLICATIONS = "com.tec.des.http.handler.OpenPublicationsHandler";
    public final static String USECASE_OPEN_STUDY_TYPES = "com.tec.des.http.handler.OpenStudyTypeHandler";
    public final static String USECASE_OPEN_STUDY_DURATION_UNITS = "com.tec.des.http.handler.OpenStudyDurationUnitHandler";
    public final static String USECASE_OPEN_STUDY_MATERIAL = "com.tec.des.http.handler.OpenStudyMaterialHandler";
    public final static String USECASE_ADD_USER_REPORT = "com.tec.des.http.handler.AddUserReportHandler";
    public final static String USECASE_VIEW_USER_REPORT = "com.tec.des.http.handler.ViewUserReportHandler";
    public final static String USECASE_EDIT_USER_REPORT = "com.tec.des.http.handler.EditUserReportHandler";
    public final static String USECASE_SAVE_USER_REPORT = "com.tec.des.http.handler.SaveUserReportHandler";
    public final static String USECASE_REFRESH_USER_REPORT = "com.tec.des.http.handler.RefreshUserReportHandler";    
    public final static String USECASE_DELETE_USER_REPORT = "com.tec.des.http.handler.DeleteUserReportHandler";
    public final static String USECASE_ADD_USER_REPORT_COLUMN = "com.tec.des.http.handler.AddUserReportColumnHandler";
    public final static String USECASE_DELETE_USER_REPORT_COLUMN = "com.tec.des.http.handler.DeleteUserReportColumnHandler";
               
 /*
  * Views
  */    
    public final static String VIEW_LOGIN = "/login.jsp";
    public final static String VIEW_ERROR_PAGE = "/errorpage.jsp";
    public final static String VIEW_LIST_STUDIES = "/liststudies.jsp";
    public final static String VIEW_STUDY_OVERVIEW_REPORT = "/studyoverviewreport.jsp";
    public final static String VIEW_STUDY_OVERVIEW_REPORT_PF = "/studyoverviewreport_pf.jsp";
    public final static String VIEW_SINGLE_STUDY_REPORT = "/singlestudyreport.jsp";
    public final static String VIEW_SINGLE_STUDY_REPORT_PF = "/singlestudyreport_pf.jsp";
    public final static String VIEW_EDIT_STUDY = "/editstudy.jsp";
    public final static String VIEW_NEW_STUDY = "/newstudy.jsp";
    public final static String VIEW_STUDY_TYPES = "/studytypes.jsp";
    public final static String VIEW_STUDY_DURATION_UNITS = "/studydurationunits.jsp";
    public final static String VIEW_STUDY_RESPONSIBLES = "/studyresponsibles.jsp";
    public final static String VIEW_PUBLICATIONS = "/publications.jsp";
    public final static String VIEW_USER_ADMIN = "/useradministration.jsp";
    public final static String VIEW_STUDY_MATERIAL = "/studymaterial.jsp";
    public final static String VIEW_ADMIN_PAGE = "/adminpage.jsp";
    public final static String VIEW_ADMIN_PAGE_EDIT = "/editadminpage.jsp";
    public final static String VIEW_GRAPHICAL_REPORT = "/graphicalreport.jsp";
    public final static String VIEW_EDIT_USER_REPORT = "/edituserreport.jsp";
    
 /*
  * Constants for sorting
  */    
    public final static String SORT_BY_STUDY_NAME = "study_name";
    public final static String SORT_BY_END_OF_STUDY = "study_end_date";
    public final static String SORT_BY_TYPE_OF_STUDY = "study_type";
    
    public final static String SORT_ORDER_DESCENDING = "desc";
    public final static String SORT_ORDER_ASCENDING = "asc";

 /*
  * Constants used in search
  */    
    public final static int MAX_HIT_LIST_LENGTH = 10;
    public final static int DEFAULT_START_POSITION = 0;
    public final static int DEFAULT_NUMBER_OF_NAVIGATIONLINKS_SHOWN = 5;
    
 /*
  * Other constants
  */    
    public final static String STUDY_MATERIAL_ISFILE = "material_isfile";
    public final static String STUDY_MATERIAL_ISURL = "material_isurl";
    
    public final static String FRONTCONTROLLER_URL = "/DesD/FrontController";
    
    
}
