package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.AddStudyTypeCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;

/**
 * 
 * Handles the add study type use case. 
 *
 * @author Per Kristian Foss
 */
public class AddStudyTypeHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    putStudyTypesOnRequest(request);  
    String studyType = request.getParameter(WebKeys.REQUEST_PARAM_ADD_STUDY_TYPE);
    Validator.validateAddStudyType(studyType);
    
    AddStudyTypeCommand command = new AddStudyTypeCommand();
    command.setStudyType(studyType);
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());  
    
    return WebConstants.VIEW_STUDY_TYPES;
  }
  
  /**
   * Put study types on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  private void putStudyTypesOnRequest(HttpServletRequest request) throws Exception {
    
    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());
      
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}