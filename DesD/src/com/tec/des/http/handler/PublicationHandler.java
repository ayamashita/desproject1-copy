package com.tec.des.http.handler;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetSelectedPublicationsCommand;
import com.tec.des.dto.PublicationDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;

/**
 * 
 * Abstract super class for all handler classes for usecases conserning 
 * Publications. 
 *
 * @author Norunn Haug Christensen
 */
public abstract class PublicationHandler extends UsecaseHandler {
    
   protected String view = WebConstants.VIEW_PUBLICATIONS; 
   
  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {
  
    String selectedPublications = getSelectedPublications(request);
    
    if (!Nuller.isReallyNull(selectedPublications)) { 
        findAndPutSelectedPublicationsOnRequest(selectedPublications, request);
    }
    
    return view;
  }

  /**
   * Fetched the selected publications and put them on the request.
   * @throws Exception
   */
  protected void findAndPutSelectedPublicationsOnRequest(String selectedPublications, HttpServletRequest request) throws Exception {
        
        GetSelectedPublicationsCommand command = new GetSelectedPublicationsCommand();   
        command.setSelectedPublications(selectedPublications);
        command.execute();
    
        Vector publications = command.getResult();
        Iterator it = publications.iterator();
        PublicationDTO publication;
        StringBuffer publicationTitles = new StringBuffer();
        while (it.hasNext()) {
            publication = (PublicationDTO)it.next(); 
            publicationTitles.append(Validator.replaceChar(publication.getTitle())); 
            if (it.hasNext())
                publicationTitles.append(Constants.ID_SEPARATOR);
        }
    
        request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PUBLICATIONS, publications);
        request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PUBLICATION_IDS, selectedPublications);
        request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PUBLICATION_TITLES, publicationTitles.toString());

  }
  
  /**
   * Returns a String containing currently selected publications id's.
   * This method is implemented by all subclasses.
   */
  protected abstract String getSelectedPublications(HttpServletRequest request) throws UserException; 
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}