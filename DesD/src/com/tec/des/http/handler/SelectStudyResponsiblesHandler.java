package com.tec.des.http.handler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetSelectedPeopleCommand;
import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;

/**
 * 
 * Handles the Select Study Responsibles use case.
 *
 * @author Per Kristian Foss
 */
public class SelectStudyResponsiblesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    String[] selectedPeople;
    if (request.getParameter(WebKeys.REQUEST_PARAM_BUTTON_ADD) != null) {
        selectedPeople = getSelectedPeopleAdd(request);
    } else {
        selectedPeople = getSelectedPeopleRemove(request);
    }
    
    GetAllPeopleCommand command_all = new GetAllPeopleCommand();   
    command_all.execute();     
    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command_all.getResult());
    
    GetSelectedPeopleCommand command_select = new GetSelectedPeopleCommand();   
    command_select.setSelectedPeople(selectedPeople);
    command_select.execute();
    Vector selected = command_select.getResult();
    if (selected.isEmpty()) {
        StudyResponsibleDTO emptyResponsible = new StudyResponsibleDTO();
        emptyResponsible.setId(Constants.EMPTY_ID_STUDY_RESPONSIBLES);
        emptyResponsible.setListName(Constants.EMPTY_VALUE_STUDY_RESPONSIBLES);
        selected.add(emptyResponsible);
     } 
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE, selected); 
    request.setAttribute(WebKeys.REQUEST_PARAM_TARGET_USECASE, request.getParameter(WebKeys.REQUEST_PARAM_TARGET_USECASE));
    return WebConstants.VIEW_STUDY_RESPONSIBLES;
  }
   
   /**
   * Returns a String[] containing people selected to be study responsibles after the >> button is pressed.
   * @throws UserException
   */
  private String[] getSelectedPeopleAdd(HttpServletRequest request) throws UserException {
     
    String[] oldSelected = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE).split(Constants.ID_SEPARATOR);
    ArrayList oldSelectedList = new ArrayList(Arrays.asList(oldSelected));
    String[] newSelected = request.getParameterValues(WebKeys.REQUEST_PARAM_ALL_PEOPLE);
    if (newSelected == null) {
        return oldSelected;        
    } 
    
    for (int i = 0; i < newSelected.length; i++) {
    	String value = newSelected[i];
    	if (!oldSelectedList.contains(value)) {
			oldSelectedList.add(value);
		}	
    }
    
    return  toArray(oldSelectedList);
  }

   /**
   * Returns a String[] containing people selected to be study responsibles after the << button is pressed.
   * @throws UserException
   */
  private String[] getSelectedPeopleRemove(HttpServletRequest request) throws UserException {

    String[] oldSelected = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE).split(Constants.ID_SEPARATOR);
    ArrayList oldSelectedList = new ArrayList(Arrays.asList(oldSelected));
    String[] removeSelected = request.getParameterValues(WebKeys.REQUEST_PARAM_SELECTED_PEOPLE);
    	
    if (removeSelected == null) {
    	return oldSelected;
    } 
    else {
    	for (int i = 0; i < removeSelected.length; i++) {
    		oldSelectedList.remove(removeSelected[i]);	
    	}    	
    }

    return toArray(oldSelectedList);
  }

/**
 * @param list
 * @return String[]
 */
private String[] toArray(ArrayList list) {
	String[] newSelected = new String[list.size()];
    int i = 0;
    for (Iterator iterator = list.iterator(); iterator.hasNext();) {
		String name = (String) iterator.next();
		newSelected[i++] = name;
	}
	return newSelected;
}

  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}