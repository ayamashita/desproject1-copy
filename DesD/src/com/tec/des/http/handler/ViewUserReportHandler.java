package com.tec.des.http.handler;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.tec.des.dao.StudyDAO;
import com.tec.des.dto.SearchDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.dto.UserStudyReportDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

public class ViewUserReportHandler extends FindStudiesHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		
		SearchDTO searchCriteria = getSearchCriteria(request); 
		
		if (searchCriteria.getReport() == null) {
			return WebConstants.VIEW_STUDY_OVERVIEW_REPORT;
		}
		
		return super.doHandle(request);
	}
	
	/* (non-Javadoc)
	 * @see com.tec.des.http.handler.FindStudiesHandler#getSearchCriteria(javax.servlet.http.HttpServletRequest)
	 */
	protected SearchDTO getSearchCriteria(HttpServletRequest request) {


		SearchDTO search = new SearchDTO();
	    UserStudyReportDTO report = getReportBy(request);
	    if (report != null) {
	    	search.setReport(report);
	    	
	    	if (StringUtils.isNotEmpty(report.getSortEndDate())) {
	    		search.setSortBy(StudyDAO.ReportColumns.END_DATE.getName());
	    		search.setSortOrder(report.getSortEndDate());
	    	}	    	
	    	search.setSortByDefault(StudyDAO.ReportColumns.TYPE.getName() + " " + UserStudyReportDTO.SortOperator.ASCENDANT.getName());
	    }
		
	    return search;
	}

}
