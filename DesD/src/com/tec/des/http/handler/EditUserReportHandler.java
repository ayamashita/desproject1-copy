package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.dto.UserStudyReportDTO;
import com.tec.des.http.WebKeys;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.util.Parser;

public class EditUserReportHandler extends UserReportHandler {
	
	public String doHandle(HttpServletRequest request) throws Exception {
		
		int userReportId = Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID));

		UserStudyReportDTO report = (UserStudyReportDTO) getUser(request).getStudyReports().get(Integer.valueOf(userReportId));
		if (report == null) {
			throw new SystemException("Report with id="+userReportId+" does not exists.");
		}
		
	    request.setAttribute(WebKeys.REQUEST_BEAN_USER_REPORT, report);
	    return getView();

	}
	
}
