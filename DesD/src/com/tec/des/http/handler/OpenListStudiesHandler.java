package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

/**
 * 
 * Initialize the list studies view.
 *
 * @author Norunn Haug Christensen
 */
public class OpenListStudiesHandler extends UsecaseHandler {

  /**
   * Initialize the view by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());

    return WebConstants.VIEW_LIST_STUDIES;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}
