package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetTextOnAdminPageCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;

/**
 * 
 * Initialize the Admin Page view.
 *
 * @author Norunn Haug Christensen
 */
public class OpenAdminPageHandler extends UsecaseHandler { 

  /**
   * Initialize the view by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception { 

    GetTextOnAdminPageCommand command = new GetTextOnAdminPageCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_ADMIN_TEXT, command.getResult());
    
    return WebConstants.VIEW_ADMIN_PAGE;
  
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}
