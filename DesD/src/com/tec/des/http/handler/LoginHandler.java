package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tec.des.command.GetTextOnAdminPageCommand;
import com.tec.des.command.GetUserCommand;
import com.tec.des.dto.UserDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Validator;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;

/**
 * 
 * Handles the login usecase.
 *
 * @author Norunn Haug Christensen
 */
public class LoginHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    String userName = request.getParameter(WebKeys.REQUEST_PARAM_USER_NAME);
    String password = request.getParameter(WebKeys.REQUEST_PARAM_PASSWORD);
    Validator.validateUserNameAndPassword(userName, password);
    
    GetUserCommand command = new GetUserCommand();
    command.setUserName(userName);
    command.setPassword(password);
    command.execute();
    
    UserDTO user = command.getResult();
    if (Nuller.isNull(user.getRoleId()))
        throw new UserException(ExceptionMessages.NO_RIGHTS); 
    
    HttpSession session = request.getSession();  
    session.setAttribute(WebKeys.SESSION_PARAM_USER, user);
    
    GetTextOnAdminPageCommand cmd = new GetTextOnAdminPageCommand();
    cmd.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_ADMIN_TEXT, cmd.getResult());

    return WebConstants.VIEW_ADMIN_PAGE;
    
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}