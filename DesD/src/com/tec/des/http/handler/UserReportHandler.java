package com.tec.des.http.handler;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tec.des.command.UserReportCommand;
import com.tec.des.dto.UserDTO;
import com.tec.des.dto.UserStudyReportDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.GeneralException;
import com.tec.shared.util.Nuller;
import com.tec.shared.util.Parser;

public abstract class UserReportHandler extends UsecaseHandler {
	
	protected String processCommand(HttpServletRequest request, UserReportCommand command) throws GeneralException {
		command.setUser(getUser(request));
		command.setReport(getReportFromRequest(request));
		request.setAttribute(WebKeys.REQUEST_BEAN_USER_REPORT, command.getResult());
	    command.execute();
	    request.setAttribute(WebKeys.REQUEST_BEAN_USER_REPORT, command.getResult());
	    return getView();
	}

	protected UserDTO getUser(HttpServletRequest request) {
		HttpSession session = request.getSession();  
		UserDTO user = (UserDTO) session.getAttribute(WebKeys.SESSION_PARAM_USER);
		return user;
	}
	
	 /**
	   * Fetches the UserStudyReportDTO from the request.
	   * @return UserStudyReportDTO 
	   */
	  protected UserStudyReportDTO getReportFromRequest(HttpServletRequest request) throws GeneralException {
		  UserStudyReportDTO report = new UserStudyReportDTO();
	      report.setId(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID)));
	      report.setName(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_NAME));
	      report.setSortEndDate(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_SORT_END_DATE));
	      report.setWhereResponsibleOperator(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_WHERE_RESPONSIBLE_OPERATOR));

	      report.setName(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_NAME));
	      String parameter = request.getParameter(WebKeys.REQUEST_PARAM_REPORT_STUDY_COLUMNS);
	      if (!Nuller.isReallyNull(parameter)) {
	        report.setStudyColumns(new ArrayList(Arrays.asList(parameter.split(Constants.ID_SEPARATOR))));
	      }
	      
	      parameter = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLE_IDS);
	      if (!Nuller.isReallyNull(parameter)) {
	        report.setResponsibles(
	            parameter.split(Constants.ID_SEPARATOR), 
	            request.getParameter(WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLES).split(Constants.ID_SEPARATOR));
	      }
	      
	          
	      return report;
	  }
	
	protected String getView() {
		String view = WebConstants.VIEW_EDIT_USER_REPORT;
		return view;
	}

	/* (non-Javadoc)
	 * @see com.tec.des.http.handler.UsecaseHandler#getRequiredRole()
	 */
	public int getRequiredRole() {
		return Constants.ROLE_STUDY_ADMIN;
	}

}
