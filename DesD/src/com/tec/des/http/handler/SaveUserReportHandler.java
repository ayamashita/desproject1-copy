package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.SaveUserReportCommand;
import com.tec.des.http.WebConstants;

public class SaveUserReportHandler extends UserReportHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
	    return processCommand(request, new SaveUserReportCommand());
	}
	
	/* (non-Javadoc)
	 * @see com.tec.des.http.handler.UserReportHandler#getView()
	 */
	protected String getView() {
		return "/FrontController?usecase="+WebConstants.USECASE_VIEW_USER_REPORT;
	}
}
