package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.DeleteStudyCommand;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.util.Parser;

/**
 * 
 * Handles the delete study use case. 
 *
 * @author Norunn Haug Christensen
 */
public class DeleteStudyHandler extends MaintainStudyHandler {

  /**
   * Handles the use case by executing the command.
   * After the study is deleted, all studies are fetched and presented to the
   * user in the study overview report, orded with the newest studies first.
   *
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    DeleteStudyCommand command = new DeleteStudyCommand();
    command.setId(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_ID_DELETE)));
    command.setMode(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_MAINTAIN_MODE)));
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA, getSearchCriteria());  
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, getAllStudies());

    return view;
    
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}