package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.AddUserReportColumnCommand;
import com.tec.des.command.UserReportCommand;
import com.tec.des.http.WebKeys;
import com.tec.shared.exceptions.GeneralException;

public class AddUserReportColumnHandler extends UserReportHandler {

	/* (non-Javadoc)
	 * @see com.tec.des.http.handler.UsecaseHandler#doHandle(javax.servlet.http.HttpServletRequest)
	 */
	public String doHandle(HttpServletRequest request) throws Exception {
		return processCommand(request, new AddUserReportColumnCommand());
	}

	/* (non-Javadoc)
	 * @see com.tec.des.http.handler.UserReportHandler#processCommand(javax.servlet.http.HttpServletRequest, com.tec.des.command.UserReportCommand)
	 */
	protected String processCommand(HttpServletRequest request, UserReportCommand command) throws GeneralException {
		((AddUserReportColumnCommand)command).setColumnName(request.getParameter(WebKeys.REQUEST_PARAM_REPORT_STUDY_COLUMN_NAME));
		
		return super.processCommand(request, command);
	}

}
