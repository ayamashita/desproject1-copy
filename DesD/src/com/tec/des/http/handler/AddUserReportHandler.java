package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.AddUserReportCommand;

public class AddUserReportHandler extends UserReportHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		return processCommand(request, new AddUserReportCommand());
	}

}
