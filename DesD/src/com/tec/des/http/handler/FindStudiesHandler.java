package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tec.des.command.FindStudiesCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.dto.SearchDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.dto.UserStudyReportDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Validator;
import com.tec.shared.util.Nuller;
import com.tec.shared.util.Parser;

/**
 * 
 * Handles the find studies use case. 
 *
 * @author Norunn Haug Christensen
 */
public class FindStudiesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * Validation of the search criterias is done before the search is performed.
   *
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    SearchDTO searchCriteria = getSearchCriteria(request);   
    putSearchParameterOnRequest(searchCriteria, request);

    Validator.validateSearchCriteria(searchCriteria);      
    FindStudiesCommand command = new FindStudiesCommand();
    command.setSearchCriteria(searchCriteria); 
    command.setMaxHitListLength(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH)));
    int startPositionFromRequest = Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_START_POSITION));
    if (Nuller.isNull(startPositionFromRequest)) {
    	startPositionFromRequest = WebConstants.DEFAULT_START_POSITION;
    }
	command.setStartPosition(startPositionFromRequest);
    command.execute();

    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, command.getResult());
    
    String view;
    String printerFriendly = request.getParameter(WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY);
    if (!Nuller.isReallyNull(printerFriendly) && Parser.parseBoolean(printerFriendly)) {
        view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT_PF;
    } else {
        view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT;
    }

    return view;
  }
  
  /**
   * Returns a SearchDTO containing the search criteria.
   */
  protected SearchDTO getSearchCriteria(HttpServletRequest request) {
    SearchDTO search = new SearchDTO();
    search.setFreeText(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TEXT));
    search.setTypeOfStudy(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY));
    search.setEndOfStudyFrom(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM));    
    search.setEndOfStudyTo(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO));    
    search.setStudyResponsibles(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES));    
    search.setSortBy(request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_BY));
    if (request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING) == null || request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING).equals(WebConstants.SORT_ORDER_ASCENDING)) {
        search.setSortOrder(WebConstants.SORT_ORDER_ASCENDING);    
    } else {
        search.setSortOrder(WebConstants.SORT_ORDER_DESCENDING);    
    }

    search.setReport(getReportBy(request));
    
    return search;
  }
  
	/**
	 * @param request
	 * @return
	 */
	protected UserStudyReportDTO getReportBy(HttpServletRequest request) {
		String userReportId = request.getParameter(WebKeys.REQUEST_PARAM_REPORT_ID);
		if (Nuller.isReallyNull(userReportId) || userReportId.equals("-1")) {
	    	return null;
	    }
		HttpSession session = request.getSession();  
		UserDTO user = (UserDTO) session.getAttribute(WebKeys.SESSION_PARAM_USER);
		UserStudyReportDTO report = (UserStudyReportDTO) user.getStudyReports().get(Integer.valueOf(userReportId));
		return report;
	}
  
  /**
   * Put the search criteria and the study types on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  private void putSearchParameterOnRequest(SearchDTO searchCriteria, HttpServletRequest request) throws Exception {
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA, searchCriteria);
    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());
      
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}