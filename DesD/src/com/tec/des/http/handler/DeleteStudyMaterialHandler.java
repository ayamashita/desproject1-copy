package com.tec.des.http.handler;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.DeleteStudyMaterialCommand;
import com.tec.des.command.GetStudyMaterialCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Parser;

/**
 * 
 * Handles the Delete Study Material use case.
 *
 * @author Per Kristian Foss
 */
public class DeleteStudyMaterialHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    String studyId = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_ID);     
    putStudyMaterialOnRequest(Parser.parseInt(studyId), request);        
    
    String toBeDeleted = getSelectedMaterial(request);
    Validator.validateDeleteStudyMaterial(toBeDeleted);
    
    DeleteStudyMaterialCommand command = new DeleteStudyMaterialCommand();  
    command.setStudyId(Parser.parseInt(studyId));
    command.setSelectedMaterial(toBeDeleted);
    command.execute();
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_STUDY_MATERIAL, command.getResult());
    
    return WebConstants.VIEW_STUDY_MATERIAL;
  }
   
   /**
   * Returns a String containing Study Material Id's selected for deletion, separated by comma
   */
  private String getSelectedMaterial(HttpServletRequest request) throws UserException {
      
    Enumeration parameterNamesFromRequest = request.getParameterNames();
    
    StringBuffer toBeDeleted = new StringBuffer();
    while (parameterNamesFromRequest.hasMoreElements()) {
      String name = (String)parameterNamesFromRequest.nextElement();      
      //If the parameter is a checkbox and is marked, it is added to the list of items to be deleted.
      if (name != null && name.startsWith(WebKeys.REQUEST_PARAM_STUDY_MATERIAL_DELETE)) {
        toBeDeleted.append(request.getParameter(name));
        toBeDeleted.append(Constants.ID_SEPARATOR);
      }
    }
    
    
    if (toBeDeleted.length() > 0)
        toBeDeleted.deleteCharAt(toBeDeleted.length()-1);
      
    return toBeDeleted.toString();
  } 
  
  /**
   * Put the Study Material on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  protected void putStudyMaterialOnRequest(int studyId, HttpServletRequest request) throws Exception {
    
    GetStudyMaterialCommand command = new GetStudyMaterialCommand();
    command.setStudyId(studyId);
    command.setFromTemp(true);
    command.setForStudyMaterialWindow(true);
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_STUDY_MATERIAL, command.getResult());
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_ID, String.valueOf(studyId));      
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN; 
  }
        
}