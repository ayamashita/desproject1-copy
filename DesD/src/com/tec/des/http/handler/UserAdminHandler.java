package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetPeopleRoleCommand;
import com.tec.des.command.GetRoleCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.ExceptionMessages;

/**
 * 
 * Handles the User Administration use case.
 *
 * @author Per Kristian Foss
 */
public class UserAdminHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    String searchText = request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TEXT);  
    boolean onlyRole;
    if (request.getParameter(WebKeys.REQUEST_PARAM_ONLY_ROLE) == null)
        onlyRole = false;
    else onlyRole = true;
    
    GetPeopleRoleCommand command = new GetPeopleRoleCommand();
    command.setOnlyRole(onlyRole);
    command.setSearchText(searchText);
    command.execute();   
    
    if (command.getResult().isEmpty())
        throw new UserException(ExceptionMessages.NO_HITS); 
    request.setAttribute(WebKeys.REQUEST_BEAN_PEOPLE_ROLE, command.getResult());    
    
    GetRoleCommand command_role = new GetRoleCommand();
    command_role.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_ROLES, command_role.getResult());       
    
    return WebConstants.VIEW_USER_ADMIN;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_DATABASE_ADMIN;
  }
  
}