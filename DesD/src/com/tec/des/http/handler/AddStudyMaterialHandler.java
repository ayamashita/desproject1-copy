package com.tec.des.http.handler;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;

import com.tec.des.command.AddStudyMaterialCommand;
import com.tec.des.command.GetStudyMaterialCommand;
import com.tec.des.dto.StudyMaterialDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.http.FrontController;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.AccessController;
import com.tec.des.util.Constants;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Message;
import com.tec.des.util.Validator;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.GeneralException;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;
import com.tec.shared.util.Parser;


/**
 * Handels the Add Study Material use case. This handler extends HttpServlet
 * instead of UsecasHandler because the form submitting this request has
 * attribute enctype="multipart/form-data" and must therfore be treated differently.
 * This handler is called directly from the JSP and is thus a controller and 
 * handler combined for the Add Study Material use case .
 *
 * @author Per Kristian Foss
 */
public class AddStudyMaterialHandler extends HttpServlet {

  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  } 


/**
 * Calls the doPost with the request and the response.
 * 
 * @param request the ServletEngine created HttpServletRequest
 * @param response the ServletEngine created HttpServletResponse
 * @throw IOException, ServletException
 */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    doPost(request, response);  
  }

/**
 * Handles all the request for the handler.
 * 
 * @param request the ServletEngine created HttpServletRequest
 * @param response the ServletEngine created HttpServletResponse
 * @throw IOException, ServletException
 */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    //The view making the request is temporarily set to the resulting view.
    //In case of an exception the user is returned to this view.  
    String view = WebConstants.VIEW_STUDY_MATERIAL;

    try {

        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(WebKeys.SESSION_PARAM_USER) == null) {
            //Session time out, forward to login page
            view = WebConstants.VIEW_LOGIN;
        } else {
            //Check user rights
            UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER);
            if (!AccessController.hasAccess(user.getRoleId(), Constants.ROLE_STUDY_ADMIN)) 
                throw new UserException(ExceptionMessages.INSUFFICIENT_RIGHTS);
            
            view = doHandle(request);
        }

      forward(request, response, view);

      
    } catch (Exception ex) { 
        // The user receives an error message and is returned to the calling view or the error page.
        String errorMessage = ex.getMessage();
        if (!(ex instanceof GeneralException)) {
            errorMessage = ExceptionMessages.SYSTEM_EXCEPTION;
        } 
        request.setAttribute(WebKeys.REQUEST_ERROR_MESSAGE, new Message(errorMessage));
        LoggerFactory.getLogger(AddStudyMaterialHandler.class).error("Error.", ex);
        try { 
            forward(request, response, view);
        } catch (Exception e) {
            //Fatal error: Can not forward to calling view.
            LoggerFactory.getLogger(AddStudyMaterialHandler.class).fatal("Fatal exception.", e);
            throw new ServletException(e.getMessage()); 
        }
    }
   }
  

/**
 * Forwards the result to the passed in url.
 * 
 * @param request the ServletEngine created HttpServletRequest
 * @param response the ServletEngine created HttpServletResponse
 * @param url the url to forward to
 * @throws ServletException if an IOException is thrown by the RequestDispatcher.
 */
  protected void forward(HttpServletRequest request, HttpServletResponse response, String url) throws IOException, ServletException, SystemException {

    RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
    LoggerFactory.getLogger(FrontController.class).debug("Forward to " + url + " , dispatcher: " + rd.toString());
    if (rd == null) {
      throw new SystemException(ExceptionMessages.UNKNOWN_USECASE_VIEW);
    }  
    rd.forward(request, response); 

  }
  
  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    String studyId = new String();    
    
    String type = null;
    StudyMaterialDTO newMaterial = new StudyMaterialDTO();
    DiskFileUpload upload = new DiskFileUpload();
    List items = upload.parseRequest(request);    
    Iterator itr = items.iterator();

    while (itr.hasNext()) {
	FileItem item = (FileItem) itr.next();
        
	// check if the current item is a form field or an uploaded file
	if (item.isFormField()) {
            String fieldName = item.getFieldName();

            // Sets the attributes for the new Study Material to be added
            if (fieldName.equals(WebKeys.REQUEST_PARAM_STUDY_MATERIAL_DESCRIPTION))
                newMaterial.setDescription(item.getString());
            else if (fieldName.equals(WebKeys.REQUEST_PARAM_STUDY_MATERIAL_TYPE)) {
                if (item.getString().equals(WebConstants.STUDY_MATERIAL_ISURL)) { 
                    newMaterial.setIsFile(false);
                    type = WebConstants.STUDY_MATERIAL_ISURL;
                 } else if (item.getString().equals(WebConstants.STUDY_MATERIAL_ISFILE)) {
                    newMaterial.setIsFile(true);
                    type = WebConstants.STUDY_MATERIAL_ISFILE;
                }
            } else if (fieldName.equals(WebKeys.REQUEST_PARAM_STUDY_MATERIAL_URL) && type == WebConstants.STUDY_MATERIAL_ISURL) 
                newMaterial.setUrl(item.getString());
            else if (fieldName.equals(WebKeys.REQUEST_PARAM_STUDY_ID)) 
                studyId = item.getString();         	        	
	} else {              
            if (type.equals(WebConstants.STUDY_MATERIAL_ISFILE)) {
                if (!Nuller.isReallyNull(item.getName())) {
                    File fullFile  = new File(item.getName());
                    //Splits the pathname into pieces
                    String[] nv = item.getName().replace('\\','/').split("/");
                    //Selects the last piece, which is the filename
                    String filename = nv[nv.length-1];          
                    newMaterial.setFileName(filename);
                    File savedFile = new File(getServletContext().getRealPath("/"), fullFile.getName());                
                    item.write(savedFile);

                    if (savedFile.length() > 0) 
                        newMaterial.setFile(savedFile);
                }
            }
	}  
    }
    
    putStudyMaterialOnRequest(Parser.parseInt(studyId), newMaterial, request);
    Validator.validateStudyMaterial(newMaterial); 
    
    AddStudyMaterialCommand command = new AddStudyMaterialCommand(); 
    command.setStudyId(Parser.parseInt(studyId));
    command.setNewMaterial(newMaterial);    
    command.execute();    
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_STUDY_MATERIAL, command.getResult());
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_MATERIAL, new StudyMaterialDTO());  
    
    return WebConstants.VIEW_STUDY_MATERIAL;
  }
  
  /**
   * Put the previously selected study material and the newly added study 
   * material on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  protected void putStudyMaterialOnRequest(int studyId, StudyMaterialDTO newMaterial, HttpServletRequest request) throws Exception {
    
    GetStudyMaterialCommand command = new GetStudyMaterialCommand();
    command.setStudyId(studyId);
    command.setFromTemp(true);
    command.setForStudyMaterialWindow(true);
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_STUDY_MATERIAL, command.getResult());
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_ID, String.valueOf(studyId));
    
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_MATERIAL, newMaterial);  
  }
  
}