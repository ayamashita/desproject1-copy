package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.FindPublicationsCommand;
import com.tec.des.http.WebKeys;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.ExceptionMessages;
import com.tec.shared.util.Nuller;

/**
 * 
 * Handles the Find Publications use case.
 *
 * @author Per Kristian Foss
 */
public class FindPublicationsHandler extends PublicationHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    String selectedPublications = getSelectedPublications(request);
    if (!Nuller.isReallyNull(selectedPublications))
        findAndPutSelectedPublicationsOnRequest(selectedPublications, request);
    
    String searchText = request.getParameter(WebKeys.REQUEST_PARAM_SEARCH_TEXT);  
    FindPublicationsCommand command = new FindPublicationsCommand();
    command.setSearchText(searchText); 
    command.execute();       
    if (command.getResult().isEmpty())
        throw new UserException(ExceptionMessages.NO_HITS); 

    request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, command.getResult());       
    
    return view;
  }
  
 /**
  * Returns a String containing currently selected Publications Id's.
  */
  protected String getSelectedPublications(HttpServletRequest request) throws UserException {
  
    return request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PUBLICATION_IDS);
    
  }  
}