package com.tec.des.http.handler;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetSelectedPeopleCommand;
import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.util.Nuller;

/**
 * 
 * Initializes the Study Responsibles view.
 *
 * @author Per Kristian Foss
 */
public class OpenStudyResponsiblesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    GetAllPeopleCommand command_all = new GetAllPeopleCommand();   
    command_all.execute();     
    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command_all.getResult());
    
    String[] selectedPeople = null;
    //Fetch the study responsibles from the window opening this window.
    String studyResponsibles = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLE_IDS);
    if (!Nuller.isReallyNull(studyResponsibles)) 
        selectedPeople = studyResponsibles.split(Constants.ID_SEPARATOR);
    
    Vector selected = new Vector();
    if (selectedPeople != null) {
        GetSelectedPeopleCommand command_select = new GetSelectedPeopleCommand();   
        command_select.setSelectedPeople(selectedPeople);
        command_select.execute();
        selected = command_select.getResult();
    } else {
        StudyResponsibleDTO emptyResponsible = new StudyResponsibleDTO();
        emptyResponsible.setId(Constants.EMPTY_ID_STUDY_RESPONSIBLES);
        emptyResponsible.setListName(Constants.EMPTY_VALUE_STUDY_RESPONSIBLES);
        selected.add(emptyResponsible);
    }
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE, selected); 
    request.setAttribute(WebKeys.REQUEST_PARAM_TARGET_USECASE, request.getParameter(WebKeys.REQUEST_PARAM_TARGET_USECASE));
    return WebConstants.VIEW_STUDY_RESPONSIBLES; 
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}