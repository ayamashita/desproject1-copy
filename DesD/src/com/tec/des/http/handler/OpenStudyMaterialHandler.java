package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetStudyMaterialCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.util.Parser;

/**
 * 
 * Initializes the Select Study Material view.
 *
 * @author Per Kristian Foss
 */
public class OpenStudyMaterialHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {
 
    String studyId = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_ID); 
    
    GetStudyMaterialCommand command = new GetStudyMaterialCommand();   
    command.setStudyId(Parser.parseInt(studyId)); 
    command.execute(); 

    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_STUDY_MATERIAL, command.getResult());
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_ID, studyId); 
    
    return WebConstants.VIEW_STUDY_MATERIAL;
  }
 
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
   public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
   }

}