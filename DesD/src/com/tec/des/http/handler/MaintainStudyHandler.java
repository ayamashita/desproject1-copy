package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.FindStudiesCommand;
import com.tec.des.command.GetStudyDurationUnitsCommand;
import com.tec.des.command.GetStudyMaterialCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.dto.SearchDTO;
import com.tec.des.dto.StudyDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.GeneralException;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.ExceptionMessages;
import com.tec.shared.util.HitList;
import com.tec.shared.util.Nuller;
import com.tec.shared.util.Parser;

/**
 * Abstract super class for all handler classes conserning maintenance of a study, 
 * containing common parameters and methods used to handle these usecases.
 * After add, update or delete is performed on a study or cancelation of one of
 * these actions, all studies is fetched and shown in the study overview report. 
 * 
 * @author Norunn Haug Christensen
 */
public abstract class MaintainStudyHandler extends UsecaseHandler {

    private int maxHitListLength = WebConstants.MAX_HIT_LIST_LENGTH;
    private int startPosition = WebConstants.DEFAULT_START_POSITION;
    protected String view = WebConstants.VIEW_STUDY_OVERVIEW_REPORT;
    
  /**
   * Empty constructor is necessary because this object is instanitated
   * using the class loader.
   */
    public MaintainStudyHandler() {
    }
    
  /**
   * Fetches all studies.
   *
   * @return HitList containing all studies. 
   */
  protected HitList getAllStudies() throws Exception {
    FindStudiesCommand command = new FindStudiesCommand();
    command.setSearchCriteria(getSearchCriteria());
    command.setMaxHitListLength(maxHitListLength);
    command.setStartPosition(startPosition);
    command.execute();

    if (command.getResult().isEmpty())
        throw new UserException(ExceptionMessages.NO_HITS); 
    
    return command.getResult();
      
  }
  
  /**
   * Returns a 'empty' searchDTO with default sort attributes.
   * @return SearchDTO 
   */
  protected SearchDTO getSearchCriteria() {
      SearchDTO search = new SearchDTO();
      search.setSortBy(WebConstants.SORT_BY_END_OF_STUDY);
      search.setSortOrder(WebConstants.SORT_ORDER_DESCENDING);
      
      return search;
  }

  /**
   * Fetches the StudyDTO from the request.
   * @return StudyDTO 
   */
  protected StudyDTO getStudyFromRequest(HttpServletRequest request) throws GeneralException {
      StudyDTO study = new StudyDTO();
      study.setId(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_ID)));
      study.setName(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_NAME));
      study.setTypeId(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_TYPE)));      
      study.setDescription(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_DESCRIPTION));
      study.setDuration(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_DURATION));
      study.setStartDate(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_START_DATE));
      study.setEndDate(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_END_DATE));
      study.setKeywords(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_KEYWORDS));
      study.setNoOfProfessionalParticipants(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_NO_PROFESSIONALS));
      study.setNoOfStudentParticipants(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_NO_STUNDENTS));
      study.setNotes(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_NOTES));
      study.setOwner(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_OWNER));
      study.setLastEditedBy(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_LAST_EDITED_BY));

      String parameter = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_DURATION_UNIT);
      if (!Nuller.isReallyNull(parameter)) {
        study.setDurationUnitId(Parser.parseInt(parameter));
      }
      
      parameter = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLE_IDS);
      if (!Nuller.isReallyNull(parameter)) {
        study.setResponsibles(
            parameter.split(Constants.ID_SEPARATOR), 
            request.getParameter(WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLES).split(Constants.ID_SEPARATOR));
      }
      
      parameter = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_PUBLICATION_IDS);
      if (!Nuller.isReallyNull(parameter))
        study.setPublications(
            parameter.split(Constants.ID_SEPARATOR), 
            request.getParameter(WebKeys.REQUEST_PARAM_STUDY_PUBLICATIONS).split(Constants.ID_SEPARATOR));
      
      if (Parser.parseBoolean(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_MATERIAL))) {
          GetStudyMaterialCommand command = new GetStudyMaterialCommand();
          command.setStudyId(study.getId());
          command.setForStudyMaterialWindow(true);
          command.setDeleteNotConfirmed(false);
          command.execute();
          study.setMaterial(command.getResult());
      }
          
          
      return study;
  }

  /**
   * Put the study, study duration units and the study types on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  protected void putParametersOnRequest(StudyDTO study, HttpServletRequest request) throws Exception {
    
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY, study);

    GetStudyTypesCommand typesCommand = new GetStudyTypesCommand();
    typesCommand.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, typesCommand.getResult());
    
    GetStudyDurationUnitsCommand unitsCommand = new GetStudyDurationUnitsCommand();
    unitsCommand.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_DURATION_UNITS, unitsCommand.getResult());
      
  }
  
  /**
   * Handle the use case and returns the name of the resulting view.
   * This method is implemented in the subclasses.
   *
   * @return The name of the resulting view. 
   */
  public abstract String doHandle(HttpServletRequest request) throws Exception;
  
  /**
   * Return the required role for the usecase.
   * This method is implemented in the subclasses.
   *
   * @return The role required to execute the usecase. 
   */
  public abstract int getRequiredRole();
      
  
}
