package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.http.WebKeys;
import com.tec.shared.exceptions.UserException;

/**
 * 
 * Initializes the Select Publications view.
 * The doHandle method is implemented in the abstract superclass, 
 * Publication Handler. 
 *
 * @author Per Kristian Foss
 */
public class OpenPublicationsHandler extends PublicationHandler {

   /**
   * Returns a String containing currently selected Publications Id's.
   */
  protected String getSelectedPublications(HttpServletRequest request) throws UserException {
  
    return request.getParameter(WebKeys.REQUEST_PARAM_STUDY_PUBLICATION_IDS);
    
  }
 
}