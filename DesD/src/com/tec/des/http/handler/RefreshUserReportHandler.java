package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.http.WebKeys;

public class RefreshUserReportHandler extends UserReportHandler {

	/* (non-Javadoc)
	 * @see com.tec.des.http.handler.UsecaseHandler#doHandle(javax.servlet.http.HttpServletRequest)
	 */
	public String doHandle(HttpServletRequest request) throws Exception {
		request.setAttribute(WebKeys.REQUEST_BEAN_USER_REPORT, getReportFromRequest(request));
	    return getView();
	}

}
