package com.tec.des.http.handler;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;

/**
 * 
 * Handles the Select Publications use case.
 * The doHandle method is implemented in the abstract superclass, Publication Handler. 
 *
 * @author Per Kristian Foss
 */
public class SelectPublicationsHandler extends PublicationHandler {

  /**
   * Returns a String containing currently selected Publications Id's.
   * @throws UserException
   */
  protected String getSelectedPublications(HttpServletRequest request) throws UserException {
    
    String selectedPublications; 
    if (request.getParameter(WebKeys.REQUEST_PARAM_BUTTON_ADD) != null) {
        selectedPublications = getSelectedPublicationsAdd(request);
    } else {
        selectedPublications = getSelectedPublicationsRemove(request);
    }
  
    return selectedPublications;
    
  }
 
  /**
   * Returns a String containing currently selected Publications Id's.
   * The string containg the previously selected, and the newly selected ids.
   * @throws UserException
   */
  private String getSelectedPublicationsAdd(HttpServletRequest request) throws UserException {
      
    String oldSelectedStr = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PUBLICATION_IDS);
    String[] oldSelected = null;
    if (!Nuller.isReallyNull(oldSelectedStr))
        oldSelected = oldSelectedStr.split(Constants.ID_SEPARATOR);
    
    StringBuffer newSelected = new StringBuffer();
    HashSet toBeAdded = new HashSet();
    
    Enumeration parameterNamesFromRequest = request.getParameterNames();
    while (parameterNamesFromRequest.hasMoreElements()) {
      String name = (String)parameterNamesFromRequest.nextElement();      
      //If the parameter is a checkbox and is marked, it is added to the list of items to be added.
      if (name != null && name.startsWith(WebKeys.REQUEST_PARAM_PUBLICATION_ADD)) {
        toBeAdded.add(request.getParameter(name));
      }
    }
    
    if (toBeAdded.size() == 0)
        newSelected.append(oldSelectedStr);
    else {

        if (oldSelected != null) {
            //Remove duplicates from old and new.
            for (int i=0; i<oldSelected.length; i++) {
                if (toBeAdded.contains(oldSelected[i]))
                    toBeAdded.remove(oldSelected[i]);
            }
            newSelected.append(oldSelectedStr).append(Constants.ID_SEPARATOR);;
        }
        
        Iterator it = toBeAdded.iterator();
        while (it.hasNext()) {
            newSelected.append((String)it.next());
            if (it.hasNext())
                newSelected.append(Constants.ID_SEPARATOR);
        }

        if (newSelected.toString().endsWith(Constants.ID_SEPARATOR))
            newSelected.deleteCharAt(newSelected.length()-1);
    }
   
    return newSelected.toString();
  }

   /**
   * Returns a String containing currently selected Publications Id's.
   * @throws UserException
   */
  private String getSelectedPublicationsRemove(HttpServletRequest request) throws UserException {
      
    String[] oldSelected = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PUBLICATION_IDS).split(Constants.ID_SEPARATOR);  
    StringBuffer newSelected = new StringBuffer();
    Enumeration parameterNamesFromRequest = request.getParameterNames();
    
    Vector toBeRemoved = new Vector();
    while (parameterNamesFromRequest.hasMoreElements()) {
      String name = (String)parameterNamesFromRequest.nextElement();      
      //If the parameter is a checkbox and is marked, it is added to the list of items to be removed.
      if (name != null && name.startsWith(WebKeys.REQUEST_PARAM_PUBLICATION_REMOVE)) {
        toBeRemoved.add(request.getParameter(name));
      }
    }
    
    if (!(toBeRemoved == null)) {
        for (int i = 0; i < oldSelected.length; i++) {
            if (!(toBeRemoved.contains(oldSelected[i])))
                newSelected.append(oldSelected[i] + Constants.ID_SEPARATOR);        
        }
    }
    
    if (!(newSelected.length() == 0))
        newSelected.deleteCharAt(newSelected.length()-1);
   
    return newSelected.toString();
  }

}