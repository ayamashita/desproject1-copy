package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.DeleteUserReportCommand;

public class DeleteUserReportHandler extends SaveUserReportHandler {

	public String doHandle(HttpServletRequest request) throws Exception {
		return processCommand(request, new DeleteUserReportCommand());
	}

	
}
