package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.UpdateTextOnAdminPageCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;

/**
 * 
 * Handles the edit admin page usecase.
 *
 * @author Norunn Haug Christensen
 */
public class EditAdminPageHandler extends UsecaseHandler { 

  /**
   *  Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception { 

    UpdateTextOnAdminPageCommand command = new UpdateTextOnAdminPageCommand();
    command.setText(request.getParameter(WebKeys.REQUEST_PARAM_ADMIN_PAGE_TEXT));
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_ADMIN_TEXT, command.getResult());
    
    return WebConstants.VIEW_ADMIN_PAGE;
  
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_DATABASE_ADMIN;
  }
  
}
