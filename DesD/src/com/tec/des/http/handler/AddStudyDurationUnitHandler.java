package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tec.des.command.AddStudyDurationUnitCommand;
import com.tec.des.command.GetStudyDurationUnitsCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;

/**
 * 
 * Handles the add study duration unit use case. 
 *
 * @author Norunn Haug Christensen
 */
public class AddStudyDurationUnitHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    putStudyDurationUnitsOnRequest(request);  
    String newDurationUnit = request.getParameter(WebKeys.REQUEST_PARAM_ADD_STUDY_DURATION_UNIT);
    Validator.validateAddStudyDurationUnit(newDurationUnit);
    
    AddStudyDurationUnitCommand command = new AddStudyDurationUnitCommand();
    command.setDurationUnit(newDurationUnit);
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_DURATION_UNITS, command.getResult());  
    
    HttpSession session = request.getSession(false);
    
    return WebConstants.VIEW_STUDY_DURATION_UNITS;
  }
  
  /**
   * Put study duration units on the request so that they are available
   * if an exception occur and the user is returned to the sender view.
   * @throws Exception
   */
  private void putStudyDurationUnitsOnRequest(HttpServletRequest request) throws Exception {
    
    GetStudyDurationUnitsCommand command = new GetStudyDurationUnitsCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_DURATION_UNITS, command.getResult());
      
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}