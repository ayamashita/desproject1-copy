package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetPeopleRoleCommand;
import com.tec.des.command.GetRoleCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;

/**
 * 
 * Initializes the User Administration view.
 *
 * @author Per Kristian Foss
 */
public class OpenUserAdminHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {
          
    GetPeopleRoleCommand command = new GetPeopleRoleCommand();
    command.setOnlyRole(true);
    command.execute();   
    GetRoleCommand command_role = new GetRoleCommand();
    command_role.execute();
        
    request.setAttribute(WebKeys.REQUEST_BEAN_PEOPLE_ROLE, command.getResult());       
    request.setAttribute(WebKeys.REQUEST_BEAN_ROLES, command_role.getResult());       
    
    return WebConstants.VIEW_USER_ADMIN;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_DATABASE_ADMIN;
  }
  
}