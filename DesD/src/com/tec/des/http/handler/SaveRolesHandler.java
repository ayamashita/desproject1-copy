package com.tec.des.http.handler;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetRoleCommand;
import com.tec.des.command.GetTextOnAdminPageCommand;
import com.tec.des.command.SaveRolesCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.UserException;


/**
 * 
 * Handles the save roles use case. 
 *
 * @author Per Kristian Foss
 */
public class SaveRolesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {
  
    Hashtable peopleRole = getPeopleRole(request);    
    
    SaveRolesCommand command = new SaveRolesCommand();
    command.setPeopleRole(peopleRole);
    command.execute();
    
    GetRoleCommand command_role = new GetRoleCommand();
    command_role.execute();
    GetTextOnAdminPageCommand command_text = new GetTextOnAdminPageCommand();
    command_text.execute();
    
    request.setAttribute(WebKeys.REQUEST_BEAN_ADMIN_TEXT, command_text.getResult());       
    return WebConstants.VIEW_ADMIN_PAGE;
  }
  
  /**
   * Returns a Hashtable containing people with their corresponding role
   * @throws UserException
   */
  private Hashtable getPeopleRole(HttpServletRequest request) throws UserException {
     
    Hashtable peopleRole = new Hashtable();  
      
    Enumeration parameterNamesFromRequest = request.getParameterNames();
    Vector people = new Vector();
    while (parameterNamesFromRequest.hasMoreElements()) {
      String name = (String)parameterNamesFromRequest.nextElement();
      if (name != null && name.startsWith(WebKeys.REQUEST_PARAM_ROLES)) {
        people.add(name); 
      } 
    }  
    Iterator it = people.iterator();
    while (it.hasNext()) {
        String param = (String)it.next();
        peopleRole.put(param.substring(WebKeys.REQUEST_PARAM_ROLES.length()),request.getParameter(param));       
    }   
    return peopleRole;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_DATABASE_ADMIN;
  }
  
}