package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;

/**
 * 
 * Handles the logout usecase. The user is logged out, and
 * the list studies view is shown.
 *
 * @author Norunn Haug Christensen
 */
public class LogoutHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    HttpSession session = request.getSession(false);  
    if (session != null)
        session.removeAttribute(WebKeys.SESSION_PARAM_USER);
    
    GetStudyTypesCommand command = new GetStudyTypesCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult());

    return WebConstants.VIEW_LIST_STUDIES;
    
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}