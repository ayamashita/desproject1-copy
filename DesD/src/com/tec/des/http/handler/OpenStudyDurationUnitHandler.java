package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetStudyDurationUnitsCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.des.util.Constants;

/**
 * 
 * Initializes the Edit Study Types view. 
 *
 * @author Per Kristian Foss
 */
public class OpenStudyDurationUnitHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {
       
    GetStudyDurationUnitsCommand command = new GetStudyDurationUnitsCommand();   
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_DURATION_UNITS, command.getResult());  

    return WebConstants.VIEW_STUDY_DURATION_UNITS;
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
  
}