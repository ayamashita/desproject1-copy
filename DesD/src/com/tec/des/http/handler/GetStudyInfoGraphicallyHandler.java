package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;

import com.tec.des.command.GetStudyInfoGraphicallyCommand;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;
import com.tec.shared.util.Nuller;

/**
 * 
 * Handles the report aggregated study information graphically use case. 
 *
 * @author Norunn Haug Christensen
 */
public class GetStudyInfoGraphicallyHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    GetStudyInfoGraphicallyCommand command = new GetStudyInfoGraphicallyCommand();
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, command.getResult().getStudyTypesAndColour()); 
    request.setAttribute(WebKeys.REQUEST_BEAN_GRAPHICAL_REPORT_ITEMS, command.getResult().getGraphicalItems());

    return WebConstants.VIEW_GRAPHICAL_REPORT;
    
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}