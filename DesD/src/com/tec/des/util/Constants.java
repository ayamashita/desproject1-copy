package com.tec.des.util;

import java.util.ResourceBundle;

/**
 * Class containig various constants and methods used in the application.
 *
 * @author  Norunn Haug Christensen
 */
public class Constants {
    
 /**
  * The class can not be instantiated
  */
  private Constants() {
  }
  
  public final static String DES_PROPERTIES_FILE = "des";
  public static final ResourceBundle MESSAGE_RESOURCES = ResourceBundle.getBundle("des");
  public static final String USER_AUTHENTICATION_SERVER_URL = MESSAGE_RESOURCES.getString("user.authentication.serverUrl");
  public static final String SIMULAWEB_SERVICE_URL = MESSAGE_RESOURCES.getString("simulaWeb.serverUrl");
  public static final String SIMULAWEB_SERVICE_METHOD_PREFIX = MESSAGE_RESOURCES.getString("simulaWeb.methodPrefix");

  public static final String RCP_AUTHENTICATION_METHOD = "check_credentials";
    
  public final static String DATE_FORMAT_PRESENTATION = "dd. MMM yyyy";
  public final static String DATE_FORMAT_EDIT = "yyyy/MM/dd"; 
  public final static String DATE_FORMAT_DATABASE = "yyyyMMdd";
  
  public final static String ERROR_MESSAGE_SEPARATOR = " <BR> ";
  public final static String DATE_SEPARATOR = "/";
  public final static String ID_SEPARATOR = ",";
  
  public final static int MAX_FILE_SIZE = 16777216;
  
  public final static String EMPTY_VALUE_STUDY_RESPONSIBLES = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  public final static String EMPTY_ID_STUDY_RESPONSIBLES = "-1";
  
  //Access
  public final static int ROLE_DATABASE_ADMIN = 1;
  public final static int ROLE_STUDY_ADMIN = 2;
  
  //Maintain modes
  public final static int MAINTAIN_MODE_FINAL = 0;
  public final static int MAINTAIN_MODE_TEMP_NEW = 1;
  public final static int MAINTAIN_MODE_TEMP_EDIT = 2;
  
  public final static int MODE_OK = 0;
  public final static int MODE_CANCEL = 1;

}
