package com.tec.des.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import com.tec.des.util.Constants;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Validator;
import com.tec.server.db.ResultSetHelper;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;

/**
 * Class encapsulating Study types data access.
 *
 * @author :  Per Kristian Foss
 */
public class StudyTypeDAO extends DAO {

 /**
  * Constructs a StudyTypesDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
    public StudyTypeDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a hashtable containing all study types. 
  * @return Hashtable
  * @throws SQLException
 * @throws SystemException 
  */

  public Hashtable getStudyTypes() throws SQLException, SystemException {

    Hashtable studyTypes = new Hashtable();
        
    StringBuffer query = new StringBuffer();    
    query.append("select study_type_id, study_type from study_type order by study_type;");
    
    LoggerFactory.getLogger(StudyTypeDAO.class).debug(query.toString()); 
    
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);

    while (rs.next()) {
        studyTypes.put(String.valueOf(rsh.getInt("study_type_id")), rsh.getString("study_type"));
    }
    
    rs.close();
    stmt.close();
    
    return studyTypes;
  }
  
 /**
  * Adds new study type to the list of study types.
  * @param the study type to add
  * @throws UserException, SQLException
 * @throws SystemException 
  */
  public void addStudyType(String studyType) throws UserException, SQLException, SystemException {

    StringBuffer query1 = new StringBuffer();
    StringBuffer query2 = new StringBuffer();    
    //Checks if new study type to be added already exists. If it does, throw UserException                
    String checkedStudyType = Validator.checkEscapes(studyType);                 
    query1.append("select study_type from study_type where study_type = '" + checkedStudyType + "';");
    
    LoggerFactory.getLogger(StudyTypeDAO.class).debug(query1.toString()); 
    
    Statement stmt = getConnection().createStatement();
    ResultSet studyTypeExists = stmt.executeQuery(query1.toString());
    try {
        if (!studyTypeExists.next()) {
            query2.append("insert into study_type (study_type) values ('" + checkedStudyType + "');");
            stmt.executeUpdate(query2.toString());    
        } else {
            throw new UserException(ExceptionMessages.STUDY_TYPE_EXISTS + studyType);        
        }
    } finally {
        studyTypeExists.close();
        stmt.close();
    }
  }
  
 /**
  * Deletes study types from the list of study types.
  * @param the study types to delete.
  * @throws UserException, SQLException
 * @throws SystemException 
  */
  public void deleteStudyType(String[] studyTypes) throws UserException, SQLException, SystemException {
      
    StringBuffer query_check = new StringBuffer(); 
    StringBuffer query = new StringBuffer();     
    StringBuffer deletelist = new StringBuffer();
     
    for (int i = 0; i < studyTypes.length; i++) {
        deletelist.append(studyTypes[i]);
        deletelist.append(Constants.ID_SEPARATOR);
    }
    deletelist.deleteCharAt(deletelist.length()-1);
  
    query_check.append("select s.study_type_id, st.study_type from study s, study_type st ");
    query_check.append("where s.study_type_id = st.study_type_id and st.study_type_id in (");
    query_check.append(deletelist + ") group by s.study_type_id, st.study_type_id;");   
    
    LoggerFactory.getLogger(StudyTypeDAO.class).debug(query_check.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query_check.toString());     
    try {
        if (!rs.next()) {
            query.append("delete from study_type where study_type_id in (");
            query.append(deletelist + ");");   
            stmt.executeUpdate(query.toString()); 
        } else {
            StringBuffer inUse = new StringBuffer();
            inUse.append(ExceptionMessages.STUDY_TYPES_IN_USE_PART1 + rs.getString("study_type") + ", ");
            while (rs.next()) {
                inUse.append(rs.getString("study_type"));
                inUse.append(", ");
            }
            inUse.deleteCharAt(inUse.length()-2);
            inUse.append(ExceptionMessages.STUDY_TYPES_IN_USE_PART2);
            throw new UserException(inUse.toString());
        }
    } finally {
        rs.close();
        stmt.close();
    }
  }

}