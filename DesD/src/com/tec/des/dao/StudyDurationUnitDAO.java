package com.tec.des.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import com.tec.des.util.Constants;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Validator;
import com.tec.server.db.ResultSetHelper;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;

/**
 * Class encapsulating Study duration unit data access.
 *
 * @author :  Per Kristian Foss
 */
public class StudyDurationUnitDAO extends DAO {

 /**
  * Constructs a StudyTypesDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
    public StudyDurationUnitDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a hashtable containing all duration units. 
  * @return Hashtable
  * @throws SQLException
 * @throws SystemException 
  */

  public Hashtable getDurationUnits() throws SQLException, SystemException {

    Hashtable durationUnits = new Hashtable();
        
    StringBuffer query = new StringBuffer();    
    query.append("select study_duration_unit_id, study_duration_unit from study_duration_unit order by study_duration_unit;");
    
    LoggerFactory.getLogger(StudyDurationUnitDAO.class).debug(query.toString()); 
    
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);

    while (rs.next()) {
        durationUnits.put(String.valueOf(rsh.getInt("study_duration_unit_id")), rsh.getString("study_duration_unit"));
    }
    
    rs.close();
    stmt.close();
    
    return durationUnits;
  }
  
 /**
  * Adds new duration unit to the list of duration units.
  * @param the duration unit to add
  * @throws UserException, SQLException
 * @throws SystemException 
  */
  public void addDurationUnit(String durationUnit) throws UserException, SQLException, SystemException {

    StringBuffer query1 = new StringBuffer();
    StringBuffer query2 = new StringBuffer();    
    
    String checkedDurationUnit = Validator.checkEscapes(durationUnit); 
    //Checks if the duration unit to be added already exists. If it does, throw UserException                
                    
    query1.append("select study_duration_unit from study_duration_unit where study_duration_unit = '" + checkedDurationUnit + "';");
    
    LoggerFactory.getLogger(StudyDurationUnitDAO.class).debug(query1.toString()); 
    
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query1.toString());
    try {
        if (!rs.next()) {
            query2.append("insert into study_duration_unit (study_duration_unit) values ('" + checkedDurationUnit + "');");
            stmt.executeUpdate(query2.toString());    
        } else {
            throw new UserException(ExceptionMessages.STUDY_DURATION_UNIT_EXISTS + durationUnit);        
        }
    } finally {
        rs.close();
        stmt.close();
    }
  }
  
 /**
  * Deletes duration units from the list of duration units.
  * @param the duration units to delete.
  * @throws UserException, SQLException
 * @throws SystemException 
  */
  public void deleteDurationUnits(String[] durationUnits) throws UserException, SQLException, SystemException {
      
    StringBuffer query_check = new StringBuffer(); 
    StringBuffer query = new StringBuffer();     
    StringBuffer deletelist = new StringBuffer();
     
    for (int i = 0; i < durationUnits.length; i++) {
        deletelist.append(durationUnits[i]);
        deletelist.append(Constants.ID_SEPARATOR);
    }
    deletelist.deleteCharAt(deletelist.length()-1);
  
    query_check.append("select sd.study_duration_unit from study s, study_duration_unit sd ");
    query_check.append("where s.study_duration_unit_id = sd.study_duration_unit_id and sd.study_duration_unit_id in (");
    query_check.append(deletelist + ") group by s.study_duration_unit_id, sd.study_duration_unit_id;");   
    
    LoggerFactory.getLogger(StudyDurationUnitDAO.class).debug(query_check.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query_check.toString());     
    try {
        if (!rs.next()) {
            query.append("delete from study_duration_unit where study_duration_unit_id in (");
            query.append(deletelist + ");");   
            stmt.executeUpdate(query.toString()); 
        } else {
            StringBuffer inUse = new StringBuffer();
            inUse.append(ExceptionMessages.STUDY_DURATION_UNITS_IN_USE_PART1 + rs.getString("study_duration_unit") + ", ");
            while (rs.next()) {
                inUse.append(rs.getString("study_duration_unit"));
                inUse.append(", ");
            }
            inUse.deleteCharAt(inUse.length()-2);
            inUse.append(ExceptionMessages.STUDY_DURATION_UNITS_IN_USE_PART2);
            throw new UserException(inUse.toString());
        }
    } finally {
        rs.close();
        stmt.close();
    }
  }

}