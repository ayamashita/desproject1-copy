package com.tec.des.dao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enum.Enum;

import com.tec.des.decorators.Decorator;
import com.tec.des.decorators.DecoratorMaterial;
import com.tec.des.decorators.DecoratorName;
import com.tec.des.decorators.DecoratorPeople;
import com.tec.des.decorators.DecoratorPublication;
import com.tec.des.dto.GraphicalDTO;
import com.tec.des.dto.GraphicalReportDTO;
import com.tec.des.dto.GraphicalStudyTypeDTO;
import com.tec.des.dto.PublicationDTO;
import com.tec.des.dto.SearchDTO;
import com.tec.des.dto.StudyDTO;
import com.tec.des.dto.StudyMaterialDTO;
import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.util.ColourGenerator;
import com.tec.des.util.Constants;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Validator;
import com.tec.server.db.ResultSetHelper;
import com.tec.server.db.SQLHelper;
import com.tec.server.db.WhereOrAndHelper;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Formatter;
import com.tec.shared.util.HitList;
import com.tec.shared.util.Nuller;
import com.tec.shared.util.Parser;

/**
 * Class encapsulating Study data access.
 *
 * @author :  Norunn Haug Christensen
 */
public class StudyDAO extends DAO {
    
    private final static String STUDY_COLUMNS = "(study_type_id, study_name, study_description, " +
        "study_end_date, study_owner, last_edited_by, study_duration, study_duration_unit_id, " +
        "study_keywords, no_professionals, no_students, study_notes, study_start_date) ";
    
    public final static class ReportColumns  extends Enum {
    
        public static final ReportColumns NAME = new ReportColumns("study_name", "name", "Study Name", new DecoratorName(), true);
        public static final ReportColumns TYPE = new ReportColumns("study_type", "type", "Type of Study", new Decorator(), true);
        public static final ReportColumns DESCRIPTION = new ReportColumns("study_description", "shortDescription", "Study Description", new Decorator(), true);
        public static final ReportColumns KEYWORDS = new ReportColumns("study_keywords", "keywords", "Keywords", new Decorator(), false);
        public static final ReportColumns NOTES = new ReportColumns("study_notes", "notes", "Study Notes", new Decorator(), false);
        public static final ReportColumns STUDENTS = new ReportColumns("no_students", "noOfStudentParticipants", "No. of Students", new Decorator(), true);
        public static final ReportColumns PROFESSIONALS = new ReportColumns("no_professionals", "noOfProfessionalParticipants", "No. of Professionals", new Decorator(), true);
        public static final ReportColumns DURATION = new ReportColumns("study_duration", "duration", "Duration of Study", new Decorator(), false);
        public static final ReportColumns DURATION_UNIT = new ReportColumns("study_duration_unit", "durationUnit", "Duration Unit", new Decorator(), false);
        public static final ReportColumns START_DATE = new ReportColumns("study_start_date", "startDate", "Start of Study", new Decorator(), true);
        public static final ReportColumns END_DATE = new ReportColumns("study_end_date", "endDate", "End Date", new Decorator(), true);
        public static final ReportColumns CREATED_BY = new ReportColumns("study_owner", "owner", "Study Owner", new Decorator(), true);
        public static final ReportColumns EDITED_BY = new ReportColumns("last_edited_by", "lastEditedBy", "Last Edited by", new Decorator(), true);
        public static final ReportColumns RESPONSIBLES = new ReportColumns("responsible_id", "responsibles", "Study Responsible", new DecoratorPeople(), false); 
        public static final ReportColumns PUBLICATIONS = new ReportColumns("study_publications", "publications", "Publication", new DecoratorPublication(), false);
        public static final ReportColumns MATERIALS = new ReportColumns("study_materials", "material", "Study Material", new DecoratorMaterial(), false);
    	
        public static final ArrayList DEFAULT_COLUMNS = new ArrayList(Arrays.asList(new String[] {NAME.getName(), TYPE.getName(), DESCRIPTION.getName(), END_DATE.getName(), CREATED_BY.getName(), RESPONSIBLES.getName(), PUBLICATIONS.getName()}));
        private String property;
        private String label;
        private Decorator decorator;
        private boolean sort;
        
		protected ReportColumns(String columnName, String getter, String label, Decorator decorator, boolean sort) {
			super(columnName);
			this.property = getter;
			this.label = label;
			this.decorator = decorator;
			this.sort = sort;
		}
		
    	public static List getEnumList() {
    		return getEnumList(ReportColumns.class);
    	}
    	
    	public static ArrayList getColumnNameList() {
    		ArrayList list = new ArrayList();
    		
    		for (Iterator iterator = getEnumList(ReportColumns.class).iterator(); iterator.hasNext();) {
    			ReportColumns column = (ReportColumns) iterator.next();
    			list.add(column.getName());
    		}
    		
      	  return list;
      	}

      	/*
      	 * @see java.lang.Object#toString()
      	 */
      	public String toString() {
      		return getName();
      	}
      	
      	/**
      	 * @param operator
      	 * @return
      	 */
      	public static ReportColumns valueOf(String operator) {
      		if (StringUtils.isEmpty(operator)) {
      			return null;
      		}
      		
      	  return (ReportColumns) getEnum(ReportColumns.class, operator);
      	}

		public String getProperty() {
			return property;
		}

		public boolean isSort() {
			return sort;
		}

		public String decorate(StudyDTO study, Decorator.Type type, HttpServletRequest request) {
			if (decorator == null) {
				return "";
			}
			return decorator.decorate(study, getProperty(), type, request);
		}

		public String getLabel() {
			return label;
		}
    }
 /**
  * Constructs a StudyDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
  public StudyDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns the study with the specified id.
  * @param id The id of the study to fetch.
  * @param readOnly Indicate if the study fetched will be shown in readonly mode
  * or editable mode. 
 * @throws Exception 
  */
  public StudyDTO getStudy(int id, boolean readOnly) throws Exception {
    
    StringBuffer query = new StringBuffer();
    query.append("select s.*, st.study_type, sd.study_duration_unit");
    boolean useTemp = !readOnly;
	
    query.append(" from study_type st, study s ");
    query.append(" left join (study_duration_unit sd) on s.study_duration_unit_id = sd.study_duration_unit_id "); 
    query.append(" where s.study_type_id = st.study_type_id ");
    query.append(" and s.study_id = " + id + ";");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    StudyDTO study = null;
    try {
        if (rs.next()) {
            study = getStudyDTOFromResultSet(rsh, readOnly?Constants.DATE_FORMAT_PRESENTATION : Constants.DATE_FORMAT_EDIT);
        } else {
            throw new UserException(ExceptionMessages.NO_STUDY);
        }
       
        if (useTemp) {
            //Clean up in temp tables.
            deleteRelationsFromTemp(study.getId(), stmt, false);
            //The study will be opened in edit mode and is copyed to temp table.
            copyStudyToTemp(study.getId(), stmt);
        }

       	study.setResponsibles(getStudyResponsibles(study.getId(), useTemp));
       	study.setPublications(getPublications(study.getId(), useTemp));
       	study.setMaterial(getStudyMaterial(study.getId(), useTemp, true));
    
    } finally {
        rs.close();
        stmt.close();
    }
       
    return study;

  }

 /**
  * Adds a new study based on the specified study object. After the study is
  * added, the corresponding study in temp_study is deleted.
  * @param study The study to be added.
  * @throws SQLException, UserException
 * @throws SystemException 
  */
  public void addStudy(StudyDTO study) throws SQLException, UserException, SystemException {
    //Fetch the id of the study in temp_study
    int tempId = study.getId();
    
    validateStudy(study.getName(), Nuller.getIntNull(), study.getTypeId());  
    StringBuffer addStudyQuery = new StringBuffer();    
    addStudyQuery.append("insert into study " + STUDY_COLUMNS + "values (");
    addStudyQuery.append(study.getTypeId() + ", ");
    addStudyQuery.append("'" + Validator.checkEscapes(study.getName()) + "', ");
    addStudyQuery.append("'" + Validator.checkEscapes(study.getDescription()) + "', ");
    addStudyQuery.append("'" + Formatter.removeCharFromString(study.getEndDate(), Constants.DATE_SEPARATOR) + "', ");
    addStudyQuery.append("'" + study.getOwner() + "', ");
    addStudyQuery.append("'" + study.getLastEditedBy() + "', ");
    addStudyQuery.append(SQLHelper.getValue(study.getDuration()) + ", ");
    addStudyQuery.append(SQLHelper.getValue(study.getDurationUnitId()) + ", ");
    addStudyQuery.append(SQLHelper.getValue(Validator.checkEscapes(study.getKeywords())) + ", ");
    addStudyQuery.append(SQLHelper.getValue(study.getNoOfProfessionalParticipants()) + ", ");
    addStudyQuery.append(SQLHelper.getValue(study.getNoOfStudentParticipants()) + ", ");
    addStudyQuery.append(SQLHelper.getValue(Validator.checkEscapes(study.getNotes())) + ", ");
    addStudyQuery.append(SQLHelper.getValue(Formatter.removeCharFromString(study.getStartDate(), Constants.DATE_SEPARATOR)) + ");");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(addStudyQuery.toString()); 
    
    Statement stmt = getConnection().createStatement();
    stmt.executeUpdate(addStudyQuery.toString());

    //Fetch the id of the inserted study
    ResultSet rs = stmt.executeQuery("select max(study_id) as id from study;");
    if (rs.next())
        study.setId(rs.getInt("id"));
    rs.close();
    
    //Add relations by fetching them from temp
    copyStudyRelationsFromTempForAdd(study.getId(), tempId, stmt);
    
    //Delete the temporary study and its relations
    stmt.executeUpdate("delete from temp_study where temp_study_id = " + tempId); 
    deleteRelationsFromTemp(tempId, stmt, false);
    
    stmt.close();
  }

 /**
  * Inserts a new empty study in the temp_study table. 
  * @return The id of the new, empty study.
  * @throws SQLException
 * @throws SystemException 
  */
  public int newStudy() throws SQLException, SystemException {
    
    Statement stmt = getConnection().createStatement();
    stmt.executeUpdate("insert into temp_study (study_name) values ('newStudy');");
    
    int id = Nuller.getIntNull();
    //Fetch the id of the inserted study
    ResultSet rs = stmt.executeQuery("select max(temp_study_id) as id from temp_study;");
    if (rs.next())
        id = rs.getInt("id");
    rs.close();
    stmt.close();
    
    return id;
  }

 /**
  * Copy the study with the specified id and all its relations and insert into
  * the corresponding temp tables.
  * @param studyId the id of the study to copy.
  * @param stmt the Statement used to perform the insert.
  * @throws SQLException
  */
  private void copyStudyToTemp(int studyId, Statement stmt) throws SQLException {
     
    String studyColumns = "study_id, study_type_id, study_name, study_description, study_end_date, " +
        "study_owner, last_edited_by, study_duration, study_duration_unit_id, study_keywords, " +
        "no_professionals, no_students, study_notes, study_start_date";   
    
    String studyMaterialColumns = "study_id, material_description, material_name, material_file, material_isfile";
    String peopleStudyColumns = "study_id, people_id";    
    String publicationStudyColumns = "study_id, publication_id";
    
    StringBuffer studyQuery = new StringBuffer();
    studyQuery.append("insert into temp_study (" + studyColumns + ") ");   
    studyQuery.append("select " + studyColumns + " from study where study_id = " + studyId + ";");        
    
    StringBuffer studyMaterialQuery = new StringBuffer();
    studyMaterialQuery.append("insert into temp_study_material (" + studyMaterialColumns + ") ");   
    studyMaterialQuery.append("select " + studyMaterialColumns + " from study_material where study_id = " + studyId + ";");        
    
    StringBuffer peopleStudyQuery = new StringBuffer();
    peopleStudyQuery.append("insert into temp_people_study (" + peopleStudyColumns + ") ");   
    peopleStudyQuery.append("select " + peopleStudyColumns + " from people_study where study_id = " + studyId + ";");        
    
    StringBuffer publicationStudyQuery = new StringBuffer();
    publicationStudyQuery.append("insert into temp_publication_study (" + publicationStudyColumns + ") ");   
    publicationStudyQuery.append("select " + publicationStudyColumns + " from publication_study where study_id = " + studyId + ";");        
    
    stmt.executeUpdate(studyQuery.toString());
    stmt.executeUpdate(studyMaterialQuery.toString());
    stmt.executeUpdate(peopleStudyQuery.toString());
    stmt.executeUpdate(publicationStudyQuery.toString());
    
  }

 /**
  * Copy the all relations to the study with the specified id from temp
  * and insert into the corresponding original tables.
  * @param studyId the id of the study to copy the relations from.
  * @param stmt the Statement used to perform the insert.
  * @throws SQLException
  */
  private void copyStudyRelationsFromTemp(int studyId, Statement stmt) throws SQLException {
     
    String studyMaterialColumns = "study_id, material_description, material_name, material_file, material_isfile";
    String peopleStudyColumns = "study_id, people_id";    
    String publicationStudyColumns = "study_id, publication_id";
    
    StringBuffer studyMaterialQuery = new StringBuffer();
    studyMaterialQuery.append("insert into study_material (" + studyMaterialColumns + ") ");   
    studyMaterialQuery.append("select " + studyMaterialColumns + " from temp_study_material where study_id = " + studyId);        
    studyMaterialQuery.append(" and (update_flag is null or update_flag = 'D');");
    
    StringBuffer peopleStudyQuery = new StringBuffer();
    peopleStudyQuery.append("insert into people_study (" + peopleStudyColumns + ") ");   
    peopleStudyQuery.append("select " + peopleStudyColumns + " from temp_people_study where study_id = " + studyId + ";");        
    
    StringBuffer publicationStudyQuery = new StringBuffer();
    publicationStudyQuery.append("insert into publication_study (" + publicationStudyColumns + ") ");   
    publicationStudyQuery.append("select " + publicationStudyColumns + " from temp_publication_study where study_id = " + studyId + ";");        
    
    stmt.executeUpdate(studyMaterialQuery.toString());
    stmt.executeUpdate(peopleStudyQuery.toString());
    stmt.executeUpdate(publicationStudyQuery.toString());
    
  }

 /**
  * Copy the all relations to the study with the specified tempStudyId from temp
  * and insert into the corresponding original tables with study_id = studyId.
  * @param studyId the id of the study to copy the relations to.
  * @param tempStudyId the id of the study to copy the relations from.
  * @param stmt the Statement used to perform the insert.
  * @throws SQLException
  */
  private void copyStudyRelationsFromTempForAdd(int studyId, int tempStudyId, Statement stmt) throws SQLException {

    StringBuffer studyMaterialQuery = new StringBuffer();
    studyMaterialQuery.append("insert into study_material (study_id, material_description, material_name, material_file, material_isfile) ");   
    studyMaterialQuery.append("select " + studyId + ", material_description, material_name, material_file, material_isfile ");
    studyMaterialQuery.append("from temp_study_material where study_id = " + tempStudyId);        
    studyMaterialQuery.append(" and (update_flag is null or update_flag = 'D');");
    
    StringBuffer peopleStudyQuery = new StringBuffer();
    peopleStudyQuery.append("insert into people_study (study_id, people_id) ");   
    peopleStudyQuery.append("select " + studyId + ", people_id from temp_people_study where study_id = " + tempStudyId + ";");        
    
    StringBuffer publicationStudyQuery = new StringBuffer();
    publicationStudyQuery.append("insert into publication_study (study_id, publication_id) ");   
    publicationStudyQuery.append("select " + studyId + ", publication_id from temp_publication_study where study_id = " + tempStudyId + ";");        
    
    stmt.executeUpdate(studyMaterialQuery.toString());
    stmt.executeUpdate(peopleStudyQuery.toString());
    stmt.executeUpdate(publicationStudyQuery.toString());
    
  } 
  
 /**
  * Update the study id to study relations connected to a specified study.
  * @param newStudyId 
  * @param oldStudyId 
  * @param stmt the Statement used to perform the insert.
  * @throws SQLException
  */
  private void updateStudyRelations(int newStudyId, int oldStudyId, Statement stmt) throws SQLException {
     
    
    stmt.executeUpdate("update people_study set study_id = " + newStudyId + " where study_id = " + oldStudyId);
    stmt.executeUpdate("update publication_study set study_id = " + newStudyId + " where study_id = " + oldStudyId);
    stmt.executeUpdate("update study_material set study_id = " + newStudyId + " where study_id = " + oldStudyId);
    
  }
  
 /**
  * Update a study based on the specified study object. After the update is done,
  * the corresponding study in temp_study is deleted.
  * @param study The study to be added.
  * @throws SQLException, UserException
 * @throws SystemException 
  */
  public void updateStudy(StudyDTO study) throws SQLException, UserException, SystemException {

    validateStudy(study.getName(), study.getId(), study.getTypeId());  

    StringBuffer updateStudyQuery = new StringBuffer();    
    updateStudyQuery.append("update study set ");
    updateStudyQuery.append("study_type_id = " + study.getTypeId() + ", ");
    updateStudyQuery.append("study_name = '" + Validator.checkEscapes(study.getName()) + "', ");
    updateStudyQuery.append("study_description = '" + Validator.checkEscapes(study.getDescription()) + "', ");
    updateStudyQuery.append("study_end_date = '" + Formatter.removeCharFromString(study.getEndDate(), Constants.DATE_SEPARATOR) + "', ");
    updateStudyQuery.append("last_edited_by = '" + study.getLastEditedBy() + "', ");
    updateStudyQuery.append("study_duration = " + SQLHelper.getValue(study.getDuration()) + ", ");
    updateStudyQuery.append("study_duration_unit_id = " + SQLHelper.getValue(study.getDurationUnitId()) + ", ");
    updateStudyQuery.append("study_keywords = " + SQLHelper.getValue(Validator.checkEscapes(study.getKeywords())) + ", ");
    updateStudyQuery.append("no_professionals = " + SQLHelper.getValue(study.getNoOfProfessionalParticipants()) + ", ");
    updateStudyQuery.append("no_students = " + SQLHelper.getValue(study.getNoOfStudentParticipants()) + ", ");
    updateStudyQuery.append("study_notes = " + SQLHelper.getValue(Validator.checkEscapes(study.getNotes())) + ", ");
    updateStudyQuery.append("study_start_date = " + SQLHelper.getValue(Formatter.removeCharFromString(study.getStartDate(), Constants.DATE_SEPARATOR)) + " ");
    updateStudyQuery.append("where study_id = " + study.getId() + ";");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(updateStudyQuery.toString()); 
    
    Statement stmt = getConnection().createStatement();
    stmt.executeUpdate(updateStudyQuery.toString());
    
    //Update all study relations by deleteing them and fetching them from temp.
    deleteRelations(study.getId(), stmt);
    copyStudyRelationsFromTemp(study.getId(), stmt);
    
    //Delete the temporary study and its relations
    stmt.executeUpdate("delete from temp_study where study_id = " + study.getId()); 
    deleteRelationsFromTemp(study.getId(), stmt, false);
    
    stmt.close();

  }
    
 /**
  * Update a study temporary in temp_study based on the specified study object.
  * @param study The study to be added.
  * @param forNewStudy If true, the study is new and has not got a study id, so
  * the temp_study_id is used for the update. Otherwise the study_id is used.
  * @param updateStudyMaterialMode the mode for update of study material
  * @return StudyDTO The updated study.
 * @throws Exception 
 */
  public StudyDTO updateStudyTemp(StudyDTO study, boolean forNewStudy, int updateStudyMaterialMode) throws Exception {
    
    StringBuffer updateStudyQuery = new StringBuffer();    
    updateStudyQuery.append("update temp_study set ");
    updateStudyQuery.append("study_type_id = " + validateStudyTypeForTempUpdate(study.getTypeId()) + ", ");
    updateStudyQuery.append("study_name = " + SQLHelper.getValue(Validator.checkEscapes(study.getName())) + ", ");
    updateStudyQuery.append("study_description = " + SQLHelper.getValue(Validator.checkEscapes(study.getDescription())) + ", ");
    updateStudyQuery.append("study_end_date = " + SQLHelper.getValue(study.getEndDate()) + ", ");
    updateStudyQuery.append("study_duration = " + SQLHelper.getValue(study.getDuration()) + ", ");
    updateStudyQuery.append("study_duration_unit_id = " + SQLHelper.getValue(study.getDurationUnitId()) + ", ");
    updateStudyQuery.append("study_keywords = " + SQLHelper.getValue(Validator.checkEscapes(study.getKeywords())) + ", ");
    updateStudyQuery.append("no_professionals = " + SQLHelper.getValue(study.getNoOfProfessionalParticipants()) + ", ");
    updateStudyQuery.append("no_students = " + SQLHelper.getValue(study.getNoOfStudentParticipants()) + ", ");
    updateStudyQuery.append("study_notes = " + SQLHelper.getValue(Validator.checkEscapes(study.getNotes())) + ", ");
    updateStudyQuery.append("study_start_date = " + SQLHelper.getValue(study.getStartDate()) + " ");
    
    if (forNewStudy)
        updateStudyQuery.append("where temp_study_id = " + study.getId() + ";");
    else 
        updateStudyQuery.append("where study_id = " + study.getId() + ";");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(updateStudyQuery.toString()); 
    
    Statement stmt = getConnection().createStatement();
    stmt.executeUpdate(updateStudyQuery.toString());

    //Update all study relations
    deleteRelationsFromTemp(study.getId(), stmt, true);
    if (!study.getResponsibles().isEmpty())
        addStudyRelations("temp_people_study", "people_id", study.getId(), study.getResponsibles().keySet(), stmt);
    if (!study.getPublications().isEmpty())
        addStudyRelations("temp_publication_study", "publication_id", study.getId(), study.getPublications().keySet(), stmt);
    if (!Nuller.isNull(updateStudyMaterialMode) && updateStudyMaterialMode == Constants.MODE_OK) {
        updateTempStudyMaterial(study.getId());
    }
    
    //Fetch the updated study from temp    
    StringBuffer getQuery = new StringBuffer();
    getQuery.append("select s.temp_study_id, s.study_id, s.study_name, s.study_description, s.study_keywords, ");
    getQuery.append("s.study_duration, sd.study_duration_unit, s.study_duration_unit_id, st.study_type, s.study_type_id, s.study_notes, ");
    getQuery.append("s.study_start_date, s.study_end_date, s.no_students, s.no_professionals, s.study_owner, s.last_edited_by ");
    getQuery.append("from study_type st, temp_study s ");
    getQuery.append("left join (study_duration_unit sd) on s.study_duration_unit_id = sd.study_duration_unit_id ");
//    getQuery.append("from temp_study s, study_type st ");
//    getQuery.append("left join study_duration_unit sd on s.study_duration_unit_id = sd.study_duration_unit_id "); 
    getQuery.append("where s.study_type_id = st.study_type_id ");
    
    if (forNewStudy)
        getQuery.append("and s.temp_study_id = " + study.getId() + ";");
    else 
        getQuery.append("and s.study_id = " + study.getId() + ";");
    
    ResultSet rs = stmt.executeQuery(getQuery.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    try {
        if (rs.next()) {
            if (forNewStudy)
                study.setId(rsh.getInt("temp_study_id"));
            else 
                study.setId(rsh.getInt("study_id"));
            study.setName(rsh.getString("study_name"));
            study.setType(rsh.getString("study_type"));
            study.setTypeId(rsh.getInt("study_type_id"));
            study.setDescription(rsh.getString("study_description"));
            study.setDuration(rsh.getString("study_duration"));
            study.setDurationUnit(rsh.getString("study_duration_unit"));
            study.setDurationUnitId(rsh.getInt("study_duration_unit_id"));
            study.setKeywords(rsh.getString("study_keywords"));
            study.setNoOfProfessionalParticipants(rsh.getString("no_students"));
            study.setNoOfStudentParticipants(rsh.getString("no_professionals"));
            study.setNotes(rsh.getString("study_notes"));
            study.setStartDate(rsh.getString("study_start_date"));
            study.setEndDate(rsh.getString("study_end_date"));
            study.setResponsibles(getStudyResponsibles(study.getId(), true));
            study.setPublications(getPublications(study.getId(), true));
            study.setMaterial(getStudyMaterial(study.getId(), true, true));
        } else {
            throw new UserException(ExceptionMessages.NO_STUDY);
        }
    } finally {
        rs.close();
        stmt.close();
    }
    
    return study; 
  }
  
 /**
  * Delete the study with the specified id and all its relations.
  * @param id the id of the study to delete.
  * @param mode denotes the mode for deletion.
  * @throws SQLException
 * @throws SystemException 
  */
  public void deleteStudy(int id, int mode) throws SQLException, SystemException { 
    
    Statement stmt = getConnection().createStatement();  
    switch (mode) {
        case(Constants.MAINTAIN_MODE_FINAL):
        stmt.executeUpdate("delete from study where study_id = " + id); 
        stmt.executeUpdate("delete from temp_study where study_id = " + id); 
        deleteRelations(id, stmt);
        break;
        case(Constants.MAINTAIN_MODE_TEMP_EDIT):
        stmt.executeUpdate("delete from temp_study where study_id = " + id); 
        break;
        case(Constants.MAINTAIN_MODE_TEMP_NEW):
        stmt.executeUpdate("delete from temp_study where temp_study_id = " + id); 
        break;
    }

    deleteRelationsFromTemp(id, stmt, false); 
    
    stmt.close();
  }
  
 /**
  * Delete all relations of the study with the specified id.
  * @param id the id of the study to delete relation to.
  * @param stmt the Statement used to perform the deletion.
  *
  * @throws SQLException
  */
  private void deleteRelations(int id, Statement stmt) throws SQLException {

    stmt.executeUpdate("delete from study_material where study_id = " + id);   
    stmt.executeUpdate("delete from people_study where study_id = " + id);
    stmt.executeUpdate("delete from publication_study where study_id = " + id);
      
  }

 /**
  * Delete all relations of the study with the specified id from temp_study.
  * @param studyId the id of the study to delete.
  * @param stmt the Statement used to perform the deletion.
  * @param notStudyMaterial If true relations from temp_study_material is not deleted.
  * @throws SQLException
  */
  private void deleteRelationsFromTemp(int studyId, Statement stmt, boolean notStudyMaterial) throws SQLException { 
    
    stmt.executeUpdate("delete from temp_people_study where study_id = " + studyId);
    stmt.executeUpdate("delete from temp_publication_study where study_id = " + studyId);
    
    if (!notStudyMaterial)
        stmt.executeUpdate("delete from temp_study_material where study_id = " + studyId);

  }
 
 /**
  * Returns a list of studies matching the search criteria. 
  * @param search SearchDTO the search criteria.
  * @param maxHitListLength the maximum number of hits to fetch.
  * @param startPosition the position in the ResultSet to start to fetch hits from.
  * @return HitList containing StudyDTOs.
 * @throws Exception 
  */
  public HitList findStudies(SearchDTO search, int maxHitListLength, int startPosition) throws Exception {

    HitList studies = new HitList();
    
    StringBuffer select = new StringBuffer("select s.*, st.study_type, sd.study_duration_unit ");
    
    //This part is changed since MySQL 5 prohibits certain left joint operations
    StringBuffer query = new StringBuffer();
    query.append(" from study_type st, people_study pes, study s ");
    query.append(" 		left join publication_study pus on s.study_id = pus.study_id ");
    query.append(" 		left join study_material sm on s.study_id = sm.study_id ");
    query.append(" 		left join (study_duration_unit sd) on s.study_duration_unit_id = sd.study_duration_unit_id "); 

    //End of correction
    
    WhereOrAndHelper whereOrAnd = new WhereOrAndHelper();     
    query.append(whereOrAnd.get() + " s.study_type_id = st.study_type_id ");
    query.append(whereOrAnd.get() + " s.study_id = pes.study_id ");

    if (search.getFreeText() != null && !StringUtils.isEmpty(search.getFreeText().trim())) {
        //The search is a free text search. All hits must contain all items in the free text field.
        StringBuffer selectlist = new StringBuffer(); 
        String[] freeTextCriteria = Validator.checkEscapes(search.getFreeText()).split(" ");

        //If the first item has no hits, return
        if (Nuller.isReallyNull(selectlist.append(searchOneFreeTextItem(freeTextCriteria[0], null, query.toString())).toString()))
            return studies;
    
        //Run if more than one word has been entered in the free text field
        if (freeTextCriteria.length > 1) {
            String newSelectlist;
            for (int i = 1; i < freeTextCriteria.length; i++) {            
                newSelectlist = searchOneFreeTextItem(freeTextCriteria[i], selectlist.toString(), query.toString());            
                selectlist.delete(0, selectlist.length());  
                selectlist.append(newSelectlist);
            }    
        }
        
        //If any studies matching the criteria has been found, 
        //the select is performed, otherwise the empty hitlist is returned.
        if (selectlist.length() > 0) {
            query.append("and s.study_id in (" + selectlist.toString() + ") ");
        } else {
            return studies;
        }
        
    } else {
        if (!Nuller.isNull(search.getTypeOfStudy())) {
            query.append(whereOrAnd.get() + " s.study_type_id = " + search.getTypeOfStudy() + " ");
        }
    
        if (!Nuller.isNull(search.getEndOfStudyFrom())) {
            query.append(whereOrAnd.get() + " s.study_end_date >= " + Formatter.removeCharFromString(search.getEndOfStudyFrom(), Constants.DATE_SEPARATOR) + " ");
        }

        if (!Nuller.isNull(search.getEndOfStudyTo())) {
            query.append(whereOrAnd.get() + " s.study_end_date <= " + Formatter.removeCharFromString(search.getEndOfStudyTo(), Constants.DATE_SEPARATOR) + " ");
        }

        if (!Nuller.isNull(search.getStudyResponsibles())) {
            //The search containts Study Responsibles. Hits must contain at least one item in Study Responsibles field.        
            String[] peopleCriteria = Validator.checkEscapes(search.getStudyResponsibles().replace("  ", "")).split(" ");
        	
            PeopleDAO peopleDB = new PeopleDAO();
            ArrayList peopleList = new ArrayList();
            for (int i = 0; i < peopleCriteria.length; i++) {
				peopleList.addAll(peopleDB.getPeople(peopleCriteria[i]));
            }
            
            if (peopleList.isEmpty()) {            	
            	return studies;
            }
            else {
            	query.append(" and pes.people_id in (");
            	peopleDB.preparePeopleIdsWhereInto(query, peopleList);
            	query.append(") ");
            }
            
        }
        if (!search.getStudyResponsibleIds().isEmpty()) {
        	
        	StringBuffer idsBuf = new StringBuffer();
        	for (Iterator iterator = search.getStudyResponsibleIds().iterator(); iterator.hasNext();) {
				String responsibleId = (String) iterator.next();
				if (idsBuf.length() > 0) {
					idsBuf.append(search.getResponsiblesOperator());
				}
				idsBuf.append(" (pes.people_id = '").append(responsibleId).append("') ");
            }
        	query.append(" and (").append(idsBuf).append(") ");
        } 
    }
    
    //Find the total number of hits by using the query before group by.
    studies.setCount(getNumberOfHits(query.toString()));
    
    query.append("group by s.study_id, s.study_name, s.study_end_date, s.study_description, st.study_type ");
    StringBuffer sortBy = new StringBuffer();
    
    if (StringUtils.isNotBlank(search.getSortByDefault())) {
    	sortBy.append(search.getSortByDefault());
    }
    if (StringUtils.isNotBlank(search.getSortBy())) {
    	 if (StringUtils.isNotBlank(search.getSortByDefault())) {
    		sortBy.append(", ");
    	}
    	sortBy.append(search.getSortBy() + " " + search.getSortOrder());
    }
    
    if (StringUtils.isNotBlank(sortBy.toString())) {
    	query.append("order by ").append(sortBy).append(";"); 
    }
    
    String sql = select.append(query).toString();
    LoggerFactory.getLogger(StudyDAO.class).debug(sql); 
    Statement stmt = getConnection().createStatement();   
    ResultSet rs = stmt.executeQuery(sql);
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    if (ResultSetHelper.skipRows(rs, startPosition)) {
        int count = 0;
		while (rs.next() && count < maxHitListLength) {
	            StudyDTO study = getStudyDTOFromResultSet(rsh, Constants.DATE_FORMAT_PRESENTATION);
	            studies.add(study);
	            count++;
	            
	            if (search.getColumns().contains(ReportColumns.RESPONSIBLES.getName())) {
	            	study.setResponsibles(getStudyResponsibles(study.getId(), false));
	            }
	            if (search.getColumns().contains(ReportColumns.PUBLICATIONS.getName())) {
	            	study.setPublications(getPublications(study.getId(), false));
	            }
	            if (search.getColumns().contains(ReportColumns.MATERIALS.getName())) {
	            	study.setMaterial(getStudyMaterial(study.getId(), false, true));
	            }
        }
    }
    rs.close();
    stmt.close();
    
    return studies;
  }   
 
 /**
  * Returns details about a Study from resultset, except study material.
  * @param ResultSetHelper
  * @param dateFormat The attributes on the study representing dates, are formatted
  * according to this date format. 
 * @param readOnly 
 * @param arrayList 
 * @throws Exception 
 * @thorws SQLException

  */
  private StudyDTO getStudyDTOFromResultSet(ResultSetHelper rsh, String dateFormat) throws Exception {

	StudyDTO study = new StudyDTO();
	
    study.setId(rsh.getInt("study_id"));
    study.setName(rsh.getString("study_name"));
    study.setType(rsh.getString("study_type"));
    study.setTypeId(rsh.getInt("study_type_id"));
    study.setDescription(rsh.getString("study_description"));
    study.setDuration(rsh.getIntString("study_duration"));
    study.setDurationUnit(rsh.getString("study_duration_unit"));
    study.setDurationUnitId(rsh.getInt("study_duration_unit_id"));
    study.setKeywords(rsh.getString("study_keywords"));
    study.setNoOfProfessionalParticipants(rsh.getIntString("no_students"));
    study.setNoOfStudentParticipants(rsh.getIntString("no_professionals"));
    study.setNotes(rsh.getString("study_notes"));
    study.setOwner(rsh.getString("study_owner"));
    study.setLastEditedBy(rsh.getString("last_edited_by"));
    
    try {
        study.setStartDate(
            Formatter.formatDate(Parser.parseDate(rsh.getString("study_start_date"), Constants.DATE_FORMAT_DATABASE), dateFormat));
        study.setEndDate(
            Formatter.formatDate(Parser.parseDate(rsh.getString("study_end_date"), Constants.DATE_FORMAT_DATABASE), dateFormat));
    } catch (UserException e) {
        //Format from database will always be correct
    }

    if (dateFormat.equals(Constants.DATE_FORMAT_EDIT)) { //Only shown i edit mode 
        study.setLastEditedBy(rsh.getString("last_edited_by"));
        study.setOwner(rsh.getString("study_owner"));
    }
    
    return study;  
  }   

  /**
  * Returns a String containing the ids of the studies matching ONE item in the
  * free text criteria. 
  * @param String one item of the free text criteria. 
  * @param String containing ids of already found studies.
  * @param String containing 'from table' and 'where' conditions for the query.
  * @return String containig ids of all studies matching this and the previous 
  * items of the free text criteria. 
  *
  * @throws SQLException
 * @throws MalformedURLException 
 * @throws SystemException 
  */
  
  private String searchOneFreeTextItem(String criteria, String foundStudies, String partOfQuery) throws SQLException, MalformedURLException, SystemException { 
 
    StringBuffer selectlist = new StringBuffer();
    StringBuffer query = new StringBuffer(); 
    query.append("select s.study_id ");
    query.append(partOfQuery);
    query.append("and (s.study_name " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.study_description " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.study_start_date " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.study_end_date " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.study_notes " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.study_owner " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.study_keywords " + SQLHelper.getLike(criteria) + " ");
    query.append("		or s.last_edited_by " + SQLHelper.getLike(criteria) + " ");
    query.append("		or st.study_type " + SQLHelper.getLike(criteria) + " ");
    query.append("		or sm.material_description " + SQLHelper.getLike(criteria) + " ");
    query.append("		or sm.material_name " + SQLHelper.getLike(criteria) + " ");
    
	PeopleDAO peopleDB = new PeopleDAO();
	StringBuffer peopleQuery = new StringBuffer();
	peopleDB.preparePeopleIdsWhere(peopleQuery , criteria);

	if (peopleQuery.length() > 0) {
		query.append("		or pes.people_id in (").append(peopleQuery).append(") ");
	}

	PublicationDAO publicationDB = new PublicationDAO();
	StringBuffer publicationQuery = new StringBuffer();
	publicationDB.preparePublicationIdsWhere(publicationQuery , criteria);

	if (publicationQuery.length() > 0) {
		query.append("		or pus.publication_id in (").append(publicationQuery).append(") ");
	}
	
    query.append(")");
    
    if (foundStudies != null && foundStudies.length() > 0)
        query.append(" and s.study_id in (" + foundStudies + ")");
    query.append(";");
  
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);

    while (rs.next()) {
        selectlist.append(String.valueOf(rsh.getInt("study_id")));
        selectlist.append(Constants.ID_SEPARATOR);
    }
    
    if (selectlist.length() > 0)
        selectlist.deleteCharAt(selectlist.length()-1);    
    
    rs.close();
    stmt.close();
    
    return selectlist.toString();
  }

 /**
  * Returns study responsibles for the study with the specified study id.
  * @return Hashtable containg study responsibles. The id is the key, the 
  * name is the value.
  * @throws SQLException
 * @throws SystemException 
 * @throws MalformedURLException 
  */
  private Hashtable getStudyResponsibles(int studyId, boolean fromTemp) throws SQLException, SystemException, MalformedURLException {
    String table;
    if (fromTemp) 
        table = "temp_people_study";
    else
        table = "people_study";

    StringBuffer query = new StringBuffer();    
    query.append("select people_id ");
    query.append("from " + table + " ");
    query.append("where study_id = " + studyId);
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    Hashtable studyResponsibles = new Hashtable();
    PeopleDAO peopleDB = new PeopleDAO();
    while (rs.next()) {
    	StudyResponsibleDTO person = new StudyResponsibleDTO();
    	peopleDB.readPerson(rsh.getString("people_id"), person);		
        studyResponsibles.put(person.getId(), person.getSingleName());
    }
    
    rs.close();
    stmt.close();
    
    return studyResponsibles;  
  }   

 /**
  * Returns publications for the study with the specified study id.
  * @return Hashtable containg publications. The id is the key, the 
  * title is the value.
 * @throws Exception 
  */
  private Hashtable getPublications(int studyId, boolean fromTemp) throws Exception {
    String table;
    if (fromTemp) 
        table = "temp_publication_study";
    else
        table = "publication_study";

    StringBuffer query = new StringBuffer();    
    query.append("select publication_id ");
    query.append("from " + table + " ");
    query.append("where study_id = " + studyId);
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    Hashtable publications = new Hashtable();
    PublicationDAO pubDB = new PublicationDAO();
    while (rs.next()) {
        PublicationDTO publication = pubDB.getPublication(rsh.getString("publication_id"));
		publications.put(publication.getId(), publication.getTitle());
    }

    rs.close();
    stmt.close();

    return publications;  
  }   

 /**
  * Returns study material for the study with the specified study id.
  * @param studyId The id of the study.
  * @param fromTemp If true the study material is fetched from 
  * temp_study_material, otherwise from study_material.
  * @param deleteNotConfirmed If true not confirmed study materials in temp_study_material
  * is deleted. This is done to avoid the temp table to fill up with rubbish.  
  * @return Vector containg all study material connected to the study.
  * @throws SQLException
 * @throws SystemException 
  */
  public Vector getStudyMaterial(int studyId, boolean fromTemp, boolean deleteNotConfirmed) throws SQLException, SystemException {
  
    String table;
    if (fromTemp) 
        table = "temp_study_material sm";
    else
        table = "study_material sm";
      
    StringBuffer query = new StringBuffer();    
    query.append("select sm.study_material_id, sm.material_description, sm.material_name, sm.material_isfile ");
    query.append("from " + table + " where sm.study_id = " + studyId);
    
    if (fromTemp)
        query.append(" and (update_flag = 'D' OR update_flag is null)");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    Vector studyMaterial = new Vector();
    StudyMaterialDTO material;
    while (rs.next()) {
        material = new StudyMaterialDTO();
        material.setId(rsh.getInt("study_material_id"));
        material.setDescription(rsh.getString("material_description"));
        material.setIsFile(rsh.getIntAsBoolean("material_isfile"));
        if (material.isFile())
            material.setFileName(rsh.getString("material_name"));
        else 
            material.setUrl(rsh.getString("material_name"));
        studyMaterial.add(material);
    }

    rs.close();
    
    if (fromTemp && deleteNotConfirmed) {
        StringBuffer deleteQuery = new StringBuffer();
        deleteQuery.append("delete from temp_study_material ");
        deleteQuery.append("where update_flag = 'A' ");
        deleteQuery.append("and study_id = " + studyId);    

        LoggerFactory.getLogger(StudyDAO.class).debug(deleteQuery.toString()); 
        stmt.executeUpdate(deleteQuery.toString());
    }
    
    stmt.close();
    
    return studyMaterial;  
  }   

 /**
  * Returns study material for the study with the specified study id.
  * @param studyId The id of the study.
  * @return Vector containg all study material connected to the study.
  * @throws SQLException
 * @throws SystemException 
  */
  public Vector getStudyMaterial(int studyId) throws SQLException, SystemException {
      
    StringBuffer query = new StringBuffer();    
    query.append("select sm.study_material_id, sm.material_description, sm.material_name, sm.material_isfile ");
    query.append("from temp_study_material sm where sm.study_id = " + studyId);
    query.append(" and (update_flag = 'A' OR update_flag is null)");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    Vector studyMaterial = new Vector();
    StudyMaterialDTO material;
    while (rs.next()) {
        material = new StudyMaterialDTO();
        material.setId(rsh.getInt("study_material_id"));
        material.setDescription(rsh.getString("material_description"));
        material.setIsFile(rsh.getIntAsBoolean("material_isfile"));
        if (material.isFile())
            material.setFileName(rsh.getString("material_name"));
        else 
            material.setUrl(rsh.getString("material_name"));
        studyMaterial.add(material);
    }

    rs.close();
    stmt.close();
    
    return studyMaterial;  
  }   
  
  
 /**
  * Returns the study material with the specified id.
  * @param id The id of the study material.
  * @param fromTemp If true the study material is fetched from 
  * temp_study_material, otherwise from study_material.
  * @return StudyMaterialDTO
  * @throws SQLException, UserException, FileNotFoundException, IOException
 * @throws SystemException 
  */
  public StudyMaterialDTO getStudyMaterialById(int id, boolean fromTemp) throws SQLException, UserException, FileNotFoundException, IOException, SystemException {
  
    String tmpDir = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE).getString("tmpDir");  
    String table;
    if (fromTemp) 
        table = "temp_study_material sm";
    else
        table = "study_material sm";
      
    StringBuffer query = new StringBuffer();    
    query.append("select sm.study_material_id, sm.material_description, sm.material_name, sm.material_file, material_isfile ");
    query.append("from " + table + " where sm.study_material_id = " + id);
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    
    StudyMaterialDTO material;
    try {
        if (rs.next()) {
            material = new StudyMaterialDTO();
            material.setId(rsh.getInt("study_material_id"));
            material.setDescription(rsh.getString("material_description"));
            material.setIsFile(rsh.getIntAsBoolean("material_isfile"));
            if (material.isFile()) {
                material.setFileName(rsh.getString("material_name"));
                Blob blob = rs.getBlob("material_file");
                int length = (int)blob.length();
                byte [] _blob = blob.getBytes(1, length); 
                File file = new File(tmpDir + rsh.getString("material_name"));
                FileOutputStream fos = new FileOutputStream(file); 
                fos.write(_blob);
                fos.close();
               material.setFile(file);                 
            } else 
                material.setUrl(rsh.getString("material_name"));         
        } else {
            throw new UserException(ExceptionMessages.NO_STUDY_MATERIAL);
        }
    } finally {
        rs.close();
        stmt.close();
    }
    
    return material;  
  }   

 /**
  * Returns the number of hits from the search given by the specified condition.
  * @throws SQLException
 * @throws SystemException 
  */ 
  private int getNumberOfHits(String condition) throws SQLException, SystemException {
    StringBuffer query = new StringBuffer("select count(distinct s.study_id) as all_hits ");  
    query.append(condition).append(";");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    int numberOfHits = 0;    
    if (rs.next()) 
        numberOfHits = rsh.getInt("all_hits");

    rs.close();
    stmt.close();
    
    return numberOfHits;
  }
  
 /**
  * Add a study relation for each specified relation id.
  * @param table the name of the table to insert a relation into
  * @param relation the name of the column to insert the relation ids
  * @param studyId the id of the study
  * @param relationIds a set containing the ids of all the relations.
  * @param stmt The statement for executing the sql. 
  * @throws SQLException
  */ 
  private void addStudyRelations(String table, String relation, int studyId, Set relationIds, Statement stmt) throws SQLException {
    String query;  
    Iterator it = relationIds.iterator();
    while (it.hasNext()) {
        query = "insert into " + table + "(study_id, " + relation + ") values (" + studyId + ", '" + it.next() + "');";
        stmt.executeUpdate(query);
    }
  }  
  
 /**
  * Add study material.
  * @param studyId the id of the study connected to the study material table.
  * @param material the StudyMaterialDTO to be added.
  * @throws SQLException, FileNotFoundException
 * @throws SystemException 
  */ 
  public void addStudyMaterial(int studyId, StudyMaterialDTO material) throws SQLException, FileNotFoundException, SystemException {
        
    StringBuffer query = new StringBuffer();
    
    query.append("insert into temp_study_material ");
    query.append("(study_id, material_description, material_name, material_isfile, material_file, update_flag) ");    
    query.append("values (" + studyId + ", '");
    query.append(Validator.checkEscapes(material.getDescription()) + "', '");
    if (material.isFile()) { 
        query.append(Validator.checkEscapes(material.getFileName()) + "', 1, ?, 'A');");
        LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
        PreparedStatement pstmt = getConnection().prepareStatement(query.toString());
        InputStream is = new FileInputStream(material.getFile());
        pstmt.setBinaryStream(1, is, (int)(material.getFile().length()));
        pstmt.executeUpdate();
        pstmt.close();
        material.getFile().delete();
    } else {
        query.append(Validator.checkEscapes(material.getUrl()) + "', 0, null, 'A');");
        LoggerFactory.getLogger(StudyDAO.class).debug(query.toString()); 
        Statement stmt = getConnection().createStatement();
        stmt.executeUpdate(query.toString());
        stmt.close();
    }
  }
  
 /**
  * Delete study material. 
  * @param toBeDeleted, the id's of the study material to be deleted.
  * @throws SQLException
 * @throws SystemException 
  */ 
  public void deleteStudyMaterial(String toBeDeleted) throws SQLException, SystemException {
      
    StringBuffer deleteQuery = new StringBuffer();
    deleteQuery.append("delete from temp_study_material ");
    deleteQuery.append("where update_flag = 'A' ");
    deleteQuery.append("and study_material_id in (" + toBeDeleted + ")");    

    LoggerFactory.getLogger(StudyDAO.class).debug(deleteQuery.toString()); 
    Statement stmt = getConnection().createStatement();
    stmt.executeUpdate(deleteQuery.toString());
      
    StringBuffer updateQuery = new StringBuffer();
    updateQuery.append("update temp_study_material ");
    updateQuery.append("set update_flag = 'D' ");
    updateQuery.append("where study_material_id in (" + toBeDeleted + ") and update_flag is null;");
    
    LoggerFactory.getLogger(StudyDAO.class).debug(updateQuery.toString()); 
    stmt.executeUpdate(updateQuery.toString());
    
    stmt.close();
  }
  
 /**
  * Updates the study material related to the study with the specified id in the 
  * temp_study material table.
  * @param studyId
  * @throws SQLException
 * @throws SystemException 
  */ 
  public void updateTempStudyMaterial(int studyId) throws SQLException, SystemException {
    
    StringBuffer updateQuery = new StringBuffer();
      
    updateQuery.append("update temp_study_material ");
    updateQuery.append("set update_flag = null ");
    updateQuery.append("where study_id = " + studyId + " ");
    updateQuery.append("and update_flag = 'A';");
        
    LoggerFactory.getLogger(StudyDAO.class).debug(updateQuery.toString()); 
    Statement stmt = getConnection().createStatement();
    int i = stmt.executeUpdate(updateQuery.toString());
    
    StringBuffer deleteQuery = new StringBuffer();
      
    deleteQuery.append("delete from temp_study_material ");
    deleteQuery.append("where study_id = " + studyId + " ");
    deleteQuery.append("and update_flag = 'D'");

    LoggerFactory.getLogger(StudyDAO.class).debug(deleteQuery.toString()); 
    stmt.executeUpdate(deleteQuery.toString());
    
    stmt.close();
  }

 /**
  * Check if a study with the input name already exist and if the study type exist.
  * @param studyName the name of the study to add or update.
  * @param studyId if studyId != IntNull the name check does not include this study id.
  * @param studyTypeId the study type chosen for this study.
  * @throws SQLException, UserException.
 * @throws SystemException 
  */ 
  private void validateStudy(String studyName, int studyId, int studyTypeId) throws SQLException, UserException, SystemException {
    
    StringBuffer errorMessage = new StringBuffer();  
    StringBuffer query = new StringBuffer();
    query.append("select study_name from study where study_name = '" + Validator.checkEscapes(studyName) + "' ");
    if (!Nuller.isNull(studyId))
        query.append("and study_id != " + studyId);
    query.append(";");

    Statement stmt = getConnection().createStatement();

    ResultSet rsNameCheck = stmt.executeQuery(query.toString());
    if (rsNameCheck.next()) 
        errorMessage.append(ExceptionMessages.STUDY_NAME_EXISTS_PART1 + studyName + ExceptionMessages.STUDY_NAME_EXISTS_PART2);        
    rsNameCheck.close();
    
    ResultSet rsTypeCheck = stmt.executeQuery("select study_type_id from study_type where study_type_id = " + studyTypeId);
    if (!rsTypeCheck.next()) {
        if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
        errorMessage.append(ExceptionMessages.NO_SUCH_STUDY_TYPE);  
    }
    rsTypeCheck.close();
    
    stmt.close();
       
    if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }

 /**
  * Check if the given study type id exists. If it exists, it is returned, otherwise
  * the first study type id in the database is returned. This method is used in 
  * updateStudyTemp, where the selected study type id for the study might have been
  * deleted and no error message should be given, because the update is to the
  * temp table, and not an actual update requested from the user. 
  * @param studyTypeId the study type chosen for this study.
  * @return studyTypeId 
 * @throws SystemException 
  * @throws SQLException.
  */ 
  private int validateStudyTypeForTempUpdate(int studyTypeId) throws SQLException, SystemException {
    
    Statement stmt = getConnection().createStatement();

    ResultSet rs = stmt.executeQuery("select study_type_id from study_type where study_type_id = " + studyTypeId);
    if (!rs.next()) {
        ResultSet rs2 = stmt.executeQuery("select study_type_id from study_type limit 1;");
        if (rs2.next())
            studyTypeId = rs2.getInt("study_type_id");
        rs2.close();
    }
    
    rs.close();
    stmt.close();
       
    return studyTypeId;
 
  }
  
  
 /**
  * Returns a File containg information about all studies in the database, fields are separated by ';'
 * @throws Exception 
  */ 
  public File exportStudies() throws Exception {
  
    String tmpDir = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE).getString("tmpDir");
    Vector studies = new Vector();  
    StringBuffer query = new StringBuffer();
    query.append("select s.study_id, s.study_name, s.study_description, s.study_notes, st.study_type, s.study_duration, ");
    query.append("sd.study_duration_unit, s.study_start_date, s.study_end_date, s.study_keywords, s.no_students, ");
    query.append("s.no_professionals, s.study_owner, s.last_edited_by ");
    query.append("from study_type st, study s ");
    query.append("left join (study_duration_unit sd) on s.study_duration_unit_id = sd.study_duration_unit_id ");
//    query.append("from study s, study_type st ");
//    query.append("left join study_duration_unit sd on s.study_duration_unit_id = sd.study_duration_unit_id "); 
    query.append("where s.study_type_id = st.study_type_id order by s.study_name");
      
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    String colDel = "; ";
    String columns = "STUDY NAME"+colDel+"STUDY DESCRIPTION"+colDel+"STUDY NOTES"+colDel+"STUDY TYPE"+colDel+"STUDY DURATION"+colDel+"START DATE"+colDel+"END DATE"+colDel+
        "KEYWORDS"+colDel+"NUMBER OF STUDENT PARTICIPANTS"+colDel+"NUMBER OF PROFESSIONAL PARTICIPANTS"+colDel+"STUDY OWNER"+colDel+"LAST EDITED BY"+colDel+
        "STUDY RESPONSIBLES"+colDel+"STUDY PUBLICATIONS"+colDel+"STUDY MATERIAL\n";
    
   File file = new File(tmpDir + "allstudies.csv");
   FileWriter fw = new FileWriter(file);
   fw.write(columns);
   while (rs.next()) {
        //Line breaks are removed from Study description and Study notes 
        String desc = rsh.getString("study_description").replace('\n',' ').replace('\r',' ');
        String notes = rsh.getString("study_notes").replace('\n',' ').replace('\r',' ');
        StringBuffer study = new StringBuffer().append(rsh.getString("study_name")).append(colDel).append(desc).append(colDel).append(notes).append(colDel)
        	.append(rsh.getString("study_type")).append(colDel).append(rsh.getIntString("study_duration")) 
        	.append(rsh.getString("study_duration_unit")).append(colDel).append(rsh.getString("study_start_date")) 
        	.append(colDel).append(rsh.getString("study_end_date")).append(colDel).append(rsh.getString("study_keywords")) 
            .append(colDel).append(rsh.getIntString("no_students")).append(colDel).append(rsh.getIntString("no_professionals")) 
            .append(colDel).append(rsh.getString("study_owner")).append(colDel).append(rsh.getString("last_edited_by")).append(colDel);

        int studyId = rsh.getInt("study_id");
        
        StringBuffer responsiblesStr = new StringBuffer();
        Enumeration responsibles = getStudyResponsibles(studyId, false).elements();
		while (responsibles.hasMoreElements()) {
			String value = (String) responsibles.nextElement();
			if (responsiblesStr.length() > 0) {
				responsiblesStr.append(": ");
			}
			responsiblesStr.append(value);
		}
		study.append(responsiblesStr).append(colDel);
		
        StringBuffer publicationsStr = new StringBuffer();
        Enumeration publications = getPublications(studyId, false).elements();
		while (publications.hasMoreElements()) {
			String value = (String) publications.nextElement();
			if (publicationsStr.length() > 0) {
				publicationsStr.append(": ");
			}
			publicationsStr.append(value.replace('\n',' ').replace('\r',' '));
		}
		study.append(publicationsStr).append(colDel);
		
        StringBuffer materialStr = new StringBuffer();
        Enumeration material = getStudyMaterial(studyId, true, false).elements();
		while (material.hasMoreElements()) {
			StudyMaterialDTO bean = (StudyMaterialDTO) material.nextElement();
			if (materialStr.length() > 0) {
				materialStr.append(": ");
			}
			materialStr.append(bean.getDescription());
            materialStr.append((bean.isFile() ? "File: " + bean.getFileName() : "URL: "+ bean.getUrl()));
		}
		study.append(materialStr).append("\n");
		
        fw.write(study.toString());
    }
        fw.close(); 
    return file;
  }

 /**
  * Returns a GraphicalReportDTO containing a Vector with GraphicalDTOs and a 
  * Vector containing all studytypes, each having their one colour. 
  * @param studyTypes a Hashtable containing all study types and study type ids.
  * @return GraphicalWrapperDTO
  * @throws SQLException, UserException
 * @throws SystemException 
  */ 
  public GraphicalReportDTO getStudyInfoGraphically(Hashtable studyTypes) throws SQLException, UserException, SystemException {
      
      GraphicalReportDTO graphicalReport = new GraphicalReportDTO();
      Vector graphicalItems = new Vector();
      
      String query;
      int earliestYear = Parser.parseInt(ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE).getString("earliestYearInReport"));
      int latestYear = 0;
      Statement stmt = getConnection().createStatement();
      ResultSet rs = stmt.executeQuery("select max(study_end_date) as max_date from study;");
      if (rs.next()) {
          String latestYearStr = rs.getString("max_date");
          if (Nuller.isReallyNull(latestYearStr)) 
               return graphicalReport;//No studies in the database, return an empty graphicalReport object. 
          else 
            latestYear = Parser.parseInt(new StringBuffer(latestYearStr).delete(4, 8).toString());
      }
      
      latestYear = Parser.parseInt(String.valueOf(latestYear).concat("0000"));
      
      GraphicalDTO graphicalStudyItem;
      GraphicalStudyTypeDTO graphicalStudyType;
      GraphicalStudyTypeDTO graphicalItem;
      String studyTypeId;
      String colour;
      Iterator it;
      Vector studyTypesAndColour = new Vector(); 
      
      boolean fetchStudyTypesAndColour = true;
      for (int i = latestYear; i >= earliestYear; i-=10000) {
        graphicalStudyItem = new GraphicalDTO();  
        graphicalStudyItem.setYear(new StringBuffer(String.valueOf(i)).delete(4, 8).toString());
        it = studyTypes.keySet().iterator();
        boolean studiesExist = false;
        ColourGenerator colourGenerator = new ColourGenerator();
        while (it.hasNext()) {
            studyTypeId = (String)it.next();
            colour = colourGenerator.getNextColour();
            if (fetchStudyTypesAndColour) {
                graphicalStudyType = new GraphicalStudyTypeDTO();  
                graphicalStudyType.setColour(colour);
                graphicalStudyType.setType((String)studyTypes.get(studyTypeId));
                studyTypesAndColour.add(graphicalStudyType);
            }

            rs = stmt.executeQuery("select count(*) as number from study where study_type_id = " + studyTypeId + " and study_end_date > " + i + " and study_end_date < " + String.valueOf(i+10000));

            if (rs.next()) {
                int number = rs.getInt("number");
                if (number != 0) {
                    graphicalItem = new GraphicalStudyTypeDTO();  
                    graphicalItem.setNumber(number);
                    graphicalItem.setColour(colour);
                    graphicalStudyItem.getStudyTypes().add(graphicalItem);
                    studiesExist = true;
                }
            }
        }
        fetchStudyTypesAndColour = false;
        graphicalStudyItem.setNoStudiesThisYear(!studiesExist);
        graphicalItems.add(graphicalStudyItem);
      }
      
      rs.close();
      stmt.close();
      
      graphicalReport.setGraphicalItems(graphicalItems);
      graphicalReport.setStudyTypesAndColour(studyTypesAndColour); 
      
      return graphicalReport;
  }
  
  
}
