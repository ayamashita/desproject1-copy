package com.tec.des.dao;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import com.tec.des.db.ConnectionPool;
import com.tec.des.util.Constants;
import com.tec.shared.exceptions.SystemException;

/**
 * Superclass for Data Access Objects. 
 * The DAO encapsulates the database connection and the sql for reading and writing data.
 * The DAO creates a database connection in the constructor. 
 * The implementing subclass uses this connection in its read/write methods.
 * The connection is freed by calling the DAO's close method.
 * The DAO super class also has methods for fetching the name of other databases that the
 * application needs to perform selects.
 *
 * @author :  Norunn Haug Christensen
 */
public abstract class DAO  {
private ConnectionPool pool = null;
  private Connection connection = null;
  private static XmlRpcClient rcpClientForAuthentication = null;
  private static XmlRpcClient rcpClient;
  
 /**
  * Constructs a DAO.
  *
  */
  public DAO() {
    super();
  }

 /**
  * Returns the database connection.
  *
  * @return The connection
 * @throws SQLException 
 * @throws SystemException 
  */
  protected Connection getConnection() throws SQLException, SystemException {
	  if (connection == null) {
		    pool = ConnectionPool.Instance();
		    connection = pool.getConnection();
	  }
	  return connection;
  }

 /**
  * Closes the DAO, frees the connection.
  *
  * @throws SQLException  
  */
  public void closeConnection() throws SQLException{
	  if (connection != null) {
		  pool.releaseConnection(connection);
	  }
  }
  
	protected XmlRpcClient getRcpClient() throws MalformedURLException {
		if (rcpClient == null) {
	    	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	    	rcpClient = new XmlRpcClient();
	    	try {
				config.setServerURL(new URL(Constants.SIMULAWEB_SERVICE_URL));
			} catch (MalformedURLException e) {
				throw e;
			}
	    	rcpClient.setConfig(config);
		}
		return rcpClient;
	}
  
	protected XmlRpcClient getRcpClientForAuthentication() throws MalformedURLException {
		if (rcpClientForAuthentication == null) {
	    	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	    	rcpClientForAuthentication = new XmlRpcClient();
	    	try {
				config.setServerURL(new URL(Constants.USER_AUTHENTICATION_SERVER_URL));
			} catch (MalformedURLException e) {
				throw e;
			}
			rcpClientForAuthentication.setConfig(config);
		}
		return rcpClientForAuthentication;
	}
	
	public Object[] getRcpBeanByFilter(String method, HashMap hash) throws MalformedURLException {
		try
		{
			if (hash == null) {
				hash = new HashMap();
			} 
			Object[] methodParams = new Object[]{ hash }; 
	    	Object resultSet = getRcpClient().execute(Constants.SIMULAWEB_SERVICE_METHOD_PREFIX+method, methodParams);
	    	return (Object[])resultSet;
		}
		catch (XmlRpcException xmlException)
		{
			return new Object[]{};
		} catch (MalformedURLException e) {
			throw e;
		}

	}

	protected String getSimulaDb() {
		System.out.print("Removed method");
		return "";
	}

	   /**
     * Closes the open result set
     * and its statement object
     *
     * @param rs the result set
     * @return true if the action was successful
     */
    public boolean closeResultSet(ResultSet rs) {
        try {
            if (rs != null) {
                Statement stmt = rs.getStatement();

                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                } else {
                    rs.close();
                    rs = null;
                }
            }

            return true;
        } catch (SQLException sqle) {
            return false;
        }
    }
}