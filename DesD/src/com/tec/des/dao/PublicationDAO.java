package com.tec.des.dao;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.tec.des.dto.PublicationDTO;
import com.tec.des.util.Constants;
import com.tec.des.util.Validator;
import com.tec.shared.exceptions.SystemException;

/**
 * Class encapsulating Publication data access.
 *
 * @author :  Per Kristian Foss
 */
public class PublicationDAO extends DAO {

    private static final String FILTER_ID = "id";
	private static final String FILTER_ABSTRACT = "abstract";
	private static final String FILTER_SOURCE = "source";
	private static final String FILTER_TITLE = "title";
	private static final String FILTER_AUTHORS = "authors";

	private static final String PARAM_SOURCE = "source";
	private static final String PARAM_TITLE = "title";
	private static final String PARAM_URL = "url";
	private static final String PARAM_ID = "id";

	private static final String RCP_METHOD = "getPublications";
	private static HashMap cache = new HashMap();
	
    


 /**
  * Constructs a PublicationDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
  public PublicationDAO() throws SystemException, SQLException {
	  super();
  }

 /**
  * Returns a Vector containing selected Publications. 
  * @param String containing Id's of selected Publications
  * @return Vector
 * @throws Exception 
  */

  public Vector getSelectedPublications(String publicationIds) throws Exception {
    
    Vector publications = new Vector();
    
    if (!StringUtils.isEmpty(publicationIds)) {
    	String[] ids = publicationIds.split(Constants.ID_SEPARATOR);
    	for (int i = 0; i < ids.length; i++) {
    		String publicationId = ids[i];
    		PublicationDTO pub = getPublication(publicationId);
    		if (pub != null) {
    			publications.add(pub);
    		}
    	}    
    }
    
    return publications;
  }
  
  /**
   * Retrieves a single publication from the publication table
   *
   * @param id the publication to retrieve
   * @return the PublicationDTO representing the given id
   */
  public PublicationDTO getPublication(String id) throws Exception {
  	
	HashMap hash = new HashMap(); 
	hash.put(FILTER_ID, StringUtils.trim(id));
  	Object[] elementList = getPublicationsByFilter(hash);
  	if (elementList == null || elementList.length == 0) {
  		throw new IllegalStateException("Publication with id="+id+" does not exist in simula web service.");
  	}
  	
  	HashMap element = (HashMap)elementList[0];
  	
  	return PublicationDAO.createPublicationBean(element);
  }
  
	/**
	 * @param filterHash
	 * @return Publications (HashMap) searched by HashMap filter
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException 
	 */
	public HashMap addToPublicationList(HashMap filterHash, HashMap publicationList) throws MalformedURLException, UnsupportedEncodingException {
		if (publicationList == null) {
			publicationList = new HashMap();
		}
	  	Object[] elementList = getPublicationsByFilter(filterHash);
	  	for (int i = 0; i < elementList.length; i++) {
	  		HashMap element = (HashMap)elementList[i];
	  		PublicationDTO publication = PublicationDAO.createPublicationBean(element);
	  		if (!publicationList.containsKey(publication.getId())) {
	  			publicationList.put(publication.getId(), publication);
	  		}
		}

	  	return publicationList;
	}
  
	/**
	 * @param hash
	 * @return Publications by filled filter
	 * @throws MalformedURLException
	 */
	public Object[] getPublicationsByFilter(HashMap hash) throws MalformedURLException {
		return getRcpBeanByFilter(RCP_METHOD, hash);
	}
  
 /**
  * Returns a Vector containing Publications, based on the given search criteria. 
  * @param String searchText containing the words typed in by user
  * @return Vector
 * @throws Exception 
  */

  public Vector findPublications(String searchText) throws Exception { 

	if (searchText == null || StringUtils.isEmpty(searchText.trim())) {
		return getPublicationList();
	}
	  
	Vector selectlist = new Vector();         
    String[] searchCriteria = Validator.checkEscapes(searchText).split(" ");
 
    HashMap selectMaplist = new HashMap();
    
    HashMap lastSearchList = new HashMap();
    if (searchCriteria.length > 0) {
        for (int i = 0; i < searchCriteria.length; i++) {            
			String oneCriteria = searchCriteria[i];
			HashMap searchList = new HashMap();
			selectMaplist.clear();
			
			HashMap filterHash = new HashMap();
			filterHash.put(FILTER_AUTHORS, oneCriteria);
			addToPublicationList(filterHash, searchList);
			
			filterHash = new HashMap();
			filterHash.put(FILTER_TITLE, oneCriteria);			
			addToPublicationList(filterHash, searchList);
			
			filterHash = new HashMap();
			filterHash.put(FILTER_SOURCE, oneCriteria);
			addToPublicationList(filterHash, searchList);
			
			filterHash = new HashMap();
			filterHash.put(FILTER_ABSTRACT, oneCriteria);
			addToPublicationList(filterHash, searchList);
			
			if (searchList.isEmpty()) {
				break;
			}
			else if (i == 0) {
				lastSearchList = searchList;
				selectMaplist.putAll(lastSearchList);
			}
			else {				
				for (Iterator iterator = lastSearchList.keySet().iterator(); iterator.hasNext();) {
					String pubId = (String) iterator.next();
					
					if (searchList.containsKey(pubId)) {					
						selectMaplist.put(pubId, searchList.get(pubId));
					}
				}
				lastSearchList.clear();
				lastSearchList.putAll(selectMaplist);
			}

			if (lastSearchList.isEmpty()) {
				break;
			}
        }    
    }

    for (Iterator iterator = selectMaplist.values().iterator(); iterator.hasNext();) {
		selectlist.add((PublicationDTO) iterator.next());
	}
    
    return selectlist;
  }

public void preparePublicationIdsWhere(StringBuffer query, String criteria) throws MalformedURLException {
	ArrayList publicationHash = new ArrayList();
	HashMap hash = new HashMap();
	hash.put(FILTER_TITLE, criteria);
	Object[] elementList = getPublicationsByFilter(hash);
	
	if (elementList != null) {
		publicationHash.addAll(Arrays.asList(elementList));
	}
	
	for (int i = 0; i < publicationHash.size(); i++) {
		HashMap element = (HashMap) publicationHash.get(i);
		if (i > 0) {
			query.append(Constants.ID_SEPARATOR);
		}
		
		query.append("'").append((String) element.get(PARAM_ID)).append("'");
	}
}
 
public static PublicationDTO createPublicationBean(HashMap map) throws UnsupportedEncodingException {

	if (!map.containsKey(PARAM_ID)) {
		return null;
	}
	
	PublicationDTO bean = new PublicationDTO();
	bean.setId((String) map.get(PARAM_ID));
	bean.setTitle(URLDecoder.decode((String) map.get(PARAM_TITLE), "UTF-8"));
	bean.setSource((String) map.get(PARAM_SOURCE));
	bean.setUrl((String) map.get(PARAM_URL));
  
	putIntoCache(bean);
	
	return bean;
}

protected static void putIntoCache(PublicationDTO bean) {
	cache.put(bean.getId(), bean);
}

public static PublicationDTO getFromCache(String publicationId) {
	if (!cache.containsKey(publicationId)) {
		PublicationDAO db;
		try {
			db = new PublicationDAO();
			db.getPublication(publicationId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	return (PublicationDTO) cache.get(publicationId);
}

/**
 * Retrieves all publications
 *
 * @return the publications
 * @throws MalformedURLException
 * @throws UnsupportedEncodingException 
 */
public Vector getPublicationList() throws MalformedURLException, UnsupportedEncodingException {
	Vector publicationList = new Vector();

	Object[] elementList = (Object[]) getPublicationsByFilter(null);
	for(int j = 0; j<elementList.length; j++)
	{
		PublicationDTO publication = PublicationDAO.createPublicationBean((HashMap)elementList[j]);

    	if (publication != null) {
    		publicationList.add(publication);
    	}
	}
    
    return publicationList;
}

}