package com.tec.des.dao;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.dto.UserDTO;
import com.tec.des.util.Constants;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Validator;
import com.tec.server.db.ResultSetHelper;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;
import com.tec.shared.exceptions.UserException;

/**
 * Class encapsulating People data access.
 *
 * @author :  Per Kristian Foss
 */
public class PeopleDAO extends DAO {

    private static final String FILTER_ID = "id";
	private static final String FILTER_FIRSTNAME = "firstname";
	private static final String FILTER_LASTNAME = "lastname";
	private static final String PARAM_ID = FILTER_ID;
	private static final String PARAM_FIRSTNAME = FILTER_FIRSTNAME;
	private static final String PARAM_LASTNAME = FILTER_LASTNAME;
	private static final String PARAM_URL = "url";
	private static final String RCP_METHOD = "getPeople";
	private static HashMap cache = new HashMap();
	
 /**
  * Constructs a PeopleDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
    public PeopleDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a Vector containing all People. 
  * @return Vector
 * @throws MalformedURLException 
  */

  public Vector getAllPeople() throws MalformedURLException {

    Vector people = new Vector();
       
	HashMap hash = new HashMap();
	hash.put("sort_on", PARAM_LASTNAME);
	Object[] elementList = (Object[]) getPeopleByFilter(hash);

	for(int j = 0; j<elementList.length; j++)
	{
		HashMap element = (HashMap)elementList[j];
        StudyResponsibleDTO person = new StudyResponsibleDTO();
        fillPersonFrom(element, person);
        people.add(person);
	}

    return people;
  }

private void fillPersonFrom(HashMap element, StudyResponsibleDTO person) {
	if (element == null || element.isEmpty()) {
		return;
	}
	
	person.setId((String) element.get(PARAM_ID));
	person.setListName((String) element.get(PARAM_LASTNAME) + ", " + (String) element.get(PARAM_FIRSTNAME));
	person.setSingleName((String) element.get(PARAM_FIRSTNAME) + " " + (String) element.get(PARAM_LASTNAME));
	person.setUrl((String) element.get(PARAM_URL));
	
	putIntoCache(person);
}
  
	/**
	 * @param hash
	 * @return Object[] of people map by filter conditions 
	 * @throws MalformedURLException
	 */
	public Object[] getPeopleByFilter(HashMap hash) throws MalformedURLException {
		LoggerFactory.getLogger(PeopleDAO.class).debug("Rcp method="+RCP_METHOD+" with params="+hash.toString());
		return getRcpBeanByFilter(RCP_METHOD, hash);

	}
  
 /**
  * Returns a Vector containing selected Study Responsibles. 
  * @param String[] containg the ids of the study responsibles to fetch.
  * @return Vector
 * @throws MalformedURLException 
  */

  public Vector getSelectedPeople(String[] selectedPeople) throws MalformedURLException {

    Vector people = new Vector();
    
    for (int i = 0; i < selectedPeople.length; i++) {
    	String personId = selectedPeople[i];
    	if (StringUtils.isNotBlank(personId) && !"-1".equals(personId)) {
    		people.add(getPerson(personId));
    	}
    }
    
    if (!people.isEmpty()) {
		Collections.sort(people, StudyResponsibleDTO.PEOPLE_COMPARATOR);
    }

    return people;
  }

public StudyResponsibleDTO getPerson(String personId) throws MalformedURLException {
	StudyResponsibleDTO person = new StudyResponsibleDTO();
	readPerson(personId, person);
	
	return person;
}

/**
 * Read Person from web services
 * @param personId
 * @param person
 * @throws MalformedURLException
 */
public void readPerson(String personId, StudyResponsibleDTO person) throws MalformedURLException {
	HashMap hash = new HashMap(); 
	hash.put(FILTER_ID, personId);	
	Object[] elementList = getPeopleByFilter(hash);

	if (elementList.length != 1) {
		throw new IllegalStateException("Person with id="+personId+" is not found.");
	}

	fillPersonFrom((HashMap)elementList[0], person);
}
 
protected static void putIntoCache(StudyResponsibleDTO bean) {
	cache.put(bean.getId(), bean);
}

public static StudyResponsibleDTO getFromCache(String responsibleId) {
	if (!cache.containsKey(responsibleId)) {
		PeopleDAO db;
		try {
			db = new PeopleDAO();
			db.getPerson(responsibleId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	return (StudyResponsibleDTO) cache.get(responsibleId);
}

 /**
  * Fetches all people matching the given searchText. 
  * @param onlyRole If true only people with role matching the search criteria is fetched.
  * @param searchText The search criteria
  * @return Vector containing UserDTOs.
  * @throws SQLException
 * @throws MalformedURLException 
 * @throws SystemException 
  */

  public Vector getPeopleRole(boolean onlyRole, String searchText) throws SQLException, MalformedURLException, SystemException {
	  
	Vector peopleRole = new Vector();
    StringBuffer query = new StringBuffer();    

    query.append("select pr.people_id, r.role_id, r.role_name from ");
    if (onlyRole) 
        query.append(" people_role pr, role r where pr.role_id = r.role_id");
    else {
        query.append(" people_role pr ");
        query.append(" left join role r on pr.role_id = r.role_id");
    }
    
    if (searchText != null && !StringUtils.isEmpty(searchText)) {
        String[] searchCriteria = Validator.checkEscapes(searchText).split(" ");
        if (onlyRole) {
            query.append(" and");
        }
        else {
            query.append(" where");
        }
        
		if (searchCriteria.length > 0) {
			
            ArrayList peopleList = new ArrayList();
            for (int i = 0; i < searchCriteria.length; i++) {
				ArrayList tempList = getPeople(searchCriteria[i]);
				if (tempList.isEmpty()) {
					return peopleRole;
				}
				peopleList.addAll(tempList);
            }

			query.append(" pr.people_id in (");preparePeopleIdsWhereInto(query, peopleList);query.append(") ");
		}
    }
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);

    while (rs.next()) {
    	
        UserDTO user = new UserDTO();
        user.setRoleId(rsh.getInt("role_id"));           
        user.setRoleName(rsh.getString("role_name"));
        readPerson(rsh.getString("people_id"), user);
        
        peopleRole.add(user);   
    }
    
    rs.close();
    stmt.close();

    Collections.sort(peopleRole, StudyResponsibleDTO.PEOPLE_COMPARATOR);
    
    return peopleRole;
  }

public void preparePeopleIdsWhere(StringBuffer query, String searchParam) throws MalformedURLException {
	ArrayList peopleList = getPeople(searchParam);
	preparePeopleIdsWhereInto(query, peopleList);
}

public void preparePeopleIdsWhereInto(StringBuffer query, ArrayList peopleList) {
	if (peopleList != null) {
		for (int i = 0; i < peopleList.size(); i++) {
			HashMap element = (HashMap) peopleList.get(i);
			if (i > 0) {
				query.append(", ");
			}
			
			query.append("'").append((String) element.get(PARAM_ID)).append("'");
		}
	}
}

public ArrayList getPeople(String searchParam) throws MalformedURLException {
	ArrayList peopleList = new ArrayList();
	HashMap hash = new HashMap();
	hash.put(FILTER_FIRSTNAME, searchParam);
	Object[] elementList = getPeopleByFilter(hash);
	if (elementList != null) {
		peopleList.addAll(Arrays.asList(elementList));
	}

	hash = new HashMap();
	hash.put(FILTER_LASTNAME, searchParam);
	elementList = getPeopleByFilter(hash);
	if (elementList != null) {
		peopleList.addAll(Arrays.asList(elementList));
	}
	return peopleList;
}
  
  /**
  * Returns a hashtable containing all Roles in the database 
  * @param Hashtable
  * @throws SQLException
 * @throws SystemException 
  */

  public Hashtable getRoles() throws SQLException, SystemException {

    Hashtable roles = new Hashtable();
    StringBuffer query = new StringBuffer();    
    
    query.append("select role_id, role_name from role;");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        roles.put(String.valueOf(rsh.getInt("role_id")), rsh.getString("role_name"));
    }   
    
    rs.close();
    stmt.close();
    
    return roles;
  }
  
 /**
  * Updates the people_role tablewith the given hashtable.
  * @throws SQLException
 * @throws SystemException 
  */

  public void setPeopleRole(Hashtable peopleRole) throws SQLException, SystemException {

    StringBuffer query_find = new StringBuffer();
    StringBuffer query_update = new StringBuffer();
    String[] peopleId = (String[])peopleRole.keySet().toArray(new String[0]);
    String[] peopleRoleId = (String[])peopleRole.values().toArray(new String[0]);
    Vector peopleWithRole = new Vector();;
    
    // Fetch all people having a role
    query_find.append("select people_id from people_role;");    
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query_find.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query_find.toString());    
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        peopleWithRole.add(rsh.getString("people_id"));
    }   
    
    // Checks previous role (if any) with new role and updates people_role table accordingly
    for (int i = 0; i < peopleId.length; i++) {       
        if (peopleWithRole.contains(peopleId[i])) {
            if (peopleRoleId[i].equals("0"))
                stmt.executeUpdate("delete from people_role where people_id = " + peopleId[i]);
            else {
                stmt.executeUpdate("update people_role set role_id = " + peopleRoleId[i] + " where people_id = " + peopleId[i] + ";");
            }
        } else {            
            if (!(peopleRoleId[i].equals("0")))
                stmt.executeUpdate("insert into people_role values (" +  peopleId[i] + Constants.ID_SEPARATOR + peopleRoleId[i] + ");");
        }
    }
    
    rs.close();
    stmt.close();
  }
  
 /**
  * Returns the user connected to the specified username and password
  * @param username (e-mail) of the user to fetch
  * @param password of the user to fetch
  * @return UserDTO
 * @throws Exception 
  */
  public UserDTO getUser(String username, String password) throws Exception {

	HashMap	hash = new HashMap();
	
	hash.put("login", username);
	hash.put("password", password);
	
	Object[] methodParams = new Object[]{hash}; 
	Object authenticationResult = getRcpClientForAuthentication().execute(Constants.RCP_AUTHENTICATION_METHOD, methodParams);        

	UserDTO user = new UserDTO();

    if (authenticationResult != null && ((Boolean)authenticationResult).booleanValue()) {
    	
    	readPerson(username, user);

        StringBuffer query = new StringBuffer();    
        query.append("select pr.people_id, r.role_id, r.role_name from ");
        query.append("	people_role pr ");
        query.append("	left join role r on pr.role_id = r.role_id ");
        query.append("where pr.people_id = '" + user.getId() + "';");
        
        LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
        Statement stmt = getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query.toString());
        ResultSetHelper rsh = new ResultSetHelper(rs);
        if (rs.next()) {
            user.setRoleName(rsh.getString("role_name"));           
            user.setRoleId(rsh.getInt("role_id"));           
        } 
        else {
            throw new UserException(ExceptionMessages.INVALID_USER_NAME_OR_PASSWORD);
        }
        
        rs.close();
        stmt.close();
        
        UserStudyReportDAO userRepDb = new UserStudyReportDAO();
		user.setStudyReports(userRepDb.getUserReports(user.getId()));
    } 
    else {
    	throw new UserException(ExceptionMessages.INVALID_USER_NAME_OR_PASSWORD);
    }

    return user;
  }
      
}