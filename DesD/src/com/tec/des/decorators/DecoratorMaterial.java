package com.tec.des.decorators;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.taglib.StudyMaterialTag;

public class DecoratorMaterial extends Decorator {

	public String decorate(Object bean, String property, Type type, HttpServletRequest request) {
		
		Object values = getValue(bean, property, type, request);
		if (values == null) {
			return "";
		}
		
		StudyMaterialTag tag = new StudyMaterialTag();
		tag.setItems((Vector) values);
		tag.setPrinterFriendly(!Type.HTML.equals(type));		
		tag.setSeparator(Type.HTML.equals(type)? "<BR>" : ", ");
		return tag.generateOutput();
	}
}
