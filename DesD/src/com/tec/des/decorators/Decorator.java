package com.tec.des.decorators;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.enum.Enum;

import com.tec.des.util.Validator;

public class Decorator {
	
    public final static class Type extends Enum {

    	public static final Type STRING = new Type("string");
    	public static final Type HTML = new Type("html");

		protected Type(String name) {
			super(name);
		}
    }
	
	public String decorate(Object bean, String property, Type type, HttpServletRequest request) {
		Object value = getValue(bean, property, type, request);
		if (value == null) {
			return "";
		}
		if (Type.HTML.equals(type)) {
			return Validator.replaceEnterWithBreak(value.toString()) + "&nbsp;";
		}
		else {
			return value.toString();
		}
	}

	protected Object getValue(Object bean, String property, Type type, HttpServletRequest request) {
		Object value = null;
		try {					
			value  = PropertyUtils.getProperty(bean, property);
		} catch (Exception e) {
			// continue
		}
		return value;
	}
	
}
