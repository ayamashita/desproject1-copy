package com.tec.des.decorators;

import com.tec.des.dao.PublicationDAO;

public class DecoratorPublication extends DecoratorLink {
	
	/* (non-Javadoc)
	 * @see com.tec.des.decorators.DecoratorLink#prepareHref(java.lang.String)
	 */
	protected String prepareHref(String key) {
		return PublicationDAO.getFromCache(key).getUrl();
	}
	
	/* (non-Javadoc)
	 * @see com.tec.des.decorators.DecoratorLink#prepareValue(java.lang.String)
	 */
	protected String prepareValue(String key) {
		return PublicationDAO.getFromCache(key).getTitle();
	}
}
