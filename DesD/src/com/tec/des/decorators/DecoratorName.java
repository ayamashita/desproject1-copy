package com.tec.des.decorators;

import javax.servlet.http.HttpServletRequest;

import com.tec.des.dto.StudyDTO;
import com.tec.des.http.WebConstants;
import com.tec.des.http.WebKeys;

public class DecoratorName extends Decorator {
 
	protected Object getValue(Object bean, String property, Type type, HttpServletRequest request) {
		Object value = super.getValue(bean, property, type, request);

		if (Type.HTML.equals(type)) {
			StringBuilder result = new StringBuilder();
			
			result.append("<a href=\"javascript:document.form.submit();\" onclick=\"document.form.target='_self';document.form."+WebKeys.REQUEST_PARAM_USECASE+".value='"+WebConstants.USECASE_SINGLE_STUDY_REPORT+"';document.form."+WebKeys.REQUEST_PARAM_STUDY_ID+".value='"+((StudyDTO)bean).getId()+"';document.form."+WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY+".value='false';\">").append(value).append("&nbsp;</a>");
			return result.toString();		 
		}

		return value;
	}
}
