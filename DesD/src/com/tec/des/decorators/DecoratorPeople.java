package com.tec.des.decorators;

import com.tec.des.dao.PeopleDAO;

public class DecoratorPeople extends DecoratorLink {

	/* (non-Javadoc)
	 * @see com.tec.des.decorators.DecoratorLink#prepareHref(java.lang.String)
	 */
	protected String prepareHref(String key) {
		return PeopleDAO.getFromCache(key).getUrl();
	}

	/* (non-Javadoc)
	 * @see com.tec.des.decorators.DecoratorLink#prepareValue(java.lang.String)
	 */
	protected String prepareValue(String key) {
		return PeopleDAO.getFromCache(key).getListName();
	}
	
}
