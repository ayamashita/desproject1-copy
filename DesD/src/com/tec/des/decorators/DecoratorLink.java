package com.tec.des.decorators;

import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

public abstract class DecoratorLink extends Decorator {
	public String decorate(Object bean, String property, Type type, HttpServletRequest request) {
		
		Hashtable list = (Hashtable) super.getValue(bean, property, type, request);
		String startLink = "";
		String endLink = "";
		String breakRow = "";

		if (Type.HTML.equals(type)) {
			endLink = "</a>";
			breakRow = "<br/>";
		}
		else {
			breakRow = ", ";
		} 

    	if (list == null) {
    		return "";
    	}
    	
    	StringBuilder string = new StringBuilder(""); 
    	
    	for (Iterator iterator = list.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
	    	if (string.length() > 1) {
	    		string.append(breakRow);
	    	}
			if (Type.HTML.equals(type)) {
				startLink = "<a href=\"" + prepareHref(key) + "\" target=\"_blank\">";
			}
			string.append(startLink).append(prepareValue(key)).append(endLink);
		}
    	
        return string.toString();
		
	}

	protected abstract String prepareHref(String key);
	protected abstract String prepareValue(String key);
}
