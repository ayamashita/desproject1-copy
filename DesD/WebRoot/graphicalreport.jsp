<%@ page import="java.util.*" %>
<%@ page import="com.tec.des.dto.GraphicalDTO" %>
<%@ taglib uri="/util" prefix="util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>[ simula . research laboratory ]</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" type="text/css" href="simula.css">
</HEAD>
<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<jsp:useBean id="graphicalReportItems" class="java.util.Vector" scope="request"/>
<jsp:useBean id="studyTypes" class="java.util.Vector" scope="request"/>
<TABLE cellspacing="4" cellpadding="4" width="100%" bgcolor="#f5f5f5"><TR><TD>
<jsp:include page="messages.jsp"/>

<TABLE cellspacing="4" cellpadding="1" bgcolor="#f5f5f5"> 
    <TR>
        <TD>&nbsp;</TD>
    </TR> 
    <TR>
        <TD>&nbsp;</TD>
        <TD colspan="2"><H1>Number of Studies by Type of Study per Year</H1></TD>
    </TR> 
    <TR>
        <TD>
            <TABLE width='70'>
                <TR><TD>&nbsp;</TD></TR>
            </TABLE>
        </TD>
        <TD>
        <TABLE><!-- Start table diagram -->
<% 
if (!graphicalReportItems.isEmpty()) {
    Iterator it = graphicalReportItems.iterator();
    GraphicalDTO graphicalItem;
        while (it.hasNext()) {
            graphicalItem = (GraphicalDTO)it.next();
%>
       <TR>
         <TD>
            <TABLE><!-- Start table year item -->
                <TR>
                    <TD class="bodytext-bold" valign="top"><%=graphicalItem.getYear()%></TD>
                </TR>
<%      if (!graphicalItem.noStudiesThisYear()) { %>
                <TR>
                    <TD>
                    <TABLE>
                    <util:iteration name="graphicalStudyType" type="com.tec.des.dto.GraphicalStudyTypeDTO" rowAlternator="row" group="<%=(Object[])graphicalItem.getStudyTypes().toArray(new Object[0])%>">
                        <TR>
                            <TD>
                            <TABLE>
                                <TR>
                                    <TD class="bodytext" valign="top"><%=graphicalStudyType.getNumber()%></TD>
                                </TR>
                            </TABLE>
                            </TD>
                            <TD>
                            <TABLE>
                                <TR bgcolor="<%=graphicalStudyType.getColour()%>">
                                    <TD class="bodytext" valign="top" width="<%=String.valueOf(graphicalStudyType.getNumber()*10)%>">&nbsp;</TD>
                                </TR>
                            </TABLE>
                            </TD>
                        </TR>
                    </util:iteration>
                    </TABLE>
                    </TD>
                </TR>
<%  } %>
                <TR>
                    <TD>&nbsp;</TD>
                </TR>
          </TABLE><!-- End table year item -->
       </TD>
    </TR>     
<%  }
 } else { %>
     <TR>
        <TD class="bodytext" valign="top">There are no studies in the database.</TD>
     </TR>
<%  } %>
    </TABLE><!-- End table year diagram -->
    </TD>
        <TD valign="top" align="right" width="60%">
            <TABLE>
            <util:iteration name="studyType" type="com.tec.des.dto.GraphicalStudyTypeDTO" rowAlternator="row" group="<%=(Object[])studyTypes.toArray(new Object[0])%>">
                <TR>
                    <TD class="bodytext" valign="top">
                        <TABLE>
                            <TR bgcolor="<%=studyType.getColour()%>">
                                <TD class="bodytext" valign="top" width="15">&nbsp;</TD>
                            </TR>
                        </TABLE>
                    </TD>
                    <TD class="bodytext" valign="top"><%=studyType.getType()%>
                    </TD>
               </TR>
            </util:iteration>
            </TABLE>
        </TD>
    </TR>
</TABLE>
</TD></TR> 
</TABLE> 
