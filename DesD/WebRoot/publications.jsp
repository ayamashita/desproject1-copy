<%@ page import="com.tec.des.http.*" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top_sml.html"/>
<jsp:useBean id="selectedPublications" class="java.util.Vector" scope="request"/>
<jsp:useBean id="selectedPublicationIds" class="java.lang.String" scope="request"/> 
<jsp:useBean id="selectedPublicationTitles" class="java.lang.String" scope="request"/> 
<jsp:useBean id="searchResult" class="java.util.Vector" scope="request"/>
<!-- Start mainframe -->
<TABLE cellspacing="0" cellpadding="0" width="100%">
<TR>
    <TD align="right"><A href="JavaScript:help('help.html#selectpublications')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
</TR>
</TABLE>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="2" cellpadding="0"> 
    <TR>
        <TD colspan="3"><H1>Select Publications</H1></TD>
    </TR> 
<% if (!selectedPublications.isEmpty()) { %>
    <TR>
        <TD colspan="2" ><P class="bodytext-bold">Selected Publications</P></TD>
        <TD align="middle"><P class="bodytext-bold">Remove</P></TD>
    </TR>
    <util:iteration name="publication" type="com.tec.des.dto.PublicationDTO" rowAlternator="row" group="<%= (Object[])selectedPublications.toArray(new Object[0]) %>">
    <TR>
        <TD class="bodytext" valign="top"><A HREF ="http://simula.no/research/engineering/publications/<%=publication.getId()%>" target='_blank'><%=publication.getTitle()%></A></TD>    
        <TD class="bodytext" valign="top"><%=publication.getSource()%></TD>
        <TD valign="top" align="center"><INPUT type="checkbox" name="<%=WebKeys.REQUEST_PARAM_PUBLICATION_REMOVE%><%=publication.getId()%>" value="<%=publication.getId()%>" class="bodytext"></TD>
    </TR>
    </util:iteration>
    <TR>
        <TD>&nbsp;</TD>
        <TD>&nbsp;</TD>
        <TD align="middle"><INPUT type="submit" name="<%=WebKeys.REQUEST_PARAM_BUTTON_REMOVE%>" value="Remove" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SELECT_PUBLICATIONS%>';" class="bodytext"></TD>
    </TR>
<% } else {%>
    <TR>
        <TD><P class="bodytext">No publications selected yet</P></TD>
    </TR>
<% } %>
</TABLE>
<BR><BR>

<TABLE cellspacing="0" cellpadding="1"> 
    <TR>
        <TD colspan="2" class="bodytext-bold">Find Publications</TD>
    </TR>
    <TR>
        <TD colspan="2">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_TEXT%>" value="" size="60">
            <INPUT type="submit" name="" value="Search" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_FIND_PUBLICATIONS%>';" class="bodytext">
        </TD>
    </TR>
    <TR>
        <TD>&nbsp;</TD>
    </TR>
<% if (!searchResult.isEmpty()) { %>
    <TR>
        <TD colspan="2" ><P class="bodytext-bold">Search results</P></TD>
        <TD align="middle"><P class="bodytext-bold">Add</P></TD>
    </TR>
    <util:iteration name="publication_add" type="com.tec.des.dto.PublicationDTO" rowAlternator="row" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
    <TR class="<%=row%>">
        <TD class="bodytext" valign="top"><A HREF ="http://simula.no/research/engineering/publications/<%=publication_add.getId()%>" target='_blank'><%=publication_add.getTitle()%></A></TD>    
        <TD class="bodytext" valign="top"><%=publication_add.getSource()%></TD>
        <TD valign="top" align="center"><INPUT type="checkbox" name="<%=WebKeys.REQUEST_PARAM_PUBLICATION_ADD%><%=publication_add.getId()%>" value="<%=publication_add.getId()%>" class="bodytext"></TD>
    </TR>
    </util:iteration>
    <TR>
        <TD>&nbsp;</TD>
        <TD>&nbsp;</TD>
        <TD align="middle">
            <INPUT type="submit" name="<%=WebKeys.REQUEST_PARAM_BUTTON_ADD%>" value="Add" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SELECT_PUBLICATIONS%>';" class="bodytext">
        </TD>
    </TR>
<% } %>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_BEAN_SELECTED_PUBLICATION_IDS%>" value="<%=selectedPublicationIds%>" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_PUBLICATIONS%>" >
</FORM>
    <TR>
        <TD>&nbsp;</TD></TR>
    <TR>
        <TD>
            <INPUT type="submit" value=" Ok " class="bodytext" onClick="JavaScript:updateOpenerWindow('<%=WebConstants.USECASE_UPDATE_STUDY%>', '<%=WebKeys.REQUEST_PARAM_STUDY_PUBLICATION_IDS%>', '<%=selectedPublicationIds%>', '<%=WebKeys.REQUEST_PARAM_STUDY_PUBLICATIONS%>', '<%=selectedPublicationTitles%>');">
            <INPUT type="submit" value="Cancel" class="bodytext" onClick="JavaScript:closeEditWindow('<%=WebConstants.USECASE_UPDATE_STUDY%>');">
        </TD>
    </TR>
</TABLE>

<!-- End content -->
<jsp:include page="bottom_sml.html"/>