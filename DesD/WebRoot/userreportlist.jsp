<%@page import="com.tec.shared.util.Nuller"%>
<%@page import="com.tec.des.dto.UserStudyReportDTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.tec.des.dto.UserDTO"%>
<%@page import="com.tec.des.http.WebKeys"%>
<%@page import="com.tec.des.http.WebConstants"%>
<%@page import="com.tec.des.util.Constants"%>
<%@ taglib uri="/util" prefix="util" %>
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<% UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER); %>
	<% if (user != null) { %>
		<FORM name="formReport" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">	        
			<TABLE cellspacing="0" cellpadding="0" width="96%">
			    <TR>
			    	<TD width="10px">
			    	</TD>
					<TD>
						<H2>
							User Reports:&nbsp;&nbsp;
				    		<select name="<%=WebKeys.REQUEST_PARAM_REPORT_ID%>"
				    				onchange="document.formReport.action='<%=WebConstants.FRONTCONTROLLER_URL%>';
				    					document.formReport.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_VIEW_USER_REPORT%>';
				    					document.formReport.submit();">
				    			<option value="">--- Select Report ---</option>
				    			<% for (Iterator iterator = user.getStudyReportsSortedlist().iterator(); iterator.hasNext();) {
					    				UserStudyReportDTO bean = (UserStudyReportDTO) iterator.next(); %>
										<option value="<%=bean.getId()%>" <%=(""+bean.getId()).equals(searchCriteria.getUserReportId()) ? "selected=\"selected\"" : ""%> >
											<%=bean.getName()%>
										</option>
								<% } %>
				    		</select>
							<A href="JavaScript:document.formReport.submit();" 
								onclick="document.formReport.target='_self';document.formReport.action='<%=WebConstants.FRONTCONTROLLER_URL%>'; 
							            document.formReport.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_ADD_USER_REPORT%>';">Create</A>
							<% if (!Nuller.isNull(searchCriteria.getUserReportId())) { %>
								&nbsp;&nbsp;
								<A href="JavaScript:document.formReport.submit();" 
									onclick="document.formReport.target='_self';document.formReport.action='<%=WebConstants.FRONTCONTROLLER_URL%>'; 
								            document.formReport.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_EDIT_USER_REPORT%>';">Edit</A>
								&nbsp;&nbsp;
								<A href="JavaScript:document.formReport.submit();" 
									onclick="document.formReport.target='_self';document.formReport.action='<%=WebConstants.FRONTCONTROLLER_URL%>'; 
								            document.formReport.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_USER_REPORT%>';return confirm('Do you really delete report!');">Delete</A>
		 					<% } %>	
	 					<H2>
					</TD>
			    </TR>
			</TABLE>
			<util:searchcriteria searchCriteria="<%= searchCriteria %>" /> 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>" value="<%=Constants.MAINTAIN_MODE_FINAL%>" > 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>" value="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" > 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_START_POSITION%>" value="" > 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>" value="" > 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="" > 
			<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID_DELETE%>" value="" > 
		</FORM>
	<% } %>