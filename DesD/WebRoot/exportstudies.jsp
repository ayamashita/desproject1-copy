<%@ page import="java.io.*,javax.servlet.*,com.tec.des.command.ExportStudiesCommand,com.tec.des.util.Message" %>
<%
//Fetches an object containing information about all studies as a csv-file
int iRead;
FileInputStream stream = null;
Message message = null;
try { 
    ExportStudiesCommand command = new ExportStudiesCommand();   
    command.execute();

    File file = command.getResult();

    response.setContentLength((int)file.length());
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition","attachment; filename=allstudies.csv");

    stream = new FileInputStream(file);
    out.clear();
    while ((iRead = stream.read()) != -1) { 
        out.write(iRead); 
    } 
    out.flush();
    stream.close();
    file.delete();
} catch (Exception ex) {
    message = new Message(ex.getMessage());
%>
<jsp:include page="top.jsp"/>
<TABLE cellspacing="6" cellpadding="6">
    <TR>
        <TD>
            <TABLE cellspacing="2" cellpadding="0"> 
            <% if (message != null) { %>
            <FONT class="errortext" ><%=message.getText()%></FONT><BR>
            <% } %>
            </TABLE>
        </TD>
    </TR> 
</TABLE> 
<jsp:include page="bottom.html"/>
<%
} finally {
    if (stream != null) {
        stream.close(); 
    }
}
%>
