
function calendar() {
	var calendarwin = window.open('','calendar','width=200,height=240,toolbar=0,scrollbars=0,location=0,status=0,menubar=0,resizable=yes');
	document.form.submit();
}

function openCalendar(url) {
	var calendarwin = window.open(url,'calendar','width=200,height=240,toolbar=0,scrollbars=0,location=0,status=0,menubar=0,resizable=yes');
}


function addDate(field, text) {
    //Check if the opener page is still displaying
    if(eval("window.opener.document.form." + field)) {
      eval("window.opener.document.form." + field + ".value = '" + text + "'");
    }
    window.close();
}

function help(helpUrl) {
    window.open(helpUrl,'helpUrl','width=460,height=550,toolbar=0,scrollbars=1,location=0,status=0,menubar=0,resizable=yes')
}

function openStudyTypes() {
	var editstwin = window.open('','editstudytypes','width=300,height=500,toolbar=0,scrollbars=0,location=0,status=0,menubar=0,resizable=yes');
	document.form.submit();
}

function openStudyTypesFromMenu() {
	var editstwin = window.open('','editstudytypesformmenu','width=300,height=500,toolbar=0,scrollbars=0,location=0,status=0,menubar=0,resizable=yes');
	document.formMenu.submit(); 
}

function openStudyDurationUnits() {
	var editsdwin = window.open('','editstudydurationunits','width=300,height=500,toolbar=0,scrollbars=0,location=0,status=0,menubar=0,resizable=yes');
	document.formMenu.submit(); 
}

function openStudyResponsibles() {
	var editsrwin = window.open('','editstudyresponsibles','width=500,height=550,toolbar=0,scrollbars=0,location=0,status=0,menubar=0,resizable=yes');
	document.form.submit();
}

function openPublications() {
	var editpwin = window.open('','editpublications','width=750,height=700,toolbar=0,scrollbars=1,location=0,status=0,menubar=0,resizable=yes');
	document.form.submit();
}

function openStudyMaterial() {
	var editsmwin = window.open('','editstudymaterial','width=750,height=700,toolbar=0,scrollbars=1,location=0,status=0,menubar=0,resizable=yes');
	document.form.submit();
}

function closeEditWindow(handler) {
if(eval("window.opener.document.form.usecase")) {
      window.opener.document.form.usecase.value = handler;
      window.opener.document.form.action = "FrontController";
      //window.opener.document.form.action = "<%=request.getContextPath()%>/FrontController";
      window.opener.document.form.target = '_self';
      window.opener.document.form.submit();
    }
    window.close();
}

function updateOpenerWindow(handler, field1, fieldvalue1, field2, fieldvalue2) {
if(eval("window.opener.document.form.usecase")) {
      eval("window.opener.document.form." + field1 + ".value = '" + fieldvalue1 + "'");	
      eval("window.opener.document.form." + field2 + ".value = '" + fieldvalue2 + "'");	
      window.opener.document.form.usecase.value = handler;
      window.opener.document.form.action = "FrontController";
      //window.opener.document.form.action = "<%=request.getContextPath()%>/FrontController";
      window.opener.document.form.target = '_self';
      window.opener.document.form.submit();
    }
    window.close();
}

function closeStudyMaterial(handler, updateMode) {
if(eval("window.opener.document.form.usecase")) {
      window.opener.document.form.usecase.value = handler;
      window.opener.document.form.study_material_update_mode.value = updateMode;
      //window.opener.document.form.action = "<%=request.getContextPath()%>/FrontController";
      window.opener.document.form.action = "FrontController";
      window.opener.document.form.target = '_self';
      window.opener.document.form.submit();
    }
    window.close();
}

function closeStudyTypes(handler, openerIsMenu) {
if (!openerIsMenu) {
	if(eval("window.opener.document.form.usecase")) {
      	window.opener.document.form.usecase.value = handler;
      	window.opener.document.form.action = "FrontController";
      	//window.opener.document.form.action = "<%=request.getContextPath()%>/FrontController";
      	window.opener.document.form.target = '_self';
      	window.opener.document.form.submit();
    	}
}
    
window.close();

}

function searchSimulaweb() {
	if (document.formMenuSearch.search_text.value){
		document.formMenuSearch.submit();
	}
	else{
		alert("Please input text to search.");
	}
}

