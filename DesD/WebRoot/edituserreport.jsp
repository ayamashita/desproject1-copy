<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Constants" %>
<%@page import="com.tec.des.dto.UserStudyReportDTO"%>
<%@page import="com.tec.des.dto.StudyDTO"%>
<%@page import="com.tec.des.dao.StudyDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.tec.server.taglib.DropDownListTag"%>
<%@page import="com.tec.des.dao.StudyDAO"%>
<%@page import="com.tec.des.decorators.Decorator"%>
<%@page import="com.tec.des.decorators.DecoratorPeople"%>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="userReport" class="com.tec.des.dto.UserStudyReportDTO" scope="request"/>
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <%=userReport.getId() > 0 ? "Edit" : "New" %> User Study Report
        </TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="4" cellpadding="4">
<TR>
	<TD>
	<jsp:include page="messages.jsp"/>
	<FORM name="form" action="" method="post">
		<TABLE>
		    <TR><TD class="bodytext-bold" valign="top">Report name*</TD></TR>
		    <TR><TD class="bodytext" valign="top"><INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_REPORT_NAME%>" value="<%=userReport.getName()%>" size="98"></TD></TR>
		    <TR><TD class="bodytext-bold" valign="top">&nbsp;</TD></TR>
		    <TR>
		    	<TD class="bodytext-bold" valign="top">
		    		Sorting 'End of Study'&nbsp;&nbsp;<util:dropdownlist optional="true" name="<%=WebKeys.REQUEST_PARAM_REPORT_SORT_END_DATE%>" optionValues="<%=(String[])UserStudyReportDTO.SortOperator.getList().toArray(new String[0])%>" selectedvalue="<%=userReport.getSortEndDate()%>" attributes="class=bodytext" />
		    	</TD>
		    </TR>
		    <TR><TD class="bodytext-bold" valign="top">&nbsp;</TD></TR>
		    <TR>
				<TD class="bodytext-bold" valign="top">
		    		Study Columns&nbsp;&nbsp;
		    		<select name="<%=WebKeys.REQUEST_PARAM_REPORT_STUDY_COLUMN_NAME%>">
		    			<% for (Iterator iterator = StudyDAO.ReportColumns.getEnumList().iterator(); iterator.hasNext();) {
			    				StudyDAO.ReportColumns bean = (StudyDAO.ReportColumns) iterator.next(); 
			    				if (!userReport.getStudyColumns().contains(bean.getName())) { %>
									<option value="<%=bean.getName()%>" <%=bean.getName().equals(userReport.getColumnSelected()) ? "selected=\"selected\"" : ""%> >
										<%=bean.getLabel()%>
									</option>
							<%  } %>
						<% } %>
		    		</select>
		    		&nbsp;&nbsp;
					<A href="JavaScript:if (document.form.<%=WebKeys.REQUEST_PARAM_REPORT_STUDY_COLUMN_NAME%>.value != '') {document.form.submit();}" 
							onclick="document.form.target='_self';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>'; 
						            document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_ADD_USER_REPORT_COLUMN%>';"><b>add</b></A>
		    		
		    	</TD>
		    </TR>
		    <TR>
		    	<TD class="bodytext-bold" valign="top">
		    		<table bgcolor="white"><tr><td></td></tr>
		    			<% for (Iterator iterator = userReport.getStudyColumns().iterator(); iterator.hasNext();) {
			    				String columnName = (String) iterator.next();
			    				StudyDAO.ReportColumns bean = StudyDAO.ReportColumns.valueOf(columnName); %>
			                      <tr>
			                        <td>&nbsp;&nbsp;</td>
			                        <td align="left" class="bodytext"><li><%=bean.getLabel()%></li></td>
			                        <td align="right" class="bodytext">&nbsp;&nbsp;
										<A href="JavaScript:document.form.submit();" 
												onclick="document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';
											            document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_USER_REPORT_COLUMN%>';
											            document.form.<%=WebKeys.REQUEST_PARAM_REPORT_DELETE_COLUMN_NAME%>.value='<%=columnName%>';"><b>Delete</b></A>
									</td>
			                      </tr>
						<% } %>
		    		</table>
		    	</TD>
		    </TR>
		    <TR><TD class="bodytext-bold" valign="top">&nbsp;</TD></TR>
		   <TR>
		       <TD class="bodytext-bold" valign="top">
		       	<A href="JavaScript:openStudyResponsibles()" 
		       		onclick="document.form.target='editstudyresponsibles';
		       				document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';
		       				document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_STUDY_RESPONSIBLES%>';
		       				document.form.<%=WebKeys.REQUEST_PARAM_TARGET_USECASE%>.value='<%=WebConstants.USECASE_REFRESH_USER_REPORT%>';">Study Responsible:</A>
		       				<% if (userReport.getResponsibles().keySet().size() > 1) { %>
			       				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Condition operator between 'Responsible':
			       				&nbsp;&nbsp;<util:dropdownlist name="<%=WebKeys.REQUEST_PARAM_REPORT_WHERE_RESPONSIBLE_OPERATOR%>" optionValues="<%=(String[])UserStudyReportDTO.WhereOperator.getList().toArray(new String[0])%>" selectedvalue="<%=userReport.getWhereResponsibleOperator()%>" attributes="class=bodytext" />
		       				<% } %>
		       				
		       </TD>
		   </TR>
		   <TR>
		   	   <% DecoratorPeople decorator = new DecoratorPeople(); %>
		       <TD class="bodytext" valign="top"><%= decorator.decorate(userReport, "responsibles", Decorator.Type.HTML, request)%></TD>
		   </TR>
			<TR><TD>&nbsp;</TD></TR>
			<TR>
			    <TD>
			        <INPUT type="submit" name="" value="Save Report" class="bodytext" onclick="document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SAVE_USER_REPORT%>';">
			       <INPUT type="submit" name="" value="Cancel" class="bodytext" onclick="document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_VIEW_USER_REPORT%>';return confirm('Discard all changes?');">
			    </TD>
			</TR>

		</TABLE>
		
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_TARGET_USECASE%>" value="" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_REPORT_ID%>" value="<%=userReport.getId()%>" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_REPORT_DELETE_COLUMN_NAME%>" value="" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_REPORT_STUDY_COLUMNS%>" value="<util:arrayprinter items="<%= (String[])userReport.getStudyColumns().toArray(new String[0]) %>" />" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLES%>" value="<util:arrayprinter items="<%= (String[])userReport.getResponsibles().values().toArray(new String[0]) %>" />" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLE_IDS%>" value="<util:arrayprinter items="<%= (String[])userReport.getResponsibles().keySet().toArray(new String[0]) %>" />" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_EDIT_USER_REPORT%>" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>" value="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" >
	</FORM>
	</TD>
</TR> 
</TABLE> 
<jsp:include page="bottom.html"/>