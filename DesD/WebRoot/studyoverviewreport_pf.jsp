<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Validator" %>
<%@ page import="com.tec.des.dao.StudyDAO"%>
<%@ page import="com.tec.des.decorators.Decorator"%>
<%@ page import="com.tec.des.dto.StudyDTO"%>
<%@ taglib uri="/util" prefix="util" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>[ simula . research laboratory ]</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" type="text/css" href="simula.css">
</HEAD>
<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<jsp:useBean id="searchResult" class="com.tec.shared.util.HitList" scope="request"/>
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<TABLE cellspacing="4" cellpadding="4" width="100%" bgcolor="#f5f5f5"><TR><TD>
<jsp:include page="messages.jsp"/>
<util:hitlist name="hitlist" mode="search" maxHits="<%=searchResult.getCount()%>" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
<TABLE cellspacing="4" cellpadding="1" bgcolor="#f5f5f5"> 
    <TR>
        <TD colspan="2"><H2>Study Overview Report</H2></TD>
        <TD colspan="4" align="right" class="bodytext" valign="top"></TD>
    </TR> 
    <TR>
    	<util:iteration name="columnName" type="String" rowAlternator="row" group="<%= (Object[])searchCriteria.getColumns().toArray(new Object[0]) %>">
    		<TD class="bodytext-bold" valign="top" nowrap="nowrap">
    			<%=StudyDAO.ReportColumns.valueOf(columnName).getLabel()%>&nbsp;&nbsp;
    		</TD>
		</util:iteration>
    </TR>
    
    <util:iteration name="study" type="StudyDTO" rowAlternator="row" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
	    <TR class="<%=row%>">
	    	<util:iteration name="columnName" type="String" rowAlternator="row" group="<%= (Object[])searchCriteria.getColumns().toArray(new Object[0]) %>">
	    		<TD class="bodytext" valign="top">
	    			<%=StudyDAO.ReportColumns.valueOf(columnName).decorate(study, Decorator.Type.STRING, request)%>&nbsp;
	    		</TD>
			</util:iteration>
	    </TR>
   	</util:iteration>
	<script type="text/javascript">
		window.opener.document.form.<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>.value = '<%=WebConstants.MAX_HIT_LIST_LENGTH%>';
		window.opener.document.form.<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>.value = '';
	</script>
</TABLE>
</util:hitlist>
</TD></TR> 
</TABLE> 
