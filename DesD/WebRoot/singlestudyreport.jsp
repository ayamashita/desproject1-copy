<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Validator" %>
<%@page import="com.tec.des.dao.StudyDAO"%>
<%@page import="com.tec.des.decorators.Decorator"%>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="study" class="com.tec.des.dto.StudyDTO" scope="request"/> 
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top"><A id="hl-link" href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;list studies</A> &gt; study overview report &gt; single study report</TD>
        <TD align="right"><A href="JavaScript:help('help.html#singlestudyreport')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="4" cellpadding="4"><TR><TD>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="4" cellpadding="0"> 
<TR>
<TD valign="top">
<TABLE width="500">
    <TR><TD class="bodytext-bold" valign="top">Study Name</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=study.getName()%></TD></TR>
    <TR><TD class="bodytext" valign="top">&nbsp;</TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Study Description</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getDescription())%></TD></TR>
    <TR><TD class="bodytext" valign="top">&nbsp;</TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Study Notes</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getNotes())%></TD></TR>
    <TR><TD class="bodytext">&nbsp;</TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Publications</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=StudyDAO.ReportColumns.PUBLICATIONS.decorate(study, Decorator.Type.HTML, request)%></TD></TR>
    <TR><TD class="bodytext">&nbsp;</TD></TR>
    <TR><TD class="bodytext">            
        <A href="JavaScript:document.form.submit();" onclick="document.form.target='_blank'; 
            document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SINGLE_STUDY_REPORT%>';
            document.form.<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>.value='true';">Printer friendly version</A></TD>
    </TR>
</TABLE>
</TD>
<TD valign="top">
<TABLE>
    <TR>
        <TD class="bodytext-bold" valign="top">Type of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getType()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Study Responsibles</TD>
        <TD class="bodytext" valign="top"><%=StudyDAO.ReportColumns.RESPONSIBLES.decorate(study, Decorator.Type.HTML, request)%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Duration of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getDuration()%>&nbsp;<%=study.getDurationUnit()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Start of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getStartDate()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">End of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getEndDate()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Keywords</TD>
        <TD class="bodytext" valign="top"><%=study.getKeywords()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">No of Student participants</TD>
        <TD class="bodytext" valign="top"><%=study.getNoOfStudentParticipants()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">No of Professional participants</TD>
        <TD class="bodytext" valign="top"><%=study.getNoOfProfessionalParticipants()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Study Material</TD>
        <TD class="bodytext" valign="top"><util:studymaterial items="<%=study.getMaterial()%>" /></TD>
    </TR>
</TABLE>
</TD>
</TR>
</TABLE>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="<%=study.getId()%>" > 
</FORM>
</TD></TR> 
</TABLE> 
<jsp:include page="bottom.html"/>