<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.*" %>
<%@ page import="com.tec.des.dao.StudyDAO" %>
<%@ page import="com.tec.des.dto.UserDTO" %>
<%@ page import="com.tec.des.decorators.Decorator"%>
<%@page import="com.tec.des.dto.StudyDTO"%>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="searchResult" class="com.tec.shared.util.HitList" scope="request"/> 
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<% UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER); %>
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top"><A id="hl-link" href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;list studies</A> &gt; study overview report</TD>
        <TD align="right"><A href="JavaScript:help('help.html#studyoverviewreport')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
	<!-- End navigation text -->
	<TABLE cellspacing="6" cellpadding="6">
	<TR><TD>
		<jsp:include page="messages.jsp"/>
		<util:hitlist name="hitlist" mode="search" maxHits="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
			<TABLE cellspacing="0" cellpadding="0" width="96%"> 
			    <TR>
			        <TD align="left"><H2>Study Overview Report</H2></TD>
			        <TD align="right" class="bodytext" valign="top"><util:searchnavigation totalHitListCount="<%= searchResult.getCount() %>" hitListLength="<%=request.getParameter(WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH)%>" startPosition="<%=request.getParameter(WebKeys.REQUEST_PARAM_START_POSITION)%>" /></TD>
			    </TR>
			</TABLE>
			<TABLE cellspacing="0" cellpadding="1">
			    <TR>
			    	<util:iteration name="columnName" type="String" rowAlternator="row" group="<%= (Object[])searchCriteria.getColumns().toArray(new Object[0]) %>">
			    		<TD class="bodytext-bold" valign="top" nowrap="nowrap">
					    <%	StudyDAO.ReportColumns column = StudyDAO.ReportColumns.valueOf(columnName); %>
			    			<%=column.getLabel()%>&nbsp;&nbsp;
					    <%	if (column.isSort()) { %>
					      		<util:studylistsort columnName="<%=columnName%>"/>&nbsp;&nbsp;
						<% 	} %>
			    		</TD>
					</util:iteration>
					
			<% if (user != null) { %>
			        <TD class="bodytext-bold" valign="top" align="center">Delete</TD> 
			<% } %>
			    </TR>
				    <util:iteration name="study" type="StudyDTO" rowAlternator="row" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
					    <TR class="<%=row%>">
					    	<util:iteration name="columnName" type="String" rowAlternator="row" group="<%= (Object[])searchCriteria.getColumns().toArray(new Object[0]) %>">
					    		<TD class="bodytext" valign="top">
					    			<%=StudyDAO.ReportColumns.valueOf(columnName).decorate(study, Decorator.Type.HTML, request)%>&nbsp;
					    		</TD>
							</util:iteration>
						<% if (user != null) { %>
						        <TD valign="top" align="center"><INPUT type="radio" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID_DELETE%>" value="<%=study.getId()%>" class="bodytext" checked></TD>
						<% } %>
					    </TR>
			    	</util:iteration>
			</TABLE>
		</util:hitlist>
		<TABLE cellspacing="4" cellpadding="1" width="100%"> 
		    <TR>
		        <TD class="bodytext">&nbsp;</TD>
		    </TR>
		    <TR>
		        <TD><INPUT type="submit" name="" value="New search" class="bodytext" onclick="document.form.target='_self';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>';"></TD>
		<% if (user != null && searchResult.size() > 0) { %>
		        <TD align="right"><INPUT type="submit" name="" value="Delete" class="bodytext" onclick="document.form.target='_self';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_STUDY%>';return confirm('Do you really want to delete selected Study?');"></TD>
		<% } %>
		    </TR>
		    <TR>
		        <TD class="bodytext">&nbsp;</TD>
		    </TR>
		<% if (searchResult.size() > 0) { %>	
		    <TR>
		        <TD class="bodytext">
		            <A href="JavaScript:document.form.submit();" onclick="document.form.target='_blank'; 
		            document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';
		            document.form.<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>.value='<%=searchResult.getCount()%>';
		            document.form.<%=WebKeys.REQUEST_PARAM_START_POSITION%>.value='<%=WebConstants.DEFAULT_START_POSITION%>';
		            document.form.<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>.value='true';">Printer friendly version</A>
		        </TD>
		    </TR>
		<% } %>
		</TABLE>
		<util:searchcriteria searchCriteria="<%= searchCriteria %>" /> 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>" value="<%=Constants.MAINTAIN_MODE_FINAL%>" > 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>" value="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" > 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_START_POSITION%>" value="" > 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>" value="" > 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="" > 
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID_DELETE%>" value="" >
		<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_REPORT_ID%>" value="<%=searchCriteria.getUserReportId()%>" > 
	</TD></TR> 
	</TABLE> 
</FORM>
<jsp:include page="bottom.html"/>