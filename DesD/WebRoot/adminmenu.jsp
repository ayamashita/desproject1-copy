<%@ page import="com.tec.des.dto.UserDTO" %>
<%@ page import="com.tec.des.http.*" %>
<!-- Start administration menu  -->
<% UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER);
if (user == null || !user.hasRole()) { %>
<TD valign="top" bgcolor="#f5f5f5">
<TABLE cellspacing="1" cellpadding="2" width='138'>
<TR><TD class="bodytext-bold">&nbsp;</TD></TR>
<TR><TD class="bodytext-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="login.jsp">Login</A></TD></TR>
</TABLE>
</TD>
<% } else { %>
<TD valign="top" bgcolor="#ffffff">
<TABLE cellspacing="1" cellpadding="2" width='138'>

<TR><TD class="bodytext-bold"><jsp:include page="userreportlist.jsp"/></TD></TR>

<FORM name="formMenu" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TR><TD class="bodytext-bold">&nbsp;</TD></TR>
<TR><TD class="bodytext-bold">&nbsp;<A href="<%=WebConstants.FRONTCONTROLLER_URL%>?<%=WebKeys.REQUEST_PARAM_USECASE%>=<%=WebConstants.USECASE_OPEN_NEW_STUDY%>">New Study</A></TD></TR>
<TR><TD class="bodytext-bold">&nbsp;<A href="<%=WebConstants.FRONTCONTROLLER_URL%>?<%=WebKeys.REQUEST_PARAM_USECASE%>=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">List Studies</A></TD></TR>
<TR><TD class="bodytext-bold">&nbsp;<A href="exportstudies.jsp">Export Studies</A></TD></TR>
<TR><TD class="bodytext-bold">&nbsp;<A href="JavaScript:openStudyTypesFromMenu()" onclick="document.formMenu.target='editstudytypesformmenu';document.formMenu.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_STUDY_TYPES%>';">Edit Study Types</A></TD></TR>
<TR><TD class="bodytext-bold">&nbsp;<A href="JavaScript:openStudyDurationUnits()" onclick="document.formMenu.target='editstudydurationunits';document.formMenu.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_STUDY_DURATION_UNITS%>';">Edit Duration Units</A></TD></TR>
    <% if (user.isDbAdmin()) { %>
<TR><TD class="bodytext-bold">&nbsp;<A href="<%=WebConstants.FRONTCONTROLLER_URL%>?<%=WebKeys.REQUEST_PARAM_USECASE%>=<%=WebConstants.USECASE_OPEN_USER_ADMIN%>">User Administration</A></TD></TR>
<TR><TD class="bodytext-bold">&nbsp;<A href="<%=WebConstants.FRONTCONTROLLER_URL%>?<%=WebKeys.REQUEST_PARAM_USECASE%>=<%=WebConstants.USECASE_OPEN_EDIT_ADMIN_PAGE%>">Edit Opening Page</A></TD></TR>
    <% } %>
<TR><TD class="bodytext-bold">&nbsp;<A href="<%=WebConstants.FRONTCONTROLLER_URL%>?<%=WebKeys.REQUEST_PARAM_USECASE%>=<%=WebConstants.USECASE_LOGOUT%>" onclick="return confirm('Do you really want to log out?');">Logout</A></TD></TR>
<TR><TD class="bodytext-bold">&nbsp;</TD></TR>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_OPENER_IS_MENU%>" value="true" > 
</FORM>
</TABLE>
</TD>
<% } %>
<!-- End administration menu -->