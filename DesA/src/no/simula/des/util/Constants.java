/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.util;

import java.util.ResourceBundle;


/**
 * This class contains global constants for the DES application
 */
public class Constants {
    //Log name  
	public final static String GLOBAL_LOG = "no.simula.des";
    public final static String DATE_PATTERN = "dd-MMM-yyyy";
    public final static int FRONTPAGE_ID = 1;
    
    //Access levels
    public static final int DBA = 2;
    public static final int SA = 1;
    public static final int NONE = 0;

    public static final ResourceBundle MESSAGE_RESOURCES = ResourceBundle.getBundle("desMessages");
    public static final ResourceBundle DES_PROPERTIES = ResourceBundle.getBundle("des");
    public final static int DEFAULT_PAGE_SIZE = Integer.valueOf(DES_PROPERTIES.getString("study.overview.default.pageSize")).intValue();;
	public static final String USER_AUTHENTICATION_SERVER_URL = DES_PROPERTIES.getString("user.authentication.serverUrl");
	public static final String SIMULAWEB_SERVICE_URL = DES_PROPERTIES.getString("simulaWeb.serverUrl");
  
}
