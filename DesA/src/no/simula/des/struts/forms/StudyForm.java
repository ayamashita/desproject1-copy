/*
 * Created on 01.okt.2003
 *
 *
 */
package no.simula.des.struts.forms;

import no.simula.des.beans.NameValueBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.data.StudyDatabase;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import java.io.Serializable;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;


/**
 * ActionForms are placeholders for data that are displayed in or collected from a page in a
 * Struts based solution, whether the page actually contains a HTML form or not.
 *
 * This form bean holds all data for editing studies
 */
public class StudyForm extends ActionForm implements Serializable {
    //Control attributes
    public final static int NEW_STUDY_ACTION = 0;
    public final static int EDIT_STUDY_ACTION = 1;
    public final static int SAVE_STUDY_OPERATION = 1;
    public final static int RELOAD_STUDY_OPERATION = 2;
    private int action;
    private int operation = 0;
	boolean addResponsiblesToStudyReport = false;

    //Required attributes from study db
    private String id;
    private String name;
    private String description;
    private String keywords;
    private String notes;

    //Input date fields. Split into day, month year.
    private String startDay;

    //Input date fields. Split into day, month year.
    private String startMonth;

    //Input date fields. Split into day, month year.
    private String startYear;
    private String endDay;
    private String endMonth;
    private String endYear;

    //Other attributes
    private String students;

    //Other attributes
    private String professionals;
    private String duration;
    private String durationUnit;
    private String type;

    //Other attributes
    private Collection responsibles;

    //Other attributes
    private Collection publications;

    //Other attributes
    private ArrayList studyMaterial = new ArrayList();
    private ArrayList studyTypeOptions;
    private ArrayList durationUnitOptions;

    //Output only
    private Date createdDate;

    //Output only
    private Date editedDate;
    private String createdBy;
    private String editedBy;
    private boolean containsErrors = false;

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public StudyForm() {
        action = NEW_STUDY_ACTION;

        studyTypeOptions = new ArrayList();
        durationUnitOptions = new ArrayList();
    }

    /**
     * Initialises an empty study form
     *
     */
    public void init() {
        id = "-1";
        name = null;
        description = null;
        keywords = null;
        notes = null;
        startDay = null;
        startMonth = null;
        startYear = null;
        endDay = null;
        endMonth = null;
        endYear = null;
        createdDate = null;
        editedDate = null;
        createdBy = null;
        editedBy = null;
        students = null;
        professionals = null;
        duration = null;
        responsibles = new ArrayList();
        studyMaterial = new ArrayList();
        this.publications = new ArrayList();

        durationUnit = "1";
        type = null;
        action = NEW_STUDY_ACTION;
    }

    /**
      * Validates the studyForm
      * Returns error when: Studyname is null <br>
      * Start and end date is not sepecified correctly and
      * the end date is before startdat.<br>
      * Error is also returned if Study description is not specified.
      *
      */
    public ActionErrors validate(ActionMapping mapping,
        javax.servlet.http.HttpServletRequest request) {
        boolean isOK = true;
        log.debug("Opretaion is:  " + operation );
        if (!(operation == this.SAVE_STUDY_OPERATION)) {
            log.debug("Opretaion is:  " + operation + " do not validate");
            return null;
        }

        if (description == null) {
            return null;
        }

        if (name == null) {
            return null;
        }

        ActionErrors errors = new ActionErrors();

        if (description.equals("")) {
            errors.add("description",
                new ActionError(
                    "error.des.study.creat.edit.missing.description",
                    "Fields requiered"));
            isOK = false;
        }

        if (this.name.equals("")) {
            errors.add("name",
                new ActionError("error.des.study.creat.edit.missing.name",
                    "Fields requiered"));
            isOK = false;
        }

        if( !name.equals("") ){
          try {
            StudyDatabase studyDb = new StudyDatabase();

            boolean isNameOk = studyDb.isStudyNameValid(name,Integer.parseInt(id));
            if(!isNameOk){
              errors.add("name",
                new ActionError("error.des.study.creat.edit.dubplicate_studyname", new String[]{name}));
              isOK = false;
            }
          }
          catch (Exception ex) {

          }

        }
        Date startDate = null;
        Date endDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);

        if( !getStartDay().equals("") ||
             !getStartMonth().equals("") ||
             !getStartYear().equals("") ){
          try {

              int tmpStartDay = Integer.parseInt(getStartDay());
              int tmpStartMonth = Integer.parseInt(getStartMonth());
              int tmpStartYear = Integer.parseInt(getStartYear());

              if ((1 > tmpStartDay) || (tmpStartDay > 31)) {
                  throw new Exception();
              }

              if ((1 > tmpStartMonth) || (tmpStartMonth > 12)) {
                  throw new Exception();
              }


               startDate = formatter.parse(getStartDateAsString(),new ParsePosition(0));

          } catch (Exception e) {
              errors.add("startDate",
                  new ActionError("error.des.study.creat.edit.startDate",
                      "Date error"));
              isOK = false;
          }
        }


        try {
            int tmpEndDay = Integer.parseInt(getEndDay());
            int tmpEndMonth = Integer.parseInt(getEndMonth());
            int tmpEndYear = Integer.parseInt(getEndYear());

            if ((1 > tmpEndDay) || (tmpEndDay > 31)) {
                throw new Exception();
            }

            if ((1 > tmpEndMonth) || (tmpEndMonth > 12)) {
                throw new Exception();
            }
            endDate = formatter.parse(this.getEndDateAsString(), new ParsePosition(0));

        } catch (Exception e) {
            errors.add("endDate",
                new ActionError("error.des.study.creat.edit.endDate",
                    "Date error"));
            isOK = false;
        }

        //formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US);


        if ((startDate != null) && (endDate != null)) {
            log.debug("startdate:" + startDate.toString() );
            log.debug("enddate:" + endDate.toString() );
            if (startDate.after(endDate)) {
                errors.add("startDate after endDate",
                    new ActionError(
                        "error.des.study.creat.edit.wrongStartDate",
                        "Date error"));
                isOK = false;
            }
        }

        /*
        if (this.getDuration().equals("")) {
            errors.add("duration",
                new ActionError("error.des.study.creat.edit.duration.missing",
                    "Duration error"));
            isOK = false;
        }
        */
        if (!getDuration().equals("")) {
            try {
                Integer.parseInt(getDuration());
            } catch (Exception e) {
                errors.add("duration",
                    new ActionError(
                        "error.des.study.creat.edit.duration.notNumber",
                        "Duration error"));
                isOK = false;
            }
        }

        /*
        if (this.getStudents().equals("")) {
            errors.add("students",
                new ActionError("error.des.study.creat.edit.students.missing",
                    "Duration error"));
            isOK = false;
        }
        */
        if (!getStudents().equals("")) {
            try {
                Integer.parseInt(getStudents());
            } catch (Exception e) {
                errors.add("students",
                    new ActionError(
                        "error.des.study.creat.edit.students.notNumber",
                        "students error"));
                isOK = false;
            }
        }

        /*
        if (this.getProfessionals().equals("")) {
            errors.add("professionals",
                new ActionError(
                    "error.des.study.creat.edit.professionals.missing",
                    "professionals error"));
            isOK = false;
        }
        */
        if (!getProfessionals().equals("")) {
            try {
                Integer.parseInt(getProfessionals());
            } catch (Exception e) {
                errors.add("professionals",
                    new ActionError(
                        "error.des.study.creat.edit.professionals.notNumber",
                        "professionals error"));
                isOK = false;
            }
        }

        //Validating responsibles
        Iterator iter = responsibles.iterator();
        int count = 0;
        while (iter.hasNext()) {
            PeopleBean item = (PeopleBean)iter.next();
            if( !item.getDeleted() ){
              count++;
            }
        }
        if(count == 0){
          errors.add("responsibles",
                    new ActionError(
                        "error.des.study.creat.edit.responsible.missing",
                        "responsible error"));
                isOK = false;

        }

        //Validating publications
        /*
        iter = publications.iterator();
        count = 0;
        while (iter.hasNext()) {
            PublicationBean item = (PublicationBean)iter.next();
            if( !item.getDeleted() ){
              count++;
            }
        }
        if(count == 0){
          errors.add("publications",
                    new ActionError(
                        "error.des.study.creat.edit.publication.missing",
                        "publications error"));
                isOK = false;

        }
        */
        if (isOK) {
            return null;
        } else {
            this.containsErrors = true;

            return errors;
        }
    }

    public boolean isContainsErrors() {
        return containsErrors;
    }

    public void setContainsErrors(boolean containsErrors) {
        this.containsErrors = containsErrors;
    }

    /**
     * Used for testing only
     */
    public static void main(String[] args) throws Exception {
        int tmpStartDay = Integer.parseInt("01");
        int tmpStartMonth = Integer.parseInt("02");
        int tmpStartYear = Integer.parseInt("2003");

        if ((1 > tmpStartDay) || (tmpStartDay > 31)) {
            throw new Exception();
        }

        if ((1 > tmpStartMonth) || (tmpStartMonth > 12)) {
            throw new Exception();
        }

        StudyForm form = new StudyForm();

        form.setStartDay("01");
        form.setStartMonth("01");
        form.setStartYear("2003");

        form.setEndDay("01");
        form.setEndMonth("01");
        form.setEndYear("2002");

        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US);

        Date endDate = formatter.parse( form.getStartDateAsString(),
                new ParsePosition(0));

        Date startDate = formatter.parse( form.getEndDateAsString(), new ParsePosition(0));

        if( startDate.after( endDate) ){
          System.out.println("StartDate: " + form.getStartDateAsString() );
          System.out.println("is befor EndDate: " + form.getEndDateAsString() );
        }

        System.out.println("StartDate: " + form.getStartDateAsString() );
        form.setDescription("Dette er en test\n p� replace\nsubstring");
        System.out.println(form.getDescriptionHtmlCoded());

         form.setStartDay("");
        form.setStartMonth("");
        form.setStartYear("");

        if( !"2".equals("") ||
             !"2".equals("") ||
             !"2".equals("") ){
          System.out.println("feil");
        }
        else{
          System.out.println("rett");
        }
        System.out.println("StartDateAsString"+form.getStartDateAsString());
    }

    // -------------- Getter and setter methods ---------------------

    /**
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @return
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedDateAsString() {
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);

        return formatter.format(createdDate);
    }

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    public String getDescriptionHtmlCoded() {
        return StringUtils.replace(description, "\n", "<br>");
    }

    /**
     * @return
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @return
     */
    public String getEditedBy() {
        return editedBy;
    }

    /**
     * @return
     */
    public Date getEditedDate() {
        return editedDate;
    }

    public String getEditedDateAsString() {
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);

        return formatter.format(editedDate);
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public String getNotes() {
        return notes;
    }

    public String getNotesHtmlCoded() {
        return StringUtils.replace(notes, "\n", "<br>");
    }

    /**
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * @param string
     */
    public void setCreatedBy(String string) {
        createdBy = string;
    }

    /**
     * @param date
     */
    public void setCreatedDate(Date date) {
        createdDate = date;
    }

    /**
     * @param string
     */
    public void setDescription(String string) {
        description = string;
    }

    /**
     * @param string
     */
    public void setKeywords(String string) {
        keywords = string;
    }

    /**
     * @param string
     */
    public void setEditedBy(String string) {
        editedBy = string;
    }

    /**
     * @param date
     */
    public void setEditedDate(Date date) {
        editedDate = date;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @param string
     */
    public void setNotes(String string) {
        notes = string;
    }

    /**
     * @param i
     */
    public void setType(String string) {
        type = string;
    }

    /**
     * @return
     */
    public int getAction() {
        return action;
    }

    /**
     * @param i
     */
    public void setAction(int i) {
        action = i;
    }

    /**
     * @return
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @return
     */
    public String getDurationUnit() {
        return durationUnit;
    }

    public String getDurationUnitFull() {
        String retValue = "";
        Iterator it = this.durationUnitOptions.iterator();

        while (it.hasNext()) {
            NameValueBean nvb = (NameValueBean) it.next();

            if (Integer.parseInt(nvb.getValue()) == Integer.parseInt(
                        durationUnit)) {
                retValue = nvb.getName();

                break;
            }
        }

        return retValue;
    }

    public String getEndDateAsString() {
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(endYear), Integer.parseInt(endMonth)-1,
            Integer.parseInt(endDay));

        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);

        return formatter.format(cal.getTime());
    }

    /**
     * @return
     */
    public String getEndDay() {
        return endDay;
    }

    /**
     * @return
     */
    public String getEndMonth() {
        return endMonth;
    }

    /**
     * @return
     */
    public String getEndYear() {
        return endYear;
    }

    /**
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * @return
     */
    public Log getLog() {
        return log;
    }

    /**
     * @return
     */
    public String getProfessionals() {
        return professionals;
    }

    public String getStartDateAsString() {
      if ( getStartDay().equals("") || getStartMonth().equals("") ||
                getStartYear().equals("")) {
         return "";
        }
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(startYear), Integer.parseInt(startMonth) -1,
            Integer.parseInt(startDay));

        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN,
                Locale.US);

        return formatter.format(cal.getTime());
    }

    /**
     * @return
     */
    public String getStartDay() {
        return startDay;
    }

    /**
     * @return
     */
    public String getStartYear() {
        return startYear;
    }

    /**
     * @return
     */
    public String getStudents() {
        return students;
    }

    /**
     * @param string
     */
    public void setDuration(String string) {
        duration = string;
    }

    /**
     * @param string
     */
    public void setDurationUnit(String string) {
        durationUnit = string;
    }

    /**
     * @param string
     */
    public void setEndDay(String string) {
        endDay = string;
    }

    /**
     * @param string
     */
    public void setEndMonth(String string) {
        endMonth = string;
    }

    /**
     * @param string
     */
    public void setEndYear(String string) {
        endYear = string;
    }

    /**
     * @param string
     */
    public void setId(String string) {
        id = string;
    }

    /**
     * @param log
     */
    public void setLog(Log log) {
        this.log = log;
    }

    /**
     * @param string
     */
    public void setProfessionals(String string) {
        professionals = string;
    }

    /**
     * @param string
     */
    public void setStartDay(String string) {
        startDay = string;
    }

    /**
     * @param string
     */
    public void setStartYear(String string) {
        startYear = string;
    }

    /**
     * @return
     */
    public String getStartMonth() {
        return startMonth;
    }

    /**
     * @param string
     */
    public void setStartMonth(String string) {
        startMonth = string;
    }

    /**
     * @param string
     */
    public void setStudents(String string) {
        students = string;
    }

    /**
     * @return
     */
    public ArrayList getDurationUnitOptions() {
        return durationUnitOptions;
    }

    /**
     * @return
     */
    public ArrayList getStudyTypeOptions() {
        return studyTypeOptions;
    }

    /**
     * @param list
     */
    public void setDurationUnitOptions(ArrayList list) {
        durationUnitOptions = list;
    }

    /**
     * @param list
     */
    public void setStudyTypeOptions(ArrayList list) {
        studyTypeOptions = list;
    }

    public String toString() {
        StringBuffer out = new StringBuffer();
        out.append("action: " + action);
        out.append("id: " + id);
        out.append(", name: " + name);
        out.append(", description: " + description);
        out.append(", type: " + type);
        out.append(", keywords: " + keywords);
        out.append(", notes: " + notes);
        out.append(", startDate: " + startDay + "/" + startMonth + "-" +
            startYear);
        out.append(", endDate: " + endDay + "/" + endMonth + "-" + endYear);
        out.append(", createdDate: " + createdDate);
        out.append(", createdBy: " + createdBy);
        out.append(", eidtedDate: " + editedDate);
        out.append(", editedBy: " + editedBy);
        out.append(", students: " + students);
        out.append(", professionals: " + professionals);
        out.append(", studyTypeOptions: " + studyTypeOptions);
        out.append(", durationUnitOptions: " + durationUnitOptions);

        return out.toString();
    }

    /**
     * @return
     */
    public Collection getPublications() {
        return publications;
    }

    /**
     * @return
     */
    public Collection getResponsibles() {
        return responsibles;
    }

    /**
     * @return
     */
    public Collection getStudyMaterial() {
        return studyMaterial;
    }

    /**
     * @param collection
     */
    public void setPublications(Collection collection) {
        publications = collection;
    }

    /**
     * @param collection
     */
    public void setResponsibles(Collection collection) {
        responsibles = collection;
    }

    /**
     * @param collection
     */
    public void setStudyMaterial(Collection collection) {
        studyMaterial = (ArrayList) collection;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

	public boolean isAddResponsiblesToStudyReport() {
		return addResponsiblesToStudyReport;
	}

	public void setAddResponsiblesToStudyReport(boolean addResponsiblesToStudyReport) {
		this.addResponsiblesToStudyReport = addResponsiblesToStudyReport;
	}




}
