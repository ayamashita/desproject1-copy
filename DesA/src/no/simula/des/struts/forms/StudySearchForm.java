/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.forms;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.beans.StudiesBean;

import org.apache.struts.action.ActionForm;


/**
 * ActionForms are placeholders for data that are displayed in or collected from a page in a
 * Struts based solution, whether the page actually contains a HTML form or not.
 *
 * This form bean contains data posted in the search form
 */
public class StudySearchForm extends ActionForm {
    private String freetext;
    private String studyType;
    private String startYear;
    private String endYear;
    private String responsible_firstname;
    private String responsible_familyname;
    private StudySortBean studySortBean = new StudySortBean();
    private StudiesBean studies;
    private boolean isSearch = false;
    private boolean isViewAll = false;
    private String userReportId = "-1";

    /**
     * @return
     */
    public String getEndYear() {
        return endYear;
    }

    /**
     * @return
     */
    public String getFreetext() {
        return freetext;
    }

    /**
     * @return
     */
    public String getStudyType() {
        return studyType;
    }

    /**
     * @param i
     */
    /**
     * @param i
     */
    public void setEndYear(String i) {
        endYear = i;
    }

    /**
     * @param string
     */
    public void setFreetext(String string) {
        freetext = string;
    }

    /**
     * @param string
     */
    /**
     * @param i
     */
    public void setStudyType(String i) {
        studyType = i;
    }

    public StudySortBean getStudySortBean() {
        return studySortBean;
    }

    public void setStudySortBean(StudySortBean studySortBean) {
        this.studySortBean = studySortBean;
    }

    public StudiesBean getStudies() {
        return studies;
    }

    public void setStudyList(StudiesBean studies) {
        this.studies = studies;
    }

    public boolean getHasNextPage() {
        if (studies.getEndIndex() < studies.getTotalStudies()) {
            return true;
        }

        return false;
    }

    public int getNextPageIndex() {
        return studies.getEndIndex() + 1;
    }

    public boolean getHasPreviousPage() {
        if ((studySortBean.getCount() == 1) && (studies.getEndIndex() > 1)) {
            return true;
        }

        if (studies.getStartIndex() > 1) {
            return true;
        }

        return false;
    }

    public int getPreviousPageIndex() {
        if (!this.getHasPreviousPage()) {
            return 1;
        }

        if (studySortBean.getCount() == 1) {
            return studies.getStartIndex();
        }

        return studies.getStartIndex() - (studySortBean.getCount() - 1);
    }

    public boolean getIsSearch() {
        return isSearch;
    }

    public void setIsSearch(boolean isSearch) {
        this.isSearch = isSearch;
    }

    public boolean getIsViewAll() {
        return this.isViewAll;
    }

    public void setIsViewAll(boolean isViewAll) {
        this.isViewAll = isViewAll;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getResponsible_familyname() {
        return responsible_familyname;
    }

    public String getResponsible_firstname() {
        return responsible_firstname;
    }

    public void setResponsible_familyname(String responsible_familyname) {
        this.responsible_familyname = responsible_familyname;
    }

    public void setResponsible_firstname(String responsible_firstname) {
        this.responsible_firstname = responsible_firstname;
    }

	public String getUserReportId() {
		return userReportId;
	}

	public void setUserReportId(String userReportId) {
		this.userReportId = userReportId;
	}
}
