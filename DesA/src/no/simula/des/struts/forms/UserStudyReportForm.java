package no.simula.des.struts.forms;

import java.util.Iterator;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.UserStudyReportBean;
import no.simula.des.struts.actions.AuthenticateAction;
import no.simula.des.struts.actions.EditUserStudyReportAction;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


/**
 * This form bean holds all data for editing user study report
 */
public class UserStudyReportForm extends ActionForm {

	String action;
	UserStudyReportBean report;
	String columnName; 
		
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
	 */
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		
		if (EditUserStudyReportAction.SAVE.equals(getAction())) {
			PeopleBean user = (PeopleBean) request.getSession().getAttribute(AuthenticateAction.SESSION_USER_KEY);
			String name = getReport().getName();
			if (errors == null) {
				errors = new ActionErrors();
			}

			// check if blank name
			if (StringUtils.isEmpty(name)) {
				errors.add("missingName", new ActionError("error.des.studyReport.edit.missing.name"));
				return errors;
			}
			
			name = name.trim();
			
			// check duplicity
			for (Iterator iterator = user.getStudyReports().entrySet().iterator(); iterator.hasNext();) {
				Entry entry = (Entry) iterator.next();
				UserStudyReportBean bean = (UserStudyReportBean) entry.getValue();
				if (name.equals(bean.getName()) && getReport().getId() != bean.getId()) {
					errors.add("duplicatedName", new ActionError("error.des.studyReport.edit.duplicated.name", name));
					break;
				}
			}
		}
		
		return errors;
	}

	public UserStudyReportBean getReport() {
		if (report == null) {
			report = new UserStudyReportBean();
		}
		return report;
	}

	public void setReport(UserStudyReportBean report) {
		this.report = report;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

}
