/*
 * Created on 30.sep.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package no.simula.des.struts.actions;

import no.simula.des.data.beans.PublicationBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.LogonForm;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class StartDeletePublicationAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm studyForm = (StudyForm) form;
        ArrayList pub = (ArrayList) studyForm.getPublications();

        String id = request.getParameter("publication");

        Iterator iter = pub.iterator();

        while (iter.hasNext()) {
            PublicationBean item = (PublicationBean) iter.next();

            if (item.getId().equals(id)) {
                request.setAttribute("name", item.getTitle());
                request.setAttribute("id", item.getId());
            }
        }

        return (mapping.findForward("success"));
    }
}
