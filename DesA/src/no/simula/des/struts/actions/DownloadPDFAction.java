/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.Decorator;
import no.simula.des.data.StudyDatabase.ReportColumns;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.struts.forms.StudySearchForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Generate PDF file of studies.
 * The user will be asked where to store the file
 */
public class DownloadPDFAction extends Action {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.debug("DownloadPDFAction called");

        StudySearchForm studyForm = (StudySearchForm)form;
        
		ArrayList studies = (ArrayList) (studyForm).getStudies().getStudies();
        
        ByteArrayOutputStream content = createContent(studies, studyForm.getStudySortBean().getColumns(), request);

        // Prepare response
        response.setContentType("application/pdf");
        response.setContentLength(content.size());
        ServletOutputStream out = response.getOutputStream();
        content.writeTo(out);
        out.flush();        
        out.close();  

        return null;
    }

    public ByteArrayOutputStream createContent(ArrayList studies, ArrayList columns, HttpServletRequest request) throws Exception {

        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, baos);
        document.open();
        
        Font headerFont = new Font(Font.HELVETICA, 6);
        Font tableHeaderFont = new Font(Font.HELVETICA, 6, Font.BOLD);
        Font tableColumnsFont = new Font(Font.HELVETICA, 6, Font.NORMAL);

        Paragraph header = new Paragraph("[ simula.research laboratory ] studies overview report", headerFont);
        header.setAlignment(Element.ALIGN_CENTER);
		document.add(header);
        document.add(new Paragraph(" "));

        
        PdfPTable table = new PdfPTable(columns.size());
        table.setWidthPercentage(100);

        for (Iterator iterator = columns.iterator(); iterator.hasNext();) {
			String columnName = (String) iterator.next();
			
			table.addCell(new Paragraph(Constants.MESSAGE_RESOURCES.getString(columnName), tableHeaderFont));
		}
        
        for (Iterator iterator = studies.iterator(); iterator.hasNext();) {
        	StudyBean study = (StudyBean) iterator.next();

            for (Iterator iterator1 = columns.iterator(); iterator1.hasNext();) {
    			String columnName = (String) iterator1.next();
    			ReportColumns column = ReportColumns.valueOf(columnName);
    			table.addCell(new Paragraph(column.decorate(study, Decorator.Type.STRING, request), tableColumnsFont));
    		}
        }
            
        document.add(table);
        document.close();

        return baos;
    }
}
