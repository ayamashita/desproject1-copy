/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.PublicationDatabase;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Adds a publication to a study
 */
public class AddPublicationAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm stdForm = (StudyForm) form;

        PublicationDatabase pdao = new PublicationDatabase();
        Collection studyPublications = stdForm.getPublications();
        //Collection studyPublications = pdao.getPublicationList(Integer.parseInt(
        //            stdForm.getId()));
        Collection publications = pdao.getPublicationList();
        Iterator iter = studyPublications.iterator();

        while (iter.hasNext()) {
            PublicationBean studPub = (PublicationBean) iter.next();
            Iterator iter2 = publications.iterator();

            while (iter2.hasNext()) {
                PublicationBean pub = (PublicationBean) iter2.next();

                if (studPub.getId().equals(pub.getId())) {
                    if (!studPub.getDeleted()) {
                        pub.setAdded(true);
                    }
                }
            }
        }

        request.getSession().setAttribute("publications", publications);

        return (mapping.findForward("success"));
    }
}
