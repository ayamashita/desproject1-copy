package no.simula.des.struts.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.beans.DateBean;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.PublicationDatabase;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action is called upon to display the details for a single study.
 */
public class ShowStudyAction extends Action {
    public final static String STUDY_ID_PARAM = "study";
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        String param = request.getParameter(STUDY_ID_PARAM);

        StudyDatabase studyDb = new StudyDatabase();
        StudyForm studyForm = (StudyForm) form;

        if (param != null) {
            try {
                //Get study from db
                int studyId = Integer.parseInt(param);

                StudyBean study = studyDb.getStudy(studyId);

                //Populate form
                studyForm.setAction(StudyForm.EDIT_STUDY_ACTION);
                populateStudyForm(studyForm, study);

                PeopleDatabase peopleDb = new PeopleDatabase();
                studyForm.setResponsibles(peopleDb.getResponsibleList(studyId));

                PublicationDatabase pubDb = new PublicationDatabase();
                studyForm.setPublications(pubDb.getPublicationList(studyId));

                StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
                studyForm.setStudyMaterial(materialDb.getStudyMaterialInfo(studyId));

                ArrayList durationOptions = studyDb.getDurationUnits();

                studyForm.setDurationUnitOptions(durationOptions);

                if (log.isTraceEnabled()) {
                    log.trace("Form values: " + form);
                }
            } catch (Exception e) {
                log.error("Error loading study", e);

                return mapping.findForward("failure");
            }
            Object messageKey = request.getAttribute("msgKey");
            if(messageKey != null) {
              request.setAttribute("msgKey", messageKey);
            }
            return mapping.findForward("success");
        } else {
            return mapping.findForward("failure");
        }
    }

    private void populateStudyForm(StudyForm form, StudyBean bean)
        throws Exception {
        form.setId(String.valueOf(bean.getId()));
        form.setName(bean.getName());
        form.setDescription(bean.getDescription());
        form.setKeywords(bean.getKeywords());
        form.setNotes(bean.getNotes());

        //TODO should type be type name or value?
        form.setType(bean.getType());

        DateBean endDate = new DateBean(bean.getEndDate());
        form.setEndDay(endDate.getDay());
        form.setEndMonth(endDate.getMonth());
        form.setEndYear(endDate.getYear());

        if(bean.getStartDate() == null){
          form.setStartDay("");
          form.setStartMonth("");
          form.setStartYear("");
        }
        else{
          DateBean startDate = new DateBean(bean.getStartDate());
          form.setStartDay(startDate.getDay());
          form.setStartMonth(startDate.getMonth());
          form.setStartYear(startDate.getYear());
        }

         if( bean.getStudents() != -1 ){
          form.setStudents(String.valueOf(bean.getStudents()));
        }
        else{
          form.setStudents("");
        }
        if( bean.getProfessionals() != -1 ){
          form.setProfessionals(String.valueOf(bean.getProfessionals()));
        }
        else{
           form.setProfessionals("");
        }
        if( bean.getDuration() != -1 ){
          form.setDuration(String.valueOf(bean.getDuration()));
        }
        else{
           form.setDuration("");
        }
        form.setDurationUnit(String.valueOf(bean.getDurationUnit()));

        //Output only
        form.setCreatedDate(bean.getCreatedDate());
        form.setCreatedBy(bean.getCreatedBy());
        form.setEditedDate(bean.getEditedDate());
        form.setEditedBy(bean.getEditedBy());
    }
}
