/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.AdminPrivilegesDatabase;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.UserStudyReportDatabase;
import no.simula.des.struts.forms.LogonForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;


/**
 * Authenticates the user
 */
public class AuthenticateAction extends Action {
    public static final String SESSION_USER_KEY = "user";
	private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
    private static final String RCP_AUTHENTICATION_METHOD = "check_credentials";
    private static XmlRpcClient rcpClient;

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        log.debug("LogonForm: " + form.toString());

        LogonForm loForm = (LogonForm) form;
        PeopleBean user = authentiateUser(loForm.getUsername(), loForm.getPassword());

        if (user != null) {
            log.debug("User privileges is: " + user.getPrivilege());
            request.getSession().setAttribute(SESSION_USER_KEY, user);

            return (mapping.findForward("success"));
        } else {
            ActionErrors errors = new ActionErrors();

            //ActionMessage msg = new ActionMessage( "des.logon.userNotAuthenticated", "Bruker er ikke autentifisert" );
            errors.add("username",
                new org.apache.struts.action.ActionError(
                    "des.logon.userNotAuthenticated", "NotAutenticated"));
            this.saveErrors(request, errors);

            //throw new org.apache.struts.action.ActionException();
            return (mapping.getInputForward());
        }
    }
    
    /**
     * Autheticates a user.
     * Returns null if the user is not authenticated
     *
     * @param String username - (people id)
     * @param String password - (in clear text)
     * @return PeopleBean, null if user not authenticated
     * @throws Exception if xml rcp error
     */
    public PeopleBean authentiateUser(String username, String password) throws Exception {
        if (username.equals("") || password.equals("")) {
            throw new Exception("The username and/or password can not be empty ");
        }
        
        
		HashMap	hash = new HashMap();

		hash.put("login", username);
		hash.put("password", password);
		
		Object[] methodParams = new Object[]{hash}; 
    	Object authenticationResult = getRcpClient().execute(RCP_AUTHENTICATION_METHOD, methodParams);        

    	PeopleBean people = null;

        if (authenticationResult != null && ((Boolean)authenticationResult).booleanValue()) {
        	PeopleDatabase pd = new PeopleDatabase();
        	people = pd.getPerson(username);
        	if (people != null) {
        		AdminPrivilegesDatabase adminPrivilegeDb = new AdminPrivilegesDatabase();
        		int priv = adminPrivilegeDb.getPeoplePrivilege(people.getId());
        		people.setPrivilege(priv);
        		
        		UserStudyReportDatabase userRepDb = new UserStudyReportDatabase();
        		people.setStudyReports(userRepDb.getUserReports(people.getId()));
        	}

        }

        return people;
    }
    
	private XmlRpcClient getRcpClient() throws MalformedURLException {
		if (rcpClient == null) {
	    	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	    	rcpClient = new XmlRpcClient();
			config.setServerURL(new URL(Constants.USER_AUTHENTICATION_SERVER_URL));
	    	rcpClient.setConfig(config);
		}
		return rcpClient;
	}
	
}
