package no.simula.des.struts.actions;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.UserStudyReportBean;
import no.simula.des.data.beans.UserStudyReportDatabase;
import no.simula.des.data.beans.UserStudyReportBean.SortOperator;
import no.simula.des.data.beans.UserStudyReportBean.WhereOperator;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.struts.forms.UserStudyReportForm;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EditUserStudyReportAction extends Action {

	private static final String FORWARD_TO_STUDIES = "studies";
	public static final String CREATE = "create";
	public static final String EDIT = "edit";
	public static final String DELETE = "delete";
	public static final Object SAVE = "save";

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		UserStudyReportForm xForm = (UserStudyReportForm) form;
		PeopleBean user = (PeopleBean) request.getSession().getAttribute(AuthenticateAction.SESSION_USER_KEY);
		if (user == null) {
			throw new IllegalStateException("User is not logged.");
		}
		int id = -1;
		if (request.getParameter("id") != null) {
			id = Integer.valueOf((String)request.getParameter("id")).intValue();
		}
		
		if (CREATE.equals(xForm.getAction())) {
			UserStudyReportBean report = new UserStudyReportBean(user.getId());
			xForm.setReport(report);
		}
		else if (EDIT.equals(xForm.getAction())) {
			if (id > 0) {
				UserStudyReportBean report = (UserStudyReportBean) user.getStudyReports().get(Integer.valueOf(id));
				
				xForm.setReport(report.getReportCopy());
			}
			else {
				return mapping.findForward(FORWARD_TO_STUDIES);	
			}
			
		}
		else if (SAVE.equals(xForm.getAction())) {
			UserStudyReportDatabase userReportDB = new UserStudyReportDatabase();
			
			UserStudyReportBean report = xForm.getReport();
			if (report.getId() > 0) {
				userReportDB.update(report);
			}
			else {
				userReportDB.create(report);
			}
			
			user.getStudyReports().remove(report.getIdObj());
			user.getStudyReports().put(report.getIdObj(), report);
			
		}
		else if (DELETE.equals(xForm.getAction())) {
			if (id > 0) {
				UserStudyReportDatabase userReportDB = new UserStudyReportDatabase();
				userReportDB.delete(id);
				user.getStudyReports().remove(Integer.valueOf(id));
			}
			
			return mapping.findForward(FORWARD_TO_STUDIES);			
		}
		else if ("addColumn".equals(xForm.getAction())) {
			if (StringUtils.isNotEmpty(xForm.getColumnName())
					&& !xForm.getReport().getStudyColumns().contains(xForm.getColumnName())) {
				xForm.getReport().getStudyColumns().add(xForm.getColumnName());
			}
		}
		else if ("removeResponsible".equals(xForm.getAction())) {
			String responsibleId = (String) request.getParameter("responsibleId");

			for (Iterator iterator = xForm.getReport().getResponsibles().iterator(); iterator.hasNext();) {
				PeopleBean people = (PeopleBean) iterator.next();				
				if (responsibleId.equals(people.getId())) {
					xForm.getReport().getResponsibles().remove(people);
					break;
				}
			}
		}
		else if ("removeColumn".equals(xForm.getAction())) {
			if (xForm.getReport().getStudyColumns().contains(xForm.getColumnName())) {
				xForm.getReport().getStudyColumns().remove(xForm.getColumnName());
			}
		}
		else if ("back".equals(xForm.getAction())) {
			xForm.setReport(null);
			return mapping.findForward("studies");
		}

		return mapping.findForward("success");
	}
}
