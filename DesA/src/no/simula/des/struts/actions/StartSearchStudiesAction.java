/*
 * Created on 14.okt.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.StudyDatabase;
import no.simula.des.struts.forms.StudySearchForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This action opens the search form where users can specify search parameters.
 */
public class StartSearchStudiesAction extends Action {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        StudyDatabase studyDb = new StudyDatabase();
        ArrayList studyTypes = studyDb.getStudyTypes();

        StudySearchForm studyForm = (StudySearchForm) form;
        studyForm.getStudySortBean().setCount(Constants.DEFAULT_PAGE_SIZE);
        studyForm.getStudySortBean().setStart(1);
        studyForm.setIsViewAll( false );
        //studyForm = (StudySearchForm) request.getSession().getAttribute("studySearchForm");
        log.debug("Freetext in startSarch: " + studyForm.getFreetext());
        ArrayList endYear = getEndYears();

        request.setAttribute("endYears", endYear);
        request.setAttribute("studyTypes", studyTypes);

        return mapping.findForward("success");
    }

  public ArrayList getEndYears() throws Exception{
    StudyDatabase studyDb = new StudyDatabase();
    ArrayList list = studyDb.getStudyEndDates();
    Hashtable tmp = new Hashtable();
    Iterator iter = list.iterator();

    while (iter.hasNext()) {
      Date item = (Date)iter.next();
      Calendar cal = Calendar.getInstance();
      cal.setTime(item);
      tmp.put( new Integer( cal.get(Calendar.YEAR) ), "");


    }
    int i =0;
    Integer[] years  = new Integer[tmp.size()];
    iter = tmp.keySet().iterator();
    while (iter.hasNext()) {
      Integer item = (Integer)iter.next();
      years[i] =  item;
      i++;
    }


    if( i==0) return new ArrayList();
    Arrays.sort( years );
    ArrayList ret = new ArrayList();
    ret.addAll( Arrays.asList( years) );
    return ret;




  }
}
