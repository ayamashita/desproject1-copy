/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.AdminPrivilegesDatabase;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.data.beans.UserStudyReportBean;
import no.simula.des.data.beans.UserStudyReportDatabase;
import no.simula.des.struts.forms.StudySearchForm;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action lists studies in various ways
 */
public class ListStudiesAction extends Action {
    //Attribute names
    private final static String STUDY_LIST_ATTRIBUTE = "studyList";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        HashMap parameters = (HashMap) request.getParameterMap();

        StudySearchForm stsForm = (StudySearchForm) form;
        String action = request.getParameter("action");
        
        if( action == null ) {
        	action="";
        }

        if(action.equals("viewAll")){
        	stsForm.setIsViewAll( true );
			log.debug("Setting view all to true");
        }
        else if(action.equals("viewFirstPage")) {
          stsForm.setIsViewAll( false );
          stsForm.getStudySortBean().setCount(Constants.DEFAULT_PAGE_SIZE);
          stsForm.getStudySortBean().setStart(1);
          log.debug("Setting view all to false");
        }
        else if("changeUserReport".equals(action)) {
        	return (mapping.findForward("success"));
        }
        else if("viewUserReport".equals(action)) {
        	parameters = new HashMap();
        	PeopleBean user = (PeopleBean) request.getSession().getAttribute(AuthenticateAction.SESSION_USER_KEY);
        	UserStudyReportBean report = (UserStudyReportBean) user.getStudyReports().get(Integer.valueOf(stsForm.getUserReportId()));
        	String sort = StudyDatabase.ReportColumns.TYPE.getName() + " " + UserStudyReportBean.SortOperator.ASCENDANT.getName();
        	
        	if (StringUtils.isNotEmpty(report.getSortEndDate())) {
        		sort += ", " + StudyDatabase.ReportColumns.END_DATE.getName() + " " + report.getSortEndDate();
        	}
        	
        	// new sort
        	StudySortBean sortBean = new StudySortBean();
        	stsForm.setStudySortBean(sortBean);
        	
        	sortBean.setOrderBy(sort);
        	sortBean.setOrderDirection("");
        	
        	if (!report.getResponsibles().isEmpty()) {
        		sortBean.setResponsibles(report.getResponsibles());
        		sortBean.setResponsiblesOperator(report.getWhereResponsibleOperator());
        	}
        	sortBean.setColumns(report.getStudyColumns());
        	
        }
        else if("reset".equals(action)) {
        	resetSearch(stsForm);
    		stsForm.setUserReportId("-1");
        }
        
        try {
            StudiesBean studies = performSearch(stsForm, parameters);
            stsForm.setStudyList(studies);
        } catch (NumberFormatException nfe) {
            log.error("Malformed request parameters", nfe);

            return mapping.findForward("failure");
        }
         catch (Exception e) {
            log.error(e.getMessage());

            return mapping.findForward("failure");
        }

        log.debug("Done fetching studies");

        return (mapping.findForward("success"));

        //failure: login screen / frontpage?
    }

	private void resetSearch(StudySearchForm stsForm) {
		stsForm.setStudySortBean(new StudySortBean());
	}

    public StudiesBean performSearch(StudySearchForm form, HashMap parameters)
        throws NumberFormatException, Exception {
        StudyDatabase list = new StudyDatabase();

        StudySortBean sortBean = form.getStudySortBean();

        if (form.getIsSearch()) {
            sortBean = new StudySortBean();
            form.setEndYear("");
            form.setStartYear("");
            form.setFreetext("");
            form.setResponsible_firstname("");
            form.setResponsible_familyname("");
            form.setIsSearch(false);
            form.setStudyType("");
        }

        sortBean.setSortParams(parameters);

        StudiesBean studies = list.getStudies(sortBean);

        //Collection studies = list.getStudies( sortBean );
        return studies;
    }
}
