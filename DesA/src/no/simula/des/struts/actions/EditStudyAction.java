/*
 * Created on 01.okt.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.beans.DateBean;
import no.simula.des.beans.NameValueBean;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.PublicationDatabase;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This Action opens the new / edit study form
 */
public class EditStudyAction extends DesAction {
    public final static String STUDY_ID_PARAM = "study";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    //protected int min_access_level = 1;
    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        log.debug("Request parameters: ");

        Enumeration enum1 = request.getParameterNames();

        StudyForm studyForm = (StudyForm) form;



        while (enum1.hasMoreElements()) {
            log.debug("\t" + enum1.nextElement());
        }

        //Check if we are edititing an existing study
        String param = request.getParameter(STUDY_ID_PARAM);
        Integer attStudyId = (Integer) request.getAttribute(STUDY_ID_PARAM);



        log.debug("attStudyId" + attStudyId);

        if (attStudyId != null) {
            param = attStudyId.toString();

            String msgKey = (String) request.getAttribute("msgKey");
            ActionMessages messages = new ActionMessages();
            messages.add("updateStudy", new ActionMessage(msgKey, ""));
            request.setAttribute("messageKey", msgKey);
            this.saveMessages(request, messages);
        }

        //Get study data
        StudyDatabase studyDb = new StudyDatabase();


        if (studyForm.isContainsErrors()) {
            studyForm.setContainsErrors(false);

            return mapping.findForward("success");
        }

        if (param != null) {
            try {
                //Get study from db
                int studyId = Integer.parseInt(param);

                StudyBean study = studyDb.getStudy(studyId);

                //Populate form
                studyForm.setAction(StudyForm.EDIT_STUDY_ACTION);
                populateStudyForm(studyForm, study);

                PeopleDatabase peopleDb = new PeopleDatabase();
                studyForm.setResponsibles(peopleDb.getResponsibleList(studyId));

                PublicationDatabase pubDb = new PublicationDatabase();
                studyForm.setPublications(pubDb.getPublicationList(studyId));

                StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
                studyForm.setStudyMaterial(materialDb.getStudyMaterialInfo(
                        studyId));

                if (log.isTraceEnabled()) {
                    log.trace("Form values: " + form);
                }
            } catch (Exception e) {
                log.error("Error loading study", e);

                return mapping.findForward("failure");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Resetting studyForm object");
            }

            //Clear any set values
            studyForm.init();
        }

        //Make sure dynamic options are included
        try {
            addDynamicOptions(request, studyDb, studyForm);
        } catch (Exception e) {
            log.error("Error retrieving dynamic options", e);

            return mapping.findForward("failure");
        }

        // TODO edit forward?
        return mapping.findForward("success");
    }

    /**
     * Populates the form if we are editing an existing study
     *
     * @param studyForm the form object (target)
     * @param study the data bean (source)
     */
    public void populateStudyForm(StudyForm form, StudyBean bean)
        throws Exception {
        form.setId(String.valueOf(bean.getId()));
        form.setName(bean.getName());
        form.setDescription(bean.getDescription());
        form.setKeywords(bean.getKeywords());
        form.setNotes(bean.getNotes());

        //TODO should type be type name or value?
        form.setType(bean.getType());

        DateBean endDate = new DateBean(bean.getEndDate());
        form.setEndDay(endDate.getDay());
        form.setEndMonth(endDate.getMonth());
        form.setEndYear(endDate.getYear());


        if(bean.getStartDate() == null){
          form.setStartDay("");
          form.setStartMonth("");
          form.setStartYear("");
        }
        else{
          DateBean startDate = new DateBean(bean.getStartDate());
          form.setStartDay(startDate.getDay());
          form.setStartMonth(startDate.getMonth());
          form.setStartYear(startDate.getYear());
        }

        if( bean.getStudents() != -1 ){
          form.setStudents(String.valueOf(bean.getStudents()));
        }
        else{
          form.setStudents("");
        }
        if( bean.getProfessionals() != -1 ){
          form.setProfessionals(String.valueOf(bean.getProfessionals()));
        }
        else{
           form.setProfessionals("");
        }
        if( bean.getDuration() != -1 ){
          form.setDuration(String.valueOf(bean.getDuration()));
        }
        else{
           form.setDuration("");
        }
        form.setDurationUnit(String.valueOf(bean.getDurationUnit()));

        //Output only
        form.setCreatedDate(bean.getCreatedDate());
        form.setCreatedBy(bean.getCreatedBy());
        form.setEditedDate(bean.getEditedDate());
        form.setEditedBy(bean.getEditedBy());
        form.setAddResponsiblesToStudyReport(false);
    }

    /**
     * Make sure alternatives in drop downs are available
     * @param request the request
     * @param studyForm the form to
     */
    private void addDynamicOptions(HttpServletRequest request,
        StudyDatabase studyDb, StudyForm studyForm) throws Exception {
        ArrayList durationOptions = studyDb.getDurationUnits();

        //request.setAttribute( "durationUnitOptions", durationOptions );
        studyForm.setDurationUnitOptions(durationOptions);

        ArrayList typeOptions = studyDb.getStudyTypes();

        //request.setAttribute( "studyTypeOptions", typeOptions );
        studyForm.setStudyTypeOptions(typeOptions);

        //Need to find studyType default value
        String studyTypeValue = "";

        if (studyForm.getAction() == StudyForm.EDIT_STUDY_ACTION) {
            for (int i = 0; i < typeOptions.size(); i++) {
                if (studyForm.getType().equals(((NameValueBean) typeOptions.get(
                                i)).getName())) {
                    studyTypeValue = ((NameValueBean) typeOptions.get(i)).getValue();

                    break;
                }
            }
        } else {
            studyTypeValue = "1";
        }

        request.setAttribute("studyType", studyTypeValue);
    }
}
