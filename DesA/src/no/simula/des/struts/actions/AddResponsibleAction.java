/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.struts.forms.UserStudyReportForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Adds a responsible (person) to a study
 */
public class AddResponsibleAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm stdForm = (StudyForm) form;
        
        if (stdForm.isAddResponsiblesToStudyReport()) {
        	UserStudyReportForm xForm = (UserStudyReportForm) request.getSession().getAttribute("userStudyReportForm");
        	stdForm.setResponsibles(xForm.getReport().getResponsibles());
        }
        
        PeopleDatabase pdao = new PeopleDatabase();
        PeoplesBean peoples = pdao.getPeoples();
                
        Collection responsibles = stdForm.getResponsibles();
        Iterator iter = responsibles.iterator();

        while (iter.hasNext()) {
            PeopleBean responsible = (PeopleBean) iter.next();
            Iterator iter2 = peoples.getPeoples().iterator();

            while (iter2.hasNext()) {
                PeopleBean people = (PeopleBean) iter2.next();

                if (people.getId().equals(responsible.getId())) {
                    if(!responsible.getDeleted()){
                      people.setAdded(true);
                    }
                }
            }
        }

        request.getSession().setAttribute("peoples", peoples);

        return (mapping.findForward("success"));
    }
}
