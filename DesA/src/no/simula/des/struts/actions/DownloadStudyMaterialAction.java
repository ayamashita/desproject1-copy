/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.StudyMaterialFileBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Starts download of a study material file
 */
public class DownloadStudyMaterialAction extends Action {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        log.debug("DownloadStudyMaterialAction called");

        String studyId = request.getParameter("studyId");
        String studyMaterialId = request.getParameter("studyMaterialId");

        if ((studyMaterialId == null) || studyMaterialId.equals("")) {
            throw new Exception(
                "The study metarial id parameter must be found on the request");
        }

        if ((studyId == null) || studyId.equals("")) {
            throw new Exception(
                "The study id parameter must be found on the request");
        }

        StudyMaterialDatabase stmDao = new StudyMaterialDatabase();
        StudyMaterialFileBean bean = stmDao.retriveStudyMaterialFile(Integer.parseInt(
                    studyMaterialId), Integer.parseInt(studyId));

        response.setContentType(bean.getContentType());
        response.addHeader("content-disposition",
            "attachment; filename=\"" + bean.getFilename() + "\"");

        ServletOutputStream out = response.getOutputStream();

        byte[] file = bean.getFile();
        int length = file.length;

        //for(int i=0; i<length; i++){
        out.write(file);

        //}
        out.flush();
        out.close();

        return null;
    }
}
