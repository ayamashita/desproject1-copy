/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.beans.NameValueBean;
import no.simula.des.beans.StudySortBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enum.Enum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * The class handles all database access related to study lists
 */
public class StudyDatabase extends DataObject {

    private static final String QUERY_RESPONSIBLE_IDS = "#responsibleIds#";
    private static final String QUERY_PUBLICATION_IDS = "#publicationIds#";
	//Prepared statements
    private final static String CHECK_STUDY_NAME_QUERY = "select count(*) from study where study_name=? and study_id != ?;";
    private final static String GET_ALL_STUDY_IDS_QUEREY = "select study_id from study  ORDER BY study_end_date;";
    private final static String SINGLE_STUDY_QUERY = 
    		"SELECT study.*, type_name " + 
	        "FROM   study, studytypes " +
	        "WHERE  (study_type = studytypes.type_id) AND (study_id = ?);";
    private final static MessageFormat STUDY_QUERY = new MessageFormat(
            "SELECT DISTINCT study.*, studytypes.type_name as type_name " +
            "FROM   study, studytypes, responsiblesstudies rs " +
            "WHERE  (study_type = studytypes.type_id) " +
            "		AND (rs.study_id = study.study_id) " +
            QUERY_RESPONSIBLE_IDS +
            "ORDER BY {0} {1};");
            //"WHERE  (study_type_id = studytypes.type_id) " + "ORDER BY {0} {1};");


      private static MessageFormat STUDY_SEARCH_QUERY = new MessageFormat(
	      "SELECT DISTINCT  s.*, st.type_name " +
	      "FROM study s " +
	      "		INNER JOIN studytypes st ON s.study_type = st.type_id AND st.type_name regexp ? " +
	      "		INNER JOIN responsiblesstudies rs ON rs.study_id = s.study_id " + QUERY_RESPONSIBLE_IDS +
	      "		LEFT JOIN study s1 " +
	      "			ON s1.study_name REGEXP ? or s1.study_keywords REGEXP ? or s1.study_notes REGEXP ? or  s1.study_description REGEXP ? " +
	      "		LEFT JOIN publicationsstudies ps " +
	      "			ON s.study_id = ps.study_id AND (s.study_end_date > ? AND s.study_end_date < ?) " + QUERY_PUBLICATION_IDS + " " +
	      "WHERE (s1.study_id = s.study_id AND ( s.study_end_date > ? AND  s.study_end_date < ? )) " +
	      "		 OR ( s.study_id = ps.study_id ) " +
	      "ORDER BY {0} {1};");

    private final static String STUDY_TYPES_QUERY = "SELECT * FROM studytypes;";

    private final static String DURATION_UNITS_QUERY = "SELECT * FROM durationunits;";

    private final static String STUDY_INSERT = "INSERT INTO study (" +
        "       study_name, " + "       study_type, " +
        "       study_description, " + "       study_keywords, " +
        "       study_notes, " + "       study_students, " +
        "       study_professionals, " + "       study_duration, " +
        "       study_duration_unit, " + "       study_start_date, " +
        "       study_end_date, " + "       study_created_by, " +
        "       study_created_date, " + "       study_edited_by, " +
        "       study_edited_date) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private final static String STUDY_UPDATE = "UPDATE study SET " +
        "       study_name=?, " + "       study_type=?, " +
        "       study_description=?, " + "       study_keywords=?, " +
        "       study_notes=?, " + "       study_students=?, " +
        "       study_professionals=?, " + "       study_duration=?, " +
        "       study_duration_unit=?, " + "       study_start_date=?, " +
        "       study_end_date=?, " + "       study_edited_by=?, " +
        "       study_edited_date=? " + "WHERE 	study_id=?;";

    public static final String DELETE_STUDY_QUERY = "DELETE FROM study WHERE study.study_id = ?;";

    public static final String DELETE_STUDY_MATERIAL_QUERY = "DELETE FROM studymaterial WHERE studymaterial.study_id = ?;";

    public static final String DELETE_PUBLICATION_RELATION_QUERY = "DELETE FROM publicationsstudies WHERE publicationsstudies.study_id=?;";

    public static final String DELETE_RESPONSIBLE_RELATION_QUERY = "DELETE FROM responsiblesstudies WHERE responsiblesstudies.study_id=?;";

    private static final String STUDY_END_DATES_QUERY ="select distinct(study_end_date) from study;";
	private static ArrayList durationUnits;

    public final static class ReportColumns  extends Enum {

        public static final ReportColumns NAME = new ReportColumns("study_name", "name", new DecoratorName(), true);
        public static final ReportColumns TYPE = new ReportColumns("study_type", "type", new Decorator(), true);
        public static final ReportColumns DESCRIPTION = new ReportColumns("study_description", "shortDescription", new Decorator(), true);
        public static final ReportColumns KEYWORDS = new ReportColumns("study_keywords", "keywords", new Decorator(), false);
        public static final ReportColumns NOTES = new ReportColumns("study_notes", "notes", new Decorator(), false);
        public static final ReportColumns STUDENTS = new ReportColumns("study_students", "studentsAsString", new Decorator(), true);
        public static final ReportColumns PROFESSIONALS = new ReportColumns("study_professionals", "professionalsAsString", new Decorator(), true);
        public static final ReportColumns DURATION = new ReportColumns("study_duration", "duration", new Decorator(), false);
        public static final ReportColumns DURATION_UNIT = new ReportColumns("study_duration_unit", "durationUnitAsString", new Decorator(), false);
        public static final ReportColumns START_DATE = new ReportColumns("study_start_date", "startDateAsString", new Decorator(), true);
        public static final ReportColumns END_DATE = new ReportColumns("study_end_date", "endDateAsString", new Decorator(), true);
        public static final ReportColumns CREATED_BY = new ReportColumns("study_created_by", "createdBy", new Decorator(), true);
        public static final ReportColumns EDITED_BY = new ReportColumns("study_edited_by", "editedBy", new Decorator(), true);
        public static final ReportColumns RESPONSIBLES = new ReportColumns("responsible_id", "responsibles", new DecoratorPeople(), false); 
        public static final ReportColumns PUBLICATIONS = new ReportColumns("study_publications", "publications", new DecoratorPublication(), false);
        public static final ReportColumns MATERIALS = new ReportColumns("study_materials", "studyMaterialsAsString", new Decorator(), false);
    	
        public static final ArrayList DEFAULT_COLUMNS = new ArrayList(Arrays.asList(new String[] {NAME.getName(), TYPE.getName(), DESCRIPTION.getName(), END_DATE.getName(), CREATED_BY.getName(), RESPONSIBLES.getName(), PUBLICATIONS.getName()}));
        private String property;
        private Decorator decorator;
        private boolean sort;
        
		protected ReportColumns(String name, String getter, Decorator decorator, boolean sort) {
			super(name);
			this.property = getter;
			this.decorator = decorator;
			this.sort = sort;
		}
		
    	public static List getEnumList() {
    		return getEnumList(ReportColumns.class);
    	}
    	
    	public static ArrayList getColumnNameList() {
    		ArrayList list = new ArrayList();
    		
    		for (Iterator iterator = getEnumList(ReportColumns.class).iterator(); iterator.hasNext();) {
    			ReportColumns column = (ReportColumns) iterator.next();
    			list.add(column.getName());
    		}
    		
      	  return list;
      	}
      	
      	/*
      	 * @see java.lang.Object#toString()
      	 */
      	public String toString() {
      		return getName();
      	}
      	
      	/**
      	 * @param operator
      	 * @return
      	 */
      	public static ReportColumns valueOf(String operator) {
      		if (StringUtils.isEmpty(operator)) {
      			return null;
      		}
      		
      	  return (ReportColumns) getEnum(ReportColumns.class, operator);
      	}

		public String getProperty() {
			return property;
		}

		public boolean isSort() {
			return sort;
		}

		public String decorate(StudyBean study, Decorator.Type type, HttpServletRequest request) {
			if (decorator == null) {
				return "";
			}
			return decorator.decorate(study, getProperty(), type, request);
		}
    }

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Retrieves a single study from the database
     *
     * @param id the id of the study to retrieve
     * @return the StudyBean object representing the study
     * @throws Exception
     */
    public StudyBean getStudy(int id, Connection conn)
        throws Exception {
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        try {
            //Connection conn = getConnection();
            pstmt = conn.prepareStatement(SINGLE_STUDY_QUERY);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            rs.first();

            StudyBean study = populateStudyBeanComplete(rs, ReportColumns.getColumnNameList());

            pstmt = conn.prepareStatement(STUDY_TYPES_QUERY);
            rs = pstmt.executeQuery();

            study.setStudyTypeOptions(getStudyTypes());

            return study;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                pstmt = null;
            }
        }
    }

    /**
     * Retrieves study data for a single study
     *
     * @param id identifies the study
     * @return a StudyBean containing the study data
     * @throws Exception
     */
    public StudyBean getStudy(int id) throws Exception {
        try {
            Connection conn = getConnection();

            return getStudy(id, conn);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * Retrieves a number of studies from the study table
     *
     * @param start row number to start on
     * @param count number of studies to retrieve
     * @return a Collection of StudyBean objects
     */
    public StudiesBean getStudies(StudySortBean sortBean)
        throws Exception {
        int start = sortBean.getStart();
        int count = sortBean.getCount();
        String orderBy = sortBean.getOrderBy();
        String orderDirection = sortBean.getOrderDirection();

        StudiesBean studies = null;
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            
            String sql = STUDY_QUERY.format(new Object[] { orderBy, orderDirection });
            StringBuffer responsiblesSql = new StringBuffer(" ");
            if (sortBean.getResponsibles() != null && !sortBean.getResponsibles().isEmpty()) {
            	responsiblesSql.append(" AND (");
            	boolean isFirst = true;
            	for (Iterator iterator = sortBean.getResponsibles().iterator(); iterator.hasNext();) {
            		if (!isFirst) {
            			responsiblesSql.append(" ").append(sortBean.getResponsiblesOperator()).append(" ");            			
            		}
            		else {
            			isFirst = false;
            		}
					PeopleBean responsible = (PeopleBean) iterator.next();
					responsiblesSql.append(ReportColumns.RESPONSIBLES).append("=").append("'").append(responsible.getId()).append("' ");
					
				}
            	responsiblesSql.append(") ");
            }
            sql = sql.replace(QUERY_RESPONSIBLE_IDS, responsiblesSql.toString());
            
			PreparedStatement pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();

            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, start, count, sortBean.getColumns());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }

    /**
     * Performs an advanced search
     *
     * @param StudySortBean containg the sort and search information
     * @return a StudiesBean object containing all the studies that matched the query
     */
    public StudiesBean advancedSearchStudies(StudySortBean sortBean)
        throws Exception {
        int start = sortBean.getStart();
        int count = sortBean.getCount();
        //String orderBy = sortBean.getOrderBy();
        //String orderDirection = sortBean.getOrderDirection();
        String searchString = sortBean.getSearchString();
        
        String studyType = sortBean.getStudyType();
        String startYear = sortBean.getStartYear();
        String endYear = sortBean.getEndYear();
        String firstName = sortBean.getResponsible_firstname();
        String familyName = sortBean.getResponsible_familyname();

        if(!endYear.equals("") && !startYear.equals("")) {
          int start_dt = Integer.parseInt(startYear);
          int end_dt = Integer.parseInt(endYear);
          if( start_dt > end_dt ){
            endYear = String.valueOf(start_dt);
            startYear = String.valueOf(end_dt);
          }
        }

        if (endYear.equals("")) {
            endYear = "3000";
        }

        if (startYear.equals("")) {
            startYear = "0";
        }


        Calendar calStartDate = Calendar.getInstance();
        calStartDate.set( Integer.parseInt( startYear ), 0, 1);

        Calendar calEndDate = Calendar.getInstance();
        calEndDate.set( Integer.parseInt( endYear ) +1 , 0, 1);



        log.debug("searcgString:" + searchString);
        log.debug("studyType:" + studyType);
        log.debug("startYear:" + startYear);
        log.debug("endYear:" + endYear);
        log.debug("responsible:" + sortBean.getResponsible_firstname() +
          " " + sortBean.getResponsible_familyname() );

        //String responsible = sortBean.getResponsible();

        if ( firstName  == null) {
            firstName = "";
         }
         if( familyName == null ) {
            familyName = "";
        }

      if( studyType == null || studyType.equals("") ){
        studyType =".*";
      }

        //STUDY_SEARCH_QUERY.getFormats();
        ResultSet rs = null;
        StudiesBean studies = null;
        String orderByColumn ="";
        if(sortBean.getOrderBy() != null && StringUtils.isNotEmpty(sortBean.getOrderBy())){
          orderByColumn = "s." + sortBean.getOrderBy();
        }

        try {
            Connection conn = getConnection();
            String formatedQuery = STUDY_SEARCH_QUERY.format(
                        new Object[] {
                            orderByColumn,
                            sortBean.getOrderDirection()
                        });
            // fill responsible ids into query
            ArrayList peopleIdList = new ArrayList();
            String preparePeopleIdsQuery = preparePeopleIdsQuery(firstName, familyName, peopleIdList);
            
            // if responsible is entered but not found, then return no studies
            if ("".equals(preparePeopleIdsQuery)
            		&& (StringUtils.isNotEmpty(firstName) || StringUtils.isNotEmpty(familyName))) {
                studies = new StudiesBean();
                studies.setStudies(new ArrayList());
                return studies;
            }

			formatedQuery = formatedQuery.replaceAll(QUERY_RESPONSIBLE_IDS, preparePeopleIdsQuery);
            
            // fill publication ids into query
            ArrayList publicationIdList = new ArrayList();
            formatedQuery = formatedQuery.replaceAll(QUERY_PUBLICATION_IDS, preparePublicationIdsQuery(searchString, publicationIdList));
            
			PreparedStatement pstmt = conn.prepareStatement(formatedQuery);
            int i = 1;
            pstmt.setString(i++, studyType ); //1
            
            // fill responsible ids into statement
            for (Iterator iterator = peopleIdList.iterator(); iterator.hasNext();) {
            	String id = (String) iterator.next();
            	pstmt.setString(i++, id); //
            }

            pstmt.setString(i++, ".*" + searchString + ".*"); 
            pstmt.setString(i++, ".*" + searchString + ".*"); 
            pstmt.setString(i++, ".*" + searchString + ".*"); 
            pstmt.setString(i++, ".*" + searchString + ".*"); 
            pstmt.setDate(i++, new java.sql.Date( calStartDate.getTime().getTime() ) ); 
            pstmt.setDate(i++, new java.sql.Date( calEndDate.getTime().getTime() )  ); 
            
            // fill publication ids into statement
            for (Iterator iterator = publicationIdList.iterator(); iterator.hasNext();) {
	    		String id = (String) iterator.next();
	    		pstmt.setString(i++, id); //
			}

            pstmt.setDate(i++, new java.sql.Date( calStartDate.getTime().getTime() ) ); //12
            pstmt.setDate(i++, new java.sql.Date( calEndDate.getTime().getTime() )  ); //13


            //pstmt.setString(i++, orderBy);
            //pstmt.setString(i++, orderDirection);
            //System.out.println("Search sql: " + pstmt.toString());

            rs = pstmt.executeQuery();



            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, start, count, StudyDatabase.ReportColumns.DEFAULT_COLUMNS);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }

	private String preparePeopleIdsQuery(String firstName, String familyName, ArrayList peopleIdList) throws MalformedURLException {
        if (StringUtils.isNotEmpty(firstName) || StringUtils.isNotEmpty(familyName)) {        	
		    PeopleDatabase peopleDB = new PeopleDatabase();
		    HashMap hash = new HashMap();
		    hash.put("firstname", firstName);
		    hash.put("lastname", familyName);
		    peopleIdList.addAll(peopleDB.getPeopleIdList(hash));
		    if (!peopleIdList.isEmpty()) {
			    if (!peopleIdList.isEmpty()) {
					return "AND rs.responsible_id in (" + prepareQueryQuestionMarks(peopleIdList.size());
			    }
		    }
        }
		return "";
	}

	private String preparePublicationIdsQuery(String searchString, ArrayList publicationIdList) throws MalformedURLException, UnsupportedEncodingException {
        if (StringUtils.isNotEmpty(searchString)) {        	
		    PublicationDatabase pubDB = new PublicationDatabase();
		    HashMap hash = new HashMap();
		    hash.put("abstract", searchString);
		    ArrayList publicationIdListForAbstract = pubDB.getPublicationIdList(hash);
		    publicationIdList.addAll(publicationIdListForAbstract);
		    hash = new HashMap();
		    hash.put("title", searchString);
		    ArrayList publicationIdListForTitle = pubDB.getPublicationIdList(hash);
		    publicationIdList.addAll(publicationIdListForTitle);

		    if (!publicationIdList.isEmpty()) {
				return "AND ps.publication_id in (" + prepareQueryQuestionMarks(publicationIdList.size());
		    }
		    else {
		        // don't select studies by publications
				return "AND (1 = 0) ";
		    }
		    
        }
		return "";
	}

	private String prepareQueryQuestionMarks(int size) {
		boolean first = true;
		String queryStr = "";
		for (int i = 0; i < size; i++) {					
			if (!first) {
				queryStr+=",";
			}
			queryStr+="?";
			first = false;
		}
		queryStr+= ")";
		return queryStr;
	}

    /**
     * Updates an existing study
     *
     * @param bean contains all data related to the study
     * @throws Exception
     */
    public void updateStudy(StudyBean bean) throws Exception {

        try {
            //Perform any updates in publications and responsibles
            Collection pubs = bean.getPublications();

            if ( pubs != null && pubs.size() > 0) {
                PublicationDatabase pubDb = new PublicationDatabase();
                pubDb.updatePublicationStudyRelationship(bean.getId(), pubs);
            }

            Collection responsibles = bean.getResponsibles();

            if (responsibles != null && responsibles.size() > 0) {
                PeopleDatabase peopleDb = new PeopleDatabase();
                peopleDb.updateResponsibleStudyRelationship(bean.getId(),
                    responsibles);
            }

            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_UPDATE);

            pstmt.setString(1, bean.getName());

            //System.out.println( "Type = " + bean.getType());
            pstmt.setInt(2, Integer.parseInt(bean.getType()));
            pstmt.setString(3, bean.getDescription());
            pstmt.setString(4, bean.getKeywords());
            pstmt.setString(5, bean.getNotes());
            pstmt.setInt(6, bean.getStudents());
            pstmt.setInt(7, bean.getProfessionals());
            pstmt.setInt(8, bean.getDuration());
            pstmt.setInt(9, bean.getDurationUnit());
            if( bean.getStartDate() != null){
              pstmt.setDate(10, new Date(bean.getStartDate().getTime()));
            }
            else{
              pstmt.setNull(10, java.sql.Types.DATE );
            }
            pstmt.setDate(11, new Date(bean.getEndDate().getTime()));
            pstmt.setString(12, bean.getEditorId());
            pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            pstmt.setInt(14, bean.getId());

            int updates = pstmt.executeUpdate();

            if (updates != 1) {
                throw new Exception("The study was not updated (" +
                    bean.getId() + ", " + bean.getName() + ")");
            }

            PeopleDatabase peopleDao = new PeopleDatabase();
            peopleDao.updateResponsibleStudyRelationship(bean.getId(),
                bean.getResponsibles());

            PublicationDatabase pubDao = new PublicationDatabase();
            pubDao.updatePublicationStudyRelationship(bean.getId(),
                bean.getPublications());

            ArrayList material = bean.getStudyMaterial();
            StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
            Iterator iter = material.iterator();

            while (iter.hasNext()) {
                StudyMaterialBean item = (StudyMaterialBean) iter.next();

                if (item.getAdded()) {
                    item.setStudy_id(bean.getId() );
                    materialDb.insertStudyMateriel(item);
                }
                else if( item.getDeleted() && item.getStudy_material_id() != -1  ){
                  materialDb.deleteStudyMaterial( item.getStudy_material_id() );
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * Creates a new study entry
     *
     * @param bean contains all data related to the study
     * @return the ID of the inserted study
     */
    public synchronized int insertStudy(StudyBean bean)
        throws Exception {
        ResultSet rs = null;
        int id = 0;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_INSERT);
            pstmt.setString(1, bean.getName());

            //System.out.println( "Type = " + bean.getType());
            pstmt.setInt(2, Integer.parseInt(bean.getType()));
            pstmt.setString(3, bean.getDescription());
            pstmt.setString(4, bean.getKeywords());
            pstmt.setString(5, bean.getNotes());
            pstmt.setInt(6, bean.getStudents());
            pstmt.setInt(7, bean.getProfessionals());
            pstmt.setInt(8, bean.getDuration());
            pstmt.setInt(9, bean.getDurationUnit());
            if( bean.getStartDate() != null){
              pstmt.setDate(10, new Date(bean.getStartDate().getTime()));
            }
            else{
              pstmt.setNull(10, java.sql.Types.DATE );
            }
            pstmt.setDate(11, new Date(bean.getEndDate().getTime()));
            pstmt.setString(12, bean.getEditorId());
            pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            pstmt.setString(14, bean.getEditorId());
            pstmt.setDate(15, new Date(new java.util.Date().getTime()));

            int updates = pstmt.executeUpdate();

            rs = pstmt.executeQuery("SELECT LAST_INSERT_ID();");

            while (rs.next()) {
                id = rs.getInt(1);
                log.debug("ID of the new study:" + id);
            }

            if (updates == 0) {
                throw new Exception("A new study was not added (" +
                    bean.getName() + ")");
            }

            PeopleDatabase peopleDao = new PeopleDatabase();
            peopleDao.updateResponsibleStudyRelationship(id,
                bean.getResponsibles());

            PublicationDatabase pubDao = new PublicationDatabase();
            pubDao.updatePublicationStudyRelationship(id, bean.getPublications());

            ArrayList material = bean.getStudyMaterial();
            StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
            Iterator iter = material.iterator();

            while (iter.hasNext()) {
                StudyMaterialBean item = (StudyMaterialBean) iter.next();

                if (item.getAdded()) {
                    item.setStudy_id(id);
                    materialDb.insertStudyMateriel(item);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.toString());
        } finally {
            closeConnection(rs);
        }

        return id;
    }

    /**
     * The method iterates through a result set and adds instantiated StudyBeans to a collection.
     *
     * @param rs the SQL resultset
     * @param start which row to start on
     * @param count how many studies to retrieve
     * @return a collection of StudyBeans
     * @throws Exception 
     */
    private StudiesBean getStudyBeans(ResultSet rs, int start, int count, ArrayList columns) throws Exception {
        StudiesBean studies = new StudiesBean();
        ArrayList beans = new ArrayList();

        rs.last();

        int total = rs.getRow();
        studies.setTotalStudies(total);

        //Start should always be above zero
        if (start > 1) {
            rs.absolute(start - 1);
            studies.setStartIndex(rs.getRow());
        } else {
            rs.beforeFirst();
            studies.setStartIndex(1);
        }

        int endRow = 0;

        for (int i = 0; rs.next() && (i < count); i++) {
            StudyBean study = populateStudyBeanComplete(rs, columns);
            
            beans.add(study);
            endRow = rs.getRow();
        }

        studies.setEndIndex(endRow);
        studies.setStudies(beans);

        return studies;
    }

    /**
     * The method instantiates a single StudyBean object from a row in a SQL result set.
     * Only attributes needed for the study list are included
     *
     * @param rs the result set from an SQL query
     * @return a single StudyBean object
     * @throws SQLException
     */
    private StudyBean populateStudyBeanBasic(ResultSet rs)
        throws SQLException {
        StudyBean study = new StudyBean();
        study.setId(rs.getInt(1));
        study.setName(rs.getString(2));
        study.setDescription(rs.getString(3));
        study.setEndDate(rs.getDate(4));
        study.setType(rs.getString(5));

        return study;
    }

    /**
     * The method instantiates a single StudyBean object from a row in a SQL result set
     *
     * @param rs the result set from an SQL query
     * @param columns 
     * @return a single StudyBean object
     * @throws Exception 
     */
    private StudyBean populateStudyBeanComplete(ResultSet rs, ArrayList columns) throws Exception {
        StudyBean study = new StudyBean();
        study.setId(rs.getInt("study_id"));
       	study.setCreatedDate(rs.getDate("study_created_date"));
       	study.setEditedDate(rs.getDate("study_edited_date"));

        //Mandatory attributes
        if (columns.contains(ReportColumns.NAME.getName())) {
        	study.setName(rs.getString("study_name"));
        }
        if (columns.contains(ReportColumns.DESCRIPTION.getName())) {
        	study.setDescription(rs.getString("study_description"));
        }
        if (columns.contains(ReportColumns.TYPE.getName())) {
        	study.setType(rs.getString("type_name"));
        }

        //Optional attributes
        if (columns.contains(ReportColumns.KEYWORDS.getName())) {
        	study.setKeywords(rs.getString("study_keywords"));
        }
        if (columns.contains(ReportColumns.NOTES.getName())) {
        	study.setNotes(rs.getString("study_notes"));
        }
        if (columns.contains(ReportColumns.STUDENTS.getName())) {
        	study.setStudents(rs.getInt("study_students"));
        }
        if (columns.contains(ReportColumns.PROFESSIONALS.getName())) {
        	study.setProfessionals(rs.getInt("study_professionals"));
        }

        //TODO Make changes to duration unit?
        if (columns.contains(ReportColumns.DURATION.getName())) {
        	study.setDuration(rs.getInt("study_duration"));
        }
        if (columns.contains(ReportColumns.DURATION_UNIT.getName())) {
        	study.setDurationUnit(rs.getInt("study_duration_unit"));
        }
        
        if (columns.contains(ReportColumns.END_DATE.getName())) {
        	study.setEndDate(rs.getDate("study_end_date"));
        }
        if (columns.contains(ReportColumns.START_DATE.getName())) {
        	study.setStartDate(rs.getDate("study_start_date"));
        }

        PeopleDatabase peopleDb = new PeopleDatabase();
        String creatorId = null;
        if (columns.contains(ReportColumns.CREATED_BY.getName())) {
        	creatorId = rs.getString("study_created_by");
        	if (StringUtils.isNotEmpty(creatorId)) {
        		PeopleBean creater = peopleDb.getPerson(creatorId);
        		study.setCreatedBy(creater.getFirst_name() + " " + creater.getFamily_name());
        	}
        }
        
        if (columns.contains(ReportColumns.EDITED_BY.getName())) {
        	String updaterId = rs.getString("study_edited_by");
        	if (StringUtils.isNotEmpty(updaterId)) {
        		if (StringUtils.equals(updaterId, creatorId)) {
        			study.setEditedBy(study.getCreatedBy());
        		}
        		else {
        			PeopleBean updater = peopleDb.getPerson(updaterId);
        			study.setEditedBy(updater.getFirst_name() + " " + updater.getFamily_name());        		
        		}
        	}
        }

        int studyId = study.getId();
        
        if (columns.contains(ReportColumns.RESPONSIBLES.getName())) {
        	study.setResponsibles(peopleDb.getResponsibleList(studyId));
        }

        if (columns.contains(ReportColumns.PUBLICATIONS.getName())) {
        	PublicationDatabase pubDb = new PublicationDatabase();
        	study.setPublications(pubDb.getPublicationList(studyId));
        }

        if (columns.contains(ReportColumns.MATERIALS.getName())) {
        	StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
        	study.setStudyMaterial((ArrayList) materialDb.getStudyMaterialInfo(studyId));
        }

        return study;
    }

    /**
     * Retrieves study type ids and names from the database
     *
     * @param rs query results
     * @return an ArrayList containing the found study types
     * @throws SQLException
     */
    public ArrayList getStudyTypes() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_TYPES_QUERY);
            rs = pstmt.executeQuery();

            ArrayList types = new ArrayList();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setName(rs.getString("type_name"));
                bean.setValue(rs.getString("type_id"));
                types.add(bean);
            }

            return types;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Retrieves duration unit ids and names from the database
     *
     * @param rs query results
     * @return an ArrayList containing the found duration units
     * @throws SQLException
     */
    public ArrayList getDurationUnits() throws Exception {
    	if (durationUnits != null && !durationUnits.isEmpty()) {
    		return durationUnits;
    	}
    	
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DURATION_UNITS_QUERY);
            rs = pstmt.executeQuery();

            durationUnits = new ArrayList();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setName(rs.getString("duration_name"));
                bean.setValue(rs.getString("duration_id"));
                durationUnits.add(bean);
            }

            return durationUnits;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Deletes a single study
     *
     * @param studyId the study to delete
     * @throws Exception
     */
    public void deleteStudy(int studyId) throws Exception {
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.DELETE_STUDY_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_PUBLICATION_RELATION_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_RESPONSIBLE_RELATION_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_STUDY_MATERIAL_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            log.error("Error deleting study " + studyId, ex);
            throw ex;
        } finally {
            this.closeConnection(null);
        }
    }

    /**
     * Retrieves all registered studies
     *
     * @return a Collection containing StudyBean objects
     * @throws Exception
     *
     * @see no.simula.des.data.beans.StudyBean
     */
    public Collection getAllStudies() throws Exception {
        ArrayList studies = new ArrayList();
        ArrayList studyIds = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.GET_ALL_STUDY_IDS_QUEREY);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                int studyId = rs.getInt(1);
                studyIds.add(new Integer(studyId));
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                pstmt = null;
            }

            Iterator ids = studyIds.iterator();

            while (ids.hasNext()) {
                int studyId = ((Integer) ids.next()).intValue();
                StudyBean study = this.getStudy(studyId, conn);
                studies.add(study);
            }

            return studies;
        } catch (Exception ex) {
            log.error("Error fetching data:", ex);
            throw ex;
        } finally {
            this.closeConnection(rs);
        }
    }

    public boolean isStudyNameValid(String studyName, int id) throws Exception{
      ResultSet rs = null;
      boolean ret = true;
      try {
         Connection conn = getConnection();
         PreparedStatement pstmt = conn.prepareStatement(CHECK_STUDY_NAME_QUERY);
         pstmt.setString(1, studyName);
         pstmt.setInt(2, id);
         rs = pstmt.executeQuery();
         if(rs.next()){
            int count = rs.getInt(1);
            if(count > 0 ) ret = false;
         }
         return ret;
      }
      catch (Exception ex) {
        log.error("Error checking validy of studyname",ex);
        throw ex;
      }
      finally{
        closeConnection(rs);
      }
    }

   public ArrayList getStudyEndDates() throws Exception{
    ResultSet rs = null;
    ArrayList dates = new ArrayList();
    try {
      Connection conn = getConnection();
      PreparedStatement pstmt = conn.prepareStatement(STUDY_END_DATES_QUERY);
      rs = pstmt.executeQuery();
      while(  rs.next() ){
        java.util.Date date = rs.getDate(1);
        dates.add(date);
      }
      return dates;
    }
    catch (Exception ex) {
      log.error("Error retrieveing study end dates", ex);
      throw ex;
    }
    finally {
      closeConnection(rs);
    }


   }

public String getDurationUnits(int durationUnit) throws Exception {
	for (Iterator iterator = getDurationUnits().iterator(); iterator.hasNext();) {
		 NameValueBean nvb =  (NameValueBean) iterator.next();
		 if (Integer.parseInt(nvb.getValue()) == durationUnit) {
             return nvb.getName();
         }
	}
	return null;
}
}
