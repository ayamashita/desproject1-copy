/*
 * Created on 13.okt.2003
 *
*/
package no.simula.des.data;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import no.simula.des.data.beans.PublicationBean;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * This class manages database access for publications related queries. As publications
 * are used in DES only as "links to related publications", the class never actually
 * deletes publications from the database - only relationships to studies are removed.
 */
public class PublicationDatabase extends DataObject {

    private static final String RCP_METHOD = "getPublications";

	private final static String RELATED_PUBLICATIONS_QUERY =
        "select publication_id from publicationsstudies where study_id = ?;";

    private final static String DELETE_PUBLICATION =
        "DELETE FROM publicationsstudies WHERE (publication_id=?) AND (study_id=?);";

    private final static String ADD_PUBLICATION =
        "INSERT IGNORE INTO publicationsstudies " + "VALUES (?, ?);";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Retrieves a single publication from the publication table
     *
     * @param id the publication to retrieve
     * @return the PublicationBean representing the given id
     */
    public PublicationBean getPublication(String id) throws Exception {
    	
		HashMap hash = new HashMap(); 
		hash.put("id", StringUtils.trim(id)); /* Keyword 'id' is used when searching criteria is based on username */
    	Object[] elementList = getPublicationsByFilter(hash);
    	HashMap element = (HashMap)elementList[0];

    	return PublicationBean.createPublicationBean(element);
    }

    /**
     * Retrievs all publications related to one study
     *
     * @param studyId identifies the study
     * @return the publications
     */
    public Collection getPublicationList(int studyId) throws Exception {
    	ArrayList publicationList = new ArrayList();
        ResultSet rs = null;
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(RELATED_PUBLICATIONS_QUERY);
            pstmt.setInt(1, studyId);

            //Find all related publications using publicationsstudies
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	String publicationId = rs.getString(1);
                if (StringUtils.isNotEmpty(publicationId)) {
                	
                	PublicationBean publication = getPublication(publicationId);
                	
                	if (publication != null) {
                		publicationList.add(publication);
                	}
                }
            }

            return publicationList;

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Retrieves all publications
     *
     * @return the publications
     * @throws MalformedURLException
     * @throws UnsupportedEncodingException 
     */
    public Collection getPublicationList() throws MalformedURLException, UnsupportedEncodingException {
    	ArrayList publicationList = new ArrayList();

		Object[] elementList = (Object[]) getPublicationsByFilter(null);
		for(int j = 0; j<elementList.length; j++)
		{
			PublicationBean publication = PublicationBean.createPublicationBean((HashMap)elementList[j]);

        	if (publication != null) {
        		publicationList.add(publication);
        	}
		}
        
        return publicationList;
    }

    /**
     * The method adds and removes relationships between publications and studies at the
     * database level.
     *
     * @param studyId identifies which study we are changing the relationships for.
     * @param publications all publications related to the study, including any added or deleted
     *                     ones.
     * @throws Exception
     */
    public void updatePublicationStudyRelationship(int studyId,
        Collection publications) throws Exception {
        if( publications == null) return;
        Iterator iterator = publications.iterator();

        //Discover any changes
        while (iterator.hasNext()) {
            PublicationBean bean = (PublicationBean) iterator.next();

            if (bean.getDeleted()) {
                //Delete publication from study
                deletePublicationFromStudy(studyId, bean.getId());
            } else if (bean.getAdded()) {
                //Add publication to study
                addPublicationToStudy(studyId, bean.getId());
            }
        }
    }

    /**
     * This method relates a publication to a study
     *
     * @param studyId
     * @param publicationId
     */
    void addPublicationToStudy(int studyId, String publicationId)
        throws Exception {
        //TODO Can we assume that relation is not already existing?
        //If the addPublication action handles this, no additional check (SELECT) is needed here
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ADD_PUBLICATION);
            pstmt.setString(1, publicationId);
            pstmt.setInt(2, studyId);

            int addCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(addCount + " publications added to study " + studyId);
            }
            /*
            if (addCount != 1) {
                throw new Exception("Publication (id=" + publicationId +
                    ") was not added from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * This method removes the relationship between a study and a publication in the DES database
     *
     * @param studyId
     * @param publicationId
     */
    void deletePublicationFromStudy(int studyId, String publicationId)
        throws Exception {
        //Delete all matching entries in publicationsstudies...
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DELETE_PUBLICATION);
            pstmt.setString(1, publicationId);
            pstmt.setInt(2, studyId);

            int delCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(delCount + " publications deleted from study " +
                    studyId);
            }
            /*
            if (delCount != 1) {
                throw new Exception("Publication (id=" + publicationId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }
    
	/**
	 * @param hash
	 * @return Publications by filled filter
	 * @throws MalformedURLException
	 */
	public Object[] getPublicationsByFilter(HashMap hash) throws MalformedURLException {
		return getRcpBeanByFilter(RCP_METHOD, hash);
	}
    
	/**
	 * @param hash
	 * @return Publication ids searched by HashMap filter
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException 
	 */
	public ArrayList getPublicationIdList(HashMap hash) throws MalformedURLException, UnsupportedEncodingException {
		
		ArrayList publicationList = new ArrayList();
    	Object[] elementList = getPublicationsByFilter(hash);
    	for (int i = 0; i < elementList.length; i++) {
    		HashMap element = (HashMap)elementList[i];
    		PublicationBean publicationBean = PublicationBean.createPublicationBean(element);
			publicationList.add(publicationBean.getId());
		}

    	return publicationList;

    	
	}
    	
}
