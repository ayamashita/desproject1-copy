package no.simula.des.data;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.data.beans.StudyBean;

public class DecoratorName extends Decorator {
 
	protected Object getValue(Object bean, String property, Type type, HttpServletRequest request) {
		Object value = super.getValue(bean, property, type, request);

		if (Type.HTML.equals(type)) {
			StringBuilder result = new StringBuilder();

			result.append("<a href=\""+request.getContextPath()+"/showStudy.do?study=").append(((StudyBean)bean).getId()).append("\">").append(value).append("&nbsp;</a>");
			return result.toString();		 
		}  

		return value;
	}
}
