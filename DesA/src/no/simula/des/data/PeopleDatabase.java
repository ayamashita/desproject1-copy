package no.simula.des.data;

import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import no.simula.des.data.beans.AdminPrivilegesBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Handles retrival of all people related data. The "people table" itself is not modified,
 * only relations between it and other tables.
 *
 */
public class PeopleDatabase extends DataObject {
    private static final String RCP_METHOD = "getPeople";
	private final static String DELETE_RESPONSIBLE =
        "DELETE FROM responsiblesstudies WHERE (responsible_id=?) AND (study_id=?);";
    private final static String ADD_RESPONSIBLE =
        "INSERT IGNORE INTO responsiblesstudies " + "VALUES (?, ?);";
    private static final String GET_STUDY_RESPONSIBLES_QUERY = "select responsible_id from responsiblesstudies r where r.study_id = ?;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * The method adds and removed relationships between responsibles and studies at the
     * database level.
     *
     * @param studyId identifies which study we are changing the relationships for.
     * @param publications all responsibles related to the study, including any added or deleted
     *                     ones.
     * @throws Exception
     */
    public void updateResponsibleStudyRelationship(int studyId,
        Collection responsibles) throws Exception {
        if(responsibles == null) return;
        Iterator iterator = responsibles.iterator();

        //Discover any changes
        while (iterator.hasNext()) {
            PeopleBean bean = (PeopleBean) iterator.next();

            if (bean.getDeleted()) {
                //Delete publication from study
                deleteResponsibleFromStudy(studyId, bean.getId());
            } else if (bean.getAdded()) {
                //Add publication to study
                addResponsibleToStudy(studyId, bean.getId());
            }
        }
    }

    /**
     * Retrieves all people from the database
     *
     * @return a PeoplesBean object containing a list of all persons
     * @throws Exception
     */
    public PeoplesBean getPeoples() throws Exception {
        
    	PeoplesBean peoples = new PeoplesBean();
    	
		HashMap hash = new HashMap();
		hash.put("sort_on", "lastname");
		Object[] elementList = (Object[]) getPeopleByFilter(hash);
		for(int j = 0; j<elementList.length; j++)
		{
			HashMap element = (HashMap)elementList[j];
			peoples.addPerson(PeopleBean.createPeopleBean(element));
		}

        AdminPrivilegesDatabase privilegesDB = new AdminPrivilegesDatabase();
        AdminPrivilegesBean priviileges = privilegesDB.getAdminPrivileges();
        peoples.setPrivileges(priviileges);

        return peoples;

    }


    /**
     * Retrives all persons listed as responsibles for a given study
     *
     * @param studyId ID of the study
     * @return a Collection of PeopleBean objects. An empty Collection if no responsibles exist
     */
    public Collection getResponsibleList(int studyId) throws Exception {
    	
    	PeoplesBean peoples = new PeoplesBean();
        ResultSet rs = null;

        try {
        	
        	Connection conn = getConnection();
            PreparedStatement stmt = conn.prepareStatement(PeopleDatabase.GET_STUDY_RESPONSIBLES_QUERY);
            stmt.setInt(1, studyId);
            rs = stmt.executeQuery();

            while (rs.next()) {
            	String peopleId = rs.getString(1);
                if (StringUtils.isNotEmpty(peopleId)) {
                	
                	PeopleBean people = getPerson(peopleId);                	
                	peoples.addPerson(people);
                }
            }

            return peoples.getPeoples();
        } catch (Exception ex) {
            log.error("Error retrieving study responsibles from study: " +
                studyId, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * This method relates a responsible to a study
     *
     * @param studyId
     * @param publicationId
     */
    void addResponsibleToStudy(int studyId, String responsibleId)
        throws Exception {
        //TODO Can we assume that relation is not already existing?
        //If the addPublication action handles this, no additional check (SELECT) is needed here
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ADD_RESPONSIBLE);
            pstmt.setString(1, responsibleId);
            pstmt.setInt(2, studyId);

            int addCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(addCount + " responsibles added to study " + studyId);
            }
            /*
            if (addCount != 1) {
                throw new Exception("Responsible (id=" + responsibleId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * This method removes the relationship between a study and a responsible in the DES database
     *
     * @param studyId
     * @param publicationId
     */
    void deleteResponsibleFromStudy(int studyId, String responsibleId)
        throws Exception {
        //Delete all matching entries in publicationsstudies...
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DELETE_RESPONSIBLE);
            pstmt.setString(1, responsibleId);
            pstmt.setInt(2, studyId);

            int delCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(delCount + " responsibles deleted from study " +
                    studyId);
            }
            /*
            if (delCount != 1) {
                throw new Exception("Responsible (id=" + responsibleId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }
    
	/**
	 * @param hash
	 * @return Object[] of people map by filter conditions 
	 * @throws MalformedURLException
	 */
	public Object[] getPeopleByFilter(HashMap hash) throws MalformedURLException {
		return getRcpBeanByFilter(RCP_METHOD, hash);

	}
	
	/**
	 * @param id
	 * @return PeopleBean by id from WEB Servises
	 * @throws MalformedURLException
	 */
	public PeopleBean getPerson(String id) throws MalformedURLException {
		HashMap hash = new HashMap(); 
		hash.put("id", id); /* Keyword 'id' is used when searching criteria is based on username */
    	Object[] elementList = getPeopleByFilter(hash);
    	if (elementList.length != 1) {
    		return null;
    	}
    	HashMap element = (HashMap)elementList[0];
    	
    	return PeopleBean.createPeopleBean(element);
	}

	public ArrayList getPeopleIdList(HashMap hash) throws MalformedURLException {
		ArrayList peopleList = new ArrayList();
    	Object[] elementList = getPeopleByFilter(hash);
    	for (int i = 0; i < elementList.length; i++) {
    		HashMap element = (HashMap)elementList[i];
    		PeopleBean peopleBean = PeopleBean.createPeopleBean(element);
			peopleList.add(peopleBean.getId());
		}

    	return peopleList;
	}


}
