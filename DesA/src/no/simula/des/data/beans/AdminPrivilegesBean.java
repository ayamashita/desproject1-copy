package no.simula.des.data.beans;

import java.util.HashMap;


/**
 * Represents the admin privilege level for a single person
 */
public class AdminPrivilegesBean {
    public static int DBA_PRIVILEGE = 2;
    public static int SA_PRIVILEGE = 1;
    public static int GUEST_PRIVILEGE = 0;
    private HashMap privileges = new HashMap();

    public void setAdminPrivileges(String people_id, int privilege) {
        privileges.put(people_id, new Integer(privilege));
    }

    public int getAdminPrivileges(String people_id) {
        Object tmp = privileges.get(people_id);

        if (tmp == null) {
            return 0;
        }

        return ((Integer) tmp).intValue();
    }
}
