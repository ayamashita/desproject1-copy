package no.simula.des.data.beans;

import no.simula.des.data.beans.AdminPrivilegesBean;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Represents a list of people
 */
public class PeoplesBean {
    private ArrayList peoples = new ArrayList();

    public ArrayList getPeoples() {
    	if (peoples == null) {
    		peoples = new ArrayList();
    	}
        return peoples;
    }

    public void setPeoples(ArrayList peoples) {
        this.peoples = peoples;
    }

    /**
     * Sets (loads) the privilege level for all people in the list
     * @param privileges
     */
    public void setPrivileges(AdminPrivilegesBean privileges) {
        Iterator iter = peoples.iterator();

        while (iter.hasNext()) {
            PeopleBean people = (PeopleBean) iter.next();
            String id = people.getId();
            int privilege = privileges.getAdminPrivileges(id);

            if (!(privilege == AdminPrivilegesBean.GUEST_PRIVILEGE)) {
                people.setPrivilege(privilege);
            }
        }
    }
    
    public void addPerson(PeopleBean peopleBean) {
    	if (peopleBean != null) {
    		getPeoples().add(peopleBean);
    	}
    }
    
}
