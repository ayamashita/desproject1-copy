/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data.beans;

import no.simula.des.data.StudyDatabase;
import no.simula.des.util.Constants;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
/**
 * Represents a single study
 *
 */
public class StudyBean {
    //Mandatory attributes
    private int id = 0;
    private String name = null;
    private String type = null;
    private String description = null;
    private Date endDate = null;

    //Optional attributes
    private int duration =-1;
    private int durationUnit;

    //TODO make sure duration unit is properly included
    private String keywords = null;
    private String notes = null;
    private int students = -1;
    private int professionals = -1;
    private Date startDate = null;
    private Collection publications;
    private Collection responsibles;
    private ArrayList studyTypeOptions;
    private ArrayList durationUnitOptions;
    private ArrayList studyMaterial = new ArrayList();

    //Auto-generated attributes
    private Date createdDate = null;
    private Date editedDate = null;
    private String createdBy = null;
    private String editedBy = null;
    private String editorId = null;

    // ------------- Getter and setter methods ---------------

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    public String getShortDescription() {
        int length = description.length();

        return ((length > 90) ? description.substring(0, 90) : description);
    }

    /**
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * @param string
     */
    public void setDescription(String string) {
        description = string;
    }

    /**
     * @param string
     */
    public void setEndDate(Date date) {
        endDate = date;
    }

    /**
     * @param i
     */
    public void setId(int i) {
        id = i;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @param i
     */
    public void setType(String string) {
        type = string;
    }

    /**
     * @return
     */
    public int getDuration() {
        return duration;
    }


    /**
     * @return
     */
    public String getDurationAsString() {
    	if (duration < 0) {
    		return "";
    	}
        return ""+duration;
    }
    
    /**
     * @return
     */
    public int getDurationUnit() {
        return durationUnit;
    }

    /**
     * @return
     */
    public String getDurationUnitAsString() {
    	StudyDatabase db = new StudyDatabase();
        try {
			return db.getDurationUnits(durationUnit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
    }
    
    /**
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    public String getStartDateAsString(){
      if(startDate==null){
        return "";
      }
      SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );
       return formatter.format(this.startDate);
    }

    /**
     * @param i
     */
    public void setDuration(int i) {
        duration = i;
    }

    /**
     * @param i
     */
    public void setDurationUnit(int i) {
        durationUnit = i;
    }

    /**
     * @param date
     */
    public void setStartDate(Date date) {
        startDate = date;
    }

    /**
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @return
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedDateAsString(){
       SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );
       return formatter.format(this.createdDate);
    }

    /**
     * @return
     */
    public String getEditedBy() {
        return editedBy;
    }

    /**
     * @return
     */
    public Date getEditedDate() {
        return editedDate;
    }

    public String getEditedDateAsString(){
      SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );
       return formatter.format(this.editedDate);
    }

    /**
     * @return
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @return
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @return
     */
    public int getProfessionals() {
        return professionals;
    }

    /**
     * @return
     */
    public String getProfessionalsAsString() {
    	if (professionals < 0) {
    		return "";
    	}
        return ""+professionals;
    }

    
    /**
     * @return
     */
    public String getStudentsAsString() {
    	if (students < 0) {
    		return "";
    	}
        return ""+students;
    }

    /**
     * @param string
     */
    public void setCreatedBy(String string) {
        createdBy = string;
    }

    /**
     * @param date
     */
    public void setCreatedDate(Date date) {
        createdDate = date;
    }

    /**
     * @param string
     */
    public void setEditedBy(String string) {
        editedBy = string;
    }

    /**
     * @param date
     */
    public void setEditedDate(Date date) {
        editedDate = date;
    }

    /**
     * @param string
     */
    public void setKeywords(String string) {
        keywords = string;
    }

    /**
     * @param string
     */
    public void setNotes(String string) {
        notes = string;
    }

    /**
     * @param i
     */
    public void setProfessionals(int i) {
        professionals = i;
    }

    /**
     * @param i
     */
    public void setStudents(int i) {
        students = i;
    }

    public String toString() {
        return "id: " + id + ", name: " + name + ", type: " + type +
        ", description: " + description + ", endDate: " + endDate;
    }

    public String getEndDateAsString() {
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN, Locale.US );

        return formatter.format(this.endDate);
    }

    /**
     * @return
     */
    public ArrayList getDurationUnitOptions() {
        return durationUnitOptions;
    }

    /**
     * @return
     */
    public ArrayList getStudyTypeOptions() {
        return studyTypeOptions;
    }

    /**
     * @param list
     */
    public void setDurationUnitOptions(ArrayList list) {
        durationUnitOptions = list;
    }

    /**
     * @param list
     */
    public void setStudyTypeOptions(ArrayList list) {
        studyTypeOptions = list;
    }

    /**
     * @return
     */
    public String getEditorId() {
        return editorId;
    }

    /**
     * @param i
     */
    public void setEditorId(String id) {
        editorId = id;
    }

    /**
     * @return
     */
    public Collection getPublications() {
        return publications;
    }

    /**
     * @return
     */
    public Collection getResponsibles() {
        return responsibles;
    }

    /**
     * @param list
     */
    public void setPublications(Collection list) {
        publications = list;
    }
    
    /**
     * @param list
     */
    public void setResponsibles(Collection list) {
        responsibles = list;
    }
  public ArrayList getStudyMaterial() {
    return studyMaterial;
  }
  public String getStudyMaterialsAsString() {
	  	if (studyMaterial == null) {
			return "";
		}
		
		StringBuilder string = new StringBuilder(""); 
		
		for (Iterator iterator = studyMaterial.iterator(); iterator.hasNext();) {
			StudyMaterialBean material = (StudyMaterialBean) iterator.next();
	    	if (string.length() > 1) {
	    		string.append(", ");
	    	}
			string.append(material.getDescription());
			string.append(" ");
			string.append((material.getIsUrl() ? "URL: "+ material.getUrl(): "File: " + material.getFilename()));
		}
		
	    return string.toString();
	  }
  
  public void setStudyMaterial(ArrayList studyMaterial) {
    this.studyMaterial = studyMaterial;
  }

public int getStudents() {
	return students;
}

}
