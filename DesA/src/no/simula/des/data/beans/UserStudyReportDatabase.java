package no.simula.des.data.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import no.simula.des.data.DataObject;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.util.Constants;

import org.apache.commons.lang.enum.Enum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UserStudyReportDatabase extends DataObject {
	private static final String TABLE_USER_STUDY_REPORT_COLUMNS = "user_study_report_columns";
	private static final String TABLE_USER_STUDY_REPORT_RESPONSIBLES = "user_study_report_responsibles";

	private static final String TABLE_USER_STUDY_REPORTS = "user_study_reports";

	private static final String SELECT_REPORTS = 
		"SELECT * FROM "+TABLE_USER_STUDY_REPORTS+" WHERE "+ColumnNames.USER_ID.getName()+" = ? ORDER BY "+ColumnNames.NAME.getName()+" ;";

    private final static String INSERT_REPORT = 
    	"INSERT INTO "+TABLE_USER_STUDY_REPORTS+" ("+
	    	ColumnNames.USER_ID.getName() + ", " +
	    	ColumnNames.NAME.getName() + ", " +
	    	ColumnNames.SORT_END_DATE.getName() + ", " +
	    	ColumnNames.WHERE_RESPONSIBLE_OPERATOR.getName() + ") " +
    	" VALUES (?, ?, ?, ?);";
    
    private final static String UPDATE_REPORT = 
    	"UPDATE "+TABLE_USER_STUDY_REPORTS+" SET "+
	    	ColumnNames.NAME.getName() + "= ?," +
	    	ColumnNames.SORT_END_DATE.getName() + "= ?," +
	    	ColumnNames.WHERE_RESPONSIBLE_OPERATOR.getName() + "= ?" +
    	" WHERE "+ ColumnNames.ID.getName() + "= ?";

    private final static String DELETE_REPORT = 
    	"DELETE FROM "+TABLE_USER_STUDY_REPORTS+
    	" WHERE "+ ColumnNames.ID.getName() + "= ?";

	private static final String SELECT_REPORT_COLUMNS = 
		"SELECT * FROM "+TABLE_USER_STUDY_REPORT_COLUMNS+" WHERE "+ColumnNames.REPORT_ID.getName()+" = ? ORDER BY "+ColumnNames.COLUMN_NAME.getName()+";";

    private final static String INSERT_REPORT_COLUMNS = 
    	"INSERT INTO "+TABLE_USER_STUDY_REPORT_COLUMNS+" ("+
	    	ColumnNames.REPORT_ID.getName() + ", " +
	    	ColumnNames.COLUMN_NAME.getName() + ") " +
    	" VALUES (?, ?);";
	
    private final static String DELETE_REPORT_COLUMNS = 
    	"DELETE FROM "+TABLE_USER_STUDY_REPORT_COLUMNS+
    	" WHERE "+ ColumnNames.REPORT_ID.getName() + "= ?";
	
	private static final String SELECT_REPORT_RESPONSIBLES = 
		"SELECT * FROM "+TABLE_USER_STUDY_REPORT_RESPONSIBLES+" WHERE "+ColumnNames.REPORT_ID.getName()+" = ? ;";
    
    private final static String INSERT_REPORT_RESPONSIBLES = 
    	"INSERT INTO "+TABLE_USER_STUDY_REPORT_RESPONSIBLES+" ("+
	    	ColumnNames.REPORT_ID.getName() + ", " +
	    	ColumnNames.RESPONSIBLE_ID.getName() + ") " +
    	" VALUES (?, ?);";

    private final static String DELETE_REPORT_RESPONSIBLES = 
    	"DELETE FROM "+TABLE_USER_STUDY_REPORT_RESPONSIBLES+
    	" WHERE "+ ColumnNames.REPORT_ID.getName() + "= ?";
	
    private final static class ColumnNames extends Enum {

    	public static final ColumnNames ID = new ColumnNames("ID");
    	public static final ColumnNames USER_ID = new ColumnNames("User_ID");
    	public static final ColumnNames NAME = new ColumnNames("Name");
    	public static final ColumnNames SORT_END_DATE = new ColumnNames("Sort_End_Date");
    	public static final ColumnNames WHERE_RESPONSIBLE_OPERATOR = new ColumnNames("Where_Responsible_Operator");

    	public static final ColumnNames REPORT_ID = new ColumnNames("Report_ID"); // for user_study_report_columns, user_study_report_responsibles tables
    	public static final ColumnNames COLUMN_NAME = new ColumnNames("Column_Name"); // for user_study_report_columns table
    	public static final ColumnNames RESPONSIBLE_ID = new ColumnNames("Responsible_ID"); // for user_study_report_responsibles table

		protected ColumnNames(String name) {
			super(name);
		}
    }
    

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
	
	public HashMap getUserReports(String userId) throws Exception {
        ResultSet rs = null;
        
        try {
        	HashMap userReports = new HashMap();
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(SELECT_REPORTS);
            pstmt.setString(1, userId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                UserStudyReportBean report = new UserStudyReportBean();
                report.setId(rs.getInt(ColumnNames.ID.getName()));
                report.setUserId(rs.getString(ColumnNames.USER_ID.getName()));
                report.setName(rs.getString(ColumnNames.NAME.getName()));
                report.setSortEndDate(rs.getString(ColumnNames.SORT_END_DATE.getName()));
                report.setWhereResponsibleOperator(rs.getString(ColumnNames.WHERE_RESPONSIBLE_OPERATOR.getName()));

	            report.setStudyColumns(readReportColumns(conn, report.getId()));
	            report.setResponsibles(readReportResponsibles(conn, report.getId()));

                userReports.put(report.getIdObj(), report);
            }
            
            return userReports;
        } catch (Exception e) {
            log.error("Error reading report.", e);
            throw e;
        } finally {        	
            closeConnection(rs);
        }
	}

	private ArrayList readReportColumns(Connection conn, int reportId) throws SQLException, Exception {
		
		PreparedStatement pstmt;
		ArrayList list = new ArrayList();
		pstmt = conn.prepareStatement(SELECT_REPORT_COLUMNS);
		pstmt.setInt(1, reportId);
		ResultSet rsColumns = null;
		try {
			rsColumns = pstmt.executeQuery();
			while (rsColumns.next()) {
				list.add(rsColumns.getString(ColumnNames.COLUMN_NAME.getName()));
			}
		} catch (Exception e) {
		    log.error("Error reading report colums.", e);
		    throw e;
		} finally {        	
		    closeResultSet(rsColumns);
		}
		return list;
	}
	
	private ArrayList readReportResponsibles(Connection conn, int reportId) throws SQLException, Exception {
		
		ArrayList list = new ArrayList();
		PreparedStatement pstmt = conn.prepareStatement(SELECT_REPORT_RESPONSIBLES);
		pstmt.setInt(1, reportId);
		ResultSet rsResponsibles = null;
		PeopleDatabase pd = new PeopleDatabase();
		try {
			rsResponsibles = pstmt.executeQuery();
			while (rsResponsibles.next()) {
				list.add(pd.getPerson(rsResponsibles.getString(ColumnNames.RESPONSIBLE_ID.getName())));
			}
		} catch (Exception e) {
			log.error("Error reading report responsibles.", e);
			throw e;
		} finally {        	
			closeResultSet(rsResponsibles);
		}
		
		return list;
	}

	/**
	 * Create object report in DB
	 * @param report
	 * @throws Exception 
	 */
	public void create(UserStudyReportBean report) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = this.getConnection();
            PreparedStatement stmt = conn.prepareStatement(INSERT_REPORT);
            int i = 1;
            stmt.setString(i++, report.getUserId());
            stmt.setString(i++, report.getName());
            stmt.setString(i++, report.getSortEndDate());
            stmt.setString(i++, report.getWhereResponsibleOperator());

            int count = stmt.executeUpdate();
            
            if (count == 0 || count > 1) {
                throw new Exception("A new report was not added (" + report.getName() + ")");
            }
            
            rs = stmt.executeQuery("SELECT LAST_INSERT_ID();");

			while (rs.next()) {
            	report.setId(rs.getInt(1));
                log.debug("ID of the new report:" + report.getId());
            }
			
			int reportId = report.getId();
			
			insertReportColumns(conn, reportId, report.getStudyColumns());
			insertReportResponsibles(conn, reportId, report.getResponsibles());

        } catch (Exception ex) {
            throw ex;
        } finally {
            super.closeConnection(rs);
        }
	}

	private void insertReportResponsibles(Connection conn, int reportId, ArrayList responsibles) throws SQLException {
		PreparedStatement stmt;
		int i;
		if (!responsibles.isEmpty()) {
			for (Iterator iterator = responsibles.iterator(); iterator.hasNext();) {
				stmt = conn.prepareStatement(INSERT_REPORT_RESPONSIBLES);
				PeopleBean people = (PeopleBean) iterator.next();
				i = 1;				
				stmt.setInt(i++, reportId);
				stmt.setString(i++, people.getId());
				stmt.executeUpdate();
			}
		}
	}

	private void insertReportColumns(Connection conn, int reportId, ArrayList studyColumns) throws SQLException {
		PreparedStatement stmt;
		int i;
		if (!studyColumns.isEmpty()) {
		    for (Iterator iterator = studyColumns.iterator(); iterator.hasNext();) {
		    	stmt = conn.prepareStatement(INSERT_REPORT_COLUMNS);
				String columnName = (String) iterator.next();
				i = 1;				
				stmt.setInt(i++, reportId);
				stmt.setString(i++, columnName);
				stmt.executeUpdate();
			}
		}
	}

	/**
	 * Update object report in DB
	 * @param report
	 * @throws Exception 
	 */
	public void update(UserStudyReportBean report) throws Exception {
        
		int reportId = report.getId();
		if (reportId < 1) {
			throw new IllegalArgumentException("A report (" + report.getName() + ") has not valid id for update.");
		}
		
		ResultSet rs = null;
        
        try {
            Connection conn = this.getConnection();
            PreparedStatement stmt = conn.prepareStatement(UPDATE_REPORT);
            int i = 1;
            stmt.setString(i++, report.getName());
            stmt.setString(i++, report.getSortEndDate());
            stmt.setString(i++, report.getWhereResponsibleOperator());
            stmt.setInt(i++, reportId);
            
            int count = stmt.executeUpdate();
            
            if (count == 0 || count > 1) {
                throw new Exception("A report (" + report.getName() + ") was not updated ");
            }
            deleteReportColumsAndResponsibles(reportId, conn);

            insertReportColumns(conn, reportId, report.getStudyColumns());
            insertReportResponsibles(conn, reportId, report.getResponsibles());

        } catch (Exception ex) {
            throw ex;
        } finally {
            super.closeConnection(rs);
        }
	}

	/**
	 * Update object report in DB
	 * @param report
	 * @throws Exception 
	 */
	public void delete(int id) throws Exception {
        
		if (id < 1) {
			throw new IllegalArgumentException("A report id (" + id + ") is not valid id for delete.");
		}
		PreparedStatement stmt = null;
        try {
            Connection conn = this.getConnection();
            stmt = conn.prepareStatement(DELETE_REPORT);
            stmt.setInt(1, id);
            stmt.executeUpdate();

            deleteReportColumsAndResponsibles(id, conn);
            
        } catch (Exception ex) {
            throw ex;
        } finally {
            super.closeConnection(null);
        }
	}

	private void deleteReportColumsAndResponsibles(int id, Connection conn) throws Exception {
		PreparedStatement stmt = null;
		
		stmt = conn.prepareStatement(DELETE_REPORT_COLUMNS);
		stmt.setInt(1, id);		
		stmt.executeUpdate();

		stmt = conn.prepareStatement(DELETE_REPORT_RESPONSIBLES);
		stmt.setInt(1, id);
		stmt.executeUpdate();
		
	}
}
