/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;


/**
 * Represents a single person
 */
public class PeopleBean {
    public PeopleBean() {
		super();
	}
    
	private String id;
    private String first_name;
    private String family_name;
    private String position;
    private int privilege = 0;
    private String url;    

    //Utility attributes
    private boolean isAdded;

    //Utility attributes
    private boolean isDeleted;
    
    private HashMap studyReports;

    public String getFamily_name() {
        return family_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getId() {
        return id;
    }

    public String getIdAsString() {
        return id;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public boolean getAdded() {
        return isAdded;
    }

    public boolean getDeleted() {
        return isDeleted;
    }

    public void setAdded(boolean b) {
        isAdded = b;
    }

    public void setDeleted(boolean b) {
        isDeleted = b;
    }
    
    public static PeopleBean createPeopleBean(HashMap map) {

    	if (map == null) {
    		return null;
    	}
    	
    	PeopleBean people = new PeopleBean();
        people.setId((String) map.get("id"));
        people.setFirst_name((String) map.get("firstname"));
        people.setFamily_name((String) map.get("lastname"));
        people.setPosition((String) map.get("jobtitle"));
        people.setUrl((String) map.get("url"));

        return people;
    }

	public HashMap getStudyReports() {
		if (studyReports == null) {
			studyReports = new HashMap();
		}
		return studyReports;
	}

	public ArrayList getStudyReportsSortedlist() {
		if (getStudyReports() == null) {
			return null;
		}
		ArrayList list = new ArrayList();
		
		for (Iterator iterator = getStudyReports().entrySet().iterator(); iterator.hasNext();) {
			Entry entry = (Entry) iterator.next();
			list.add(entry.getValue());
		}
		
		Comparator studyReportComparator = new Comparator() {
	        public int compare(Object o1, Object o2) {
	            return ((UserStudyReportBean) o1).getName().toLowerCase().compareTo(((UserStudyReportBean) o2).getName().toLowerCase());
	        }
	    };
		
	    Collections.sort(list, studyReportComparator);
		
		return list;
	}
	
	public void setStudyReports(HashMap userReports) {
		this.studyReports = userReports;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
