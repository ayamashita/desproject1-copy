package no.simula.des.data.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enum.Enum;

public class UserStudyReportBean {
	
	int id = -1;
	String userId;
	String name;
	SortOperator sortEndDate = null;
	WhereOperator whereResponsibleOperator;
	ArrayList responsibles;
	ArrayList studyColumns;
	
    public final static class WhereOperator extends Enum {

    	public static final WhereOperator AND = new WhereOperator("AND");
    	public static final WhereOperator OR = new WhereOperator("OR");

    	protected WhereOperator(String name) {
			super(name);
		}
    	
    	public static List getEnumList() {
    	  return getEnumList(WhereOperator.class);
    	}
    	
    	/*
    	 * @see java.lang.Object#toString()
    	 */
    	public String toString() {
    		return getName();
    	}
    	
    	/**
    	 * @param operator
    	 * @return
    	 */
    	public static WhereOperator valueOf(String operator) {
    		if (StringUtils.isEmpty(operator)) {
    			return null;
    		}
    		
    	  return (WhereOperator) getEnum(WhereOperator.class, operator);
    	}
    }

    public final static class SortOperator extends Enum {

    	public static final SortOperator ASCENDANT = new SortOperator("ASC");
    	public static final SortOperator DESCENDANT = new SortOperator("DESC");

    	protected SortOperator(String name) {
			super(name);
		}
    	
    	public static List getEnumList() {
    	  return getEnumList(SortOperator.class);
    	}
    	
    	/*
    	 * @see java.lang.Object#toString()
    	 */
    	public String toString() {
    		return getName();
    	}
    	
    	/**
    	 * @param operator
    	 * @return
    	 */
    	public static SortOperator valueOf(String operator) {
    		if (StringUtils.isEmpty(operator)) {
    			return null;
    		}
    		
    	  return (SortOperator) getEnum(SortOperator.class, operator);
    	}
    }
	
	public UserStudyReportBean(String userId) {
		super();
		this.userId = userId;
	}

	public UserStudyReportBean() {
		super();
	}
	
	public int getId() {
		return id;
	}
	/**
	 * @return id (Integer) for using as HashMap key
	 */
	public Integer getIdObj() {
		return Integer.valueOf(id);
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSortEndDate() {
		if (sortEndDate == null) {
			return "";
		}
		
		return sortEndDate.toString();
	}

	public void setSortEndDate(String sortEndDate) {
		this.sortEndDate = SortOperator.valueOf(sortEndDate);
	}

	public String getWhereResponsibleOperator() {
		if (whereResponsibleOperator == null) {
			whereResponsibleOperator = WhereOperator.AND;
		}
		return whereResponsibleOperator.toString();
	}

	public void setWhereResponsibleOperator(String whereResponsibleOperator) {
		this.whereResponsibleOperator = WhereOperator.valueOf(whereResponsibleOperator);
	}


	public ArrayList getResponsibles() {
		if (responsibles == null) {
			responsibles = new ArrayList();
		}

		Comparator studyReportComparator = new Comparator() {
	        public int compare(Object o1, Object o2) {
	            return ((PeopleBean) o1).getFamily_name().compareTo(((PeopleBean) o2).getFamily_name());
	        }
	    };
		
	    Collections.sort(responsibles, studyReportComparator);
		
		return responsibles;
	}
	
	public void setResponsibles(ArrayList responsibles) {
		this.responsibles = responsibles;
	}

	public ArrayList getStudyColumns() {
		if (studyColumns == null) {
			studyColumns = new ArrayList();
		}
		return studyColumns;
	}

	public void setStudyColumns(ArrayList studyColumns) {
		this.studyColumns = studyColumns;
	}

	public UserStudyReportBean getReportCopy() {
		UserStudyReportBean copyReport = new UserStudyReportBean();
		
		copyReport.setId(this.getId());
		copyReport.setUserId(this.getUserId());
		copyReport.setName(this.getName());
		copyReport.setSortEndDate(this.getSortEndDate());
		copyReport.setWhereResponsibleOperator(this.getWhereResponsibleOperator());
		
		copyReport.getResponsibles().addAll(this.getResponsibles());
		copyReport.getStudyColumns().addAll(this.getStudyColumns());
		
		return copyReport;
	}

	
	
	
}
