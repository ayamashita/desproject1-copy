/*
 * Created on 13.okt.2003
 *
 */
package no.simula.des.data.beans;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;


/**
 * The class represents an entry in the publication table
 */
public class PublicationBean {
    private String id;
    private int year;
    private String title;
    private String pubAbstact;
    private boolean isExternal = false;
    private String authors;
    private String url;

    //Utility attributes
    private boolean isAdded;

    //Utility attributes
    private boolean isDeleted;

    public String toString() {
        StringBuffer out = new StringBuffer();
        out.append("id=" + id);
        out.append(", authors=" + authors);
        out.append(", year=" + year);
        out.append(", title=" + title);
        out.append(", isExtenal=" + isExternal);
        out.append(", abstract=" + pubAbstact);

        return out.toString();
    }

    /**
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * @return
     */
    public String getAbstact() {
        return pubAbstact;
    }

    public String getShortAbstact() {
        int length = pubAbstact.length();

        if (length > 100) {
            return pubAbstact.substring(0, 100) + "...";
        }

        return pubAbstact;
    }

    /**
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return
     */
    public int getYear() {
        return year;
    }

    /**
     * @param i
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param string
     */
    public void setAbstact(String string) {
        pubAbstact = string;
    }

    /**
     * @param string
     */
    public void setTitle(String string) {
        title = string;
    }

    /**
     * @param i
     */
    public void setYear(int i) {
        year = i;
    }

    /**
     * @return
     */
    public boolean getAdded() {
        return isAdded;
    }

    /**
     * @return
     */
    public boolean getDeleted() {
        return isDeleted;
    }

    /**
     * @param b
     */
    public void setAdded(boolean b) {
        isAdded = b;
    }

    /**
     * @param b
     */
    public void setDeleted(boolean b) {
        isDeleted = b;
    }

    public void setExternal(boolean b){
      this.isExternal = b;
    }

    public boolean getExternal( ){
      return isExternal;
    }
    
    public static PublicationBean createPublicationBean(HashMap map) throws UnsupportedEncodingException {

    	if (!map.containsKey("id")) {
    		return null;
    	}
    	
    	PublicationBean bean = new PublicationBean();
        bean.setId((String) map.get("id"));
		bean.setTitle(URLDecoder.decode((String) map.get("title"), "UTF-8"));
        bean.setAbstact((String) map.get("abstract"));
        String year = (String) map.get("year");
        if (year != null && StringUtils.isNotEmpty(year)) {
        	bean.setYear(Integer.valueOf(year).intValue());
        }
    	Object[] authorsMap = (Object[])map.get("authors");
    	for (int i = 0; i < authorsMap.length; i++) {
    		HashMap authorMap = (HashMap)authorsMap[i];
			String author = authorMap.get("firstname") + " " + authorMap.get("lastname");
    		bean.addAuthor(author);
    	}
    	bean.setUrl((String) map.get("url"));

        return bean;
    }

	public String getAuthors() {
		return authors;
	}
	
	public String getAuthorsShort() {
		if (authors != null) {
	        if (authors.length() > 23) {
	            return authors.substring(0, 20) + "...";
	        }
		}
        return authors;
	}

	public void addAuthor(String author) {
		if (authors == null) {
			this.authors = author;			
		}
		else {
			this.authors += ", " + author;
		}
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
    
}
