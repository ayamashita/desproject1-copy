package no.simula.des.data;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.data.beans.PeopleBean;

public class DecoratorPeople extends Decorator {
	protected Object getValue(Object bean, String property, Type type, HttpServletRequest request) {

		Collection responsibles = (Collection) super.getValue(bean, property, type, request);
		String startLink = "";
		String endLink = "";
		String breakRow = "";

		if (Type.HTML.equals(type)) {
			endLink = "&nbsp;</a>";
			breakRow = "<br/>&nbsp;";
		}
		else {
			breakRow = " ";
		} 

    	if (responsibles == null) {
    		return "";
    	}
    	
    	StringBuilder string = new StringBuilder(""); 
    	
    	for (Iterator iterator = responsibles.iterator(); iterator.hasNext();) {
			PeopleBean responsible = (PeopleBean) iterator.next();
	    	if (string.length() > 1) {
	    		string.append(",").append(breakRow);
	    	}
			if (Type.HTML.equals(type)) {
				startLink = "<a href=\""+responsible.getUrl()+"\">";
			}
			string.append(startLink).append(responsible.getFirst_name()).append(" ").append(responsible.getFamily_name()).append(endLink);
		}
    	
        return string.toString();

	}
}
