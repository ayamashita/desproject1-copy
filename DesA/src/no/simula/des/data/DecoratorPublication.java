package no.simula.des.data;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.data.beans.PublicationBean;

public class DecoratorPublication extends Decorator {
	protected Object getValue(Object bean, String property, Type type, HttpServletRequest request) {
		Collection publications = (Collection) super.getValue(bean, property, type, request);
		String startLink = "";
		String endLink = "";
		String breakRow = "";

		if (Type.HTML.equals(type)) {
			endLink = "&nbsp;</a>";
			breakRow = "<br/>&nbsp;";
		} 
		else {
			breakRow = " ";
		}

    	if (publications == null) {
    		return "";
    	}
    	
    	StringBuilder string = new StringBuilder(""); 
    	
    	for (Iterator iterator = publications.iterator(); iterator.hasNext();) {
			PublicationBean publication = (PublicationBean) iterator.next();
	    	if (string.length() > 1) {
	    		string.append(",").append(breakRow);
	    	}
			if (Type.HTML.equals(type)) {
				startLink = "<a href=\""+publication.getUrl()+"\">";
			}
			string.append(startLink).append(publication.getTitle()).append(endLink);
		}
    	
        return string.toString();
	}
}
