package no.simula.des.data;

import no.simula.des.data.beans.AdminPrivilegesBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;


/**
 * Data object responsible for retrieving and making changes to users' admin privileges
 *
 */
public class AdminPrivilegesDatabase extends DataObject {
    private static final String GET_PRIVILEGES_QUERY = "select * from adminprivileges";

    //  private static final String GET_PRIVILEGE_QUERY = "select * from adminprivileges where people_id = {1}";
    private static final String UPDATE_PRIVILEGE_QUERY = "UPDATE adminprivileges SET admin_privileges=? where people_id=?";
    private static final String INSERT_PRIVILEGE_QUERY = "INSERT  adminprivileges (people_id, admin_privileges) VALUES(?,?);";
    private static final String REMOVE_PRIVILEGE_QUERY = "DELETE FROM adminprivileges WHERE people_id=?";
    private static final String GET_PEOPLE_PRIVILEGE = "select admin_privileges from adminprivileges where people_id = ?;";
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * This method retrives the admin privilege level for a single user
     *
     * @param peopleId the ID of the person to get privileges for
     * @return an integer representing the user's privilege level
     * @throws Exception
     */
    public int getPeoplePrivilege(String peopleId) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = super.getConnection();
            PreparedStatement stmt = conn.prepareStatement(AdminPrivilegesDatabase.GET_PEOPLE_PRIVILEGE);
            stmt.setString(1, peopleId);
            rs = stmt.executeQuery();

            int privilege = 0;

            while (rs.next()) {
                privilege = rs.getInt(1);
            }

            return privilege;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            log.debug("Closing connection");
            super.closeConnection(rs);
        }
    }

    /**
     * This method retireves the admin privilege level for all users
     *
     * @return a bean containing all privilege data
     * @throws Exception
     */
    public AdminPrivilegesBean getAdminPrivileges() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = super.getConnection();
            PreparedStatement stmt = conn.prepareStatement(AdminPrivilegesDatabase.GET_PRIVILEGES_QUERY);
            rs = stmt.executeQuery();

            AdminPrivilegesBean privileges = gerPrivileges(rs);

            return privileges;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            log.debug("Closing connection");
            super.closeConnection(rs);
        }
    }

    /**
     * Updates the admin privileges table
     *
     * @param entrie an object containing the CHANGES made to the admin privileges
     * @return the number of entries updated
     * @throws Exception
     */
    public int updateAdminPrivileges(HashMap entrie) throws Exception {
        ResultSet rs = null;
        Connection conn = super.getConnection();
        Iterator iter = entrie.keySet().iterator();
        int updatedEntries = 0;

        try {
            while (iter.hasNext()) {
                String id = (String) iter.next();
                int privilege = Integer.valueOf((String) entrie.get(id)).intValue();

                PreparedStatement stmt;

                if (privilege == 0) {
                    stmt = conn.prepareStatement(AdminPrivilegesDatabase.REMOVE_PRIVILEGE_QUERY);
                    stmt.setString(1, id);

                    int removed = stmt.executeUpdate();
                    updatedEntries += removed;
                } else {
                    stmt = conn.prepareStatement(AdminPrivilegesDatabase.UPDATE_PRIVILEGE_QUERY);
                    stmt.setInt(1, privilege);
                    stmt.setString(2, id);

                    int updated = stmt.executeUpdate();
                    updatedEntries += updated;

                    if (updated == 0) {
                        stmt = conn.prepareStatement(AdminPrivilegesDatabase.INSERT_PRIVILEGE_QUERY);
                        stmt.setString(1, id);
                        stmt.setInt(2, privilege);

                        int inserted = stmt.executeUpdate();
                        updatedEntries += inserted;
                    }
                }
            }

            return updatedEntries;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            log.debug("Closing connection");
            super.closeConnection(rs);
        }
    }

    //---------------- Private helper methods ---------------
    private AdminPrivilegesBean gerPrivileges(ResultSet rs)
        throws SQLException {
        AdminPrivilegesBean bean = new AdminPrivilegesBean();

        while (rs.next()) {
            String people_id = rs.getString(1);
            int privilege = rs.getInt(2);
            bean.setAdminPrivileges(people_id, privilege);
        }

        return bean;
    }
}
