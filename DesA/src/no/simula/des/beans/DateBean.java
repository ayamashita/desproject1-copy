/*
 * Created on 08.okt.2003
 *
 */
package no.simula.des.beans;

import no.simula.des.util.Constants;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;


/**
 * Class that handles conversion between date objects and
 * date fields split into day, month, and year.
 *
 */
public class DateBean {
    private GregorianCalendar calendar;

    public DateBean(Date date) {
        //Split date object into day, month, year
        calendar = new GregorianCalendar();
        calendar.setTime(date);
    }

    public DateBean(int year, int month, int day) {
        //As months in calendar objects are 0 based, we subtract 1
        calendar = new GregorianCalendar(year, month - 1, day);
    }

    public String getDay() {
        return String.valueOf(calendar.get(GregorianCalendar.DAY_OF_MONTH));
    }

    public String getMonth() {
        //As months in calendar objects are 0 based, we add 1
        return String.valueOf(calendar.get(GregorianCalendar.MONTH) + 1);
    }

    public String getYear() {
        return String.valueOf(calendar.get(GregorianCalendar.YEAR));
    }

    public Date getDate() {
        return calendar.getTime();
    }

    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_PATTERN);

        return formatter.format(calendar.getTime());
    }
}
