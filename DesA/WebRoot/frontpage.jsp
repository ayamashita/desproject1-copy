<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%@ include file="head.jsp" %>	
    <table bgcolor="#F5F5F5" border="10" width="100%">  
    	<tr>
    		<td></td>
    		<td class="path"><logic:present name="user"><a href="frontPage.do">Front page</a></logic:present></td>
    	</tr>
    	<tr>
    		<td><img src="" width="10" height="0" border="0">
    		</td>
    		<td>
    		<h1>DES Admin tool</h1>	
    		</td>
    	</tr>
    	<tr>
    		<td></td>
    		<td colspan="1" align="right" class="bodytext-bold">
    		<logic:present name="user">
    			<logic:greaterThan name="user" property="privilege" value="1">
    				<a href="editFrontPage.do">Edit this page</a>&nbsp;&nbsp;
    			</logic:greaterThan>
    		</logic:present>
    		<logic:present name="user">
				  <logic:greaterThan name="user" property="privilege" value="0">
					 <a href="editStudy.do">Create new study</a>&nbsp;&nbsp;&nbsp;&nbsp;
					</logic:greaterThan>
	  		</logic:present>
    		<a href="listStudies.do">Study overview</a>&nbsp;&nbsp;
    		<a href="startSearchStudies.do">Study search</a>
    		<img src="" width="15" height="0" border="0">
    		<hr style="height: 1px; color: black; width: 100%;">
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<img src="" width="10" height="0" border="0">
				  <img src="" width="0" height="600" border="0">
    		</td>
    		<td class="bodytext" width="100%" valign="top">
    			<bean:write name="frontPageText" 	filter="false"/>
    		</td>
    	</tr>
    
    </table>
    
    
    
   <%@ include file="footer.jsp" %>