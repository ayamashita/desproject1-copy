
<%@page import="no.simula.des.data.StudyDatabase"%>
<%@page import="no.simula.des.data.Decorator"%>
<%@page import="no.simula.des.data.beans.StudyBean"%><%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>



  <%@ include file="head.jsp" %>	


<table border="0" width="100%" bgcolor="#F5F5F5" cellpadding="0" cellspacing="0">
  <tr>
			<td></td>
			<td colspan="20" class="path"><div id="navActions"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present><a href="listStudies.do">Studies</a> > study search result</div></td>
	</tr>
  <tr>
  	<td><img class="blockOnPrint" src="" border="0" width="10" height="0"></td>
  	<td colspan="20"><h1>Study search result</h1>
  	</td>
  	<td><img class="blockOnPrint" src="" border="0" width="10" height="0"></td>
  </tr>
  <tr>
  		<td></td>
  		<td colspan="20">
  		<table width="100%">
  		<tr>
	  	<td class="bodytext">
	  		<b>
	  			<bean:write name="studySearchForm" property="studySortBean.start"/>-<bean:write name="studySearchForm" property="studies.endIndex"/>&nbsp;studies
    			&nbsp;(total <bean:write name="studySearchForm" property="studies.totalStudies"/>)
    		</b> 	
	  	</td>
	  	<td align="right" class="bodytext-bold"><div id="navActions">
	  		<logic:present name="user">
    			<logic:greaterThan name="user" property="privilege" value="0">
	  				<a href="editStudy.do">Create new study</a>
    			</logic:greaterThan>
    		</logic:present>	
	  		&nbsp;&nbsp;&nbsp;&nbsp;<a href="startSearchStudies.do">New study search</a>&nbsp;&nbsp;
	  		<a href="listStudies.do">Study overview</a>&nbsp;&nbsp;
	  		<logic:equal name="studySearchForm" property="hasPreviousPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/performSearchStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="previousPageIndex">&#60;&#60;Previous page</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasPreviousPage" value="false">
    		</logic:equal>
    		<logic:equal name="studySearchForm" property="hasNextPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/performSearchStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="nextPageIndex">Next page&#62;&#62;</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasNextPage" value="false">
				      &nbsp;
			<html:link target="_pdf" page="/downloadPDF.do">Printer friendly version</html:link>&nbsp;&nbsp;&nbsp;
    </logic:equal>
    		</div>
	  	</td>
	  	</tr>
	  	</table>
	  	</td>
	  	<td></td>
  </tr>
  <tr>
  	<td></td>
  	<td colspan="20">
  	<hr style="height: 1px; color: black; width: 100%;">
  	</td>
  	<td></td>
  </tr>
 
   
   <tr class="bodytext-bold">
  		<td></td>
    	<logic:iterate id="columnName" name="studySearchForm" property="studySortBean.columns" type="String">
    		<th align="left" nowrap="nowrap">
		    <%	
		    	StudyDatabase.ReportColumns column = StudyDatabase.ReportColumns.valueOf(columnName);
		    	if (column.isSort()) { %>
			    	<html:link page="/performSearchStudies.do?start=1" paramId="sort" paramName="columnName">
		      			<bean:message name="columnName"/>&nbsp;<img class="blockOnPrint" src="images/Icon_sort_arrow.gif">
		      		</html:link>&nbsp;&nbsp;
			<% 	} else { %>
					<bean:message name="columnName"/>
			<% 	} %>
    		</th>
    	</logic:iterate>
    	<td></td>    
  </tr> 

<tr>
  	<td></td>
  	<td colspan="20">
  	<hr style="height: 1px; color: black; width: 100%;">
  	</td>
  	<td></td>
  <tr>
  
<%boolean color= true;%>
<logic:iterate id="study" name="studySearchForm" property="studies.studies" type="StudyBean">
    <tr class="bodytext" <%=(color==true?"bgcolor=\"#cccccc\"":"") %>  valign="top">    
    <td  bgcolor="#F5F5F5"></td>
    <logic:iterate id="columnName" name="studySearchForm" property="studySortBean.columns" type="String">
	    <td align="left">
			<%=StudyDatabase.ReportColumns.valueOf(columnName).decorate(study, Decorator.Type.HTML, request)%>
	    </td>
	</logic:iterate>
    <td align="right">
    	<logic:present name="user">
    		<logic:greaterThan name="user" property="privilege" value="0"><div id="navActions">
      		<html:link page="/editStudy.do"  paramId="study" paramName="study" paramProperty="id">Edit</html:link>&nbsp;
      		</div>
      	</logic:greaterThan>
      </logic:present>
    </td>
    <td align="right">
    	<logic:present name="user">
    		<logic:greaterThan name="user" property="privilege" value="0"><div id="navActions">
      		<a href="Javascript:openPopup('deleteStudy.do?study=<bean:write name="study" property="id"/>','DeleteStudy' );" >Delete</a>
      		</div>
      	</logic:greaterThan>
      </logic:present>
    </td>
    <td bgcolor="#F5F5F5"></td>
  </tr>
  <%color=(color==true?false:true); %>
</logic:iterate>
 <tr>
   	<td></td>
   	<td colspan="20">
   	<hr style="height: 1px; color: black; width: 100%;">
   	</td>
   	<td></td>
  </tr>
 <tr>
  		<td></td>
  		<td colspan="20">
  		<table width="100%">
  		<tr>
	  	<td class="bodytext">
	  	&nbsp;
	  	</td>
	  	<td align="right" class="bodytext-bold"><div id="navActions">
	  		<logic:present name="user">
    			<logic:greaterThan name="user" property="privilege" value="0">
	  				<a href="editStudy.do">Create new study</a>
	  			</logic:greaterThan>
	  		</logic:present>	
	  		&nbsp;&nbsp;&nbsp;&nbsp;<a href="startSearchStudies.do">New study search</a>&nbsp;
	  		<a href="listStudies.do">Study overview</a>&nbsp;&nbsp;
	  		<logic:equal name="studySearchForm" property="hasPreviousPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/performSearchStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="previousPageIndex">&#60;&#60;Previous  page</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasPreviousPage" value="false">
    		</logic:equal>
    		<logic:equal name="studySearchForm" property="hasNextPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/performSearchStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="nextPageIndex">Next page&#62;&#62;</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasNextPage" value="false">
				      &nbsp;
			<html:link target="_pdf" page="/downloadPDF.do?onlySearchedStudies=true">Printer friendly version</html:link>&nbsp;&nbsp;&nbsp;	   
    </logic:equal>
    		</div>
	  	</td>
	  	</tr>
	  	</table>
	  	</td>
	  	<td></td>
  </tr>
  
</table>

 <%@ include file="footer.jsp" %>
