<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

  
<%@ include file="head.jsp" %>	
    <table bgcolor="#F5F5F5" border="0" width="100%" cellpadding="0" cellspacing="0">  
    	<tr>
    		<td><img src="" width="10" height="0" border="0">
    		</td>
    		<td colspan="1">
    		<h1>Edit Administration opening page</h1>	
    		</td>
    	</tr>
    	<tr valign="top">
    		<td></td>
    		<td colspan="1" align="right" class="bodytext-bold">
    		<table border="0" width="100%" cellpadding="0" cellspacing="0">
    			<tr>
    				<td class="bodytext">
						  Enter new information or change existing content, then click "Save changes"
    				</td>
    				<td class="bodytext-bold" align="right">
						  <a href="javascript: document.frontPageForm.submit();">Save changes</a>&nbsp;&nbsp;
						  <a href="frontPage.do">Cancel</a>
    				</td>
    				<td><img src="" width="15" height="0" border="0">
    				</td>
    			</tr>
    			<tr>
    				<td colspan="3"><hr style="height: 1px; color: black; width: 100%;">
    				</td>
    			</tr>
    		</table>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<img src="" width="10" height="0" border="0">
    		</td>
    		<td colspan="1" class="bodytext" width="100%" valign="top">
    			<html:form action="/editFrontPage">
    			<html:hidden property="id" />
    			<html:hidden property="page_id" />
    			<html:textarea property="text" rows="20" cols="80"/><br>
    			</html:form>
    		</td>
    	</tr>
    	<tr>
    		<td></td>
    			<td colspan="1"><hr style="height: 1px; color: black; width: 100%;">
    		</td>
    	</tr>
    	<tr>
    		<td colspan="2" class="bodytext-bold" align="right">
										  <a href="javascript: document.frontPageForm.submit();">Save changes</a>&nbsp;&nbsp;
										  <a href="frontPage.do">Cancel</a><img src="" width="20" height="0" border="0">
    		</td>
    	</tr>
    	<tr>
			    		<td></td>
			    			<td colspan="1"><a></a><img src="" width="0" height="15" border="0">
			    		</td>
    	</tr>
    </table>
    
    
    
 <%@ include file="footer.jsp" %>