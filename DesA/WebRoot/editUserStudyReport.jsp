
<%@page import="no.simula.des.data.StudyDatabase"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%@page import="no.simula.des.data.beans.UserStudyReportBean"%>

<html>
<body>
  <head>
    <title>DES Admin</title>
    <script language='javascript' type='text/javascript' src='/des/desScript.js'></script>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1	">
  </head>

	<%@ include file="head.jsp" %>   
	<html:form action="/editUserStudyReportAction">
       <html:hidden property="action" value="save" />
		<table bgcolor="#F5F5F5" border="0" width="100%" class="bodytext">
			<tr>
				<td colspan="3">
					<logic:greaterThan name="userStudyReportForm" property="report.id" value="0">
						<h1>Edit User Study Report</h1>
					</logic:greaterThan>
					<logic:lessThan name="userStudyReportForm" property="report.id" value="1">
							<h1>New User Study Report</h1>
					</logic:lessThan>
					<span style="color:red;">
						<b><html:errors/></b>
					</span>
				</td>
				<td>	
					<img src="" width="10" height="0">
				</td>
			</tr>
			<tr>
			<td colspan="2">
				<logic:greaterThan name="userStudyReportForm" property="report.id" value="0">
					Enter new information or change existing content, then click "Save changes"
				</logic:greaterThan>
				<logic:lessThan name="userStudyReportForm" property="report.id" value="1">				
					Complete form below, then click "Save study". Mandatory fields are marked with *
				</logic:lessThan>
			</td>
			<td align="right" class="bodytext-bold">
				<html:link href="JavaScript:document.userStudyReportForm.submit();">Save report</html:link>&nbsp;&nbsp;&nbsp;<html:link href="JavaScript:document.userStudyReportForm.action.value='back';document.userStudyReportForm.submit();">Back</html:link>	
			</td>
			<td>
				
			</td>
			</tr>
			<tr>
				<td colspan="3">
				<hr style="height: 1px; color: black; width: 100%;">
				</td>
				<td></td>
			<tr>
		</table>
	    <table bgcolor="#F5F5F5" border="0" width="100%" class="bodytext">
	    	<tr>
	      		<td class="bodytext-bold"><b>Report name *</b></td>
		    </tr>
		    <tr>
		    	<td><html:text property="report.name" size="60"/></td>
		    </tr>
		    <tr>
		    	<td>&nbsp;</td>
		    </tr>
	    	<tr>
	      		<td><b>Sorting 'End of Study':</b>&nbsp;&nbsp;
		    		<html:select property="report.sortEndDate">
		    			<html:option value="">------</html:option>
						<html:option value="<%=UserStudyReportBean.SortOperator.ASCENDANT.toString()%>">ascending</html:option>			
						<html:option value="<%=UserStudyReportBean.SortOperator.DESCENDANT.toString()%>">descending</html:option>
		    		</html:select>
		    	</td>
		    </tr>
		    <tr>
		    	<td>&nbsp;</td>
		    </tr>
		    <tr>
		    	<td valign="top">
					<table>
				    	<tr>
					    	<td><b>Study Columns:</b>
					    		<bean:define id="columns" name="userStudyReportForm" property="report.studyColumns" type="ArrayList"/>
					    		<html:select property="columnName" value="">
					    			<html:option value="">---Select Column---</html:option>
					    			<% for (Iterator iterator = StudyDatabase.ReportColumns.getEnumList().iterator(); iterator.hasNext();) {
						    				StudyDatabase.ReportColumns bean = (StudyDatabase.ReportColumns) iterator.next(); 
						    				if (!columns.contains(bean.toString())) { %>
												<html:option value="<%=bean.toString()%>"><bean:message key="<%=bean.toString()%>"/></html:option>
										<%  } %>
									<% } %>
					    		</html:select>
					    		&nbsp;&nbsp;
					    		<html:link href="JavaScript:document.userStudyReportForm.action.value='addColumn';document.userStudyReportForm.submit();"><b>add</b></html:link>
					    	</td>
					    </tr>
					    <tr>
					    	<td>
					    		<table bgcolor="white">
				                  <logic:iterate id="columnName" name="userStudyReportForm" property="report.studyColumns">
				                      <tr>
				                        <td>&nbsp;&nbsp;</td>
				                        <td align="left"><li><bean:message name="columnName"/></li></td>
				                        <td align="right">&nbsp;&nbsp;<a href="editUserStudyReportAction.do?action=removeColumn&columnName=<bean:write name="columnName"/>">Delete</a></td>
				                      </tr>
				                  </logic:iterate>
					    		</table>
					    	</td>					    
					    </tr>
					</table>
				</td>
				<td width="70%" valign="top">
					<table width="90%">
				    	<tr>
					    	<td align="left">
					    		<b>Responsible:
						    		&nbsp;&nbsp;
						    		<html:link href="Javascript:document.userStudyReportForm.action.value='addResponsibles'; openPopupGeneral('addResponsibles.do?addResponsiblesToStudyReport=true','addResponsibles','430','800','yes' );">add responsibles</html:link>
						    		<bean:define id="responsibles" name="userStudyReportForm" property="report.responsibles" type="ArrayList"/>
					    		</b>
					    		<% if (responsibles.size() > 1) { %>
						    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    		<b>Condition operator between 'Responsible':</b>
						    		&nbsp;
						    		<html:select property="report.whereResponsibleOperator">
										<html:option value="<%=UserStudyReportBean.WhereOperator.AND.toString()%>"><%=UserStudyReportBean.WhereOperator.AND.toString()%></html:option>			
										<html:option value="<%=UserStudyReportBean.WhereOperator.OR.toString()%>"><%=UserStudyReportBean.WhereOperator.OR.toString()%></html:option>
						    		</html:select>
						    	<% } %>
					    	</td>
					    </tr>
					    <tr>
					    	<td>
			                  <table width="100%" bgcolor="white">
				                  <logic:iterate id="responsible" name="userStudyReportForm" property="report.responsibles">
				                    <logic:equal name="responsible" property="deleted" value="false">
					                      <tr>
					                        <td align="left"><a href='<bean:write name="responsible" property="url" filter="true"/>'><bean:write name="responsible" property="first_name" filter="true"/> <bean:write name="responsible" property="family_name" filter="true"/></a></td>
					                        <td align="left"><bean:write name="responsible" property="position" filter="true"/></td>
					                        <td align="right"><a href="editUserStudyReportAction.do?action=removeResponsible&responsibleId=<bean:write name="responsible" property="id" filter="true"/>">Delete</a></td>
					                      </tr>
				                    </logic:equal>
				                  </logic:iterate>
		                    	</table>		
					    	</td>		    	
					    </tr>
					</table>
				</td>	
		    </tr>
		    <tr>
		    	<td>&nbsp;</td>
		    </tr>
	    </table>
    </html:form>

 <%@ include file="footer.jsp" %>
