<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>Add publications</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0"> 
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
   	<table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#ef1607">
			<td><img src="" width="0" height="70"></td>
			<td>&nbsp;&nbsp;</td>
   			<td class="h1"><font color="#ADAEAD">Select publication</font></td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   		<td><img src="" width="0" height="500"></td>
		<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<html:form action="/publicationAdded" method="get">
   			<table width="100%" cellspacing="2" cellpadding="0" border="0">
			   			<tr>
			   				<td colspan="5" class="h2"><b>Select publications</b></td>
			   			</tr>
			   			<tr>
								<td colspan="5" class="bodytext">Tick check-boxes below, then click "Submit"</td>
							</tr>
							<tr>
								<td colspan="1">&nbsp;</td>
								<td colspan="4" align="right" class="bodytext-bold"><!--Create new publication&nbsp;&nbsp;--><a href="JavaScript:document.studyForm.submit();">Submit</a>&nbsp;&nbsp;<a href="Javascript:window.self.close()">Cancel</a></td>
							</tr>
			   			<tr>
								<td colspan="5" bgcolor="black"><img src="" width="0" height="1"></td>
							</tr>
			   			<tr>
			   				<td width="5%"> &nbsp;&nbsp;</td>
								<td align="left" width="20%" class="bodytext-bold">Authors</td>
								<td align="left" width="5%" class="bodytext-bold">Year</td>
								<td align="left" width="35%" class="bodytext-bold">Title</td>
								<td align="left" width="35%" class="bodytext-bold">Publication notes</td>
			   			</tr>
			   			<tr>
								<td colspan="5" bgcolor="black"><img src="" width="0" height="1"></td>
							</tr>
							<%boolean color= true;%>
							<logic:iterate id="publication" name="publications">
								
								<tr <%=(color==true?"bgcolor=\"white\"":"") %> valign="top" >
									<td>
										<input type="checkbox" name="<bean:write name="publication" property="id"/>"
											<logic:equal name="publication" property="added" value="true">
												checked
											</logic:equal> >
									</td>
									<td class="bodytext"><span title="<bean:write name="publication" property="authors"/>"><bean:write name="publication" property="authorsShort"/></span></td>
									<td class="bodytext"><bean:write name="publication" property="year"/></td>
									<td class="bodytext"><bean:write name="publication" property="title"/></td>
									<td class="bodytext"><bean:write name="publication" property="shortAbstact"/></td>
			   				</tr>
			   				<%color=(color==true?false:true); %>
						</logic:iterate>
						
						<tr>
							<td colspan="5" bgcolor="black"><img src="" width="0" height="1"></td>
						</tr>
			   			
						<tr>
							<td colspan="1">&nbsp;</td>
							<td colspan="4" align="right" class="bodytext-bold">Create new publication&nbsp;&nbsp;<a href="JavaScript:document.studyForm.submit();">Submit</a>&nbsp;&nbsp;<a href="Javascript:window.self.close()">Cancel</a></td>
						</tr>
							
						
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   	
   		<!-- <html:submit/> -->
   		</html:form>
   	</table>
   
   
   </body>
   
</html>
