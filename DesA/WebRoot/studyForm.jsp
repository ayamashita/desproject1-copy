<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>DES Admin</title>
    <script language='javascript' type='text/javascript' src='/des/desScript.js'></script>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1	">
  </head>

			<%@ include file="head.jsp" %>   

		<table bgcolor="#F5F5F5" border="0" width="100%" class="bodytext">
			<tr>
				<td colspan="3">
					<logic:equal name="studyForm" property="action" value="1">
						<h1>Edit Study</h1>
						<logic:messagesPresent message="true">
													                	<span style="color:green;"><b>
													                  <bean:message key='<%=(String)request.getAttribute("messageKey")%>' />
													                  </b>
													                  </span>
                </logic:messagesPresent>
					</logic:equal>
					<logic:notEqual name="studyForm" property="action" value="1">
							<h1>New Study</h1>
					</logic:notEqual>
					<span style="color:red;">
						<b><html:errors/></b>
					</span>
				</td>
				<td>	
					<img src="" width="10" height="0">
				</td>
			</tr>
			<tr>
							<td colspan="2">
								<logic:equal name="studyForm" property="action" value="1">
									Enter new information or change existing content, then click "Save changes"
								</logic:equal>
								<logic:notEqual name="studyForm" property="action" value="1">
										Complete form below, then click "Save study". Mandatory fields are marked with *
								</logic:notEqual>
							</td>
							<td align="right" class="bodytext-bold">
								<a href="JavaScript:document.studyForm.operation.value='1';document.studyForm.submit();">Save study</a>&nbsp;&nbsp;&nbsp;<a href="JavaScript:location.href='listStudies.do'">Cancel</a>	
							</td>
							<td>
								
							</td>
			</tr>
			<tr>
				<td colspan="3">
				<hr style="height: 1px; color: black; width: 100%;">
				</td>
				<td></td>
			<tr>
						
		</table>
	<html:form action="/saveStudy">
    <table bgcolor="#F5F5F5" border="0" width="100%" class="bodytext">
      <tr class="bodytext-bold">
        <td>
        <html:hidden property="operation" value="2"/>
        Study name *</td>
        <td>Admin</td>
      </tr>
      <tr>
        <td><html:text property="name" size="60"/></td>
        <td>
          <logic:equal name="studyForm" property="action" value="1">
            Created <bean:write name="studyForm" property="createdDateAsString" filter="true"/>, by <bean:write name="studyForm" property="createdBy" filter="true"/><br>
            Last edited <bean:write name="studyForm" property="editedDateAsString" filter="true"/>, by <bean:write name="studyForm" property="editedBy" filter="true"/>
          </logic:equal>
        </td>
      </tr>

      <tr>
        <td>
          <table border="0" class="bodytext">
            <tr>
              <td><b>Start date</b> <font size="1">(dd/mm-yyyy)</font></td>
              <td><b>End date *</b> <font size="1">(dd/mm-yyyy)</font> </td>
              <td><b>Duration <b></td>
              <td><b>Study type *</b></td>
            </tr>
            <tr>
              <td>
                <html:text property="startDay" size="2" maxlength="2"/>/
                <html:text property="startMonth" size="2" maxlength="2"/>-
                <html:text property="startYear" size="4" maxlength="4"/><img src="" width="15" height="0">
              </td>
              <td>
                <html:text property="endDay" size="2" maxlength="2"/>/
                <html:text property="endMonth" size="2" maxlength="2"/>-
                <html:text property="endYear" size="4" maxlength="4"/><img src="" width="15" height="0">
              </td>
              <td>
                <html:text property="duration" size="2" maxlength="4"/>
                <html:select property="durationUnit" value="<%=(String)request.getAttribute( "durationUnit")%>">
                  <html:optionsCollection label="name" property="durationUnitOptions"/>
                </html:select><img src="" width="15" height="0">
              </td>
              <td>
                <html:select property="type" value="<%=(String)request.getAttribute( "studyType")%>">
                  <html:optionsCollection label="name" property="studyTypeOptions"/>
                </html:select>
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table border="0" class="bodytext">
            <tr>
              <td><span class="bodytext-bold">Keywords</span> (use comma to separate keywords)</td>
            </tr>
            <tr>
              <td><html:text property="keywords" size="40"/></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td>Study description *</td>
        <td>Study notes</td>
      </tr>

      <tr>
        <td><html:textarea property="description" rows="7" cols="65"/></td>
        <td><html:textarea property="notes" rows="7" cols="40"/></td>
      </tr>

      <tr>
        <td>
          <table border="0" valign="top" width="100%">
            <tr>
              <td width="65%" valign="top">
                <table border="0" valign="top"  class="bodytext" cellpadding="0" cellspacing="0" width ="100%">
                	<%boolean color= true;%>	
      
                  	<tr>
                  		<td colspan="3"><span class="bodytext-bold">Responsibles *</span><a href="Javascript:document.studyForm.submit();openPopupGeneral('addResponsibles.do?','addResponsibles','430','800','yes' );">(add responsibles)</a></td>
                  	</tr>
                  
                  <logic:iterate id="responsible" name="studyForm" property="responsibles">
                    <logic:equal name="responsible" property="deleted" value="false">
                      <tr <%=(color==true?"bgcolor=\"white\"":"") %>>
                        <td align="left">
                          <a href='<bean:write name="responsible" property="url" filter="true"/>'><bean:write name="responsible" property="first_name" filter="true"/> <bean:write name="responsible" property="family_name" filter="true"/></a>
                        </td>
                        <td align="left">
                          <bean:write name="responsible" property="position" filter="true"/>
                        </td>
                        <td align="right">
                        	<a href="Javascript:openPopup('startRemoveResponsible.do?responsible=<bean:write name="responsible" property="id" filter="true"/>' );">Delete</a>
                          <!--<a href='removeResponsible.do?responsible=<bean:write name="responsible" property="id" filter="true"/>'>Delete</a>-->
                        </td>
                      </tr>
                    </logic:equal>
                    <%color=(color==true?false:true); %>
                    </logic:iterate>
                </table>
              </td>
              <td>
              	<img src="" width="5" height="0">
              </td>
              <td>
                <table border="0" valign="top" class="bodytext">
                  <tr><td colspan="2"><b>No. of participants </b></td></tr>
                  <tr><td>Students</td><td><html:text property="students" size="2"/></td></tr>
                  <tr><td>Professionals</td><td><html:text property="professionals" size="2"/></td></tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
        <td valign="top">
          <table border="0" width="350" class="bodytext" cellpadding="0" cellspacing="0" width ="100%">
           <tr>
             <td colspan="2" >
                 <b>Study material </b>
                 <html:link page="/startStudyMaterial.do"  target="studyMaterial" onclick="document.studyForm.submit();openPopupGeneral('','studyMaterial','420','340')">(Add study material)</html:link>
             </td>
             <%int i=0; %>
             <% color= true;%>	
             <logic:iterate id="material" name="studyForm" property="studyMaterial">
             	 <logic:equal name="material" property="deleted" value="false">
               <tr <%=(color==true?"bgcolor=\"white\"":"") %>>
                 <td align="left">
                   <logic:equal name="material" property="isUrl" value="true">
                     <a href="<bean:write name="material" property="url" filter="true"/>" target="_new"><bean:write name="material" property="description" filter="true"/></a>
                   </logic:equal>
                   <logic:equal name="material" property="isUrl" value="false">
                    <logic:notEqual name="material" property="study_material_id" value="-1">
                     <a href='downloadStudyMaterial.do?studyMaterialId=<bean:write name="material" property="study_material_id" filter="true"/>&studyId=<bean:write name="material" property="study_id" filter="true"/>'><bean:write name="material" property="description" filter="true"/></a>
                   </logic:notEqual>
                   	<logic:equal name="material" property="study_material_id" value="-1">
                   			<bean:write name="material" property="description" filter="true"/>
                   	</logic:equal>
                   </logic:equal>
                 </td>
                 <td align="right">
                   <a href="Javascript:openPopup('deleteStudyMaterial.do?studyMaterialId=<%=i++%>','popup');">Delete</a>
                 </td>
               </tr>
               <%color=(color==true?false:true); %>
               </logic:equal>
             </logic:iterate>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan="2">
          <table border="0" cellpadding="0" cellspacing="0" class="bodytext" width="60%">
            <tr>
              <td><b>Publication </b> <a href="Javascript:document.studyForm.submit();openPopupGeneral('addPublication.do?','addPublication','720','800','yes' );">(add publication)</a></td>
            </tr>
            <% color= true;%>	
            <logic:iterate id="publication" name="studyForm" property="publications">

            <logic:equal name="publication" property="deleted" value="false">
              <tr <%=(color==true?"bgcolor=\"white\"":"") %> >
                <td align="left">
                  <span title="<bean:write name="publication" property="authors"/>"><bean:write name="publication" property="authorsShort"/></span> -
                  <bean:write name="publication" property="year" filter="true"/>: 
                  <a href='<bean:write name="publication" property="url" filter="true"/>'><bean:write name="publication" property="title" filter="true"/></a>
                </td>
                <td align="right">
                	<a href="Javascript:openPopup('startRemovePublication.do?publication=<bean:write name="publication" property="id" filter="true"/>' );">Delete</a>
                  <!--<a href='removePublication.do?publication=<bean:write name="publication" property="id" filter="true"/>'>Delete</a>-->
                </td>
              </tr>
            </logic:equal>
            <%color=(color==true?false:true); %>
            </logic:iterate>
          </table>
        </td>
      </tr>

      <tr align="right">
        <td colspan="2" align="right"></td>
      </tr>

    </table>
    </html:form>

 <%@ include file="footer.jsp" %>
