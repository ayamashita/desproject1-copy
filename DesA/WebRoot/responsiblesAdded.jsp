<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>Add responsibles</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Expires" content="0"> 
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="Javascript:window.opener.location.href='editStudy.do?study=<bean:write name="studyForm" property="id"/>';window.self.close()">
  <!-- 	<body onLoad="Javascript:window.opener.location.href='studyForm.jsp';window.self.close()"> -->
   	<table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#fe0f0c">
			<td><img src="" width="0" height="70"></td>
			<td>&nbsp;&nbsp;</td>
   			<td class="h1">Select responsibles</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   		<td><img src="" width="0" height="65"></td>
		<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<table width="100%" cellspacing="2" cellpadding="0" border="0">
			   			<tr>
			   				<td class="h2"><b>The list of responsibles is updated</b></td>
			   			</tr>
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
			   				<td><img src="" width="0" height="30"></td>
								<td>&nbsp;&nbsp;</td>
						   	<td align="center" class="bodytext-bold">
						   		<a href="Javascript:window.opener.location.href='editStudy.do?study=<bean:write name="studyForm" property="id"/>';window.self.close()">OK</a></td> 
								<td>&nbsp;&nbsp;</td>
   		</tr>
   	</table>
   </body>
</html>
