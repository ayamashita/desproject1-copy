-- create new tables for user study report

	drop table user_study_reports;

    create table user_study_reports (
       ID INT not null auto_increment,
       User_ID VARCHAR(100) not null,
       Name VARCHAR(200) not null,
       Sort_End_Date VARCHAR(10) CHECK (Sort_End_Date IN ('ASC','DESC')),
       Where_Responsible_Operator VARCHAR(10) CHECK (Where_Condition_Operator IN ('OR','AND')),
        primary key (ID)
    );
   
    create unique index IX_user_study_reports_User_ID on user_study_reports(User_ID, Name);
    
    create table user_study_report_columns (
       Report_ID INT not null,
       Column_Name VARCHAR(100) not null,
       sequence int 
    );
    
    create unique index IX_user_study_report_columns_Report_ID_Column_Name on user_study_report_columns(Report_ID, Column_Name);
    
    create table user_study_report_responsibles (
       Report_ID INT not null,
       Responsible_ID VARCHAR(100) not null
    );
    
    create unique index IX_user_study_report_responsibles_Report_ID_Responsible_ID on user_study_report_responsibles(Report_ID, Responsible_ID);