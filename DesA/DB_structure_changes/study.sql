-- changing int columns to VARCHAR(100) type

	alter table `study` modify 
	    `study_created_by` VARCHAR(100) null;
	    
	alter table `study` modify 
	    `study_edited_by` VARCHAR(100) null;
   