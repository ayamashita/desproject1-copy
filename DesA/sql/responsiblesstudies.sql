-- MySQL dump 8.22
--
-- Host: localhost    Database: des
---------------------------------------------------------
-- Server version	3.23.57-nt

--
-- Table structure for table 'responsiblesstudies'
--

CREATE TABLE responsiblesstudies (
  responsible_id int(4) NOT NULL default '0',
  study_id int(10) NOT NULL default '0',
  PRIMARY KEY  (responsible_id,study_id)
) TYPE=ISAM PACK_KEYS=1;

--
-- Dumping data for table 'responsiblesstudies'
--


INSERT INTO responsiblesstudies VALUES (1,1);
INSERT INTO responsiblesstudies VALUES (12,2);
INSERT INTO responsiblesstudies VALUES (17,1);

