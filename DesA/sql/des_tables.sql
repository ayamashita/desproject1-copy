
CREATE TABLE adminprivileges (
  people_id int(4) NOT NULL,
  admin_privileges int(1) NOT NULL,
  PRIMARY KEY  (people_id)
) TYPE=ISAM PACK_KEYS=1;


CREATE TABLE descontent (
  id int(4) NOT NULL auto_increment,
  page_id int(4) NOT NULL, 
  text text NOT NULL default '',
  PRIMARY KEY  (id, page_id)
) TYPE=ISAM PACK_KEYS=1;


CREATE TABLE durationunits (
  duration_id int(3) NOT NULL auto_increment,
  duration_name varchar(100) NOT NULL default '',
  PRIMARY KEY  (duration_id)
) TYPE=ISAM PACK_KEYS=1;


CREATE TABLE study (
  study_id int(10) NOT NULL auto_increment,
  study_name varchar(200) NOT NULL default '',
  study_type int(3) NOT NULL default '0',
  study_description text NOT NULL,
  study_keywords varchar(100) default NULL,
  study_notes text,
  study_students int(3) default NULL,
  study_professionals int(3) default NULL,
  study_duration int(4) default NULL,
  study_duration_unit int(1) default NULL,
  study_start_date date default NULL,
  study_end_date date NOT NULL default '0000-00-00',
  study_created_by int(4) default NULL,
  study_created_date date default NULL,
  study_edited_by int(4) default NULL,
  study_edited_date date default NULL,
  PRIMARY KEY  (study_id),
  UNIQUE KEY study_id (study_id)
) TYPE=ISAM PACK_KEYS=1;

CREATE TABLE studymaterial(
  study_material_id int(10) NOT NULL auto_increment,		
  study_id int(10) NOT NULL,
  study_material_description VARCHAR(255) NOT NULL default '',
  study_material_isUrl char NOT NULL default '0',
  study_material_file LONGBLOB,	
  study_material_filename varchar(255),
  study_material_filetype varchar(255),		
  study_material_url varchar(255),  	
  PRIMARY KEY (study_material_id,study_id)
) TYPE=ISAM PACK_KEYS=1;

CREATE TABLE studytypes (
  type_id int(3) NOT NULL auto_increment,
  type_name varchar(100) NOT NULL default '',
  PRIMARY KEY  (type_id),
  UNIQUE KEY type_id (type_id)
) TYPE=ISAM PACK_KEYS=1;



--
-- Creating m-n relation tables
--

CREATE TABLE publicationsstudies (
  publication_id int(10) NOT NULL default '0',
  study_id int(10) NOT NULL default '0',
  PRIMARY KEY  (publication_id,study_id)
) TYPE=ISAM PACK_KEYS=1;

CREATE TABLE responsiblesstudies (
  responsible_id int(4) NOT NULL default '0',
  study_id int(10) NOT NULL default '0',
  PRIMARY KEY  (responsible_id,study_id)
) TYPE=ISAM PACK_KEYS=1;



--
-- Adding necessary data for table 'studytypes'
--

INSERT INTO studytypes VALUES (1,'Experiment');
INSERT INTO studytypes VALUES (2,'Case study');
INSERT INTO studytypes VALUES (3,'Multi-case');
INSERT INTO studytypes VALUES (4,'Interviews');
INSERT INTO studytypes VALUES (5,'Questionnaire');


--
-- Adding necessary data for table 'durationunits'
--

INSERT INTO durationunits VALUES (1,'Hours');
INSERT INTO durationunits VALUES (2,'Days');
INSERT INTO durationunits VALUES (3,'Weeks');
INSERT INTO durationunits VALUES (4,'Months');

--
-- Adds front page contents to table 'descontent'
--

INSERT INTO descontent VALUES (1,1,'<b>Welcome to the Simula Database for Emperical Studies!!</b><br>\r\n<br>\r\nOn these pages you can, by clicking <u>\"studies\"</u> in the global menu above:\r\n<br>\r\n<br>\r\n<li>Get a study overview of all the studies registered in DES\r\n<li>Perform a study search among all the studies registered in DES\r\n<li>Create a new study or edit an existing\r\n<li>Aggregate a study report\r\n<li>Export data to file</li>\r\n<br>\r\n<br>\r\nGood luck!\r\n\r\n\r\n');